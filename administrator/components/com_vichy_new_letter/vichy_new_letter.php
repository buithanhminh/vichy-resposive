<?php 
	defined('_JEXEC') or die();
	JLoader::register('Vichy_new_letterHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'helper.php');
	$controller = JRequest::getCmd('controller','newletter');
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)){
		require_once $path;
	}
	
	$controller = ucwords(strtolower($controller));
	$classname = 'Vichy_new_letterController'.$controller; 
	$controller = new $classname();
	$task = JRequest::getVar('task');
	$controller->execute($task);
	$controller->redirect();