<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.controller');
	require_once(JPATH_ROOT.'/MailChimp.php');
	class Vichy_new_letterControllerNewletter extends JController{
		function __construct() {
			parent::__construct($config);
			$this->registerTask('add', 'edit');
		}
  
		function display() {
			JRequest::setVar('view', 'newletter');
	    	parent::display();
		}		

    	function remove() {
    		JRequest::checkToken() or jexit( 'Invalid Token' );
	        $model = $this->getModel('Newletter');
	        $flag = $model->deleteRows();
	        if($flag == '0') {
	            $msg = JText::_( 'Lỗi : Một hoặc nhiều bản tin không thể xoá');
	        } else {
            if($flag!='1'){
                $msg = JText::_('').$flag;
            }else{
                $msg = JText::_( 'Bản tin đã được xoá !' );
            }
        }
        
        $this->setRedirect('index.php?option=com_vichy_new_letter&controller=newletter', $msg );
    	}
    	
		function publish(){
			
		}
		
		function unpublish()
		{
			
		}

		function edit() 
		{
	    	JRequest::setVar('view', 'newletter');
	    	JRequest::setVar('layout', 'form');
	    	JRequest::setVar('hidemainmenu', 1); 
	    	parent::display();
	    }
	    
		// function save(){

		// 	JRequest::checkToken() or jexit( 'Invalid Token' );
	
	 // 		$post = JRequest::get( 'post' );
	 // 		$post['content'] = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );

	 // 		$user = JFactory::getUser();
	 // 		$admin_name = $user->username;

	 // 		$db =& JFactory::getDBO();

	 // 		$query = "INSERT INTO #__vichy_subscribe_info(content,sent_by)
	 // 							 VALUES ('$post[content]','$admin_name')";

		// 	$db->setQuery($query);
		// 	$result=$db->query();

		// 	$emails = Vichy_new_letterHelper::getEmail();

		// 	if($result)
		// 	{
		// 		if(count($emails)>0)
		// 		{
		// 			$success=0;
		// 	       	$config = JFactory::getConfig();
	 //        		$data['fromname'] = $config->get('fromname');
		// 			$data['mailfrom'] = $config->get('mailfrom');
		// 			$emailSubject	= 'Bản tin từ Vichy';
		// 			foreach ($emails as $key => $v) {
		// 				$emailBody = "Chào bạn ,<br />".$post['content'];
		// 				$mailer =& JFactory::getMailer();
		// 				$mailer->IsHTML(true);
	 //        			$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $v->email, $emailSubject, $emailBody,1);
	 //        			if($return)
	 //        				$success+=1;
		// 			}

		// 		}			

		// 		$msg = JText::_( "Đã gửi thông báo đến $success khách hàng!" );
	 //    		$link = 'index.php?option=com_vichy_new_letter&controller=newletter';
	 //    		$this->setRedirect($link, $msg);	
		// 	}
		// 	else
		// 	{
		// 		$msg = JText::_( 'Lỗi không thể lưu xuống cơ sở dữ liệu' );
	 //    		$link = 'index.php?option=com_vichy_new_letter&controller=newletter';
	 //    		$this->setRedirect($link, $msg);
		// 	}
  
		// }

		function save(){
			JRequest::checkToken() or jexit( 'Invalid Token' );
			$mailchimp = new Drewm\MailChimp(MAILCHIMP_KEY);
			$lists = $mailchimp->call('lists/list',array(
				'sort_dir'   => 'ASC'
			)) ;
			$listid = $lists['data'][ELEMENT_MC]['id'];
			$from_email = $lists['data'][ELEMENT_MC]['default_from_email'];
			$grouping_id = 'interests-'.$_POST['grouping_id'];

			$group = str_replace(',', '&#44;', $_POST['groups']);
			$content = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );
			// echo $content;
			$patern = "/src=\"/";
			$replace = "src=\"http://vichy.com.vn/";
			$content = preg_replace($patern, $replace, $content);
			$subject = $_POST['subject'];
			$campaign = $mailchimp->call('campaigns/create',array(
				'type'         => 'regular',
				'options'      => array(
					'list_id'       => $listid,
					'subject'       => "$subject",
					'from_email'    => "$from_email",
					'from_name'     => 'Vichy',
					'auto_footer'   => TRUE, // Lets MailChimp add a footer to the mail.
					'inline_css'    => TRUE,
					'generate_text' => TRUE,
				),
				'content'      => array(
					'html'          => $content,
				),
				'segment_opts' => array(
					'match'         => 'any',
					'conditions'    => array(
										array('field' => $grouping_id, 'op' => 'one', 'value' => $group)
									)
				),
			));

			// echo '<pre>';
			// print_r($campaign);
			// echo '</pre>';
			// die();

			$send = $mailchimp->call('campaigns/send',array(
				'cid' => $campaign['id']
			));
			if($send['complete']){
				$msg = JText::_( 'Đã gửi thành công' );
			}else{
				$msg = JText::_( 'Có lỗi xảy ra' );
				// echo '<pre>';
				// print_r($campaign);
				// echo '</pre>';
				// die();
			}
    		$link = 'index.php?option=com_vichy_new_letter&controller=newletter';
    		$this->setRedirect($link, $msg);
		}

		
		function cancel(){
			JRequest::checkToken() or jexit( 'Invalid Token' );
			$this->setRedirect('index.php?option=com_vichy_new_letter&controller=newletter');
		}

	}
	
?>