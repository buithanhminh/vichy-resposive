<?php

	defined( '_JEXEC' ) or die( 'Restricted Access' );
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_new_letterModelNewletter extends JModel{

		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT id,content,sent_by,sent_date from #__vichy_subscribe_info";
			
			return $query;
    	}

    	function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}

    	  function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
		    
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));	
		    $row =& $this->getTable();
		    $arr = array();	
	    	foreach ($cids as $cid) {
	    		
	    		$sql = "select * from vc_vichy_subscribe_info where id = $cid";
	    		$db->setQuery($sql);
                $res = $db->loadObject();
                $flag;
                if(count($res)){
				 		$db->setQuery("Delete from vc_vichy_subscribe_info where id = ".$res->id);
				 		$flag = $db->query();
                }
	    	}
	     	return $flag;
		}
	}
	
?>