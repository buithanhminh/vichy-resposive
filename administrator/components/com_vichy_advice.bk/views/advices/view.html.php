<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_adviceViewAdvices extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_adviceHelper::addSubmenu('advices');
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor); 
              
			$listAdvices = & $this->get('Data');
			$this->assignRef('listAdvices', $listAdvices);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>