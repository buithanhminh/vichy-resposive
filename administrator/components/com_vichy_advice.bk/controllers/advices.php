<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_adviceControllerAdvices extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'Advices');
    	parent::display();
	}
	function edit(){
    	JRequest::setVar('view', 'Advices');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }
    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_advice' );
	}
	function save(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Advices');
	    $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );
        if (empty($id)){
            if(!isset($data['is_question'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form');
                return false;
            }

            if(empty($data['text'])){
                $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form');
                return false;
            }

            if(empty($data['catid'])){
                $this->setMessage(JText::_( 'Thể loại không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form');
                return false;
            }
            
        }else{

            if(!isset($data['is_question'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form&task=edit&cid[]='.$id);
                return false;
            }
            
            if(empty($data['text'])){
                $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form&task=edit&cid[]='.$id);
                return false;
            }

            if(empty($data['catid'])){
                $this->setMessage(JText::_( 'Thể loại không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form&task=edit&cid[]='.$id);
                return false;
            }
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
    	} else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

    	$link = 'index.php?option=com_vichy_advice&controller=advices';
    	$this->setRedirect($link, $msg);
	}

    function publish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Advices');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_advice&controller=advices', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Advices');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_advice&controller=advices', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Advices');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều câu hỏi (trả lời) không thể xoá');
        } else {
            $msg = JText::_( 'Cửa hàng này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_advice&controller=advices', $msg );
    }
}