<?php
abstract class Vichy_productHelper{
	public static function addSubmenu($submenu) 
    {
            JSubMenuHelper::addEntry(JText::_('VICHY_PRODUCTS'),
                                     'index.php?option=com_vichy_product', $submenu == 'products');

            JSubMenuHelper::addEntry(JText::_('VICHY_STEPS'),
                                     'index.php?option=com_vichy_product&controller=steps', $submenu == 'steps');


            JSubMenuHelper::addEntry(JText::_('VICHY_CATEGORIES'),
                                     'index.php?option=com_categories&view=categories&extension=com_vichy_product',
                                     $submenu == 'categories');
            // set some global property
            $document = JFactory::getDocument();
            //$document->addStyleDeclaration('.icon-48-helloworld ' . '{background-image: url(../media/com_helloworld/images/tux-48x48.png);}');
            if ($submenu == 'categories') 
            {
                    $document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION_CATEGORIES'));
            }
    }

    public static function listLanguage(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT lang_code, title_native' .
            ' FROM #__languages' .
            ' ORDER BY lang_code ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }

    public static function getStepByCategory($cid){
        $db = & JFactory::getDbo();
        // $sql = "SELECT s.*, p.name, p.image from vc_vichy_step as s join vc_vichy_product as p on s.product_id = p.id where s.category_id = $cid order by s.no_of_steps";
        $sql = "SELECT s.*, p.name, p.image, sn.step_name from vc_vichy_step as s left join vc_vichy_product as p on s.product_id = p.id left join vc_vichy_step_name as sn on sn.category_id = s.category_id and sn.no_of_steps = s.no_of_steps where s.category_id = $cid order by s.no_of_steps";
        // die($sql);
        $db->setQuery($sql);
        $db->query();
        return $db->loadObjectList();
    }

    public static function getCategories(){
        $db = & JFactory::getDBO();
        $lang = JFactory::getLanguage();
        $query = "SELECT id, title, parent_id FROM #__categories where extension = 'com_vichy_product' and published = '1' and language = 'vi-VN' order by title ASC";
        $db->setQuery($query);
        $listCategories = $db->loadObjectList();
        return $listCategories;
    }
    public static function recursive($data,$parent='1',$space='',$select=''){
        if (count($data)) {
            foreach ($data as $k => $v) {
                if($v->parent_id == $parent){
                    $id = $v->id;
                    if($v->parent_id == '1'){
                        if($k>0) echo '</optgroup>';
                        echo '<optgroup label="'.$v->title.'">';
                    }else{
                        if(is_array($select)){
                            $flag = false;
                            foreach ($select as $k1 => $v2) {
                                if($id == $v2){
                                    $flag = true;
                                    break;
                                }
                            }
                            if($flag == true){
                                echo "<option value='$id' selected='selected'>$space".$v->title."</option>";
                            }else{
                                echo "<option value='$id'>$space".$v->title."</option>";
                            }
                        }else{
                            if($select != '' && $id == $select){
                                echo "<option value='$id' selected='selected'>$space".$v->title."</option>";
                            }else{
                                echo "<option value='$id'>$space".$v->title."</option>";
                            }
                        }
                    }
                    unset($data[$k]);
                    self::recursive($data,$id,$space.'--',$select);
                }
            }
        }
    }
}