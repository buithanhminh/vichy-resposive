<?php
defined( '_JEXEC' ) or die;
jimport( 'joomla.application.component.model' );

class Vichy_productModelSteps extends JModel{
	var $_data = null;
    function __construct(){	
	    parent::__construct();
		$option = JRequest::getCmd('option');
		$mainframe =& JFactory::getApplication();
	}

	function _buildQuery(){
		$query = "Select DISTINCT(s.category_id),c.title from #__vichy_step as s join #__categories as c on s.category_id = c.id";
		if(isset($_POST['search'])){
			$query .= " where c.title like '%".$_POST['search']."%'";
		}
		return $query;
	}

	function populateState(){ 	
 		$app = JFactory::getApplication();
 		
 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
 		$this->setState('limit', $limit);
 		
 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
 		$this->setState('start', $limitstart);
 	}

	function getStart(){
 		
 		$total = $this->totalItems();
 		$limit = $this->getState('limit');
 		$start = $this->getState('start');
 		
 		if($start > $total - $limit){
 			$start = (ceil($total/$limit - 1))*$limit;
 		}
 		return $start;
	}

	function &getData(){
        if (empty( $this->_data ))
        {
            $query = $this->_buildQuery();
            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
        }
        return $this->_data;
	}

	function totalItems(){
		$query = $this->_buildQuery();
		$total = $this->_getListCount($query);
		return $total;
	}

	function getPagination(){
 		jimport('joomla.html.pagination');
 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
 		return $pagination;
	}
}