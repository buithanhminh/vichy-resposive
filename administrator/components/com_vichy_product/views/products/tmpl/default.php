<?php 
	defined('_JEXEC') or die;

	JToolBarHelper::title(JText::_( 'Products' ), 'generic.png');
	JToolBarHelper::addNewX();
	JToolBarHelper::publish();
	JToolBarHelper::unpublish();
	JToolBarHelper::deleteList();

  $listCategories = Vichy_productHelper::getCategories();
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

  <table class="adminform">
    <tr>
      
      <td width="100%">
        <?php echo JText::_( 'Tên sản phẩm' ); ?>
        <input type="text" name="search" id="search" value="<?php echo $_POST['search'];?>"  onChange="document.adminForm.submit();" />
        <button onclick="this.form.submit();"><?php echo JText::_( 'Tìm kiếm' ); ?></button>
      </td>     

      <td nowrap="nowrap">
        Lọc theo danh mục :
        <select id="filter_category" name="filter_category" class="inputbox" size="1" onchange="Joomla.submitform();">   
          <?php if($_POST['filter_category'] == '0'){?>
            <option value="0" selected="true">Mặc định</option>
          <?php }else{?>
            <option value="0">Mặc định</option>
          <?php } ?>
          <?php Vichy_productHelper::recursive($listCategories,'1','',$_POST['filter_category']); ?>
        </select>
      </td>
      <td nowrap="nowrap">
        Lọc theo trạng thái :
        <select id="filter_state" name="filter_state" class="inputbox" size="1" onchange="Joomla.submitform();">
          
          <?php if($_POST['filter_state'] == 'default'){?>
            <option value="default" selected="true">Mặc định</option>
          <?php }else{?>
            <option value="default">Mặc định</option>
          <?php } ?>
          
          <?php if($_POST['filter_state'] == '1'){?>
            <option value="1" selected="true">Xuất bản</option>
          <?php }else{?>
            <option value="1" >Xuất bản</option>
          <?php } ?>

           <?php if($_POST['filter_state'] == '0'){?>
            <option value="0" selected="true">Không xuất bản</option>
          <?php }else{?>
            <option value="0">Không xuất bản</option>
          <?php } ?>

        </select>
      </td>
    </tr>
  </table>

  <table class="adminlist" cellspacing="1">   
       <tr>
           <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->ListProducts); ?>);" /></th>
           <th width="30" align="center">Id</th>
           <th align="center">Tên sản phẩm</th>
           <th align="center">Hình ảnh</th>
           <th align="center">Đánh giá</th>
           <th align="center">Yêu thích</th>
           <th align="center">Sản phẩm mới</th>
           <th align="center">Xuất bản</th>
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->ListProducts); $i < $n; $i++)
        {
            $row    =& $this->ListProducts[$i];
            $link   = 'index.php?option=com_vichy_product&controller=products&task=edit&cid[]='. $row->id;            
            $published  = JHTML::_('grid.published', $row, $i );
        ?>
            <tr class="<?php echo "row$k"; ?>">
               <td width="20"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
               <td align="center"><?php echo $row->id; ?></td>  
               <td align="center"><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($row->name); ?></a></td>               
               <td align="center"><img width="100" src="<?php echo JURI::root() ?>components/com_vichy_product/uploads/products/<?php echo $row->image; ?>" /></td>
               <td align="center">
                  <?php for($j=1; $j<=5; $j++){ ?>
                    <?php if(!empty($row->rating) && $row->rating >0 && $j<=$row->rating ){ ?>
                        <img src="<?php echo JURI::root(); ?>media/star_red.png" style="border:none;float:left;" />
                    <?php }else{ ?>
                        <img src="<?php echo JURI::root(); ?>media/star_grey.png" style="border:none;float:left;" />
                    <?php } ?> 
                  <?php } ?>
                  (<?php echo (empty($row->reviews)) ? '0' : $row->reviews; ?> người đánh giá)
               </td>
               <td class="center">
                <?php 
                  if($row->is_favorite){
                    ?>
                        <a title="On" href="javascript:void(0);" class="home_page" id="0<?php echo $row->id;?>"><img alt="" src="templates/bluestork/images/admin/featured.png"></a>
                    <?php
                  }else{
                    ?>
                        <a title="Off" href="javascript:void(0);" class="home_page" id="1<?php echo $row->id;?>"><img alt="" src="templates/bluestork/images/admin/disabled.png"></a>
                    <?php
                  }
                ?>
              </td>
              <td class="center">
                <?php 
                  if($row->is_new){
                    ?>
                        <a title="On" href="javascript:void(0);" class="product_new" id="0<?php echo $row->id;?>"><img alt="" src="templates/bluestork/images/admin/featured.png"></a>
                    <?php
                  }else{
                    ?>
                        <a title="Off" href="javascript:void(0);" class="product_new" id="1<?php echo $row->id;?>"><img alt="" src="templates/bluestork/images/admin/disabled.png"></a>
                    <?php
                  }
                ?>
              </td>
               <td align="center"><?php echo $published; ?></td>                   
            </tr>
                
        <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
           </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_product" />                        
    <input type="hidden" name="controller" value="products" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>
<script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('.home_page').click(function(){
      var id = jQuery(this).attr('id');
      var  home_page = id.substring(0,1);
      var  product_id = id.substring(1);
      window.location="<?php echo JURI::root(); ?>administrator/index.php?option=com_vichy_product&task=featured&home_page="+ home_page +"&id="+product_id;      
      return false;
    });

    jQuery('.product_new').click(function(){
      var id = jQuery(this).attr('id');
      var is_new = id.substring(0,1);
      var product_id = id.substring(1);
      window.location="<?php echo JURI::root(); ?>administrator/index.php?option=com_vichy_product&task=product_new&is_new="+ is_new +"&id="+product_id;      
      return false;
    });
  });
</script>