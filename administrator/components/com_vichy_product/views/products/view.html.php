<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_productViewProducts extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_productHelper::addSubmenu('products');
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor); 
              
			$listProducts = & $this->get('Data');
			$this->assignRef('ListProducts', $listProducts);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>