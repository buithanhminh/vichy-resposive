<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_productViewSteps extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_productHelper::addSubmenu('steps');
            
            $listSteps = & $this->get('Data');
			$this->assignRef('items', $listSteps);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);

	        parent::display($tpl);
    	}
	}
?>