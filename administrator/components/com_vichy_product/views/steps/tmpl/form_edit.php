<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
    $doc->addStyleSheet('templates/bluestork/js/jtable/themes/lightcolor/gray/jtable.css');
    $doc->addStyleSheet('components/com_vichy_product/css/style.css');
	$doc->addStyleSheet('components/com_vichy_product/css/simplegrid.css');
	
    JToolBarHelper::save();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $db = & JFactory::getDbo();
    $db->setQuery("SELECT title from #__categories where id = ".$cid[0]);
    $db->query();
    $res = $db->loadObject();
    $product_step = Vichy_productHelper::getStepByCategory($cid[0]);
    $db->setQuery("SELECT max(no_of_steps) as max from vc_vichy_step where category_id = ".$cid[0]);
    $db->query();
    $max_step = $db->loadObject();
    JToolBarHelper::title(JText::_( 'Edit Step'), 'generic.png');
    JToolBarHelper::cancel( 'cancel', 'Close' );
    $arr_prod = array();
?>
<fieldset>
    <legend>Product details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <div><?php echo $res->title; ?></div>
        <a href="javascript:void(0)" id="add" data-step="<?php echo $max_step->max + 1; ?>">Add more steps</a>
        <div id="wrap_steps_categories">
        
            <?php foreach($product_step as $k => $v){ ?>
            <?php $arr_prod[] = $v->no_of_steps.'#'.$v->product_id; ?>
            <?php if($k==0 || $product_step[$k-1]->no_of_steps != $v->no_of_steps){ ?>
            <div class="wrap_step">
            <div class="grid grid-pad">
                <div style="width:100%">
                    Name of step: <input type="text" name="step_name[]" style="float:none;" value="<?php echo $v->step_name; ?>" />
                    <input type="hidden" name="step_name_no[]" value="<?php echo $v->no_of_steps; ?>" />
                </div>
                <div class="button_step">
                    <a href="javascript:void(0)" <?php echo (empty($v->name)) ? 'data-update='.$v->id : ''; ?> data-step="<?php echo $v->no_of_steps; ?>" class="open_popup">Add more products for step <?php echo $v->no_of_steps; ?></a>
                </div>
                
            <?php } ?>
                <?php if(!empty($v->name)){ ?>
                <div class="step_product col-1-3" id="SelectedRowList_<?php echo $v->no_of_steps; ?>">
                    <div class="step_product_item content">
                        <div class="item-name">
                        <?php echo $v->name; ?>
                        </div>
                        <div class="item-image">
                            <img style="width:150px;" src="<?php echo JURI::root().'components/com_vichy_product/uploads/products/'.$v->image ?>" />
                        </div>
                        <div style="width:100%">
                        <a style="float:left" href="javascript:void(0)" class="edit" data-step="<?php echo $v->no_of_steps; ?>" data-id="<?php echo $v->id ?>">Edit</a>
                        <a style="float:right" href="javascript:void(0)" class="delete" data-step="<?php echo $v->no_of_steps; ?>" data-id="<?php echo $v->id ?>">Delete</a>
                        </div>
                        
                        <input type="hidden" name="step_product_item[]" value="<?php echo $v->no_of_steps.'|'.$v->product_id ?>" />
                    </div>
                </div>
                <?php } ?>
            <?php if($product_step[$k+1]->no_of_steps != $v->no_of_steps){ ?>
            </div>
             </div>
            <?php } ?>
            
            <?php } ?>
            
        </div>
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="" />
        <input type="hidden" name="cid[]" value="<?php echo $cid[0]; ?>" />
        <input type="hidden" name="option" value="com_vichy_product" />
        <input type="hidden" name="controller" value="steps" />
        <input type="hidden" name="task" value=""/>
        
    </form>
</fieldset>
<div id="dialog-form" style="width: 600px;display:none;">
<div class="filtering">
    <form>
        Product name: <input type="text" name="name" id="name" />
        <button type="submit" id="LoadRecordsButton">Load records</button>
    </form>
</div>
<div id="ProductsTableContainer" style="width: 600px;"></div>
</div>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="templates/bluestork/js/jtable/jquery.jtable.js"></script>
<script type="text/javascript">
    str = '<?php echo implode("|",$arr_prod); ?>';
    var json = str.split('|');
    var current_step;
    var edit_id;
    var max_step;
    var arr;
    var edit_more;
    var category_id = <?php echo $cid[0] ?>;
    var update;
    var add_more;

    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }

    jQuery(document).ready(function() {

        // jQuery.ajax({
        //     url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=count_no_of_steps',
        //     type: 'post',
        //     data: 'cate_id=<?php echo $cid[0] ?>',
        //     success:function(kq){
        //         arr = jQuery.parseJSON(kq);
        //         max_step = parseInt(arr[arr.length - 1].no_of_steps);
        //         // new_obj = {'no_of_steps':max_step+1, 'count':'3'};
        //         // arr.push(new_obj);
        //     }
        // });
        
        //Prepare jTable
        jQuery('#ProductsTableContainer').jtable({
            title: 'Table of product',
            paging: true,
            pageSize: 5,
            sorting: true,
            defaultSorting: 'name ASC',
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
            selectOnRowClick: false, //Enable this to only select using checkboxes
            actions: {
                listAction: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=products&task=get_product_ajax'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Product Name',
                    width: '40%'
                },
                image: {
                    title: 'Image',
                    width: '40%'
                }
            },
            recordsLoaded: function(event, data) {
                //add attribute step to class jtable-data-row
                for(i=0;i<json.length;i++){
                    temp = json[i].split('#');
                    jQuery('.jtable-data-row').each(function(index) {
                        if(jQuery(this).attr('data-record-key') == temp[1]){
                            jQuery(this).attr('step', temp[0]);
                        }
                    });
                }
            }
        });

        //Load person list from server
        jQuery('#ProductsTableContainer').jtable('load');

        //Re-load records when user click 'load records' button.
        jQuery('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            jQuery('#ProductsTableContainer').jtable('load', {
                name: jQuery('#name').val()
            });
        });

        jQuery( "#dialog-form" ).dialog({
          autoOpen: false,
          height: 600,
          width: 650,
          modal: true,
          buttons: {
            "Select": function() {                
                f = '';
                jQuery('.jtable-row-selected').each(function(index) {
                    var step = jQuery(this).attr('step');
                    if(step == '' || step == null){
                        jQuery(this).attr('step',current_step);
                    }else{
                        if(step != current_step){
                            // jQuery( this ).dialog( "close" );
                            f = 1;
                            alert('One of these products was chosen for step '+step);
                        }else{
                            // jQuery( this ).dialog( "close" );
                            f = 2;
                            alert('This product was chosen for this step');
                        }
                    }
                });

                if(f == 1 || f == 2){
                    jQuery( this ).dialog( "close" );
                }
                var $selectedRows = jQuery('#ProductsTableContainer').jtable('selectedRows');
                if ($selectedRows.length > 0) {
                    //Show selected rows
                    $selectedRows.each(function () {
                        var record = jQuery(this).data('record');
                        if(update != '' && update != null){
                            jQuery.ajax({
                                url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=editStep',
                                type: 'POST',
                                data: 'id='+update+'&pid='+record.id,
                                success: function(kq){
                                    if(kq == 1){
                                        location.reload();
                                    }
                                }
                            });
                        }
                        if(edit_id != '' && edit_id != null){
                            jQuery.ajax({
                                url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=editStep',
                                type: 'POST',
                                data: 'id='+edit_id+'&pid='+record.id,
                                success: function(kq){
                                    if(kq == 1){
                                        location.reload();
                                    }
                                }
                            });
                        }
                        if(edit_more != '' && edit_more != null){
                            jQuery.ajax({
                                url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=addStep',
                                type: 'POST',
                                data: 'step='+current_step+'&cate_id=<?php echo $cid[0]; ?>&pid='+record.id,
                                success: function(kq){
                                    if(kq == 1){
                                        location.reload();
                                    }
                                }
                            });
                        }
                        if(add != '' && add != null){
                            jQuery.ajax({
                                url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=addMoreStep',
                                type: 'POST',
                                data: 'step='+current_step+'&cate_id=<?php echo $cid[0]; ?>&pid='+record.id,
                                success: function(kq){
                                    if(kq == 1){
                                        location.reload();
                                    }
                                }
                            });
                        }
                    });
                }
                jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
               jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
               edit_more = '';
               edit_id = '';
               add='';
               update ='';
               jQuery( this ).dialog( "close" );
            }
          },
          close: function() {
                edit_more = '';
                edit_id = '';
                add = '';
                update ='';
                jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
          }
        });

        jQuery('#wrap_steps_categories').on('click','.open_popup',function(e){
            current_step = jQuery(this).attr('data-step');
            // add = jQuery(this).attr('data-add');
            // if(add == '' || add == null){
                // edit_more = 1;
            // }
            update = jQuery(this).attr('data-update');
            if(update == '' || update == null){
                edit_more = 1;
            }
            add = '';
            // e.preventDefault();
            // jQuery('#ProductsTableContainer').jtable('load', {
            //     cid: category_id
            // });
            jQuery( "#dialog-form" ).dialog( "open" );
        });

        jQuery('#wrap_steps_categories').on('click','.edit',function(){
            current_step = jQuery(this).attr('data-step');
            edit_id = jQuery(this).attr('data-id');
            edit_more = '';
            add = '';
            update = '';
            jQuery( "#dialog-form" ).dialog( "open" );
        });

        jQuery('#wrap_steps_categories').on('click','.delete',function(){

            if(window.confirm("Do you want to delete this product in this step?")){
                id = jQuery(this).attr('data-id');
                current_step = jQuery(this).attr('data-step');
                // step_d = parseInt(arr[current_step-1].count);
                data_ajax = 'id='+id+'&cid=<?php echo $cid[0] ?>&step='+current_step;
                // if(step_d > 1){
                //     data_ajax += '&flag=1';
                // }
                jQuery.ajax({
                    url: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=steps&task=deleteStep',
                    type: 'POST',
                    data: data_ajax,
                    success: function(kq){
                        if(kq == 1){
                            location.reload();
                        }
                    }
                });
            }
            
        });

        jQuery('#add').click(function() {
            current_step = jQuery(this).attr('data-step');
            if(current_step <= 5){
                add = 1;
                edit_more = '';
                update = '';
                // jQuery('#ProductsTableContainer').jtable('load', {
                //     cid: category_id
                // });
                jQuery( "#dialog-form" ).dialog( "open" );
            }else{
                alert('Lỗi');
            }
        });

    });
</script>