﻿<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    JHTML::_('behavior.mootools');
    $doc = JFactory::getDocument();
    $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');
    $doc->addStyleSheet('templates/bluestork/js/jtable/themes/lightcolor/gray/jtable.css');
    $doc->addStyleSheet('components/com_vichy_product/css/style.css');
	$doc->addStyleSheet('components/com_vichy_product/css/simplegrid.css');
	
    JToolBarHelper::save();
    $listCategories = Vichy_productHelper::getCategories();
    JToolBarHelper::title(JText::_( 'Add Step' ), 'generic.png');
    JToolBarHelper::cancel();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
 ?>
<fieldset>
    <legend>Product details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
		<table>
            <tr>
                <td>Choose category</td>
                <td>
                    <select id="category" name="category_id" class="inputbox" size="1">   
                      <?php if($_POST['category_id'] == '0'){?>
                        <option value="0" selected="true">Choose category</option>
                      <?php }else{?>
                        <option value="0">Choose category</option>
                      <?php } ?>
                      <?php Vichy_productHelper::recursive($listCategories,'1','',$_POST['category_id']); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Number of steps in category</td>
                <td>
                    <input type="text" name="no_step_category" id="no_step_category" value="" />
                    <input type="button" name="add_no_step" id="add_no_step" value="Ok" />
                </td>
            </tr>
        </table>
        <div id="wrap_steps_categories">

        </div>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="" />
        <input type="hidden" name="cid[]" value="" />
        <input type="hidden" name="option" value="com_vichy_product" />
        <input type="hidden" name="controller" value="steps" />
        <input type="hidden" name="task" value=""/>
    </form>
</fieldset>

<div id="dialog-form" style="width: 600px;display:none;">
<div class="filtering">
    <form>
        Tên sản phẩm: <input type="text" name="name" id="name" />
        <button type="submit" id="LoadRecordsButton">OK</button>
    </form>
</div>
<div id="ProductsTableContainer" style="width: 600px;"></div>
</div>
<script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="templates/bluestork/js/jtable/jquery.jtable.js"></script>
<script type="text/javascript">
    var arr = new Array();
    var current_step;
    var n;
    var category_id;
    jQuery(document).ready(function() {
        function submitbutton(pressbutton) {
            var form = document.adminForm;
            if (pressbutton == 'cancel') {
                submitform( pressbutton );
                return;
            }
        }

        Joomla.submitbutton = function(task){
            if (task == 'save'){

                if(jQuery('#category').val() == 0){
                    alert('You have to choose your category');
                }else{
                    for(i=1;i<=jQuery('div.wrap_step').length;i++){
                        text = jQuery('#SelectedRowList_'+i).html();
                        if(text == ''){
                            jQuery('#SelectedRowList_'+i).html('<input type="hidden" name="step_product_item[]" value="'+i+'|0" />');
                        }
                    }
                    Joomla.submitform(task);
                }
            } else Joomla.submitform(task);
            return false;
        };
         
        jQuery('#no_step_category').keypress(function(e) {
            if(e.which == 13){
                category_id = jQuery('#category').val();
                if(category_id == '0'){
                	alert("Please choose a product");
                }else{
                    // e.preventDefault();
                    // jQuery('#ProductsTableContainer').jtable('load', {
                    //     cid: category_id
                    // });

                    n = jQuery('div.wrap_step').length;
                    if(n == 0){
                        var no_steps = jQuery('#no_step_category').val();
                        no_steps = Math.round(no_steps);
                        html_code = '';
                        if(jQuery.isNumeric(no_steps) && no_steps>0){
                            for(i=1;i<=no_steps;i++){
                                html_code += '<div class="wrap_step">';
                                html_code += '<div style="width:100%">';
                                html_code += 'Name of step: <input type="text" name="step_name[]" style="float:none;" />';
                                html_code += '</div>';
                                html_code += '<div class="button_step">';
                                html_code += '<a href="javascript:void(0)" data-step="'+i+'" class="open_popup">Step '+i;
                                html_code += '</a>';
                                html_code += '</div>';
                                html_code += '<div class="step_product" ><div class="grid grid-pad" id="SelectedRowList_'+i+'">';
                                html_code += '</div>';
                                html_code += '</div>';
                                html_code += '</div>';
                            }
                            jQuery('#wrap_steps_categories').html(html_code);
                        }else{
                            alert("Please enter an unsigned number");
                        }
                    }else{
                        alert('Lỗi');
                    }
                }
                
                return false;
            }
        });
        jQuery('#add_no_step').click(function(e) {
            category_id = jQuery('#category').val();
            if(category_id == '0'){
                alert("Please choose a product");
            }else{
                // e.preventDefault();
                // jQuery('#ProductsTableContainer').jtable('load', {
                //     cid: category_id
                // });

                n = jQuery('div.wrap_step').length;
                if(n == 0){
                    var no_steps = jQuery('#no_step_category').val();
                    no_steps = Math.round(no_steps);
                    html_code = '';
                    if(jQuery.isNumeric(no_steps) && no_steps>0){
                        for(i=1;i<=no_steps;i++){
                            html_code += '<div class="wrap_step">';
                            html_code += '<div style="width:100%">';
                            html_code += 'Name of step: <input type="text" name="step_name[]" style="float:none;" />';
                            html_code += '</div>';
                            html_code += '<div class="button_step">';
                            html_code += '<a href="javascript:void(0)" data-step="'+i+'" class="open_popup">Step '+i;
                            html_code += '</a>';
                            html_code += '</div>';
                            html_code += '<div class="step_product" ><div class="grid grid-pad" id="SelectedRowList_'+i+'">';
                            html_code += '</div>';
                            html_code += '</div>';
                            html_code += '</div>';
                            
                        }
                        jQuery('#wrap_steps_categories').html(html_code);
                    }else{
                        alert("Please enter an unsigned number");
                    }
                }else{
                    alert('Lỗi');
                }
            }
            return false;
        });

        //Prepare jTable
        jQuery('#ProductsTableContainer').jtable({
            title: 'Table of product',
            paging: true,
            pageSize: 5,
            sorting: true,
            defaultSorting: 'name ASC',
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
            selectOnRowClick: false, //Enable this to only select using checkboxes
            actions: {
                listAction: '<?php echo JURI::root() ?>administrator/index.php?option=com_vichy_product&controller=products&task=get_product_ajax'
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                name: {
                    title: 'Product Name',
                    width: '40%'
                },
                image: {
                    title: 'Image',
                    width: '40%'
                }
            },
            recordsLoaded: function(event, data) {
                //add attribute step to class jtable-data-row
                for(i=0;i<arr.length;i++){
                    temp = arr[i].split('#');
                    jQuery('.jtable-data-row').each(function(index) {
                        if(jQuery(this).attr('data-record-key') == temp[1]){
                            jQuery(this).attr('step', temp[0]);
                        }
                    });
                }
            }
        });

        //Load person list from server
        jQuery('#ProductsTableContainer').jtable('load');

        //Re-load records when user click 'load records' button.
        jQuery('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            jQuery('#ProductsTableContainer').jtable('load', {
                name: jQuery('#name').val(),
                // cid: category_id
            });
        });

        jQuery( "#dialog-form" ).dialog({
          autoOpen: false,
          height: 600,
          width: 650,
          modal: true,
          buttons: {
            "Select": function() {
                flag = '';
                jQuery('.jtable-row-selected').each(function(index) {
                    var step = jQuery(this).attr('step');
                    if(step == '' || step == null){
                        jQuery(this).attr('step',current_step);
                    }else{
                        if(step != current_step){
                            flag = 1;
                            alert('One of these products was chosen for step '+step);
                        }else{
                            flag = 2;
                            alert('This product was chosen for this step');
                        }
                    }
                });

                if(flag == 1 || flag == 2){
                    jQuery( this ).dialog( "close" );
                }
                
                var $selectedRows = jQuery('#ProductsTableContainer').jtable('selectedRows');
                if ($selectedRows.length > 0) {
                    //Show selected rows
                    
                    
                    $selectedRows.each(function () {
                        var record = jQuery(this).data('record');
                        arr.push(current_step+'#'+record.id);
                        var step_product_item = '<div class="col-1-3" ><div class="step_product_item content">';
                        step_product_item += '<div class="item-name">';
                        step_product_item += record.name;
                        step_product_item += '</div>';
                        step_product_item += '<div class="item-image">';
                        step_product_item += record.image;
                        step_product_item += '</div>';
                        step_product_item += '<a href="javascript:void(0)" class="del">Delete</a>';
                        step_product_item += '<input type="hidden" name="step_product_item[]" value="'+current_step+'|'+record.id+'" />';
                        step_product_item += '</div></div>';
                        jQuery('#SelectedRowList_'+current_step).append(step_product_item);
                    });
                }
                jQuery( this ).dialog( "close" );
            },
            Cancel: function() {
               jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
               jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
               jQuery( this ).dialog( "close" );
            }
          },
          close: function() {
                jQuery('.jtable-column-header-container input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-selecting-column input[type=checkbox]').attr('checked', false);
                jQuery('.jtable-row-selected').removeClass('jtable-row-selected');
          }
        });

        jQuery('#wrap_steps_categories').on('click','.open_popup',function(){
            current_step = jQuery(this).attr('data-step');
            jQuery( "#dialog-form" ).dialog( "open" );
        });

        jQuery('#wrap_steps_categories').on('click','.del',function(){
            tmp = jQuery(this).siblings('input[type=hidden]').val();
            pro_id = tmp.substring(tmp.indexOf('|')+1);
            jQuery('.jtable-data-row').each(function(index) {
                if(jQuery(this).attr('data-record-key') == pro_id){
                    jQuery(this).attr('step', '');
                }
            });
            for(i=0;i<arr.length;i++){
                temp = arr[i].split('#');
                if(temp[1] == pro_id){
                    arr.splice(i,1);
                }
            }
            jQuery(this).parent().remove();
        });
    });
</script>