<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_productControllerSteps extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'add');
        $this->registerTask('edit', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'Steps');
    	parent::display();
	}

    function add(){
        JRequest::setVar('view', 'Steps');
        JRequest::setVar('layout', 'form');
        JRequest::setVar('hidemainmenu', 1); 
        parent::display();
    }

	function edit(){
    	JRequest::setVar('view', 'Steps');
    	JRequest::setVar('layout', 'form_edit');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }

    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_product&controller=steps');
	}

	function save(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Steps');
	    $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $cat_id = $cid[0];
        $data = JRequest::get( 'post' );
        $db = & JFactory::getDbo();
        if($cat_id == 0){
            if(!$data['category_id']){
            	$this->setMessage(JText::_( 'Please choose your category'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=steps&layout=form');
                return false;
            }
            $db->setQuery("Select DISTINCT(category_id) from #__vichy_step where category_id = ".$data['category_id']);
            $db->query();
            $ret = $db->loadObjectList();
            if(count($ret)){
                $this->setMessage(JText::_( 'This category has some products for its step'));
                $this->setRedirect('index.php?option=com_vichy_product&controller=steps&controller=steps&task=edit&cid[]='.$data['category_id']);
                return false;
            }
            if(!isset($data['step_product_item'])){
            	$this->setMessage(JText::_( 'Please select the products for your steps'),'error');
                $this->setRedirect('index.php?option=com_vichy_product&controller=steps&layout=form');
                return false;
            }
            foreach ($data['step_product_item'] as $k => $v) {
            	$tmp = explode('|', $v);
            	$sql = "Insert into #__vichy_step (`category_id`,`product_id`,`no_of_steps`) value ('".$data['category_id']."','".$tmp[1]."','".$tmp[0]."')";
            	$db->setQuery($sql);
            	$db->query();
            }
            foreach ($data['step_name'] as $k => $v) {
                
                $s = $k + 1;
                $db->setQuery("Insert into #__vichy_step_name values (null, $data[category_id], $s, '$v')");
                $db->query();
            }
        }else{
            // if(!isset($data['step_product_item'])){
            //     $this->setMessage(JText::_( 'Please select the products for your steps'),'error');
            //     $this->setRedirect('index.php?option=com_vichy_product&controller=steps&controller=steps&task=edit&cid[]='.$cat_id);
            //     return false;
            // }
            // if(isset($data['edit_id'])){
            //     foreach ($data['edit_id'] as $k => $v) {
            //         $tmp = explode('|',$v);

            //         $db->setQuery("Update #__vichy_step set product_id = ".$tmp[2].", modify_date='".date('Y-m-d h:i:s')."' where id = ".$tmp[0]);
            //         $db->query();
            //     }
            // }
            // if(isset($data['add_id'])){
            //     foreach ($data['add_id'] as $k => $v) {
            //         $tmp1 = explode('|',$v);

            //         $db->setQuery("Insert into #__vichy_step values (null, ".$tmp1[1].", $cat_id, ".$tmp1[0].", null, null)");
            //         $db->query();
            //     }
            // }
            foreach ($data['step_name'] as $k=> $v) {
                // $db->setQuery("select id from vc_vichy_step_name where category_id = $cat_id and no_of_steps = ".$data['step_name_no'][$k]);
                // $step_tmp = $db->loadObject();
                // if(count($step_tmp)){
                if($v != ''){
                    $db->setQuery("select id from #__vichy_step_name where category_id = $cat_id and no_of_steps = ".$data['step_name_no'][$k]);
                    $db->query();
                    if($db->getNumRows()<1){
                        $lang = 'vi-VN';
                        $sql = "Insert into #__vichy_step_name values (null,null, $cat_id, ".$data['step_name_no'][$k].", '$v','$lang')";
                        $db->setQuery($sql);
                    }else{
                        $db->setQuery("Update #__vichy_step_name set step_name = '$v' where category_id = $cat_id and no_of_steps = ".$data['step_name_no'][$k]);
                    }
                    $db->query();
                }else{
                    $msg = JText::_( 'Tên của step '.$data['step_name_no'][$k].' không được để trống' );
                    $link = 'index.php?option=com_vichy_product&controller=steps&task=edit&cid[]='.$cat_id;
                    $this->setRedirect($link, $msg);
                    return false;
                }
                // }else{
                //     $db->setQuery("Insert into #__vichy_step_name values (null, $cat_id, ".$data['step_name_no'][$k].", '$v')");
                //     $db->query();
                // }
            }
        }
        $msg = JText::_( 'Lưu thành công!' );
    	$link = 'index.php?option=com_vichy_product&controller=steps';
    	$this->setRedirect($link, $msg);
	}

    function delete(){
        $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $cat_id = $cid[0];
        $db = & JFactory::getDbo();
        $db->setQuery("DELETE from #__vichy_step where category_id = ".$cat_id);
        $db->query();

        $db->setQuery("DELETE from #__vichy_step_name where category_id = ".$cat_id);
        $db->query();

        $msg = JText::_( 'Xóa thành công' );
        $link = 'index.php?option=com_vichy_product&controller=steps';
        $this->setRedirect($link, $msg);
    }

    function deleteStep(){
        $id = $_POST['id'];
        $cid = $_POST['cid'];
        // $flag = isset($_POST['flag']) ? $_POST['flag'] : '';
        $step = $_POST['step'];
        $db = & JFactory::getDbo();
        // if($flag == ''){
        //     $db->setQuery('Select no_of_steps from #__vichy_step where id = '.$id);
        //     $db->query();
        //     $no_of_steps = $db->loadObject()->no_of_steps;
        // }

        //kiem tra trong step nay con bao nhieu san pham, neu > 1 thi xoa record, neu = 1 thi update product_id = 0
        $db->setQuery("Select count(id) as count from #__vichy_step where category_id=$cid and no_of_steps=$step");
        $check1 = $db->loadObject()->count;
        if($check1 > 1){
            $db->setQuery('Delete from #__vichy_step where id = '.$id);
            $db->query();
        }else{
            $db->setQuery('Update #__vichy_step set product_id = 0 where id = '.$id);
            $db->query();
        }

        // $db->setQuery('Delete from #__vichy_step where id = '.$id);
        // $db->query();


        // if($flag == ''){
        //     $db->setQuery("Update #__vichy_step set no_of_steps = no_of_steps - 1, modify_date = '".date('Y-m-d h:i:s')."' where category_id = ".$cid." and no_of_steps > ".$no_of_steps);
        //     $db->query();
        // }

        echo '1';

        exit();
    }

    function editStep(){
        $id = $_POST['id'];
        $pid = $_POST['pid'];
        $db = & JFactory::getDbo();
        $db->setQuery("Update #__vichy_step set product_id = $pid, modify_date = '".date('Y-m-d h:i:s')."' where id =".$id);
        $db->query();

        echo '1';

        exit();
    }

    function addStep(){
        $cate_id = $_POST['cate_id'];
        $step = $_POST['step'];
        $pid = $_POST['pid'];
        $db = & JFactory::getDbo();
        $db->setQuery("Insert into #__vichy_step values (null, ".$pid.", $cate_id, ".$step.", null, null)");
        $db->query();

        echo '1';

        exit();
    }

    function addMoreStep(){
        $cate_id = $_POST['cate_id'];
        $step = $_POST['step'];
        $pid = $_POST['pid'];
        $db = & JFactory::getDbo();
        $db->setQuery("Insert into #__vichy_step values (null, ".$pid.", $cate_id, ".$step.", null, null)");
        $db->query();

        $db->setQuery("Insert into #__vichy_step_name values (null, $cate_id, ".$step.", 'Step $step')");
        $db->query();

        echo '1';

        exit();
    }

    function count_no_of_steps(){
        $cate_id = $_POST['cate_id'];
        $db = & JFactory::getDbo();
        $db->setQuery("SELECT no_of_steps,count(no_of_steps) as count FROM `vc_vichy_step` where category_id = $cate_id group by no_of_steps");
        $db->query();
        $data = $db->loadObjectList();
        $tmp = json_decode(json_encode($data),true);
        echo json_encode($tmp);
        exit();
    }

}