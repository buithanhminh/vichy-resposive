<?php
    defined('_JEXEC') or die;
     
    class TableProducts extends JTable {
    	var $id = 0;
        var $name;
        var $description;
        var $image;
        var $effectiveness;
        var $tolerance;
        var $pleasure;
        var $video;
        var $buy_online_link;
        var $is_favorite;
        var $published;
        var $create_date;
        var $modify_date;
        var $language;
        var $link;
     
        function __construct(&$db) {
        	parent::__construct('#__vichy_product', 'id', $db);
        }
    }
?>
