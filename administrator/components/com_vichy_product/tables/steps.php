<?php
    defined('_JEXEC') or die;
     
    class TableSteps extends JTable {
    	var $id = 0;
        var $product_id;
        var $category_id;
        var $no_of_steps;
        var $create_date;
        var $modify_date;
     
        function __construct(&$db) {
        	parent::__construct('#__vichy_step', 'id', $db);
        }
    }
?>
