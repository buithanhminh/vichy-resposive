<?php
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');

JToolBarHelper::save();

$cid = JRequest::getVar('cid',  0, '', 'array');
JArrayHelper::toInteger($cid, array(0));
$db =& JFactory::getDBO();
$ciddef = $cid[0];

if ($ciddef > 0) {	
	
	$query = "SELECT * FROM `#__vichy_comment_product` WHERE id = ".$ciddef." LIMIT 0,1";
	$db->setQuery($query);
	$group = $db->loadObject();
	$query = "select email, name from #__users where username = '".$group->username."'";
	$db->setQuery($query);
	$user = $db->loadObject();
	JFilterOutput::objectHTMLSafe( $group, ENT_QUOTES, '' );
	
	JToolBarHelper::title(JText::_( 'Trả lời phản hồi' ), 'generic.png');
	JToolBarHelper::cancel( 'cancel', 'Close' );
	$editor = & JFactory::getEditor();
}


?>

<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Câu hỏi :</td>
			<td><span><?php echo $group->comment; ?></span></td>
		</tr>
		<tr>
			<td>Trả lời :</td>
			<td>
				<?php    
            		echo @$editor->display( 'answer',  $group->answer, '550', '300', '75', '20', array() ) ; 
                ?>
			</td>
		</tr>
	</table>			
	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" id="id" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
	<input type="hidden" name="username" value="<?php if(!empty($user->name))  echo($user->name);else echo $group->username; ?>" />
	<input type="hidden" name="email" value="<?php if(!empty($user->email))  echo($user->email);else echo ''; ?>" />
	<input type="hidden" name="option" value="com_vichy_comment_product" />
	<input type="hidden" name="controller" value="feedback" />
	<input type="hidden" name="task" value="" />
</form>