<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_servicesControllerServices extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'services');
    	parent::display();
	}

	function edit(){
    	JRequest::setVar('view', 'services');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }
    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_services' );
	}
	function save(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Services');
        $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );
        $size = $_FILES['file']['size'];
        $max_size = 307200;
        if (empty($id)){
            if(empty($_POST['content'])){
                $this->setMessage(JText::_( 'Đối tượng dịch vụ không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_services&controller=services&layout=form');
                return false;
            }
          
        }else{
            
             if(empty($_POST['content'])){
                $this->setMessage(JText::_( 'Đối tượng dịch vụ không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_services&controller=services&layout=form');
                return false;
            }
            
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
        } else {
            $msg = JText::_( 'Lưu thất bại !' );
        }
        $link = 'index.php?option=com_vichy_services&controller=services';
        $this->setRedirect($link, $msg);
    }

    function publish(){        

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Services');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_services');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_services&controller=services', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Services');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_services');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_services&controller=services', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Services');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều ảnh thể xoá');
        } else {
            $msg = JText::_( 'Ảnh này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_services&controller=services', $msg );
    }
}