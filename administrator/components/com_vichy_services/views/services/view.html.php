<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_servicesViewServices extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_servicesHelper::addSubmenu('services');          
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor);            
			$listServices = & $this->get('Data');
			$this->assignRef('listServices', $listServices);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>