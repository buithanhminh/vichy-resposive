<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_servicesModelServices extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();
		}
		
		function _buildQuery(){

			$query = "SELECT s.*, c.title FROM #__vichy_services as s join #__categories as c where c.id = s.catid";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );
	    	$data['content'] = JRequest::getVar( 'content', '', 'post', 'string', JREQUEST_ALLOWRAW );
	    	// echo $data['content'];die();
            if(empty($data['cid'][0])){
            	// $json['object']=$data['object'];
            	// $json['origin']=$data['origin'];
            	// $json['effective']=$data['effective'];
            	// $json['advice']=$data['advice'];
            	// $json['time']=$data['time'];
            	// $json['price']=$data['price'];
            	// $new_content=json_encode($json);

                $db = & JFactory::getDBO();
                $sql = "INSERT into #__vichy_services(content,catid,language,published) 
                values ('$data[content]','$data[category]','$data[language]','1')";
                
                $db->setQuery($sql);
                $db->query();
               
            }else{

            	
            	$db = & JFactory::getDBO();
                $sql = "UPDATE #__vichy_services 
                set content = '$data[content]', catid='$data[category]',language = '$data[language]' where id = ".$data['cid'][0];
                $db->setQuery($sql);
                $db->query();
            }
	    	return true;
		}
		
	    function deleteRows() {
		    // $cids = JRequest::getVar('cid', 0, 'post', 'array');
      //       $db = & JFactory::getDBO();
		    // JArrayHelper::toInteger($cids, array(0));	
		    // $row =& $this->getTable();		
	    	// foreach ($cids as $cid) {

      //           $query = "SELECT image FROM #__vichy_home where id=".$cid;
      //           $db->setQuery($query);
      //           $result = $db->loadObject();

      //           $localtion = '../components/com_vichy_store/uploads/others/'.$result->image;

      //           if($data['position']==1)
      //           		$location = '../components/com_vichy_services/uploads/slide_show/'.$result->image;

      //           unlink($localtion);

      //           if (!$row->delete( $cid )) {
  	   //          	$this->setError($row->getErrorMsg());
  	   //          	return false;
      //           }
               
	    	// }
	     // 	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_services'
					. ' SET publish = ' . (int) $publish
					. ' WHERE id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>