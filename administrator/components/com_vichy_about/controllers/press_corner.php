<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_aboutControllerPress_corner extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'press_corner');
    	parent::display();
	}

	function edit(){
    	JRequest::setVar('view', 'press_corner');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }
    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_about' );
	}
	function save(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Press_corner');
	    $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );

		$size = $_FILES['file']['size'];
        $max_size = 1007200;
        if (empty($id))/*add new item*/
        {
            if(empty($data['title'])){
                $this->setMessage(JText::_( 'Vui lòng nhập tên'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;
            }

            if(empty($data['description'])){
                $this->setMessage(JText::_( 'Vui lòng nhập mô tả'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;
            }

            if(empty($_FILES['file']['name'])){
                $this->setMessage(JText::_( 'Vui lòng chọn hình ảnh đại diện'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;
            }      

            if ((($_FILES["file"]["type"] != "image/gif")
					&& ($_FILES["file"]["type"] != "image/jpeg")
					&& ($_FILES["file"]["type"] != "image/jpg")
					&& ($_FILES["file"]["type"] != "image/pjpeg")
					&& ($_FILES["file"]["type"] != "image/x-png")
					&& ($_FILES["file"]["type"] != "image/png"))
			)
			{
				$this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;
			}
            
            if($size > $max_size){
                
                $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 1Mb !'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;     
            }

            if(empty($_FILES['pdf']['name'])){
                $this->setMessage(JText::_( 'Vui lòng tải file pdf'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;
            }

            if ($_FILES["pdf"]["type"] != "application/pdf"){
                $this->setMessage(JText::_( 'File pdf không hợp lệ !'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form');
                return false;
            }
            
        }else/*edit item*/
        {
            
            if(empty($data['title'])){
                $this->setMessage(JText::_( 'Vui lòng nhập tên'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form&task=edit&cid[]='.$id);
                return false;
            }

            if(empty($data['description'])){
                $this->setMessage(JText::_( 'Vui lòng nhập mô tả'),'error');
                $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form&task=edit&cid[]='.$id);
                return false;
            }

            if(!empty($_FILES["file"]["name"])) {
                if ((($_FILES["file"]["type"] != "image/gif")
                    && ($_FILES["file"]["type"] != "image/jpeg")
                    && ($_FILES["file"]["type"] != "image/jpg")
                    && ($_FILES["file"]["type"] != "image/pjpeg")
                    && ($_FILES["file"]["type"] != "image/x-png")
                    && ($_FILES["file"]["type"] != "image/png"))
                )
                {
                    $this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form&task=edit&cid[]='.$id);
                    return false;
                }
                
                if($size > $max_size){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 1Mb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form&task=edit&cid[]='.$id);
                    return false;     
                }
            }

            if(!empty($_FILES["pdf"]["name"])){
                if ($_FILES["pdf"]["type"] != "application/pdf"){
                    $this->setMessage(JText::_( 'File pdf không hợp lệ !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner&layout=form&task=edit&cid[]='.$id);
                    return false;
                }
            }
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
    	} else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

    	$link = 'index.php?option=com_vichy_about&controller=press_corner';
    	$this->setRedirect($link, $msg);
	}

    function publish(){        

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Press_corner');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_about');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_about&controller=press_corner', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Press_corner');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_about');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Press_corner');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều ảnh thể xoá');
        } else {
            $msg = JText::_( 'Ảnh này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_about&controller=press_corner', $msg );
    }
}