<?php
abstract class Vichy_aboutHelper{
	public static function addSubmenu($submenu) 
    {
            JSubMenuHelper::addEntry(JText::_('Nước khoáng dưỡng da'),
                                     'index.php?option=com_vichy_about&controller=thermal_spa_warter', $submenu == 'thermal_spa_warter');

            JSubMenuHelper::addEntry(JText::_('Nghiên cứu và cải tiến'),
                                     'index.php?option=com_vichy_about&controller=reseach', $submenu == 'reseach');
            JSubMenuHelper::addEntry(JText::_('Mô hình tư vấn da'),
                                     'index.php?option=com_vichy_about&controller=consulting', $submenu == 'consulting');
            JSubMenuHelper::addEntry(JText::_('Góc báo chí'),
                                     'index.php?option=com_vichy_about&controller=magazine', $submenu == 'magazine');
    }

    public static function listLanguage(){
        $db =& JFactory::getDbo();
        $db->setQuery(
            'SELECT lang_code, title_native' .
            ' FROM #__languages' .
            ' ORDER BY lang_code ASC'
        );
        $options = $db->loadObjectList();
        return $options;
    }
}