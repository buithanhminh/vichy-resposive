<?php 
  defined('_JEXEC') or die;

  JToolBarHelper::title(JText::_( 'Góc báo chí' ), 'generic.png');
  JToolBarHelper::addNewX();
  JToolBarHelper::publish();
  JToolBarHelper::unpublish();
  JToolBarHelper::deleteList();
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

    <table class="adminlist" cellspacing="1">   
        <tr>
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->list); ?>);" /></th>
            <th width="30" align="center">Id</th>
            <th align="center">Image</th>
            <th align="center">Tên</th>                        
            <th align="center">Xuất bản</th>
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->list); $i < $n; $i++)
        {
            $row    =& $this->list[$i];
            $image_link = JURI::root()."components/com_vichy_about/uploads/images/$row->image";
            $link   = 'index.php?option=com_vichy_about&controller=press_corner&task=edit&cid[]='. $row->id;
            $published  = JHTML::_('grid.published', $row->published, $i );
        ?>
            <tr class="<?php echo "row$k"; ?>">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
                <td align="center"><?php echo $row->id; ?></td>                 
                <td align="center"><a href="<?php echo $link; ?>"><img width="100" src="<?php echo $image_link; ?>" /></a></td>
                <td align="center"><?php echo $row->title; ?></td>                
                <td align="center"><?php echo $published; ?></td>
            </tr>
                
         <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_about" />                        
    <input type="hidden" name="controller" value="press_corner" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>