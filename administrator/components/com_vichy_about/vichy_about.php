<?php 
	defined('_JEXEC') or die();
	// require helper file
	JLoader::register('Vichy_aboutHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'vichy_about.php');
	$controller = JRequest::getCmd('controller','press_corner');
	$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
	if(file_exists($path)){
		require_once $path;
	}
	$controller = ucwords(strtolower($controller));
	$classname = 'Vichy_aboutController'.$controller;	
	$controller = new $classname();
	$task = JRequest::getVar('task');
	$controller->execute($task);
	$controller->redirect();
?>