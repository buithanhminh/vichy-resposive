<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_diagnosticControllerMapping extends JController{
   
    function __construct(){
        parent::__construct();
        $this->registerTask('add', 'add');        
        $this->registerTask('edit', 'edit');
    }

    function display(){
        JRequest::setVar('view', 'Mapping');
        parent::display();
    }
    function add(){
        JRequest::setVar('view', 'Mapping');
        JRequest::setVar('layout', 'form');
        JRequest::setVar('hidemainmenu', 1); 
        parent::display();
    }
    function edit(){
        JRequest::setVar('view', 'Mapping');
        JRequest::setVar('layout', 'form_edit');
        JRequest::setVar('hidemainmenu', 1); 
        parent::display();
    }
    function cancel(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=mapping' );
    }
    function save(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Mapping');
        $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );        
        if ($data['form'] == 'add'){
            // if($data['form'] == 'add'){
            //     $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
            //     $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=mapping&layout=form');
            //     return false;
            // }
            if($data['type'] == 'advice'){
                if(empty($data['name'])){
                    $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=mapping&layout=form_edit&task=edit&cid[]='.$id);
                    return false;
                }
            }
            
        }else{
            if($data['type'] == 'advice'){
                if(empty($data['name'])){
                    $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=mapping&layout=form_edit&task=edit&cid[]='.$id);
                    return false;
                }
            }
            
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
        } else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

        $link = 'index.php?option=com_vichy_diagnostic&controller=mapping';
        $this->setRedirect($link, $msg);
    }

    function publish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Diagnostics');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_diagnostic&controller=diagnostics', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Diagnostics');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Mapping');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều câu hỏi (trả lời) không thể xoá');
        } else {
            $msg = JText::_( 'Câu hỏi (trả lời) này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics', $msg );
    }

    function loadAnswers(){
        $ques_id = $_POST['ques_id'];
        $respone = array();
        $list_ans = Vichy_diagnosticHelper::getListAnswers($ques_id);
        if(!empty($list_ans)){            
            $html = '';
            foreach ($list_ans as $key => $value) {
                $html   .= '<div style="float:left;width:100%;">
                                <input type="checkbox" name="answer_'.$value->answer_id.' class="ans_check" value="'.$value->answer_id.'" id="'.$value->answer_id.'" /><label for="'.$value->answer_id.'" style="clear:none;width:70%;">'.$value->answer_text.'</label>
                            </div>';
            }
        }else{
            $html = "Vui lòng chọn câu hỏi";
        }
        $respone['html_select'] = $html;
        echo json_encode($respone);
        exit();
    }

    function addResProduct(){        
        $product_id = $_POST['product_id'];
        $result_id = $_POST['result_id'];        
        $db = & JFactory::getDbo();
        
        $step_product = $_POST['step_product'];
        if(isset($step_product) && $step_product == 3){
            $product_step2 = $_POST['product_step2'];
            $query = "Update #__vichy_result_product set product_id_2 = $product_id where product_id =".$product_step2." and result_id =".$result_id;
        }else{
            $query = "Insert into #__vichy_result_product (id, result_id, product_id) values (null, $result_id, $product_id)";
        }
        $db->setQuery($query);
        $db->query();

        echo '1';

        exit();
    }

    function editResProduct(){
        $product_id = $_POST['product_id'];
        $result_id = $_POST['result_id'];
        $product_id_old = $_POST['product_id_old'];
        $db = & JFactory::getDbo();

        $step_product = $_POST['step_product'];
        if(isset($step_product) && $step_product == 3){
            $query = "Update #__vichy_result_product set product_id_2 = $product_id where product_id_2 =".$product_id_old." and result_id =".$result_id;
        }else{
            $query = "Update #__vichy_result_product set product_id = $product_id where product_id =".$product_id_old." and result_id =".$result_id;
        }
        
        $db->setQuery($query);
        $db->query();

        echo '1';

        exit();
    }

    function deleteProduct(){
        $product_id = $_POST['product_id'];
        $result_id = $_POST['result_id'];
        
        $db = & JFactory::getDbo();

        $step_product = $_POST['step_product'];
        if(isset($step_product) && $step_product == 3){
            $query = "Update #__vichy_result_product set product_id_2 = null where product_id_2 =".$product_id." and result_id =".$result_id;
        }else{
            $query = 'Delete from #__vichy_result_product where product_id = '.$product_id.' and result_id = '.$result_id;
        }

        $db->setQuery($query);
        $db->query();

        echo '1';

        exit();
    }

}