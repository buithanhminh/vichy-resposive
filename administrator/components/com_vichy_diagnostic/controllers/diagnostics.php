<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_diagnosticControllerDiagnostics extends JController{
   
    function __construct(){
        parent::__construct();
        $this->registerTask('add', 'edit');
    }

    function display(){
        JRequest::setVar('view', 'Diagnostics');
        parent::display();
    }
    function edit(){
        JRequest::setVar('view', 'Diagnostics');
        JRequest::setVar('layout', 'form');
        JRequest::setVar('hidemainmenu', 1); 
        parent::display();
    }
    function cancel(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics' );
    }
    function save(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Diagnostics');
        $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );
        $size_icon = $_FILES['icon']['size'];
        $max_size_icon = 307200;
        $size_bg = $_FILES['background']['size'];
        $max_size_bg = 307200;
        if (empty($id)){
            if(!isset($data['is_question'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form');
                return false;
            }

            if(empty($data['text'])){
                $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form');
                return false;
            }
            
            if(!empty($_FILES['icon']['name'])){
                if ((($_FILES["icon"]["type"] != "image/gif")
                        && ($_FILES["icon"]["type"] != "image/jpeg")
                        && ($_FILES["icon"]["type"] != "image/jpg")
                        && ($_FILES["icon"]["type"] != "image/pjpeg")
                        && ($_FILES["icon"]["type"] != "image/x-png")
                        && ($_FILES["icon"]["type"] != "image/png"))
                )
                {
                    $this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form');
                    return false;
                }
                
                if($size_icon > $max_size_icon){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form');
                    return false;     
                }
            }
            if(!empty($_FILES['background']['name'])){
                if ((($_FILES["background"]["type"] != "image/gif")
                        && ($_FILES["background"]["type"] != "image/jpeg")
                        && ($_FILES["background"]["type"] != "image/jpg")
                        && ($_FILES["background"]["type"] != "image/pjpeg")
                        && ($_FILES["background"]["type"] != "image/x-png")
                        && ($_FILES["background"]["type"] != "image/png"))
                )
                {
                    $this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form');
                    return false;
                }
                
                if($size_bg > $max_size_bg){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form');
                    return false;     
                }
            }
        }else{

            if(!isset($data['is_question'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form&task=edit&cid[]='.$id);
                return false;
            }
            
            if(empty($data['text'])){
                $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form&task=edit&cid[]='.$id);
                return false;
            }

            if(!empty($_FILES['icon']['name'])){
                if ((($_FILES["icon"]["type"] != "image/gif")
                    && ($_FILES["icon"]["type"] != "image/jpeg")
                    && ($_FILES["icon"]["type"] != "image/jpg")
                    && ($_FILES["icon"]["type"] != "image/pjpeg")
                    && ($_FILES["icon"]["type"] != "image/x-png")
                    && ($_FILES["icon"]["type"] != "image/png"))
                )
                {
                    $this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form&task=edit&cid[]='.$id);
                    return false;
                }
                
                if($size_icon > $max_size_icon){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form&task=edit&cid[]='.$id);
                    return false;     
                }
            }
            if(!empty($_FILES['background']['name'])){
                if ((($_FILES["background"]["type"] != "image/gif")
                    && ($_FILES["background"]["type"] != "image/jpeg")
                    && ($_FILES["background"]["type"] != "image/jpg")
                    && ($_FILES["background"]["type"] != "image/pjpeg")
                    && ($_FILES["background"]["type"] != "image/x-png")
                    && ($_FILES["background"]["type"] != "image/png"))
                )
                {
                    $this->setMessage(JText::_( 'Hình ảnh không hợp lệ !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form&task=edit&cid[]='.$id);
                    return false;
                }
                
                if($size_bg > $max_size_bg){
                    
                    $this->setMessage(JText::_('Kích cỡ hình ảnh phải nhỏ hơn 300Kb !'),'error');
                    $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics&layout=form&task=edit&cid[]='.$id);
                    return false;     
                }
            }
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
        } else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

        $link = 'index.php?option=com_vichy_diagnostic&controller=diagnostics';
        $this->setRedirect($link, $msg);
    }

    function publish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Diagnostics');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_diagnostic&controller=diagnostics', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Diagnostics');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Diagnostics');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều câu hỏi (trả lời) không thể xoá');
        } else {
            $msg = JText::_( 'Câu hỏi (trả lời) này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics', $msg );
    }

    function loadCategories(){
        $language = $_POST['language'];        

        $list_cate = Vichy_diagnosticHelper::getCategories($language);
        foreach ($list_cate as $key => $value) {                        
            $html_cate .= '<option value="'.$value->id.'">'.strip_tags($value->title).'</option>';
        }

        $catid = $list_cate[0]->id;
        $list_question = Vichy_diagnosticHelper::getListQuestion($language, $catid);
        $html_ques = '<option value="0">Không có cấp cha</option>';                    
        foreach ($list_question as $key => $value) {                        
            $html_ques .= '<option value="'.$value->id.'">'.strip_tags($value->text).'</option>';
        }

        $respone['option_cate'] = $html_cate;
        $respone['option_question'] = $html_ques;
        echo json_encode($respone);

        exit();
    }

    function loadQuestion(){
        $language = $_POST['language'];
        $catid = $_POST['catid'];        
        
        $list_question = Vichy_diagnosticHelper::getListQuestion($language, $catid);
        $html_ques = '<option value="0">Không có cấp cha</option>';                    
        foreach ($list_question as $key => $value) {                        
            $html_ques .= '<option value="'.$value->id.'">'.strip_tags($value->text).'</option>';
        }

        $respone['option_question'] = $html_ques;
        echo json_encode($respone);

        exit();
    }
}