<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_diagnosticControllerDiagnostics_users extends JController{
   
    function __construct(){
        parent::__construct();        
    }

    function display(){
        JRequest::setVar('view', 'Diagnostics_users');
        parent::display();
    }    

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Diagnostics_users');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Không thể xoá được thông tin');
        } else {
            $msg = JText::_( 'Đã xóa thông tin thành công !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_diagnostic&controller=diagnostics_users', $msg );
    }

    function loadCategories(){
        $language = $_POST['language'];        

        $list_cate = Vichy_diagnosticHelper::getCategories($language);
        foreach ($list_cate as $key => $value) {                        
            $html_cate .= '<option value="'.$value->id.'">'.strip_tags($value->title).'</option>';
        }

        $catid = $list_cate[0]->id;
        $list_question = Vichy_diagnosticHelper::getListQuestion($language, $catid);
        $html_ques = '<option value="0">Không có cấp cha</option>';                    
        foreach ($list_question as $key => $value) {                        
            $html_ques .= '<option value="'.$value->id.'">'.strip_tags($value->text).'</option>';
        }

        $respone['option_cate'] = $html_cate;
        $respone['option_question'] = $html_ques;
        echo json_encode($respone);

        exit();
    }

    function loadDetail(){
        $user_id = $_POST['user_id'];
        
        $list = Vichy_diagnosticHelper::getDiagnosticsUsers($user_id);
        $list_answers = Vichy_diagnosticHelper::getDiagnosticsAnswers();
        $list_answers_users = Vichy_diagnosticHelper::getListAnswersUsers($user_id);
        $html = '';
        $i = 0;
        $before_item = $list[0]->id;
        $index = 1;
        foreach ($list as $key => $value) {
            $current_item = $value->id;
            $i++;
            if($i == 1){
                $html .= '<ul style="list-style-type: square;"><span class="question">'.$index++.' .'.strip_tags($value->text).'</span>';
                foreach ($list_answers as $k => $v) {
                    if($v->parent_id == $value->id){
                        if($this->checkInAnswerUsers($v->id, $list_answers_users)){
                            $class = "answers_users";
                        }else{
                            $class = '';
                        }

                        $html .= '<li class="'.$class.'" data-answer="'.$v->id.'">'.strip_tags($v->text).'</li>';
                    }
                }
            }
            if($i != 1 && $before_item != $current_item){
                $html .= '<ul style="list-style-type: square;"><span class="question">'.$index++.' .'.strip_tags($value->text).'</span>';
                foreach ($list_answers as $k => $v) {
                    if($v->parent_id == $value->id){
                        if($this->checkInAnswerUsers($v->id, $list_answers_users)){
                            $class = "answers_users";
                        }else{
                            $class = '';
                        }

                        $html .= '<li class="'.$class.'" data-answer="'.$v->id.'">'.strip_tags($v->text).'</li>';
                    }
                }
            } 
            if($i == 1){
                $html .= '</ul>';
            }
            
            if($i != 1 && $before_item != $current_item){
                $html .= '</ul>';
            }
            $before_item = $current_item;
        }
        $respone['content_diagnostic'] = $html;
        echo json_encode($respone);

        exit();
    }

    function searchPopup(){
        $user_id = $_POST['user_id'];
        $popup_input = $_POST['popup_input'];
        
        $list = Vichy_diagnosticHelper::getDiagnosticsUsers($user_id, $popup_input);
        $list_answers = Vichy_diagnosticHelper::getDiagnosticsAnswers();
        $list_answers_users = Vichy_diagnosticHelper::getListAnswersUsers($user_id);
        $html = '';
        $i = 0;
        $before_item = $list[0]->id;
        $index = 1;
        foreach ($list as $key => $value) {
            $current_item = $value->id;
            $i++;
            if($i == 1){
                $html .= '<ul style="list-style-type: square;"><span class="question">'.$index++.' .'.strip_tags($value->text).'</span>';
                foreach ($list_answers as $k => $v) {
                    if($v->parent_id == $value->id){
                        if($this->checkInAnswerUsers($v->id, $list_answers_users)){
                            $class = "answers_users";
                        }else{
                            $class = '';
                        }

                        $html .= '<li class="'.$class.'" data-answer="'.$v->id.'">'.strip_tags($v->text).'</li>';
                    }
                }
            }
            if($i != 1 && $before_item != $current_item){
                $html .= '<ul style="list-style-type: square;"><span class="question">'.$index++.' .'.strip_tags($value->text).'</span>';
                foreach ($list_answers as $k => $v) {
                    if($v->parent_id == $value->id){
                        if($this->checkInAnswerUsers($v->id, $list_answers_users)){
                            $class = "answers_users";
                        }else{
                            $class = '';
                        }

                        $html .= '<li class="'.$class.'" data-answer="'.$v->id.'">'.strip_tags($v->text).'</li>';
                    }
                }
            } 
            if($i == 1){
                $html .= '</ul>';
            }
            
            if($i != 1 && $before_item != $current_item){
                $html .= '</ul>';
            }
            $before_item = $current_item;
        }
        $respone['content_diagnostic'] = $html;
        echo json_encode($respone);

        exit();
    }

    function checkInAnswerUsers($answer_id, $list){
        if(empty($list)){
            return false;
        }
        for($i = 0; $i < count($list); $i++){
            if($answer_id == $list[$i]){
                return true;
            }
        }
        return false;
    }
}