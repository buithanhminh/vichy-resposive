<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_diagnosticModelDiagnostics extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();			
		}
		
		function _buildQuery(){
			$where_cate = '';
			$where_state = '';
			//$where_type = '';
			if(!empty($_POST['filter_category'])){
				$where_cate = 'and qa.catid = '.$_POST['filter_category'];
			}
			if(isset($_POST['filter_state']) && $_POST['filter_state'] != 'default'){
				$where_state = "and qa.published = ".$_POST['filter_state'];
			}
			if(isset($_POST['filter_language']) && $_POST['filter_language'] != '0'){
				$where_language = "and qa.language = '".$_POST['filter_language']."'";
			}else{				
				$where_language = !isset($_POST['filter_language']) ? " and qa.language = 'vi-VN' " : '';
			}
			$query = "SELECT qa.*,c.title FROM #__vichy_qa as qa join #__categories as c on c.id = qa.catid where qa.type = 'diagnostic' $where_cate $where_state $where_language ";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		//$limit = $app->getUserStateFromRequest('global.limit', 'limit', 30);
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

		    $row =& $this->getTable();
	    	$data = JRequest::get( 'post' );
	    	$data['type'] = "diagnostic";
            $data['text'] = JRequest::getVar( 'text', '', 'post', 'string', JREQUEST_ALLOWRAW );
            if(!isset($data['parent_id'])){
            	$data['parent_id'] = 0;
            }
            $db = & JFactory::getDbo();
            if(empty($data['id'])){
            	$type_icon = $_FILES['icon']['type'];
                $extension_icon = strtolower(substr($type_icon, strpos($type_icon,'/') + 1));
                $tmp_name_icon = $_FILES['icon']['tmp_name'];
                $new_name_icon = md5(time()).'.'.$extension_icon;
                $location_icon = '../components/com_vichy_advice/uploads/diagnostics/'.$new_name_icon;
                if(move_uploaded_file($tmp_name_icon, $location_icon)){
                    $data['icon'] =  $new_name_icon;
                }
                
                $type_bg = $_FILES['background']['type'];
                $extension_bg = strtolower(substr($type_bg, strpos($type_bg,'/') + 1));
                $tmp_name_bg = $_FILES['background']['tmp_name'];
                $new_name_bg = md5(time()).'.'.$extension_bg;
                $location_bg = '../components/com_vichy_advice/uploads/diagnostics/'.$new_name_bg;
                if(move_uploaded_file($tmp_name_bg, $location_bg)){
                    $data['background'] =  $new_name_bg;
                }

            	$query = "Insert into #__vichy_qa (id, text, parent_id, is_question, type, catid, language, published, background, icon, type_answer, style_diagnostic) 
            			values (null, '$data[text]', $data[parent_id], $data[is_question],'$data[type]',$data[catid],'$data[language]',1,'$data[background]','$data[icon]', '$data[type_answer]', '$data[style_diagnostic]')";
            }else{
            	if(!empty($_FILES['icon']['name'])){
            		$type_icon = $_FILES['icon']['type'];
	                $extension_icon = strtolower(substr($type_icon, strpos($type_icon,'/') + 1));
	                $tmp_name_icon = $_FILES['icon']['tmp_name'];
	                $new_name_icon = md5(time()).'.'.$extension_icon;
	                $location_icon = '../components/com_vichy_advice/uploads/diagnostics/'.$new_name_icon;
	                if(move_uploaded_file($tmp_name_icon, $location_icon)){
	                    $data['icon'] =  $new_name_icon;
	                }else{
	                	$this->setError("Lỗi không up được file");
	                	return false;
	                }
            	}else{
            		$data['icon'] = $data['icon-image'];
            	}
            	if(!empty($data['icon-image']) && $data['icon']!=$data['icon-image']){
            		$localtion_gallery_icon = '../components/com_vichy_advice/uploads/diagnostics/'.$data['icon-image'];
                	unlink($localtion_gallery_icon);
            	}

            	if(!empty($_FILES['background']['name'])){
            		$type_bg = $_FILES['background']['type'];
	                $extension_bg = strtolower(substr($type_bg, strpos($type_bg,'/') + 1));
	                $tmp_name_bg = $_FILES['background']['tmp_name'];
	                $new_name_bg = md5(time()).'.'.$extension_bg;
	                $location_bg = '../components/com_vichy_advice/uploads/diagnostics/'.$new_name_bg;
	                if(move_uploaded_file($tmp_name_bg, $location_bg)){
	                    $data['background'] =  $new_name_bg;
	                }else{
	                	$this->setError("Lỗi không up được file");
	                	return false;
	                }
            	}else{
            		$data['background'] = $data['background-image'];
            	}
            	if(!empty($data['background-image']) && $data['background']!=$data['background-image']){
            		$localtion_gallery_bg = '../components/com_vichy_advice/uploads/diagnostics/'.$data['background-image'];
                	unlink($localtion_gallery_bg);
            	}

            	$query = "Update #__vichy_qa set `text` = '$data[text]', parent_id = $data[parent_id], is_question = $data[is_question], catid = $data[catid], language = '$data[language]', background = '$data[background]', icon = '$data[icon]', type_answer = '$data[type_answer]', style_diagnostic = '$data[style_diagnostic]' where id = $data[id]";
            }
            $db->setQuery($query);
            $ret =  $db->query();
            if($ret == 1){
            	return true;
            }else{
            	return false;
            }
	    	
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));
	    	foreach ($cids as $cid) {

                // if (!$row->delete( $cid )) {
  	            	// $this->setError($row->getErrorMsg());
  	            	// return false;
                // }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_qa'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .') or parent_id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>