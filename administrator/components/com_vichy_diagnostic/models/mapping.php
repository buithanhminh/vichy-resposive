<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_diagnosticModelMapping extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();			
		}
		
		function _buildQuery(){
			$where_cate = '';
			$where_state = '';
			$where_style = '';
			$where_search = '';

			if(!empty($_POST['filter_search'])){
				$where_search = " and qa.text like '%".$_POST['filter_search']."%' ";
			}
			if(!empty($_POST['filter_category'])){
				$where_cate = 'and qa.catid = '.$_POST['filter_category'];
			}
			// if(isset($_POST['filter_state']) && $_POST['filter_state'] != 'default'){
			// 	if($_POST['filter_state'] == '1'){
			// 		$where_state = "and map.answer_id is null ";
			// 	}else if($_POST['filter_state'] == '0'){
			// 		$where_state = "and map.answer_id is not null ";	
			// 	}
				
			// }
			if(!empty($_POST['filter_style'])){
				$where_style = "and qa.style_diagnostic = '".$_POST['filter_style']."'";
			}
			
			$query = "SELECT distinct qa.id, map.answer_id, qa.*,c.title FROM #__vichy_qa as qa left join vc_vichy_diagnostic_map as map on qa.id = map.answer_id join #__categories as c on c.id = qa.catid where qa.type = 'diagnostic' and c.id not in(60,155) $where_cate $where_style $where_search ";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		//$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', 30);
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
    	

	    function store() {

	    	$data = JRequest::get( 'post' );
            $data['name'] = JRequest::getVar( 'name', '', 'post', 'string', JREQUEST_ALLOWRAW );
            
            $db = & JFactory::getDbo();
            if($data['form'] == 'add'){
            	if(empty($data['result_id'])){
            		if($data['type'] == 'advice'){
            			$query = "INSERT into #__vichy_result (id, name, type) 
	            			values (null, '$data[name]', '$data[type]')";
            		}else{
            			$aid = JRequest::getVar('aid',  0, '', 'array');
	        			JArrayHelper::toInteger($aid, array(0));
	        			foreach ($aid as $key => $value) {
	        				$query_result = "INSERT into #__vichy_result (id, name, type) 
	            							values (null, '".$data["type_skin_$value"]."', '$data[type]')";
	        				$db->setQuery($query_result);
	        				$res = $db->query();
	        				if($res == 1){
	        					$result_id = $this->getIDMax();	            	            	
			            		$map_id = $this->getMapID_ByResult($result_id);

				            	if(empty($map_id)){
					            		
					            		//insert dignostic_map
					            		$sql_map = "INSERT into #__vichy_diagnostic_map (id, parent_id, result_id, answer_id) values(null, '".$map_id."', '".$result_id."', '".$value."')";
					            		$db->setQuery($sql_map);
							            $db->query();
							    }
				            	if(!empty($data['step_product_item_'.$value])){
				            		if(empty($map_id)){	            		
					            		//insert product for result
						            	foreach ($data['step_product_item'.$value] as $k => $v) {
							            	$tmp = explode('|', $v);
							            	$sql = "INSERT into #__vichy_result_product (`result_id`,`product_id`) value ('".$result_id."','".$tmp[1]."')";
							            	$db->setQuery($sql);
							            	$db->query();
							            }
						        	}
					        	}
	        				}
	        			}
	        			return true;
            		}	            	
	            	
	            }else{           	
	            	if($data['type'] == 'advice'){
	            		$query = "UPDATE #__vichy_result set `name` = '$data[name]', type = '$data[type]' where id = $data[result_id]";
	            	}else{
	            		$rid = JRequest::getVar('rid',  0, '', 'array');
	        			JArrayHelper::toInteger($rid, array(0));
	        			foreach ($rid as $key => $value) {
	        				$query_result = "UPDATE #__vichy_result set `name` = '".$data["type_skin_$value"]."' where id = $value";
	        				$db->setQuery($query_result);
	        				$res = $db->query();
	        			}
	            	}
	            }
            }else{
            	$rid = JRequest::getVar('rid',  0, '', 'array');
        		if(!empty($rid)){
	        		JArrayHelper::toInteger($rid, array(0));
	            	if(empty($rid)){

		            	$query = "INSERT into #__vichy_result (id, name, type) 
		            			values (null, '$data[name]', '$data[type]')";
		            	
		            }else{           	
		            	
	        			foreach ($rid as $key => $value) {
	        				$query_result = "UPDATE #__vichy_result set `name` = '".$data["type_skin_$value"]."' where id = $value";
	        				$db->setQuery($query_result);
	        				$res = $db->query();
	        			}
		            }
	        	}
	            return true;
            }
            
            $db->setQuery($query);
            $ret =  $db->query();
            if($ret == 1){
       //      	if($data['form'] == 'add'){
       //      		$result_id = $this->getIDMax();	            	
       //      	}else{
       //      		$result_id = $data['result_id'];
       //      	}
       //      	$map_id = $this->getMapID_ByResult($result_id);

       //      	if(empty($map_id)){
	            		
	      //       		//insert dignostic_map
	      //       		$sql_map = "INSERT into #__vichy_diagnostic_map (id, parent_id, result_id, answer_id) values(null, '".$map_id."', '".$result_id."', '".$data['answer_id']."')";
	      //       		$db->setQuery($sql_map);
			    //         $db->query();
			    // }
			    if($data['form'] == 'add'){
            		$result_id = $this->getIDMax();	            	            	
            		$map_id = $this->getMapID_ByResult($result_id);

	            	if(empty($map_id)){		            		
		            		//insert dignostic_map
		            		$sql_map = "INSERT into #__vichy_diagnostic_map (id, parent_id, result_id, answer_id) values(null, '".$map_id."', '".$result_id."', '".$data['answer_id']."')";
		            		$db->setQuery($sql_map);
				            $db->query();
				    }
	            	if(!empty($data['step_product_item'])){
	            		if(empty($map_id)){	            		
		            		//insert product for result
			            	foreach ($data['step_product_item'] as $k => $v) {
				            	$tmp = explode('|', $v);
				            	$sql = "INSERT into #__vichy_result_product (`result_id`,`product_id`) value ('".$result_id."','".$tmp[1]."')";
				            	$db->setQuery($sql);
				            	$db->query();
				            }
			        	}
		        	}
		        }
            	return true;
            }else{
            	return false;
            }
	    	
		}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));
	    	foreach ($cids as $cid) {

                // if (!$row->delete( $cid )) {
  	            	// $this->setError($row->getErrorMsg());
  	            	// return false;
                // }
               
	    	}
	     	return true;
		}
        

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_qa'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .') or parent_id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}

		function getIDMax(){
	        $query = "SELECT max(id) from #__vichy_result";
	        $this->_db->setQuery($query);
	        $id = $this->_db->loadResult();
	        return $id;
	    }

	    function getMapID_ByResult($result_id){
	        $query = "SELECT id from #__vichy_diagnostic_map where result_id = $result_id";
	        $this->_db->setQuery($query);
	        $id = $this->_db->loadResult();
	        return !empty($id) ? $id : 0;
	    }

	    function getListAnswer(){
	    	$query = "SELECT distinct qa.id, map.answer_id, qa.*,c.title FROM #__vichy_qa as qa left join vc_vichy_diagnostic_map as map on qa.id = map.answer_id join #__categories as c on c.id = qa.catid where qa.type = 'diagnostic' and c.id not in(60,155) ";
			$this->_db->setQuery($query);
			$list = $this->_db->loadObjectList();

			return $list;
	    }
	}
	
?>