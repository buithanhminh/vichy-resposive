<?php

	defined( '_JEXEC' ) or die;
 	jimport( 'joomla.application.component.model' );
 
	class Vichy_diagnosticModelDiagnostics_users extends JModel{
	   
		var $_data = null;
	    function __construct(){	
		    parent::__construct();
			$option = JRequest::getCmd('option');
			$mainframe =& JFactory::getApplication();			
		}
		
		function _buildQuery(){
			$where_search = '';

			if(!empty($_POST['filter_search'])){
				$where_search = " and (u.name like '%".$_POST['filter_search']."%' or u.username like '%".$_POST['filter_search']."%') ";
			}
			$query = "SELECT u.id as user_id, u.name, u.username, u.email, u.myskin, r.name as result_text
							from #__users as u inner join #__vichy_result as r on u.myskin = r.id
							where myskin <> '' $where_search ";
			return $query;
    	}
        
        function totalItems(){
			$query = $this->_buildQuery();
			$total = $this->_getListCount($query);
			return $total;
		}


		function getStart(){
 		
	 		$total = $this->totalItems();
	 		$limit = $this->getState('limit');
	 		$start = $this->getState('start');
	 		
	 		if($start > $total - $limit){
	 			$start = (ceil($total/$limit - 1))*$limit;
	 		}
	 		return $start;
 		}
 		
	 	function getPagination(){
	 		jimport('joomla.html.pagination');
	 		$pagination = new JPagination($this->totalItems(), $this->getStart(), $this->getState('limit'));
	 		return $pagination;
 		}
 		
 		function populateState(){ 	
	 		$app = JFactory::getApplication();
	 		
	 		$limit = $app->getUserStateFromRequest('global.limit', 'limit', $app->getCfg('list_limit', 10));	 		
	 		$this->setState('limit', $limit);
	 		
	 		$limitstart = $app->getUserStateFromRequest('limitstart', 'limitstart', 0);
	 		$this->setState('start', $limitstart);
	 	}

	    function &getData(){
	        if (empty( $this->_data ))
	        {
	            $query = $this->_buildQuery();
	            $this->_data = $this->_getList( $query, $this->getStart(),$this->getState('limit',10));
	        }
	        return $this->_data;
    	}
		
	    function deleteRows() {
		    $cids = JRequest::getVar('cid', 0, 'post', 'array');
            $db = & JFactory::getDBO();
		    JArrayHelper::toInteger($cids, array(0));
	    	foreach ($cids as $cid) {
	    		$this->deleteDiagnosticsUsers($cid);
	    	}
	     	return true;
		}
        
        function deleteDiagnosticsUsers($user_id){
        	$db = & JFactory::getDBO();
        	$query = "UPDATE #__users set myskin = NULL where id = $user_id";
        	$flag = false;
        	$db->setQuery($query);
        	if($db->query()){
        		$flag = true;
        	}

        	$query_result = "DELETE from vc_vichy_qa_user where user_id = $user_id and is_registration_question = 0 ";
        	$db->setQuery($query_result);
        	if($db->query()){
        		$flag = true;
        	}else{
        		$flag = false;
        	}
        	return $flag;
        }

		function publish($cid = array(), $publish = 1){
			$user 	=& JFactory::getUser();
	
			if (count( $cid ))
			{
										
				$cids = implode( ',', $cid );
	
				$query = 'UPDATE #__vichy_qa'
					. ' SET published = ' . (int) $publish
					. ' WHERE id IN ('. $cids .') or parent_id IN ('. $cids .')';
					
				$this->_db->setQuery( $query );
				if (!$this->_db->query()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
			return $cid;
		}
	}
	
?>