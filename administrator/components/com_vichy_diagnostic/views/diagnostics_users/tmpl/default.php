<?php 
    defined('_JEXEC') or die;

    JToolBarHelper::title(JText::_( 'Diagnostics' ), 'generic.png');    
    JToolBarHelper::deleteList();
    $document = & JFactory::getDocument();
    $document->addStyleSheet('components/com_vichy_diagnostic/css/style.css');
    //$document->addScript('components/com_vichy_diagnostic/js/script.js');   
    
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table class="adminform">
        <tr>
            <td nowrap="nowrap" style="text-align:left;">
                Tìm kiếm :
                <input name="filter_search" id="filter_search" class="inputbox" value="<?php echo $_POST['filter_search']; ?>" />
                <button type="submit" class="btn">Tìm</button>
                <button type="button" onclick="document.id('filter_search').value='';this.form.submit();">Xóa</button>
            </td>            
                   
        </tr>
    </table>
    <?php
        $list = $this->listDiagnosticsUsers;
        $n = count($list);
    ?>

    <table class="adminlist" cellspacing="1">   
        <tr class="row1" style="line-height: 30px">
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo $n; ?>);" /></th>            
            <th width="30" align="center">ID</th>
            <th align="center" style="width:20%;">Têm</th>
            <th align="center" style="width:20%;">Username</th>
            <th align="center" style="width:20%;">Email</th>
            <th align="center" style="width:20%;">Loại da</th>            
            <th align="center"style="width:10%;">Chi tiết</th>
        </tr>
        <tbody>
        <?php

            if(!empty($list)){
                for($i = 0; $i < $n; $i++) {
                    $row = $list[$i];

        ?>
        <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
            <td width="30" align="center"><?php echo JHTML::_('grid.id', $i, $row->user_id); ?></td>                  
            <td align="center"><?php echo $row->user_id; ?></td>
            <td><?php echo $row->name; ?></td>
            <td><?php echo $row->username; ?></td>
            <td align="center"><?php echo $row->email; ?></td>
            <td align="center"><?php echo strip_tags($row->result_text); ?></td>
            <td align="center"><a href="javascript:void(0);" class="topopup" data-userid="<?php echo $row->user_id; ?>">Chi tiết</a></td>
        </tr>        

        <?php
                }
            }else{
                echo '<tr><td colspan="7" align="center">Không có dữ liệu</td></tr>';
            }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    
    <!-- popup detail -->
    <div id="toPopup" class="toPopupContent"> 
            
        <div class="close"></div>
        <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_top">
            Tìm kiếm :
                <input name="popup_input" id="popup_input" class="inputbox" value="" />
                <button type="button" class="search_popup" task='search'>Tìm</button>
                <button type="button" class="search_popup" task='del'>Xóa</button>
        </div>
        <div id="popup_content" class="popup_content">
            <div id="content">

            </div>
        </div>

    </div>
    
    <div class="loader"></div>
    <div id="backgroundPopup" class="backgroundPopup"></div>

    <div id="base-url-diagnostics-users" data-url="<?php echo JURI::root();?>administrator/index.php?option=com_vichy_diagnostic&controller=diagnostics_users"></div>
    <!-- end -->

    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_diagnostic" />                        
    <input type="hidden" name="controller" value="diagnostics_users" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>

<script>
jQuery(function($) {
    var id;
    var popup_input;
    $(".search_popup").click(function(){
        if(jQuery(this).attr('task') == 'del'){
            jQuery("#popup_input").val('');
        }
        popup_input = jQuery("#popup_input").val();
        jQuery.ajax({
            type: "POST",
            url: jQuery('#base-url-diagnostics-users').attr('data-url')+'&task=searchPopup',
            dataType: "json",
            data: "user_id="+id+"&popup_input="+popup_input,
            success: function(data_page) {
                jQuery("#content").html(data_page.content_diagnostic);
            }
        });
    });
    $("a.topopup").click(function() {
        id = jQuery(this).attr('data-userid');
        /* ajax load data */        
        jQuery.ajax({
            type: "POST",
            url: jQuery('#base-url-diagnostics-users').attr('data-url')+'&task=loadDetail',
            dataType: "json",
            data: "user_id="+id,
            success: function(data_page) {
                jQuery("#content").html(data_page.content_diagnostic);
            }
        });
        /* end ajax */

        loading();
        setTimeout(function(){
            loadPopup();
        }, 500);
    return false;
    });
    
    /* event for close the popup */
    $("div.close").hover(
        function() {
            $('span.ecs_tooltip').show();
        },
        function () {
            $('span.ecs_tooltip').hide();
        }
    );
    
    $("div.close").click(function() {
        disablePopup();
    });
    
    $(this).keyup(function(event) {
        if (event.which == 27) {
            disablePopup();
        }   
    });
    
    $("div#backgroundPopup").click(function() {
        disablePopup();
    });
    
    $('a.livebox').click(function() {
        alert('Hello World!');
    return false;
    });
    

     /************** start: functions. **************/
    function loading() {
        $("div.loader").show();  
    }
    function closeloading() {
        $("div.loader").fadeOut('normal');  
    }
    
    var popupStatus = 0;
    
    function loadPopup() { 
        if(popupStatus == 0) {
            closeloading();
            $("#toPopup").fadeIn(0500);
            $("#backgroundPopup").css("opacity", "0.7"); 
            $("#backgroundPopup").fadeIn(0001); 
            popupStatus = 1;
        }   
    }
        
    function disablePopup() {
        if(popupStatus == 1) {
            $("#toPopup").fadeOut("normal");  
            $("#backgroundPopup").fadeOut("normal");  
            popupStatus = 0;
        }
    }
    /************** end: functions. **************/
}); // jQuery End
</script>