<?php 
    defined('_JEXEC') or die;

    JToolBarHelper::title(JText::_( 'Diagnostics' ), 'generic.png');
    JToolBarHelper::addNewX();
    JToolBarHelper::publish();
    JToolBarHelper::unpublish();
    JToolBarHelper::deleteList();

    $listCategories = Vichy_diagnosticHelper::getCategories($_POST['filter_language']);
    $listLanguage = Vichy_diagnosticHelper::listLanguage();
    
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
    <table class="adminform">
        <tr>
            <td nowrap="nowrap" style="text-align:right;">
                Lọc theo danh mục :
                <select id="filter_category" name="filter_category" class="inputbox" size="1" onchange="Joomla.submitform();">   
                    <?php if($_POST['filter_category'] == '0'){?>
                    <option value="0" selected="true">Mặc định</option>
                    <?php }else{?>
                    <option value="0">Mặc định</option>
                    <?php } ?>
                    <?php
                        $list_child = $listCategories;
                        foreach ($listCategories as $key => $item) {
                            
                            if($item->parent_id == '1'){
                                echo "<optgroup label='". $item->title ."'>";
                                foreach ($list_child as $k => $v) {
                                    if($item->id == $v->parent_id){
                                        $select = ($_POST['filter_category'] == $v->id) ? 'selected = "selected"' : '';
                                        $value = $v->id;
                                        echo '<option value="'.$value.'" '.$select.'>'.$v->title.'</option>';
                                    }                        
                                }
                                echo "</optgroup>";
                            }                        
                        }
                    ?>
                </select>
                Lọc theo trạng thái :
                <select id="filter_state" name="filter_state" class="inputbox" size="1" onchange="Joomla.submitform();">

                    <?php if($_POST['filter_state'] == 'default'){?>
                    <option value="default" selected="true">Mặc định</option>
                    <?php }else{?>
                    <option value="default">Mặc định</option>
                    <?php } ?>

                    <?php if($_POST['filter_state'] == '1'){?>
                    <option value="1" selected="true">Xuất bản</option>
                    <?php }else{?>
                    <option value="1" >Xuất bản</option>
                    <?php } ?>

                    <?php if($_POST['filter_state'] == '0'){?>
                    <option value="0" selected="true">Không xuất bản</option>
                    <?php }else{?>
                    <option value="0">Không xuất bản</option>
                    <?php } ?>

                </select>

                Ngôn ngữ :
                <select id="filter_language" name="filter_language" class="inputbox" size="1" onchange="Joomla.submitform();">   
                    <?php 
                        if(!isset($_POST['filter_language'])){
                           $_POST['filter_language'] = 'vi-VN';
                        }
                    ?>
                    
                    <option value="0">Tất cả</option>
                    <?php
                        foreach ($listLanguage as $k => $v) {
                            $select = ($_POST['filter_language'] == $v->lang_code) ? 'selected = "selected"' : '';
                            $value = $v->lang_code;
                            echo '<option value="'.$value.'" '.$select.'>'.$v->title_native.'</option>';                            
                        }
                    ?>
                </select>
            </td>            
                   
        </tr>
    </table>

    <table class="adminlist" cellspacing="1">   
        <tr class="row1" style="line-height: 30px">
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listDiagnostics); ?>);" /></th>            
            <th align="center">Nội dung</th>
            <th align="center">Danh mục</th>
            <th align="center">Loại</th>
            <th align="center">Xuất bản</th>
            <th width="30" align="center">ID</th>
        </tr>
        <tbody>
        <?php 
        $k = 1;
        $n = count($this->listDiagnostics);
        if($this->listDiagnostics[0]->is_question == 0){
            $question_id_first = Vichy_diagnosticHelper::getQuestionIDFromAnsID($this->listDiagnostics[0]->id);
        }
        for ($i=0; $i < $n; $i++)
        {
            $row    =& $this->listDiagnostics[$i];
            $link   = 'index.php?option=com_vichy_diagnostic&controller=diagnostics&task=edit&cid[]='. $row->id;
            $published  = JHTML::_('grid.published', $row, $i );
            if($row->is_question == 1){
        ?>
            <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>                  
                <td><a href="<?php echo $link; ?>"><strong><?php echo strip_tags($row->text); ?></strong></a></td>
                <td><?php echo $row->title; ?></td>
                <td align="center"><?php echo ($row->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>
                <td align="center"><?php echo $published; ?></td>
                <td align="center"><?php echo $row->id; ?></td>
            </tr>               
        <?php
                $k = 1 - $k;
            }
                       
            for($j=0; $j < $n; $j++)
            {
                $row2 =& $this->listDiagnostics[$j];
                $link   = 'index.php?option=com_vichy_diagnostic&controller=diagnostics&task=edit&cid[]='. $row2->id;
                $published  = JHTML::_('grid.published', $row2, $j );            

                if($row2->parent_id == $row->id){
        ?>
            <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
                <td width="30"><?php echo JHTML::_('grid.id', $j, $row2->id); ?></td>                  
                <td><span class="gi">|—</span><a href="<?php echo $link; ?>"><?php echo strip_tags($row2->text); ?></a></td>
                <td><?php echo $row->title; ?></td>
                <td align="center"><?php echo ($row2->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>
                <td align="center"><?php echo $published; ?></td>
                <td align="center"><?php echo $row2->id; ?></td>
            </tr>
        <?php
                $k = 1 - $k;
                }else if($row2->parent_id === $question_id_first && $question_id_first != null){
        ?>
            <tr class="<?php echo "row$k"; ?>" style="line-height: 30px">
                <td width="30"><?php echo JHTML::_('grid.id', $j, $row2->id); ?></td>                  
                <td><span class="gi">|—</span><a href="<?php echo $link; ?>"><?php echo strip_tags($row2->text); ?></a></td>
                <td><?php echo $row->title; ?></td>
                <td align="center"><?php echo ($row2->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>
                <td align="center"><?php echo $published; ?></td>
                <td align="center"><?php echo $row2->id; ?></td>
            </tr>
        <?php 
                    $k = 1 - $k;
                }

            }
            $question_id_first = null;
        ?>

        <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_diagnostic" />                        
    <input type="hidden" name="controller" value="diagnostics" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>