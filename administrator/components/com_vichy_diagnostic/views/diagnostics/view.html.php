<?php
	defined('_JEXEC') or die;
	jimport('joomla.application.component.view');
	
	class Vichy_diagnosticViewDiagnostics extends JView{
	   
		function display($tpl = null){
	        // Set the submenu
            Vichy_diagnosticHelper::addSubmenu('question_answer');
            $editor = & JFactory::getEditor();
            $this->assignRef('editor', $editor); 
              
			$listDiagnostics = & $this->get('Data');
			$this->assignRef('listDiagnostics', $listDiagnostics);
                        
            $Pagination = & $this->get('Pagination');
			$this->assignRef('Pagination', $Pagination);
	        parent::display($tpl);
    	}
	}
?>