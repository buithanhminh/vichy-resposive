<?php 
	defined('_JEXEC') or die;

	JToolBarHelper::title(JText::_( 'Trang chủ' ), 'generic.png');
	JToolBarHelper::addNewX();
	JToolBarHelper::publish();
	JToolBarHelper::unpublish();
	JToolBarHelper::deleteList();
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

    <table class="adminlist" cellspacing="1">   
        <tr>
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listHomeImages); ?>);" /></th>
            <th width="30" align="center">Id</th>
            <th align="center">Image</th>
            <th align="center">Vị trí</th>                        
            <th align="center">Xuất bản</th>
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->listHomeImages); $i < $n; $i++)
        {
            $row    =& $this->listHomeImages[$i];
            $image_link = JURI::root()."components/com_vichy_home/uploads/others/$row->image";
            if($row->position ==1)
                $image_link = JURI::root()."components/com_vichy_home/uploads/slide_show/$row->image";
            $link   = 'index.php?option=com_vichy_home&controller=home&task=edit&cid[]='. $row->id;
            $published  = JHTML::_('grid.published', $row->publish, $i );
            $position_name;

            switch ($row->position) {
                case 1:
                    $position_name = HOME_POSITION_SLIDESHOW_NAME;
                    break;
                
                case 2:
                    $position_name = HOME_POSITION_MAIN_ROW_TOP_LEFT_NAME;
                    break;

                case 3:
                    $position_name = HOME_POSITION_MAIN_ROW_TOP_RIGHT_NAME;
                    break;

                case 4:
                    $position_name = HOME_POSITION_MAIN_ROW_BOTTOM_LEFT_NAME;
                    break;

                case 5:
                    $position_name = HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1_NAME;
                    break;

                case 6:
                    $position_name = HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2_NAME;
                    break;
            }
        ?>
            <tr class="<?php echo "row$k"; ?>">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
                <td align="center"><?php echo $row->id; ?></td>                 
                <td align="center"><a href="<?php echo $link; ?>"><img width="100" src="<?php echo $image_link; ?>" /></a></td>
                <td align="center"><?php echo $position_name; ?></td>                
                <td align="center"><?php echo $published; ?></td>
            </tr>
                
         <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_home" />                        
    <input type="hidden" name="controller" value="home" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>