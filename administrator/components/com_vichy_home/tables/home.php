<?php
    defined('_JEXEC') or die;
     
    class TableHome extends JTable {
    	var $id = 0;
        var $position;        
        var $image;
        var $publish;
        var $published_date;     
     
        function __construct(&$db) {
        	parent::__construct('#__vichy_home', 'id', $db);
        }
    }
?>
