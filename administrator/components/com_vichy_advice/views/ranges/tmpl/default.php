<?php 
	defined('_JEXEC') or die;

	JToolBarHelper::title(JText::_( 'Add range for your need' ), 'generic.png');
	JToolBarHelper::addNewX();
	JToolBarHelper::deleteList();
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

    <table class="adminlist" cellspacing="1">   
        <tr>
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listStores); ?>);" /></th>
            <th align="center">Your Need</th>
            <th align="center">Range</th>           
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->listStores); $i < $n; $i++)
        {
            $row    =& $this->listStores[$i];
            $link   = 'index.php?option=com_vichy_advice&controller=ranges&task=edit&id='. $row->yourneed_id;
            $published  = JHTML::_('grid.published', $row, $i );
        ?>
            <tr class="<?php echo "row$k"; ?>">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>                
                <td align="center"><a href="<?php echo $link; ?>"><?php echo $row->yourneed; ?></a></td>
                <td align="center"><?php echo $row->range; ?></td>
            </tr>
                
        <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php //echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_advice" />                        
    <input type="hidden" name="controller" value="ranges" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>