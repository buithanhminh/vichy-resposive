<?php 
	defined('_JEXEC') or die;

	JToolBarHelper::title(JText::_( 'Advices' ), 'generic.png');
	JToolBarHelper::addNewX();
	JToolBarHelper::publish();
	JToolBarHelper::unpublish();
	JToolBarHelper::deleteList();

    $listCategories = Vichy_adviceHelper::getYourNeed();
    $new_cate = array();
    foreach ($listCategories as $v) {
        $pa = $v->parent_id;
        $new_cate[$pa][] = $v;
    }
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

    <table class="adminform">
        <tr>

            <td nowrap="nowrap">
                Lọc theo danh mục :
                <select id="filter_category" name="filter_category" class="inputbox" size="1" onchange="Joomla.submitform();">   
                    <?php if($_POST['filter_category'] == '0'){?>
                    <option value="0" selected="true">Mặc định</option>
                    <?php }else{?>
                    <option value="0">Mặc định</option>
                    <?php } ?>
                    <?php
                        foreach ($new_cate[10] as $k => $v) {
                            $select = ($_POST['filter_category'] == $v->id) ? 'selected = "selected"' : '';
                            if($v->id == '11'){
                                $value = '';
                                $select = '';
                            }else{
                                $value = $v->id;
                            }
                            echo '<option value="'.$value.'" '.$select.'>'.$v->child_title.'</option>';
                            if(isset($new_cate[$v->id])){
                                foreach ($new_cate[$v->id] as $k1 => $v1) {
                                    $select_c = ($_POST['filter_category'] == $v1->id) ? 'selected = "selected"' : '';
                                    echo '<option value="'.$v1->id.'" '.$select_c.'>--'.$v1->child_title.'</option>';
                                }
                            }
                        }
                    ?>
                </select>
            </td>
            <td nowrap="nowrap">
                Lọc theo loại :
                <select id="filter_type" name="filter_type" class="inputbox" size="1" onchange="Joomla.submitform();">

                    <?php if($_POST['filter_type'] == 'default'){?>
                    <option value="default" selected="true">Mặc định</option>
                    <?php }else{?>
                    <option value="default">Mặc định</option>
                    <?php } ?>

                    <?php if($_POST['filter_type'] == '1'){?>
                    <option value="1" selected="true">Câu hỏi</option>
                    <?php }else{?>
                    <option value="1" >Câu hỏi</option>
                    <?php } ?>

                    <?php if($_POST['filter_type'] == '0'){?>
                    <option value="0" selected="true">Câu trả lời</option>
                    <?php }else{?>
                    <option value="0">Câu trả lời</option>
                    <?php } ?>

                </select>
            </td>
            <td nowrap="nowrap">
                Lọc theo trạng thái :
                <select id="filter_state" name="filter_state" class="inputbox" size="1" onchange="Joomla.submitform();">

                    <?php if($_POST['filter_state'] == 'default'){?>
                    <option value="default" selected="true">Mặc định</option>
                    <?php }else{?>
                    <option value="default">Mặc định</option>
                    <?php } ?>

                    <?php if($_POST['filter_state'] == '1'){?>
                    <option value="1" selected="true">Xuất bản</option>
                    <?php }else{?>
                    <option value="1" >Xuất bản</option>
                    <?php } ?>

                    <?php if($_POST['filter_state'] == '0'){?>
                    <option value="0" selected="true">Không xuất bản</option>
                    <?php }else{?>
                    <option value="0">Không xuất bản</option>
                    <?php } ?>

                </select>
            </td>
        </tr>
    </table>
    <table class="adminlist" cellspacing="1">   
        <tr>
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listAdvices); ?>);" /></th>
            <th width="30" align="center">Id</th>
            <th align="center">Nội dung</th>
            <th align="center">Thể loại</th>
            <th align="center">Loại</th>
            <th align="center">Xuất bản</th>
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->listAdvices); $i < $n; $i++)
        {
            $row    =& $this->listAdvices[$i];
            $link   = 'index.php?option=com_vichy_advice&controller=advices&task=edit&cid[]='. $row->id;
            $published  = JHTML::_('grid.published', $row, $i );
        ?>
            <tr class="<?php echo "row$k"; ?>">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
                <td align="center"><?php echo $row->id; ?></td>  
                <td align="center"><a href="<?php echo $link; ?>"><?php echo $row->text; ?></a></td>
                <td align="center"><?php echo $row->title; ?></td>
                <td align="center"><?php echo ($row->is_question == 1) ? 'Câu hỏi' : 'Câu trả lời'; ?></td>
                <td align="center"><?php echo $published; ?></td>
            </tr>
                
        <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_advice" />                        
    <input type="hidden" name="controller" value="advices" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>