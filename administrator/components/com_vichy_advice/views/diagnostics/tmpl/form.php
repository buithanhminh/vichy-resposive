<?php
    defined('_JEXEC') or die;
    JHTML::_('behavior.tooltip');
    $doc = JFactory::getDocument();
    // $doc->addStyleSheet('//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css');

    JToolBarHelper::save();
    $db =& JFactory::getDBO();
    $cid = JRequest::getVar('cid',  0, '', 'array');
    $task = JRequest::getVar('task');
    JArrayHelper::toInteger($cid, array(0));
    $ciddef = $cid[0];
    if ($ciddef > 0){   
        
        $query = "SELECT * FROM #__vichy_qa WHERE id = ".$ciddef;
        $db->setQuery($query);
        $group = $db->loadObject();
        JFilterOutput::objectHTMLSafe($group, ENT_QUOTES, '' );
        
        JToolBarHelper::title(JText::_( 'Edit Diagnostic'), 'generic.png');
        JToolBarHelper::cancel( 'cancel', 'Close' );    
    }
    else{
        JToolBarHelper::title(JText::_( 'Add Diagnostic' ), 'generic.png');
        JToolBarHelper::cancel();
    
    }
    $sql = "Select * from vc_vichy_qa where type='diagnostic' and is_question = 1 and published = 1";
    $db->setQuery($sql);
    $listQuestion = $db->loadObjectList();
    $listLanguage = Vichy_adviceHelper::listLanguage();
    $listCategories = Vichy_adviceHelper::getCategories();
?>
<script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
    }
</script>
<fieldset>
    <legend>Advice details</legend>
    <form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
        <table>
            <tr>
                <td><strong>Ngôn ngữ :</strong></td>
                <td>
                    <?php $selected = (!empty($group->language)) ? $group->language : 'vi-VN'; ?>
                    <select name="language">
                    <option value="*">All</option>
                    <?php
                        echo JHtml::_('select.options', $listLanguage, 'lang_code', 'title_native', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><strong>Chọn danh mục :</strong></td>
                <td>
                    <?php $selected = (!empty($group->catid)) ? $group->catid : ''; ?>
                    <select name="catid">
                    <?php
                        echo JHtml::_('select.options', $listCategories, 'id', 'title', $selected); 
                    ?>
                    </select>
                </td>
            </tr>
            <?php if(!isset($group->is_question) || ($group->is_question == '1')){ ?>     
            <tr>
                <td><strong>Chọn loại</strong></td>
                <td>
                    <input type="radio" name="is_question" value="1" <?php echo ($group->is_question == '1') ? 'checked="checked"' : '' ?> />Câu hỏi
                </td>
            </tr>
            <?php } ?>
            <?php if(count($listQuestion)){ ?>
            <?php if(!isset($group->is_question) || ($group->is_question == '0')){ ?>
            <tr>
                <td></td>
                <td>
                    <input type="radio" name="is_question" value="0" <?php echo ($group->is_question == '0') ? 'checked="checked"' : '' ?> />Câu trả lời
                </td>
            </tr>
            <?php } ?>
            <tr class="choose_question">
                <td><strong>Chọn câu hỏi :</strong></td>
                <td>
                    <select name="parent_id">
                    <option value="0">Không có cấp cha</option>
                    <?php
                        foreach ($listQuestion as $key => $value) {
                            $selected_q = '';
                            if($group->parent_id == $value->id){
                                $selected_q = 'selected="selected"';
                            }
                            echo '<option value="'.$value->id.'"'. $selected_q.'>'.strip_tags($value->text).'</option>';
                        }
                    ?>
                    </select>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td><strong>Nội dung :</strong></td>
                <td>
                <?php           
                     echo @$this->editor->display( 'text',  $group->text, '550', '300', '75', '20', array() ) ; 
                 ?>
                </td>
            </tr>
        </table>
        
        <?php echo JHTML::_( 'form.token' ); ?>
        <input type="hidden" name="id" value="<?php if(!empty($group->id))  echo($group->id);else echo '';  ?>" />
        <input type="hidden" name="cid[]" value="<?php if(!empty($group->id))  echo($group->id);else echo ''; ?>" />
        <input type="hidden" name="option" value="com_vichy_advice" />
        <input type="hidden" name="controller" value="diagnostics" />
        <input type="hidden" name="task" value=""/>
    </form>
</fieldset>
<script type="text/javascript">
    // jQuery(document).ready(function(){
    //     jQuery('input[name="is_question"]').click(function(){
    //         if(jQuery(this).val() == '0'){
    //             jQuery('.choose_question').show();
    //         }else{
    //             jQuery('.choose_question').hide();
    //         }
    //     })
    // })
</script>