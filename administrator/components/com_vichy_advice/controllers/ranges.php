<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_adviceControllerRanges extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
        JRequest::setVar('view', 'Ranges');
        parent::display();
    }

    function edit(){
    	JRequest::setVar('view', 'ranges');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }

    function save()
    {
    	JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Ranges');
	    $data = JRequest::get( 'post' );
	    $id =  $data['id'];
	    if(empty($id)){
	    	if(empty($data['catid'])){
	    		$this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=ranges&layout=form');
                return false;
	    	}
	    	if(count($data['cid']) == 0){
	    		$this->setMessage(JText::_( 'Vui lòng chọn dong san pham'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=ranges&layout=form');
                return false;
	    	}
		}else{
			if(count($data['cid']) == 0){
	    		$this->setMessage(JText::_( 'Vui lòng chọn dong san pham'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=ranges&task=edit&id='.$id);
                return false;
	    	}
		}
	    

        $result=$model->store();

        if($result)
        {
        	$this->setMessage(JText::_( 'Thanh cong'));
        }else{
        	$this->setMessage(JText::_( 'error'),'error');
        }
        $this->setRedirect('index.php?option=com_vichy_advice&controller=ranges');
    }
}
?>