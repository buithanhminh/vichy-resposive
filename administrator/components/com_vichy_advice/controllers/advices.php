<?php
defined('_JEXEC') or die;
jimport('joomla.application.component.controller');

class Vichy_adviceControllerAdvices extends JController{
   
	function __construct(){
		parent::__construct();
		$this->registerTask('add', 'edit');
	}

	function display(){
		JRequest::setVar('view', 'Advices');
    	parent::display();
	}
	function edit(){
    	JRequest::setVar('view', 'Advices');
    	JRequest::setVar('layout', 'form');
    	JRequest::setVar('hidemainmenu', 1); 
    	parent::display();
    }
    function cancel(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect('index.php?option=com_vichy_advice' );
	}
	function save(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
	    $model = $this->getModel('Advices');
	    $cid = JRequest::getVar('cid',  0, '', 'array');
        JArrayHelper::toInteger($cid, array(0));
        $id = $cid[0];
        $data = JRequest::get( 'post' );
        if (empty($id)){
            if(!isset($data['is_question'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form');
                return false;
            }

            if(empty($data['text'])){
                $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form');
                return false;
            }

            if(empty($data['catid'])){
                $this->setMessage(JText::_( 'Thể loại không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form');
                return false;
            }
            
        }else{

            if(!isset($data['is_question'])){
                $this->setMessage(JText::_( 'Vui lòng chọn loại cho nội dung này'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form&task=edit&cid[]='.$id);
                return false;
            }
            
            if(empty($data['text'])){
                $this->setMessage(JText::_( 'Nội dung không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form&task=edit&cid[]='.$id);
                return false;
            }

            if(empty($data['catid'])){
                $this->setMessage(JText::_( 'Thể loại không được bỏ trống'),'error');
                $this->setRedirect('index.php?option=com_vichy_advice&controller=advices&layout=form&task=edit&cid[]='.$id);
                return false;
            }
        }

        if ($model->store()) {
            $msg = JText::_( 'Lưu thành công!' );
    	} else {
            $msg = JText::_( 'Lưu thất bại !' );
        }

    	$link = 'index.php?option=com_vichy_advice&controller=advices';
    	$this->setRedirect($link, $msg);
	}

    function publish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) PUBLISH' ) );
        } else {
    
            $model = $this->getModel('Advices');
    
            if(!$model->publish($cid, 1)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) PUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
    
        $this->setRedirect( 'index.php?option=com_vichy_advice&controller=advices', $msg );
    }
        
    function unpublish(){

        JRequest::checkToken() or jexit( 'Invalid Token' );
            
        $cid    = JRequest::getVar( 'cid', array(0), 'post', 'array' );
    
        if (!is_array( $cid ) || count( $cid ) < 1) {
            $msg = '';
            JError::raiseWarning(500, JText::_( 'SELECT ITEM(S) UNPUBLISH' ) );
        } else {
    
            $model = $this->getModel('Advices');
    
            if(!$model->publish($cid, 0)) {
                JError::raiseError(500, $model->getError());
            }
    
            $msg    = JText::_( 'ITEM(S) UNPUBLISHED');
            
            $cache = &JFactory::getCache('com_vichy_advice');
            $cache->clean();
        }
            
        $this->setRedirect('index.php?option=com_vichy_advice&controller=advices', $msg);
    }

    function remove(){
        JRequest::checkToken() or jexit( 'Invalid Token' );
        $model = $this->getModel('Advices');
        
        if(!$model->deleteRows()) {
            $msg = JText::_( 'Lỗi : Một hoặc nhiều câu hỏi (trả lời) không thể xoá');
        } else {
            $msg = JText::_( 'Câu hỏi (trả lời) này đã được xoá !' );
        }
        
        $this->setRedirect('index.php?option=com_vichy_advice&controller=advices', $msg );
    }

    function listAnswerByCat(){
        $catid = JRequest::getVar( 'catid');
        $id = JRequest::getVar( 'id');
        $db = & JFactory::getDBO();
        $sql = "SELECT * from vc_vichy_qa where type='advice' and is_question = 1 and catid = $catid and published = 1";
        $db->setQuery($sql);
        $listQuestion = $db->loadObjectList();
        $data = '';
        $count = 0;
        if($listQuestion){
            foreach ($listQuestion as $key => $value) {
                $count++;
                $selected_q = '';
                if($id == $value->id){
                    $selected_q = 'selected="selected"';
                }
                $data .= '<option value="'.$value->id.'"'. $selected_q.'>'.$count.'. '.strip_tags($value->text).'</option>';
            }
        }else{
            $data .= '<option value="">Thể loại này không có câu hỏi</option>';
        }
        echo $data;
        exit();
    }
}