<?php 
	defined('_JEXEC') or die;

	JToolBarHelper::title(JText::_( 'Spa' ), 'generic.png');
	JToolBarHelper::addNewX();
	JToolBarHelper::publish();
	JToolBarHelper::unpublish();
	JToolBarHelper::deleteList();
  
?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">

    <table class="adminlist" cellspacing="1">   
        <tr>
            <th width="30" align="center"><input type="checkbox" name="toggle" value=""  onclick="checkAll(<?php echo count($this->listStores); ?>);" /></th>
            <th width="30" align="center">Id</th>
            <th align="center">Tên cửa hàng</th>
            <th align="center">Hình ảnh</th>
            <th align="center">Địa chỉ</th>
            <th align="center">Điện thoại</th>
            <th align="center">Email</th>
            <th align="center">Xuất bản</th>
            <th align="center">Ngôn ngữ</th>
        </tr>
        <tbody>
        <?php 
        $k = 0;
        for ($i=0, $n=count($this->listStores); $i < $n; $i++)
        {
            $row    =& $this->listStores[$i];
            $link   = 'index.php?option=com_vichy_store&controller=spa&task=edit&cid[]='. $row->id;
            $published  = JHTML::_('grid.published', $row, $i );
        ?>
            <tr class="<?php echo "row$k"; ?>">
                <td width="30"><?php echo JHTML::_('grid.id', $i, $row->id); ?></td>
                <td align="center"><?php echo $row->id; ?></td>  
                <td align="center"><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($row->name); ?></a></td>
                <td align="center"><img width="100" src="<?php echo JURI::root() ?>components/com_vichy_store/uploads/spa/<?php echo $row->image; ?>" /></td>
                <td align="center"><?php echo $row->address; ?></td>
                <td align="center"><?php echo $row->phone; ?></td>
                <td align="center"><?php echo $row->email; ?></td>
                <td align="center"><?php echo $published; ?></td>
                <td align="center"><?php echo $row->language; ?></td>
            </tr>
                
        <?php $k = 1 - $k;}?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="15"><?php echo $this->Pagination->getListFooter();?></td>
            </tr>
        </tfoot>
    </table>
    <?php echo JHTML::_( 'form.token' ); ?>                 
    <input type="hidden" name="view" value="" />                            
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="option" value="com_vichy_store" />                        
    <input type="hidden" name="controller" value="spa" />
    <input type="hidden" name="boxchecked" value="0" /> 
</form>