<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_search
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$document = & JFactory::getDocument();
$document->addStyleSheet('modules/mod_new_letter/css/style.css');
$cat_error = $_GET['cat_error'];
$common_error = $_GET['common_error'];
$success = $_GET['subscribe'];

$curLanguage = JFactory::getLanguage();
$lang = $curLanguage->getTag();

$db = & JFactory::getDbo();

if($lang == 'vi-VN'){
	$query = "Select sg.catid, sg.subcribe_group, c.title from vc_vichy_subcribe_group as sg join vc_categories as c on sg.catid = c.id where sg.status = 1";
}else{
	$query = "Select sg.catid, sg.subcribe_group, c.title from vc_vichy_subcribe_group as sg join
			(select title, reference_id as id from vc_categories as cate inner join vc_jf_translationmap as tm on cate.id = tm.translation_id and reference_table='categories') as c on sg.catid = c.id where sg.status=1";
}

$db->setQuery($query);
$group_name = $db->loadObjectList();

if($lang == 'vi-VN'){
	$title_newsletter = "Đăng Ký nhận e-mail Newsletter";
	$mess_success = "Chúc mừng bạn đã đăng kí thành công! Vichy sẽ gửi đến bạn email về những vấn đề bạn quan tâm.";
	$error_register = "*E-mail nhập vào không đúng đinh dạng";
	$mess_remind = "*Bạn chưa chọn nhu cầu nào";
	$alert_error = "*Có lỗi xảy ra";
	$mess_input_email = "Nhập địa chỉ e-mail";
	$mess_check_yourneed = "Để nhận được email theo đúng nhu cầu da của bạn, vui lòng đánh dấu những nhu cầu mà bạn quan tâm:";

	$lbl_submit = "Nhận E-mail";
}else{
	$title_newsletter = "Sign up to receive e-mail Newsletter";
	$mess_success = "Congratulations, you have successfully registered! Vichy email will be sent to you on issues you care about.";
	$error_register = "*E-mail entered incorrect formatting";
	$mess_remind = "*You have not selected any demand";
	$alert_error = "*An error has occurred";
	$mess_input_email = "Enter your e-mail address";
	$mess_check_yourneed = "To receive e-mail in accordance with need of your skin, please check the need that interest you:";

	$lbl_submit = "Receive E-mail";
}
?>
<div class="newsletter" style="z-index:99999 !important;"></div>

<div class="messagepop_newletter pop_newletter" style="<?php if($cat_error || $common_error || $success) echo 'display:block;'; else echo 'display:none;';?>">
		<div class="close_newsletter"></div>
		<div class="header_registerEmail"><?php echo $title_newsletter; ?></div>
    	<div class="registerEmail">
    	<?php if($success){ ?>
    		<div class="label_email"><?php echo $mess_success; ?></div>
    	<?php }else{ ?>
    	<form method="POST" action="<?php echo JROUTE::_('index.php?option=com_vichy_new_letter&task=registerEmail'); ?>">
			<p style="color:red;display:none;" class="validate-error" ><?php echo $error_register; ?></p>
			<?php
				if($cat_error){
					echo '<p style="color:red;">'.$mess_remind.'</p>';
				}
				if($common_error){
					echo '<p style="color:red;">'.$alert_error.'</p>';
				}
			?>
			<div class="label_email"><?php echo $mess_input_email; ?></div>
	    	<input type="text" size="20" name="email" id="email-newletter" class="email" placeholder="E-mail" /><br />
	    	<div class="label_group" style="margin-bottom:10px;"><?php echo $mess_check_yourneed; ?></div>
	    	<?php
			foreach($group_name as $v){
				echo '<input type="checkbox" name="catid[]" value="'.$v->subcribe_group.'" />'.$v->title.'<br />';
			}
			?>
			<input type="hidden" name="grouping_id" value="<?php echo $groups[0]['name']; ?>" />
			<input type="hidden" name="return_fail" value="<?php echo $_SERVER['QUERY_STRING']; ?>" />
	    	<input type="submit" value="<?php echo $lbl_submit; ?>" name="commit" id="newletter_message_submit" class="message_submit"/>
	    	<img class="loading_newsletter" src="<?php echo JURI::root() ?>modules/mod_new_letter/images/9.gif" />
    	</form>
    	<?php } ?>
    	</div>
		<script type="text/javascript" src="modules/mod_new_letter/js/script.js"></script>
    	<div id="com-vichy-newletter-url" data-url="<?php echo JURI::root() ?>"></div>

</div>



