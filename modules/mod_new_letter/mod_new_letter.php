<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_search
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

require_once JPATH_ROOT.'/MailChimp.php';
$mailchimp = new Drewm\MailChimp(MAILCHIMP_KEY);
$lists = $mailchimp->call('lists/list',array(
	'sort_dir'   => 'ASC'
)) ;
$listid = $lists['data'][ELEMENT_MC]['id'];
$groups = $mailchimp->call('lists/interest-groupings',array(
		'id' => $listid,
		'counts'=> 'false'
	)) ;
require JModuleHelper::getLayoutPath('mod_new_letter', $params->get('layout', 'default'));
