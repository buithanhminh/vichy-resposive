<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.keepalive');
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_login/css/style.css');
?>
<script type="text/javascript">

	jQuery(document).ready(function(){
		jQuery(".myskin").click(function(){
			jQuery(".popup-login").show();
		});
	});

    jQuery(document).mouseup(function(e){
        var container = jQuery(".popup-login");

        if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        container.hide();
	    }
    });
</script>
<div class="login">
	<?php
		$user = & JFactory::getUser();
		$user_id = $user->id; 
	?>
	<?php if($user_id) {?>
		<span class="in"><?php echo shortDesc($user->name,12); ?></span> / <span class="myskin">
			<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
				<input type="submit" name="Submit" class="button" id="button-logout" value="<?php echo JText::_('JLOGOUT'); ?>" />
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.logout" />
				<input type="hidden" name="return" value="<?php echo $return; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</span>
	<?php }else{?>
		<a href="<?php echo JROUTE::_('index.php?option=com_users&view=registration'); ?>" class="in">Đăng kí</a> / <span class="myskin">Đăng nhập</span>
		<div class="popup-login">
			<div class="login-form">
				<h1>ĐĂNG NHẬP</h1>
				<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >
					<?php if ($params->get('pretext')): ?>
					<div class="pretext">
						<p><?php echo $params->get('pretext'); ?></p>
					</div>
					<?php endif; ?>
					<p id="form-login-username">
						<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" />
					</p>
					<p id="form-login-password">
						<input id="modlgn-passwd" type="password"  name="password" class="inputbox" size="18"  />
					</p>
					
					<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
					<p id="form-login-remember">
						<input id="modlgn-remember" type="checkbox" name="remember" class="checkbox" value="yes"/>
						<label class="modlgn-remember" for="modlgn-remember"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?></label>
						<a class="forgot-password" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
							<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
					</p>
					<?php endif; ?>
					<p class="form-connect-facebook">
						Or connect with Facebook</br>
						<img src="<?php echo JURI::root();?>modules/mod_login/images/facebook-connect.png">
					</p>
					<input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN') ?>" />
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo $return; ?>" />
					<?php echo JHtml::_('form.token'); ?>

					<?php if ($params->get('posttext')): ?>
						<div class="posttext">
						<p><?php echo $params->get('posttext'); ?></p>
						</div>
					<?php endif; ?>
				</form>
			</div>
		</div>
	<?php } ?>								
</div>

