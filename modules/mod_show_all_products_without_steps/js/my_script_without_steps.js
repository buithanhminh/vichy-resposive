jQuery(document).ready(function() {
	base_url = jQuery('#com-vichy-product-url_without_steps').attr('data-url');	
	$(".list_products_without_steps").each(function(){
		layout = jQuery(this).find("#com-vichy-product-url_without_steps").attr('data-layout');	
		cl = jQuery(this).find("#com-vichy-product-url_without_steps").attr('data-class');
		paging_name=jQuery(this).find("#com-vichy-product-url_without_steps").attr('data-paging-name');
		
		pagination(1,layout,cl,paging_name);  		
		
	});

jQuery(".class_layout_product_new_without_steps").on('click',".paging_new_without_steps >a",function(){
	   		page = jQuery(this).attr('page');
	    	pagination(page,0,'class_layout_product_new_without_steps','paging_new_without_steps');	    	
	    	
		}); 
jQuery(".class_layout_product_favorite_without_steps").on('click',".paging_favorite_without_steps >a",function(){
	   		page = jQuery(this).attr('page');
	    	pagination(page,1,'class_layout_product_favorite_without_steps','paging_favorite_without_steps');	    	
	    	
		});

	function pagination(page,layout,cl,paging_name){
	    // jQuery('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');                 
	    jQuery.ajax ({
	        type: "POST",
	        url: base_url,
	        data: "page="+page+'&layout='+layout+'&paging_name='+paging_name,
	        success: function(data_page) { 
	            // jQuery('#loading').fadeOut('fast');
	      
	            jQuery("."+cl).html(data_page);  
	        }
	    });
	}
});