<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_archive
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$layout = $params->get('tmp_layout',0);
// 	if(strpos($layout, '.php')>0){
// 		$layout = substr($layout, 0, -4);
// 	}

//if($layout==0){
//		$list_products = modShowProductWithoutStepsHelper::getNewProducts($params);
//	}else if($layout == 1){
//		$list_products = modShowProductWithoutStepsHelper::getFavoriteProducts($params);
//	}

$params->def('count', 10);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));


// $list = modArchiveHelper::getList($params);
require JModuleHelper::getLayoutPath('mod_show_all_products_without_steps', 'list_products');

