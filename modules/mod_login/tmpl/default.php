<?php
	defined('_JEXEC') or die;
	JHtml::_('behavior.keepalive');
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_login/css/style.css');
	$checkLgf = $_GET['login_fail'];
	
	$return_fail = substr($_SERVER['REDIRECT_URL'],1);
	$return_success = $return_fail;
	$checkLogin = $_GET['not_login'];
	$confirm = $_GET['confirm_pass'];

	$user = & JFactory::getUser();
	$user_id = $user->id;
    $user_id = 74;

$curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
    if($language_tag == 'vi-VN'){
        $language = "'*','".$language_tag."'";
    }else{
        $language = "'".$language_tag."'";            
    }
    if($language_tag == 'vi-VN'){
    	$lbl_login = "Đăng nhập";
    	$lbl_login_fail = "Sai tên đăng nhập hoặc mật khẩu";
    	$lbl_register = "Đăng kí";
    	$lbl_login_fail_pass = "Mật khẩu của bạn đã được đặt lại";
    	$lbl_facebook = "Hoặc đăng nhập bằng Facebook";
    }else{
    	$lbl_login = "Login";
    	$lbl_login_fail = "Incorrect username or password";
    	$lbl_register = "Sign up";
    	$lbl_login_fail_pass = "Your password has been reset";
    	$lbl_facebook = "Or log in with Facebook";
    }
?>
<script type="text/javascript">

    if(window.innerWidth>992) {
        jQuery(document).ready(function () {

            jQuery(".myskin").click(function () {
                jQuery(document).prop('title', 'Vichy | <?php echo $lbl_login; ?>');
                if (jQuery(".popup-login").is(':visible'))
                    jQuery(".popup-login").hide();
                else {
                    jQuery('.login').show();
                    jQuery(".popup-login").show();
                }
            });

        });

        jQuery('.my_skin').hover(function (e) {
            jQuery('.login').show();

        }, function (e) {
            if (!jQuery(".popup-login").is(':visible'))
                jQuery('.login').hide();
        });

        jQuery(document).mouseup(function (e) {
            var container = jQuery(".popup-login");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                if (jQuery('.login').is(':visible'))
                    jQuery('.login').hide();
                container.hide();
            }
        });
    }
</script>
<div class="login" style="<?php if($checkLgf || $checkLogin || $confirm) echo 'display:block;'; else echo 'display:none;';?>">
	<?php
		$user = & JFactory::getUser();
		$user_id = $user->id; 

		$curLanguage = JFactory::getLanguage();
	    $language_tag = $curLanguage->getTag();
	    if($language_tag == 'vi-VN'){
	        $language = "'*','".$language_tag."'";
	    }else{
	        $language = "'".$language_tag."'";            
	    }
	    if($language_tag == 'vi-VN'){
	    	$lbl_login = "Đăng nhập";
	    	$lbl_login_fail = "Sai tên đăng nhập hoặc mật khẩu";
	    	$lbl_register = "Đăng kí";
	    	$lbl_login_fail_pass = "Mật khẩu của bạn đã được đặt lại";
	    	$lbl_facebook = "Hoặc đăng nhập bằng Facebook";
	    }else{
	    	$lbl_login = "Login";
	    	$lbl_login_fail = "Incorrect username or password";
	    	$lbl_register = "Sign up";
	    	$lbl_login_fail_pass = "Your password has been reset";
	    	$lbl_facebook = "Or log in with Facebook";
	    }

	?>
	<?php if($user_id) {?>
		<a class="in" href="<?php echo JROUTE::_('index.php?option=com_users&view=profile'); ?>"><?php echo shortDesc($user->name,12); ?></a> / <span class="myskin">
			<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
				<input type="submit" name="Submit" class="button" id="button-logout" value="<?php echo JText::_('JLOGOUT'); ?>" />
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="user.logout" />
				<input type="hidden" name="return" value="<?php echo $return; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</span>
	<?php }else{?>
		<a href="<?php echo JROUTE::_('index.php?option=com_users&view=registration'); ?>" class="in"><?php echo $lbl_register; ?></a> / <span class="myskin"><?php echo $lbl_login; ?></span>
		<div class="popup-login" style="<?php if($checkLgf || $checkLogin || $confirm) echo 'display:block;'; else echo 'display:none;';?>">
			<div class="login-form">
				<a href="<?php echo JROUTE::_('index.php?option=com_users&amp;lang=vi');?>"><h1 ><?php echo $lbl_login; ?></h1></a>
				<?php if($checkLgf) { ?>
				<div class="messege_login_fail"><?php echo $lbl_login_fail; ?></div>
				<?php } ?>
				<?php if($confirm) { ?>
				<div class="messege_login_fail"><?php echo $lbl_login_fail_pass; ?></div>
				<?php } ?>
				<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >
					<?php if ($params->get('pretext')): ?>
					<div class="pretext">
						<p><?php echo $params->get('pretext'); ?></p>
					</div>
					<?php endif; ?>
					<p id="form-login-username">
						<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" />
					</p>
					<p id="form-login-password">
						<input id="modlgn-passwd" type="password"  name="password" class="inputbox" size="18"  />
					</p>
					
					<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
					<p id="form-login-remember">
						<input id="modlgn-remember" type="checkbox" name="remember" class="checkbox" value="yes"/>
						<label class="modlgn-remember" for="modlgn-remember"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?></label>
						<a class="forgot-password" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
							<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
					</p>
					<?php endif; ?>
					<p class="form-connect-facebook">
						<?php echo $lbl_facebook; ?></br>
						<a href="<?php echo JRoute::_('index.php?option=com_users&task=registration.login_facebook') ;?>">
						<img src="<?php echo JURI::root();?>modules/mod_login/images/facebook-connect.png">
						</a>
					</p>
					<input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN') ?>" />
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo $return; ?>" />
					<input type="hidden" name="return_success" value="<?php echo $return_success; ?>" />
					<input type="hidden" name="return_fail" value="<?php echo $return_fail; ?>" />
					<?php echo JHtml::_('form.token'); ?>

					<?php if ($params->get('posttext')): ?>
						<div class="posttext">
						<p><?php echo $params->get('posttext'); ?></p>
						</div>
					<?php endif; ?>
				</form>
			</div>
		</div>
	<?php } ?>								
</div>

