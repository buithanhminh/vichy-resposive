<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_articles_archive
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modShowProductHelper
{
	function getNewProducts()
    {
//       echo '<nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">
//            <div class="container">
//                <div class="navbar-header sub-menu-spa-header">
//                    <a class="navbar-brand" href="#" style="color:white;">Sản phẩm</a>
//                    <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
//                    </button>
//                </div>
//                <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1">
//                    <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">
//                        <!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
//                        <!--                    <li><a href="#">Link</a></li>-->
//                        <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a"  href="'. JURI::root().'index.php?option=com_vichy_product&view=product_landing&Itemid=103">Sản phẩm mới nhất
//                            </a></li>
//                        <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a"  href="'. JURI::root().'index.php?option=com_vichy_product&view=product_landing&Itemid=103">Sản phẩm yêu thích
//                            </a></li>
//                        <li  class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">
//                            <a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nhu cầu của bạn <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="'. JURI::root().'components/com_vichy_product/images/btn-plus.png"></a>
//                            <ul class="dropdown-menu">
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=62&Itemid=103">Dưỡng sắng da và trị thâm, nám</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=63&Itemid=103">Giảm dầu,giảm mụn, se khít lỗ chân lông</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=64&Itemid=103">Chống lão hóa và làm săn chắc da</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=65&Itemid=103">Chống nắng</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=61&Itemid=103">Làm sạch và dưỡng ẩm</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=56&Itemid=103">Chăm sóc cơ thể</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=58&Itemid=103">Chăm sóc tóc và da đầu</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=59&Itemid=103">Trang điểm</a></li>
//                            </ul>
//                        </li>
//                        <li  class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">
//                            <a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Chức năng sản phầm <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="'. JURI::root().'components/com_vichy_product/images/btn-plus.png"></a>
//                            <ul class="dropdown-menu">
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=15&Itemid=103">Sữa rửa mặt</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=16&Itemid=103">Nước cân bằng</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=50&Itemid=103">Tẩy tế bào chết</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=51&Itemid=103">Nước khoáng dưỡng da</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=52&Itemid=103">Tính chất đặc trị</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=53&Itemid=103">Kem dưỡng</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=54&Itemid=103">Chống nắng</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_yourneed&yid=55&Itemid=103">Mặt nạ</a></li>
//                            </ul>
//                        </li>
//                        <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;">
//                            <a  href="#" class="sub-menu-a dropdown-toggle sub-menu-a-margin dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Các dòng sản phẩm <img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="'. JURI::root().'components/com_vichy_product/images/btn-plus.png"></a>
//                            <ul class="dropdown-menu">
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=9&Itemid=103">NORMADERM</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=33&Itemid=103">BI-WHITE</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=36&Itemid=103">AQUALIA THERMAL</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=34&Itemid=103">LIFTACTIV</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=47&Itemid=103">UV PRO SECURE</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=172&Itemid=103">IDEALIA</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=44&no_step=1&Itemid=103">DERCOS</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=46&no_step=1&Itemid=103">BODYCARE</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&amp;view=product&amp;rid=48&amp;id=57&amp;Itemid=103&amp;no_step=1&amp">AERAMINERAL</a></li>
//                                <li class="bg-li-menu-mobile1"><a  class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=13&no_step=1&Itemid=103">CAPITAL SOLEIL</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&view=product_range&rid=35&no_step=1&Itemid=103">PURETE THERMAL</a></li>
//                                <li class="bg-li-menu-mobile1"><a class="sub-menu-a sub-menu-a-margin1" style="color:#fff !important" href="'. JURI::root().'index.php?option=com_vichy_product&amp;view=product&amp;rid=21&amp;id=16&amp;Itemid=103&amp;no_step=1&amp">THERMAL SPA WATER</a></li>
//                            </ul>
//                        </li>
//                        <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a"  href="'. JURI::root().'index.php?option=com_vichy_product&view=product_landing&Itemid=103">Tất cả các dòng sản phẩm
//                            </a></li>
//                    </ul>
//                </div><!-- /.navbar-collapse -->
//            </div>
//        </nav>';
        $db = & JFactory::getDbo();
        
        // $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
        //                     Group_Concat(tb1.skincare) as skincare,
        //                     Group_Concat(tb1.product_range) as product_range
        //                 from  (
        //                         select p.*,c.main_color,
        //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.id end as cid,
        //                             case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
        //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
        //                         from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

        //                         where p.id = pc.product_id 
        //                             and c.id = pc.category_id
        //                             and p.is_new=1
        //                         Group by p.id,c.parent_id
        //                        ) as tb1 
        //                      group by tb1.id
        //                      limit 0,5"
        //                 ;
        
        $curLanguage = JFactory::getLanguage();
        $language_tag = $curLanguage->getTag();
        if($language_tag == 'vi-VN'){
            $language = "'*',".$db->quote($language_tag);            
        }else{
            $language = $db->quote($language_tag);
        }
        $query_new="SELECT  a.description,a.id AS pid,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS cid, GROUP_CONCAT( DISTINCT (
                    c.parent_id
                    ) ) AS parent_id, GROUP_CONCAT( c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color 
                    FROM vc_vichy_product AS a
                    INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
                    INNER JOIN vc_categories AS c ON b.category_id = c.id
                    AND a.is_new = 1
                    AND a.published = 1
                    AND a.language IN ($language)
                    GROUP BY a.id
                    limit 0,5";
    
        $db->setQuery($query_new);
        $list_data = $db->loadObjectList();
        
        return $list_data;
    }

	function getFavoriteProducts($start,$display)
    {
        $db = & JFactory::getDbo();
        
        // $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_favorite,tb1.rating,tb1.reviews,
        //                     Group_Concat(tb1.skincare) as skincare,
        //                     Group_Concat(tb1.product_range) as product_range
        //                 from  (
        //                         select p.*,c.main_color,c.id as cid,
        //                             case when c.parent_id =".SKIN_CARE." then Group_Concat(c.title) end as skincare,
        //                             case when c.parent_id in(".PRODUCT_RANG.','.PRODUCT_RANG_WITHOUT_STEP.") then c.title end as product_range
        //                         from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

        //                         where p.id = pc.product_id 
        //                             and c.id = pc.category_id
        //                             and p.is_favorite=1
        //                         Group by p.id,c.parent_id
        //                        ) as tb1 
        //                      group by tb1.id
        //                      limit 0,5"
        //                 ;
        
        $curLanguage = JFactory::getLanguage();
        $language_tag = $curLanguage->getTag();
        if($language_tag == 'vi-VN'){
            $language = "'*',".$db->quote($language_tag);            
        }else{
            $language = $db->quote($language_tag);            
        }
          $query_new="SELECT  a.description,a.id AS pid,a.is_new,a.is_favorite, a.name AS product_name, a.create_date AS created_product, a.rating, a.reviews, a.image, GROUP_CONCAT( c.parent_id, '|', b.category_id ) AS cid, GROUP_CONCAT( DISTINCT (
                    c.parent_id
                    ) ) AS parent_id, GROUP_CONCAT(c.parent_id,  '|', c.title ) AS product_range, GROUP_CONCAT( DISTINCT(c.main_color) ) as main_color 
                    FROM vc_vichy_product AS a
                    INNER JOIN vc_vichy_product_category AS b ON b.product_id = a.id
                    INNER JOIN vc_categories AS c ON b.category_id = c.id                    
                    AND a.is_favorite = 1
                    AND a.published = 1
                    AND a.language IN ($language)
                    GROUP BY a.id
                    limit 0,5";

        $db->setQuery($query_new);
        $list_data = $db->loadObjectList();
        
        return $list_data;
    }
    
    function countTotal($featured){

            $db = & JFactory::getDbo();

            $curLanguage = JFactory::getLanguage();
            $language_tag = $curLanguage->getTag();
            if($language_tag == 'vi-VN'){
                $language = "'*',".$db->quote($language_tag);
            }else{
                $language = $db->quote($language_tag);
            }

            $query= "SELECT COUNT(p.id) as count from vc_vichy_product p where p.$featured =1 and p.language in($language)";

            $db->setQuery($query);
            $result = $db->loadObject();
           
            return $result->count;            

        }
}
