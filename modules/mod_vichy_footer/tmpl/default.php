<?php 
	defined('_JEXEC') or die;
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_vichy_footer/css/style.css');
	$arr = array();
	foreach ($list_cate_yourneed as $v) {
		$pa=$v->parent_id;
		$arr[$pa][] = $v;
	}

	$lang_code = $arr_option['lang_code'];
	if($lang_code == 'vi-VN'){
		$lbl_skin_needs = "NHU CẦU CỦA BẠN";
		$lbl_skin_care = "CHỨC NĂNG SẢN PHẨM";
		$lbl_vichy_ranges = "CÁC DÒNG SẢN PHẨM";
		$lbl_all_products = "TẤT CẢ DÒNG SẢN PHẨM";
		$title_all_products = "TẤT CẢ DÒNG SẢN PHẨM";
	}else{
		$lbl_skin_needs = "SKIN NEEDS";
		$lbl_skin_care = "SKIN CARE";
		$lbl_vichy_ranges = "VICHY RANGES";
		$lbl_all_products = "ALL PRODUCTS";
		$title_all_products = "SEE ALL VICHY PRODUCTS";
	}
?>



<div class="footer_request">
	<h4><?php echo $lbl_skin_needs; ?></h4>
	<div class="footer_request_left">
		<?php foreach($arr[$arr_option['your_need_id']] as $v){ ?>
			<?php if(isset($arr[$v->id])){ ?>
			<div class="sub_1"><a class="sub_1_a" href="#"><?php echo $v->child_title; ?></a></div>
			<?php foreach($arr[$v->id] as $v1){ ?>
			<div class="sub_2"><a class="sub_2_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v1->id.'&Itemid=103'); ?>"><?php echo $v1->child_title; ?></a></div>
		<?php }}} ?>
	</div>
	<div class="footer_request_right">
		<?php foreach($arr[$arr_option['your_need_id']] as $v){ ?>
			<?php if(!isset($arr[$v->id])){ ?>
			<div class="sub_1"><a class="sub_1_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v->id.'&Itemid=103'); ?>"><?php echo $v->child_title; ?></a></div>
			
		<?php }}?>
	</div>
	<div class="clear"></div>
</div>

<div class="footer_item">
	<h4><?php echo $lbl_skin_care; ?></h4>
	<div class="footer_item_sub">
		
			<?php 
			$i=0;
			foreach ($list_cate as $k=>$v) { 
				if($v->parent_id == $arr_option['skin_care_id']){
					$i++;
					if($i == 1){ 
						echo '<div class="sub_main_left">';
					}
			?>

					<div class="sub_main">
						<a class="sub_main_a" title="<?php echo $v->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_yourneed&yid='.$v->id.'&Itemid=103'); ?>"><?php echo $v->title; ?></a>
					</div>
			<?php 
					if($i == 4){ 
						echo '</div>'; 
						$i=0;
					}
				}
			}?>
		</div>
		
		<div class="clear"></div>				
	</div>
</div>

<div class="footer_item" style="margin-right:0px;">
	<h4><?php echo $lbl_vichy_ranges; ?></h4>
	<div class="footer_item_sub">
	
			<?php 
			$i=0;
			foreach ($list_cate as $k=>$v) {
				if($v->parent_id == $arr_option['product_rang_id'] || $v->parent_id == $arr_option['product_rang_without_step_id']){
					$i++;
					if($i == 1){ 
						echo '<div class="sub_main_left">';
					}
			?>

			<div class="sub_main">
				<a class="sub_main_a uppercase" title="<?php echo $v->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id.'&Itemid=103'); ?>"><?php echo $v->title; ?></a>
			</div>
			<?php 
					if($i == 5){ 
						echo '</div>'; 
						$i=0;
					}
				}
			}?>
			<div class="sub_main">
				<a class="sub_main_a" title="<?php echo $title_all_products; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing&Itemid=103'); ?>"><?php echo $lbl_all_products; ?></a>
			</div>
</div>
		
	<div class="clear"></div>				
</div>
</div>

<div class="clear"></div>