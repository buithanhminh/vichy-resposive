<?php 
	defined('_JEXEC') or die;
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_vichy_footer/css/style.css');
?>
<div class="footer_request">
	<h4>Nhu cầu của bạn</h4>
	<div class="footer_request_left">
		<div class="sub_1"><a class="sub_1_a">Chăm sóc da mặt</a></div>
		<div class="sub_2"><a class="sub_2_a">Làm sạch và dưỡng ẩm</a></div>
		<div class="sub_2"><a class="sub_2_a">Dưỡng sáng da và trị thâm</a></div>
		<div class="sub_2"><a class="sub_2_a">Chống nắng</a></div>
	</div>
	<div class="footer_request_right">
		<div class="sub_1"><a class="sub_1_a">Chăm sóc cơ thể</a></div>
		<div class="sub_1"><a class="sub_1_a">Chăm sóc sức khoẻ tóc và da đầu</a></div>
		<div class="sub_1"><a class="sub_1_a">Trang điểm</a></div>
	</div>
	<div class="clear"></div>
	<!--<ul>
		<?php foreach ($list_cate as $v) {?>
			<?php if($v->parent_id == YOUR_NEED){?>
				<li><a title="<?php echo $v->title; ?>" href="#"><?php echo $v->title; ?></a></li>
			<?php } ?>
		<?php } ?>
	</ul>-->
</div>

<div class="footer_item">
	<h4>CÁC DÒNG SẢN PHẨM</h4>
	<div class="footer_item_sub">
	<?php foreach ($list_cate as $r) {?>
		<?php if($r->parent_id == PRODUCT_RANG){?>
			<div class="sub_main">
				<a class="sub_main_a" title="<?php echo $r->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&Itemid=103'); ?>"><?php echo $r->title; ?></a>
			</div>
		<?php }?>
	<?php } ?>
	</div>
</div>

<div class="footer_item" style="margin-right:0px;">
	<h4>CÁC DÒNG SẢN PHẨM</h4>
	<div class="footer_item_sub">
	<?php foreach ($list_cate as $r) {?>
		<?php if($r->parent_id == PRODUCT_RANG){?>
			<div class="sub_main">
				<a class="sub_main_a" title="<?php echo $r->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&Itemid=103'); ?>"><?php echo $r->title; ?></a>
			</div>
			<?php } ?>
		<?php } ?>
</div>
</div>
<div class="clear"></div>