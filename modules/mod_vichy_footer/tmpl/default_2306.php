<?php 
	defined('_JEXEC') or die;
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_vichy_footer/css/style.css');
?>
<div class="footer_request">
	<h4>Nhu cầu của bạn</h4>
	<div class="footer_request_left">
		<div class="sub_1"><a class="sub_1_a">Chăm sóc da mặt</a></div>
		<div class="sub_2"><a class="sub_2_a">Làm sạch và dưỡng ẩm</a></div>
		<div class="sub_2"><a class="sub_2_a">Dưỡng sáng da và trị thâm, nám</a></div>
		<div class="sub_2"><a class="sub_2_a">Giảm dầu,giảm mụn,se khit lỗ chân lông</a></div>
		<div class="sub_2"><a class="sub_2_a">Chống lão hóa và làm săn chắc da</a></div>
		<div class="sub_2"><a class="sub_2_a">Chống nắng</a></div>
	</div>
	<div class="footer_request_right">
		<div class="sub_1"><a class="sub_1_a">Chăm sóc cơ thể</a></div>
		<div class="sub_1"><a class="sub_1_a">Chăm sóc sức khoẻ tóc và da đầu</a></div>
		<div class="sub_1"><a class="sub_1_a">Trang điểm</a></div>
	</div>
	<div class="clear"></div>
	<!--<ul>
		<?php foreach ($list_cate as $v) {?>
			<?php if($v->parent_id == YOUR_NEED){?>
				<li><a title="<?php echo $v->title; ?>" href="#"><?php echo $v->title; ?></a></li>
			<?php } ?>
		<?php } ?>
	</ul>-->
</div>

<div class="footer_item">
	<h4>CHỨC NĂNG SẢN PHẨM</h4>
	<div class="footer_item_sub">
		<div class="sub_main_left">
			<div class="sub_main"><a class="sub_main_a">Sữa rửa mặt</a></div>
			<div class="sub_main"><a class="sub_main_a">Tính chất đặc trị</a></div>
			<div class="sub_main"><a class="sub_main_a">Nước cân bằng</a></div>
			<div class="sub_main"><a class="sub_main_a">Kem dưỡng</a></div>
			<div class="sub_main"><a class="sub_main_a">Tẩy tế bào chết</a></div>
			</div>
		
		<div class="sub_main_right">
			<div class="sub_main"><a class="sub_main_a">Chống nắng</a></div>
			<div class="sub_main"><a class="sub_main_a">Nước khoáng dưỡng da</a></div>
			<div class="sub_main"><a class="sub_main_a">Mặt nạ</a></div>
		</div>
		<div class="clear"></div>				
	</div>
</div>

<div class="footer_item" style="margin-right:0px;">
	<h4>CÁC DÒNG SẢN PHẨM</h4>
	<div class="footer_item_sub">
	<div class="sub_main_left_branch">
		<div class="sub_main"><a class="sub_main_a">PURETE THERMALE</a></div>
		<div class="sub_main"><a class="sub_main_a">EAU THERMALE</a></div>
		<div class="sub_main"><a class="sub_main_a">AQUALIA THERMAL</a></div>
		<div class="sub_main"><a class="sub_main_a">BI-WHITE</a></div>
		<div class="sub_main"><a class="sub_main_a">NORMADERM</a></div>
	</div>
	
	<div class="sub_main_right_branch">
		<div class="sub_main"><a class="sub_main_a">LIFACTIV</a></div>
		<div class="sub_main"><a class="sub_main_a">CAPITAL SOLEIL</a></div>
		<div class="sub_main"><a class="sub_main_a">BODY CARE</a></div>
		<div class="sub_main"><a class="sub_main_a">DERCOS</a></div>
		<div class="sub_main"><a class="sub_main_a">AERATEINT</a></div>
	</div>
	<div class="clear"></div>		
	<!--<?php foreach ($list_cate as $r) {?>
		<?php if($r->parent_id == PRODUCT_RANG){?>
			<div class="sub_main">
				<a class="sub_main_a" title="<?php echo $r->title; ?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$r->id.'&Itemid=103'); ?>"><?php echo $r->title; ?></a>
			</div>
			<?php } ?>
	<?php } ?>-->
</div>
</div>
<div class="clear"></div>