<?php
	defined('_JEXEC') or die;
	require_once (dirname(__FILE__).DS.'helper.php');

	
	$layout = $params->get('tmp_layout','default');
	
	if(strpos($layout, '.php')>0){
		$layout = substr($layout, 0, -4);
	}
	
	$arr_option = vichyFooterHelper::getListOptions();

	$list_cate = vichyFooterHelper::getCategoryWithParent($params, $arr_option);

	$list_cate_yourneed = vichyFooterHelper::getCategoryWithParent_YourNeed($arr_option);
	
	$path = JModuleHelper::getLayoutPath ( 'mod_vichy_footer', $layout );
	require($path);

?>