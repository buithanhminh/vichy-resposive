<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_breadcrumbs
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
	$layout= $_GET['Itemid'];
	$class="breadcrumbs";
	// if($layout==101||$layout==108||$layout==107)
	// {
	// 	$class="breadcrumbs-right-style";
	// }
?>
<div class = "vichy-breadcrumb">
<!-- <div class="breadcrumbs<?php echo $moduleclass_sfx; ?>"> -->
<div class="<?php echo $class; ?>">
<?php if ($params->get('showHere', 1))
	{
		echo '<span class="showHere">' .JText::_('MOD_BREADCRUMBS_HERE').'</span>';
	}

	// Get rid of duplicated entries on trail including home page when using multilanguage
	for ($i = 0; $i < $count; $i ++)
	{
		if ($i == 1 && !empty($list[$i]->link) && !empty($list[$i-1]->link) && $list[$i]->link == $list[$i-1]->link)
		{
			unset($list[$i]);
		}
	}

	// Find last and penultimate items in breadcrumbs list
	end($list);
	$last_item_key = key($list);
	prev($list);
	$penult_item_key = key($list);
	$i=1;

	// Generate the trail
	foreach ($list as $key=>$item) :
	// Make a link if not the last item in the breadcrumbs
	$show_last = $params->get('showLast', 1);
	$breadcrumb_level = "breadcrumb_level_".$i;
	$custom_separator='<img src="'.JURI::root().'templates/vichy/images/arrow_'.$i.'.png" />';
	$custom_separator_span='<img src="'.JURI::root().'templates/vichy/images/span_arrow_'.$i.'.png" />';
	if ($key != $last_item_key)
	{
		// Render all but last item - along with separator
		if (!empty($item->link))
		{
			echo '<div class="vichy-breadcrumb-inactive '. $breadcrumb_level.'"><a href="' . $item->link . '" class="pathway">' . $item->name . '</a></div>';
		}
		else
		{
			echo '<div class="vichy-breadcrumb-active '. $breadcrumb_level.'"><span>' . $item->name . '</span></div>';
		}

		if (($key != $penult_item_key) || $show_last)
		{
			//echo ' '.$separator.' ';
			echo $custom_separator;
		}

	}
	elseif ($show_last)
	{
		// Render last item if reqd.
		if(count($list)==1)
			$custom_separator_span="";
		echo '<div class="vichy-breadcrumb-active '. $breadcrumb_level.'"><span>' . $item->name . '</span>'.$custom_separator_span.'</div>';
	}
	$i++;
	endforeach; ?>
</div><!--end of breadcrumb-->
</div><!--end of vichy-breadcrumb-->
