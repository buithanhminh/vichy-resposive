﻿<div class="clear"></div>
<?php
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
$document = & JFactory::getDocument();
$document->addStyleSheet('modules/mod_vichy_product_step/css/style.css');
$color1 = '';
$bg2 = '';
$current_product_id=$_GET['id'];
$db = & JFactory::getDbo();
$db->setQuery('SELECT main_color from vc_categories where id = '.$_GET['rid']);
$main_color = $db->loadObject();
	if(!empty($main_color->main_color)){
		switch ($main_color->main_color) {
			case 'normaderm':
				$color1 = '#197205';
                $color2 = '#8cbf4f';
				$border_color='#197205';
				break;
			case 'capital':
				$color1 = '#ff7200';
                $color2 = '#ffa146';
				$border_color='#ff8b06';
				break;
			case 'thermal':
				$color1 = '#448ac9';
                $color2 = '#57a5df';
				$border_color='#82add8';
				break;
			case 'aqualia_thermal':
			    	$color1 = '#448ac9';
                $color2 = '#57a5df';
			    	$border_color='#95c7ea';
			   	 break;
			case 'bi_white':
			    	$color1 = '#717e85';
                $color2 = '#9aa6af';
			    	$border_color='#c7c7c7';
			    	break;
            case 'blue-2':
                    $color1 = '#c20c5c';
                $color2 = '#9aa6af';
                    $border_color='#c7c7c7';
                    break;
			case 'destock':
			    	$color1 = '#099575';
                $color2 = '#0fb18d';
			    	$border_color='#32d2ae';
			    	break;
			case 'dercos':
			    	$color1= '#8b0305';
                $color2 = '#dd1734';
			    	$border_color='#8e090c';
			    	break;
			case 'liftactiv':
			    	$color1 = '#1a74a3';
                $color2 = '#418db3';
			    	$border_color='#6cafc9';
			    	break;
			case 'purete_thermal':
			    	$color1 = '#448ac9';
                $color2 = '#57a5df';
			    	$border_color='#04a0db';
			    	break;
            case 'idealia':
                $color1 = '#e55482';
                $color2 = '#ed88a7';
                $border_color='#e55482';
                break;
		}
		// $bg1 = $v->main_color.'_1.png';
		$bg2 = $main_color->main_color.'_2.png';
	}
$new_arr = array();
foreach ($steps as $v) {
	$no_of_step = $v->no_of_steps;
	$new_arr[$no_of_step][] = $v;
}

function getListTransID(){
    $db = & JFactory::getDbo();
    $sql = "SELECT id from vc_categories where parent_id = 8";
    $db->setQuery($sql);
    $list = $db->loadResultArray();
    $str_reference_id = implode(',', $list);

    $query = "SELECT reference_id, translation_id from vc_jf_translationmap where language = 'en-GB' and reference_table='categories' and reference_id in ($str_reference_id) ";
    $db->setQuery($query);
    $list_id = $db->loadObjectList();
    $result = array();
    if(!empty($list_id)){
    	foreach ($list_id as $key => $value) {
    		$result[$value->reference_id] = $value->translation_id;
    	}
    }
    return $result;
}
$lang = $language_tag;
if($lang == 'vi-VN'){
	$lbl_steps = "Các bước chăm sóc da:";
	$lbl_steps_name = "Bước";
	$lbl_new = "Mới";
	$lbl_favorite = "Yêu thích";
	$lbl_reviews = "người đánh giá";
}else{
	$lbl_steps = "Skin care steps:";
	$lbl_steps_name = "Step";
	$lbl_new = "New";
	$lbl_favorite = "Top Favorite";
	$lbl_reviews = "reviews";

	$list_cate_step = getListTransID();
}
?>
	<link href="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<!-- custom scrollbars plugin -->
	<script src="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#product_steps_id").mCustomScrollbar({
					autoHideScrollbar:false,
					theme:"light-thin"
				});
			});
		})(jQuery);
        $(document).ready(function(){
            $('.nav-tabs .tabs-product').first().find('a').css({'background':"<?php echo $color1;?>"});
//            $('.nav-tabs').first().find('li').css({'background':"<?php //echo $color1;?>//"});
           $('.tabs-product').click(function(){
               $('.tabs-product').find('a').css({'background':"transparent"});
//               $('.tabs-product').css({'background':<?php //echo $color2;?>//});
               $(this).find('a').css({'background':"<?php echo $color1;?>"});
//               $(this).css({'background':"<?php //echo $color1;?>//"});
           });
        });
	</script>

<?php

if(count($new_arr)){
    ?>
    <div class="wrapper_product_step no-width-height row pc-style">
        <div class="wrapper_product_step_content box_shadow no-width-height col-xs-12">
            <div class="routine no-width-height" style="background:<?php echo $color1; ?>; text-align: center; padding-left:0;"><?php echo $lbl_steps; ?></div>
            <div class="routine_steps no-width-height no-padding row no-margin" style="background:url(<?php echo JURI::root().'components/com_vichy_product/images/'.$bg2; ?>);overflow: hidden;">
                <?php foreach ($new_arr as $k => $v) { ?>
                    <div class="step_item no-padding pc-style">
                        <div class="no_step no-width-height"><div class="no-border"><?php echo $lbl_steps_name; ?> <?php echo $k; ?></div></div>
                        <div class="step_name no-width-height"><?php echo $v[0]->step_name; ?></div>
                    </div>
                <?php } ?>
            </div>

            <div class="product_steps no-padding" id="product_steps_id">
                <?php foreach ($new_arr as $k => $v) { ?>
                    <div class="product_steps_item no-width-height no-border  <?php if($k!=count($new_arr)) echo 'border_step';?>">
                        <div class="no-width-height no-padding row no-margin" style="background:url(<?php echo JURI::root().'components/com_vichy_product/images/'.$bg2; ?>);overflow: hidden;">
                            <div class="step_item no-width-height mobile-style">
                                <div class="no_step no-width-height"><div class="no-border"><?php echo $lbl_steps_name; ?> <?php echo $k; ?></div></div>
                                <div class="step_name no-width-height"><?php echo $v[0]->step_name; ?></div>
                            </div>
                        </div>
                        <?php foreach ($v as $k1 => $v1) { ?>
                            <?php if($v1->product_id != '0'){ ?>
                                <?php
                                $arr = explode(',', $v1->product_range);
                                $product_range = '';
                                $no_step='';
                                foreach ($arr as $value) {
                                    $titlelist = explode('|', $value);
                                    if(count($titlelist)>1 && ($titlelist[0]==$_product_rang || $titlelist[0]==$_product_rang_without_step))
                                    {
                                        $product_range = $titlelist[1];
                                        if($titlelist[0] == $_product_rang_without_step){
                                            $no_step = '&no_step=1';
                                        }
                                        break;
                                    }

                                }

                                $arr_category = explode(',', $v1->category_id);
                                $rid = '';
                                foreach ($arr_category as $value) {
                                    $categorylist = explode('|', $value);
                                    if(count($categorylist)>1 && ($categorylist[0]==PRODUCT_RANG || $categorylist[0]==PRODUCT_RANG_WITHOUT_STEP)){
                                        $rid = $categorylist[1];
                                        break;
                                    }
                                }
                                $rid = ($language_tag == 'en-GB') ? $list_cate_step[$rid] : $rid;
                                ?>
                                <a href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v1->product_id.'&rid='.$rid.$no_step); ?>">
                                    <div class="product_item col-xs-12 no-width-height">
                                        <?php
                                        if($v1->product_id==$current_product_id)
                                        {
                                        ?>
                                        <div class='wrap_current_product' style="border: 1px solid <?php echo $border_color;?>;">
                                            <?php
                                            }
                                            ?>
                                            <div class="image_product">
                                                <img width="147" height="290" src="<?php echo JURI::root() ?>components/com_vichy_product/uploads/products/<?php echo $v1->image; ?>" />
                                            </div>
                                            <div class="product_range">
                                                <?php
                                                echo $product_range;

                                                ?>
                                            </div>
                                            <div class="product_name no-width-height"><?php echo shortDesc($v1->product_name,500); ?></div>

                                            <!-- <div class="product_skin_care"> -->
                                            <?php

                                            // $arr = explode(',', $v1->product_range);
                                            // $skincare = null;
                                            // foreach ($arr as $value) {
                                            // 				$skincarelist = explode('|', $value);
                                            // 				if(count($skincarelist)>1 && $skincarelist[0]==SKIN_CARE)
                                            // 				{
                                            // 					  if(empty($skincare))
                                            // 					  {
                                            // 					  	 $skincare .=$skincarelist[1];
                                            // 					  }else {
                                            // 					  	$skincare .=','.$skincarelist[1];
                                            // 					  }

                                            // 				}

                                            // }
                                            // echo $skincare; ?>

                                            <!-- </div> -->
                                            <?php if($v1->is_new){ ?>
                                                <div class="product_new">Mới</div>
                                            <?php }
                                            else
                                            {
                                                if($v1->is_favorite)
                                                { ?>
                                                    <div class="product_favorite">
                                                        <div class="favorite_featured"><?php echo $lbl_favorite; ?></div>
                                                    </div>
                                                <?php }
                                            }
                                            ?>
                                            <div class="product_box_detail">
                                                <div class="product_star_review">
                                                    <?php for($j=1; $j<=5; $j++){ ?>
                                                        <?php if(!empty($v1->rating) && $v1->rating >0 && $j<=$v1->rating ){ ?>
                                                            <img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_blue.png">
                                                        <?php }else{ ?>
                                                            <img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_grey.png">
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                                <div style="clear:both;"></div>
                                                <div class="product_number_review"><?php  echo !empty( $v1->reviews)? $v1->reviews:0 ;?> <?php echo $lbl_reviews; ?></div>
                                            </div>
                                            <?php
                                            if($v1->product_id==$current_product_id)
                                            {
                                            ?>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    </div>
                                </a>
                            <?php }else{ ?>
                                <div class="product_item col-xs-12 no-width-height" style="border:none !important;">
                                    <!--<div class="image_product"></div>
                                    <div class="product_range"></div>
                                    <div class="product_name"></div>
                                    <div class="product_skin_care"></div>
                                    <div class="product_star_review"></div>
                                    <div style="clear:both;"></div>
                                    <div class="product_number_review"></div>-->

                                </div>
                            <?php } ?>
                        <?php } //end foreach $v ?>
                    </div>
                <?php } ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>



<div class="wrapper_product_step no-width-height row mobile-style" style="display: none;">
<div class="wrapper_product_step_content box_shadow no-width-height col-xs-12">
	<div class="routine no-width-height" style="background:<?php echo $color1; ?>; text-align: center; padding-left:0;"><?php echo $lbl_steps; ?></div>
	<div class="routine_steps no-width-height no-padding row no-margin" style="background:url(<?php echo JURI::root().'components/com_vichy_product/images/'.$bg2; ?>);overflow: hidden;">
		<?php foreach ($new_arr as $k => $v) { ?>
		<div class="step_item no-padding pc-style">
			<div class="no_step no-width-height"><div class="no-border"><?php echo $lbl_steps_name; ?> <?php echo $k; ?></div></div>
			<div class="step_name no-width-height"><?php echo $v[0]->step_name; ?></div>
		</div>
		<?php } ?>
	</div>
	
	<div class="product_steps no-padding" id="product_steps_id">
        <div class="container">
            <ul class="nav nav-tabs row">
            <?php
            $temp =0;
            foreach ($new_arr as $k => $v) {
                ?>
                    <li class="wrap tabs-product <?php echo ($temp == 0)?('active'):('');?> <?php echo ($temp == 1 || $temp == 2 || $temp ==0)?('col-xs-4'):('col-xs-6')?>" style="height:42px;<?php echo ($temp==2||$temp==4)?("border-left:0"):('')?>;background:<?php echo $color2; ?>;" >
<!--                       <div class="wrap" style="height:40px;">-->
                        <a data-toggle="tab" href="#tabs<?php echo $k?>"><b><?php echo $lbl_steps_name; ?> <?php echo $k; ?></b> <br><i><?php echo $v[0]->step_name; ?></i></a>
<!--                       </div>-->
                    </li>
            <?php
            $temp++;
            } ?>
            </ul>
                <div class="tab-content">
                    <?php foreach ($new_arr as $k => $v) { ?>
                    <div id="tabs<?php echo $k;?>" class="tab-pane fade in <?php echo ($k==1)?('active'):('')?>">
                        <?php foreach ($v as $k1 => $v1) { ?>
                            <?php if($v1->product_id != '0'){ ?>
                                <?php
                                $arr = explode(',', $v1->product_range);
                                $product_range = '';
                                $no_step='';
                                foreach ($arr as $value) {
                                    $titlelist = explode('|', $value);
                                    if(count($titlelist)>1 && ($titlelist[0]==$_product_rang || $titlelist[0]==$_product_rang_without_step))
                                    {
                                        $product_range = $titlelist[1];
                                        if($titlelist[0] == $_product_rang_without_step){
                                            $no_step = '&no_step=1';
                                        }
                                        break;
                                    }

                                }

                                $arr_category = explode(',', $v1->category_id);
                                $rid = '';
                                foreach ($arr_category as $value) {
                                    $categorylist = explode('|', $value);
                                    if(count($categorylist)>1 && ($categorylist[0]==PRODUCT_RANG || $categorylist[0]==PRODUCT_RANG_WITHOUT_STEP)){
                                        $rid = $categorylist[1];
                                        break;
                                    }
                                }
                                $rid = ($language_tag == 'en-GB') ? $list_cate_step[$rid] : $rid;
                                ?>
                                <a href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v1->product_id.'&rid='.$rid.$no_step); ?>">
                                    <div class="product_item col-xs-12 no-width-height">
                                        <?php
                                        if($v1->product_id==$current_product_id)
                                        {
                                        ?>
                                        <div class='wrap_current_product' style="border: 1px solid <?php echo $border_color;?>;">
                                            <?php
                                            }
                                            ?>
                                            <div class="image_product">
                                                <img width="147" height="290" src="<?php echo JURI::root() ?>components/com_vichy_product/uploads/products/<?php echo $v1->image; ?>" />
                                            </div>
                                            <div class="product_range">
                                                <?php
                                                echo $product_range;

                                                ?>
                                            </div>
                                            <div class="product_name no-width-height"><?php echo shortDesc($v1->product_name,500); ?></div>

                                            <!-- <div class="product_skin_care"> -->
                                            <?php

                                            // $arr = explode(',', $v1->product_range);
                                            // $skincare = null;
                                            // foreach ($arr as $value) {
                                            // 				$skincarelist = explode('|', $value);
                                            // 				if(count($skincarelist)>1 && $skincarelist[0]==SKIN_CARE)
                                            // 				{
                                            // 					  if(empty($skincare))
                                            // 					  {
                                            // 					  	 $skincare .=$skincarelist[1];
                                            // 					  }else {
                                            // 					  	$skincare .=','.$skincarelist[1];
                                            // 					  }

                                            // 				}

                                            // }
                                            // echo $skincare; ?>

                                            <!-- </div> -->
                                            <?php if($v1->is_new){ ?>
                                                <div class="product_new">Mới</div>
                                            <?php }
                                            else
                                            {
                                                if($v1->is_favorite)
                                                { ?>
                                                    <div class="product_favorite">
                                                        <div class="favorite_featured"><?php echo $lbl_favorite; ?></div>
                                                    </div>
                                                <?php }
                                            }
                                            ?>
                                            <div class="product_box_detail">
                                                <div class="product_star_review">
                                                    <?php for($j=1; $j<=5; $j++){ ?>
                                                        <?php if(!empty($v1->rating) && $v1->rating >0 && $j<=$v1->rating ){ ?>
                                                            <img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_blue.png">
                                                        <?php }else{ ?>
                                                            <img style="border:none;float:left;" src="<?php echo JURI::root() ?>media/small_star_grey.png">
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                                <div style="clear:both;"></div>
                                                <div class="product_number_review"><?php  echo !empty( $v1->reviews)? $v1->reviews:0 ;?> <?php echo $lbl_reviews; ?></div>
                                            </div>
                                            <?php
                                            if($v1->product_id==$current_product_id)
                                            {
                                            ?>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    </div>
                                </a>
                            <?php }else{ ?>
                                <div class="product_item col-xs-12 no-width-height" style="border:none !important;">
                                    <!--<div class="image_product"></div>
                                    <div class="product_range"></div>
                                    <div class="product_name"></div>
                                    <div class="product_skin_care"></div>
                                    <div class="product_star_review"></div>
                                    <div style="clear:both;"></div>
                                    <div class="product_number_review"></div>-->

                                </div>
                            <?php } ?>
                        <?php } //end foreach $v ?>
                    </div>
                    <?php } ?>
                </div>


            </div>
	</div>
</div>
</div>



<script>
	jQuery(document).ready(function(){
		var sum = jQuery('.routine_steps').width();
		var count = <?php echo count($new_arr); ?>;
		item_width = Math.round(sum/count);
		jQuery('.step_item').width(item_width-1);
		jQuery('.product_steps_item').width(item_width-1);

		//set css for favorite with lang=en-GB
		var sessionLang = "<?php echo $lang; ?>";
		if(sessionLang == 'en-GB'){
			jQuery('.favorite_featured').css({'font-size':'9px', 'margin-top':'18px'});
		}

        $(".wrap").dotdotdot({
        });
	});

</script>
<?php } ?>