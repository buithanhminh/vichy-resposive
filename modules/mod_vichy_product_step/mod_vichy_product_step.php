<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_feed
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$arr_option = vichyFooterHelper::getListOptions();
$_product_rang_without_step = $arr_option['product_rang_without_step_id'];
$_product_rang = $arr_option['product_rang_id'];

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();

if(isset($_GET['no_step']) && $_GET['no_step'] == '1'){
	$steps = modVichyStepHelper::getRangeNoStep($_GET['rid'], $language_tag);
	require JModuleHelper::getLayoutPath('mod_vichy_product_step', 'default_no_step');
}else{
	$steps = modVichyStepHelper::getStep($_GET['rid'], $language_tag);
	require JModuleHelper::getLayoutPath('mod_vichy_product_step', 'default');
}
