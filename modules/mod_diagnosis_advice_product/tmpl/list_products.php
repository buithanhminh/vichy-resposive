<?php 
	defined('_JEXEC') or die('Restricted access'); 
	$document = & JFactory::getDocument();
	$document->addStyleSheet('modules/mod_diagnosis_advice_product/css/style.css');
?>
<div class = "wrap_diagnosis_advice_product pc-style">
	
	<div class="diagnosis_advice_product_item box_shadow" style="width:446px;">
	<a href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107'); ?>">
		<img width="446" height="183" src="<?php echo JURI::root() ?>modules/mod_diagnosis_advice_product/images/diagnosis.jpg" />
		<div class="diagnosis_advice_product_item_text">
			<div class="diagnosis_advice_product_item_title"><?php echo $title_diagnosis; ?></div>
			<div class="diagnosis_advice_product_item_body">
				<?php echo $text_diagnosis; ?>
			</div>
		</div>
	</a>
	</div>
	<div class="diagnosis_advice_product_item box_shadow" style="margin-left:12px;">
	<a href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107'); ?>">
		<img width="444" height="183" src="<?php echo JURI::root() ?>modules/mod_diagnosis_advice_product/images/advice.jpg" />
		<div class="diagnosis_advice_product_item_text">
			<div class="diagnosis_advice_product_item_title"><?php echo $title_advice; ?></div>
			<div class="diagnosis_advice_product_item_body">
				<?php echo $text_advice; ?>
			</div>
		</div>
	</a>
	</div>
</div>