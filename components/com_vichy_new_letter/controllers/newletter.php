<?php
defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_ROOT.'/MailChimp.php';
class Vichy_new_letterControllerNewletter extends JController{
	public function __construct(){
		parent::__construct();
	}

	public function registerEmail(){
		$db = & JFactory::getDbo();
		$mailchimp = new Drewm\MailChimp(MAILCHIMP_KEY);
		$lists = $mailchimp->call('lists/list',array(
			'sort_dir'   => 'ASC'
		)) ;
		// $listid = $lists['data'][ELEMENT_MC]['id'];
		$listid = 'e51124a46d';
		$email = $_POST['email'];
		$cat = $_POST['catid'];
		$grouping_id = $_POST['grouping_id'];
		$return=$_POST['return_fail'];
		if(count($cat) == 0){
			$this->setRedirect(JRoute::_(JURI::root().'?cat_error=true'));
			return false;
		}

		$sql="SELECT * FROM `vc_vichy_subscribe_email` where email = '$email'";
		$db->setQuery($sql);
		$count_email = $db->loadObject();
		$email_id = '';
		$merge_vars = array(
		    'groupings' => array(
		        array(
		            'name' => "$grouping_id",
		            'groups' => $cat
		        )
		    )
		);
		
		if(count($count_email)){
			$result = $mailchimp->call('lists/subscribe', array(
		                'id'                => $listid,
		                'email'             => array('email'=>$email),
		                'merge_vars'        => $merge_vars,
		                'double_optin'      => 'true',
		                'send_welcome'      => 'true',
		                'replace_interests' => 'true',
		                'update_existing'   => 'true'
		    ));
		    if(empty($result['email'])){
		    	$this->setRedirect(JRoute::_(JURI::root().'?common_error=true'));
				return false;
		    }
		}else{
			$result = $mailchimp->call('lists/subscribe', array(
		                'id'                => $listid,
		                'email'             => array('email'=>$email),
		                'merge_vars'        => $merge_vars,
		                'double_optin'      => 'true',
		                'send_welcome'      => 'true'
		    ));
			if(!empty($result['email'])){
				$sql2="INSERT INTO `vc_vichy_subscribe_email` values (null, '$email')";
				// die($sql2);
				$db->setQuery($sql2);
				$db->query();
				// $email_id = mysql_insert_id();
			}else{
				// echo '<pre>';
				// print_r($result);
				// echo '</pre>';
				// die();
				$this->setRedirect(JRoute::_(JURI::root().'?common_error=true'));
				return false;
			}
		}

		$this->setRedirect(JRoute::_(JURI::root().'?subscribe=true'));
		
		// foreach($cat as $v){
		// 	$sql3="INSERT INTO `vc_vichy_subscribe_email_cat` values ($email_id,$v)";
		// 	$query3=mysql_query($sql3);
		// }

		// if(count($result)){
		// 	echo '<pre>';
		//     print_r($result);
		//     echo '</pre>';
		//     die();
		// }
	}
	
}