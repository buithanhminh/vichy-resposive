<?php

	defined('_JEXEC') or die;

	class Vichy_adviceHelper {
        function getContentByType($type,$cat_id=63){
			$db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
        	$sql = "SELECT q.*,c.title_description,c.title,c.params 
                    from vc_categories c
                    Left JOIN #__vichy_qa as q on q.catid=c.id
                    where ((q.published > 0 and q.type='$type' and q.language = '".str_replace(' ', '', $lg->getTag())."') or (q.published is NULL and q.type is NULL))  and c.id=$cat_id 
                    ";
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
		}

        function getCategoryWithParent_YourNeed($yourneed_id){
            $db = & JFactory::getDbo();
            $session =& JFactory::getSession();
            $lang = $session->get('lang');            
            $sql = "SELECT g.id,c.parent_id as grand_id,g.title as child_title,g.parent_id,c.title from #__categories as c Join #__categories as g On c.id=g.parent_id where c.published > 0 and LOCATE('com_vichy', c.extension) > 0 and (c.parent_id=".$yourneed_id." OR g.parent_id=".$yourneed_id.") and g.language = '".str_replace(' ', '', $lang)."'";
            //// ."' ORDER BY c.rgt ";

            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }

        function getCategoriesByGroup($yourneed)
        {
            $db = & JFactory::getDbo();
            $sql =  "SELECT a.*,c_r.main_color,c_r.title,c_r.params,c_r.title_description FROM #__vichy_advice_range a
                        
                        Join vc_categories c_r on a.range_id = c_r.id

                        where a.yourneed_id=$yourneed

                        ";
           
            $db->setQuery($sql);
            $db->query();
            return $db->loadObjectList();
        }

        function getQuestion(){
            $db = & JFactory::getDbo();
            $sql_cat = "SELECT c.id as cate_id, c.title from #__categories as c where c.extension = 'com_vichy_diagnostic' and c.published = 1 and c.id in(165,166,167,168) order by c.id ASC";
            $db->setQuery($sql_cat);
            $db->query();
            $list_cate = array();
            $list_cate = $db->loadObjectList();
            
            $sql_ques = "SELECT q.id as question_id, q.text as question_text, q.is_question, q.catid, q.background, q.icon, q.type_answer, a.id as answer_id, a.text as answer_text from vc_vichy_qa as q inner join vc_vichy_qa as a on q.id = a.parent_id and q.published=1 and q.type='diagnostic' order by q.id ASC";
            $db->setQuery($sql_ques);
            $db->query();
            $list_ques = array();
            $list_ques = $db->loadObjectList();
            
            $result = array();
            if(!empty($list_cate)){
                foreach($list_cate as $key=>$value){
                    foreach($list_ques as $v){
                        if($value->cate_id == $v->catid){
                            $result[$v->question_id]['cate_id'] = $value->cate_id;
                            $result[$v->question_id]['title'] = $value->title;
                            $result[$v->question_id]['question'] = $v;
                            $result[$v->question_id]['answers'][] = $v;
                        }
                    }
                }
            }
            return $result;         
        }

        function getQuestionByCate($cate_id){
            $db = & JFactory::getDbo();
            $sql_cat = "SELECT c.id as cate_id, c.title from #__categories as c where c.extension = 'com_vichy_diagnostic' and c.published = 1 and id = $cate_id order by c.id ASC";
            $db->setQuery($sql_cat);
            $db->query();
            $list_cate = array();
            $list_cate = $db->loadObjectList();
            
            $sql_ques = "SELECT q.id as question_id, q.text as question_text, q.is_question, q.catid, q.background, q.icon, q.type_answer, a.id as answer_id, a.text as answer_text from vc_vichy_qa as q inner join vc_vichy_qa as a on q.id = a.parent_id and q.published=1 and q.type='diagnostic' order by q.id ASC";
            $db->setQuery($sql_ques);
            $db->query();
            $list_ques = array();
            $list_ques = $db->loadObjectList();
            
            $result = array();
            if(!empty($list_cate)){
                foreach($list_cate as $key=>$value){
                    foreach($list_ques as $v){
                        if($value->cate_id == $v->catid){
                            $result[$v->question_id]['cate_id'] = $value->cate_id;
                            $result[$v->question_id]['title'] = $value->title;
                            $result[$v->question_id]['question'] = $v;
                            $result[$v->question_id]['answers'][] = $v;
                        }
                    }
                }
            }
            return $result;         
        }

        function getResultByCate($user_id, $cate_id){
            $db =& JFactory::getDbo();
            $query = "SELECT distinct r.id, q.*, q.text as question_text, a.id as answer_id, a.text as answer_text, r.name as result_text
                            from vc_vichy_qa as q inner join vc_vichy_qa_user as qu on q.id = qu.question_id inner join vc_vichy_qa as a on a.id = qu.answer_id 
                            inner join vc_vichy_diagnostic_map as map on a.id = map.answer_id
                            inner join vc_vichy_result as r on map.result_id = r.id
                            where q.catid = $cate_id and qu.user_id = $user_id";
            $db->setQuery($query);
            $db->query();
            $result = array();
            $result = $db->loadObjectList();

            return $result;
        }
        function getQuestionSectionOne($user_id, $cate_id){
            $db =& JFactory::getDbo();
            $query = "SELECT q.*, q.text as question_text, a.id as answer_id, a.text as answer_text
                            from vc_vichy_qa as q inner join vc_vichy_qa_user as qu on q.id = qu.question_id inner join vc_vichy_qa as a on a.id = qu.answer_id
                            where q.catid = $cate_id and qu.user_id = $user_id and q.id in(187,192)";
            $db->setQuery($query);
            $db->query();
            $result = null;
            $result = $db->loadObjectList();

            return $result;
        }

        function getResultSectionOne($user_id, $cate_id){
            $db =& JFactory::getDbo();
            $query = "SELECT distinct r.id, q.*, q.text as question_text, a.id as answer_id, a.text as answer_text, r.name as result_text
                            from vc_vichy_qa as q inner join vc_vichy_qa_user as qu on q.id = qu.question_id inner join vc_vichy_qa as a on a.id = qu.answer_id 
                            inner join vc_vichy_diagnostic_map as map on a.id = map.answer_id
                            inner join vc_vichy_result as r on map.result_id = r.id
                            where r.type = 'advice' and q.catid = $cate_id and qu.user_id = $user_id";
            $db->setQuery($query);
            $db->query();
            $result = null;
            $result = $db->loadObjectList();

            return $result;
        }

        function getResultFromT_U($user_id, $answer_t_id, $answer_u_id){
            $db =& JFactory::getDbo();
            $query = "SELECT distinct map.parent_id, map.result_id, map.answer_id, map2.answer_id as answer_id_2, r.id as result_id, r.name as result_name, p.name as product_name, p.id as product_id, p.image as product_image
                            from vc_vichy_qa_user as qu inner join vc_vichy_diagnostic_map as map on qu.answer_id = map.answer_id
                            inner join vc_vichy_diagnostic_map as map2 on map.parent_id = map2.parent_id and map.parent_id <> 0 and map.answer_id <> map2.answer_id
                            inner join vc_vichy_result as r on map.result_id = r.id
                            inner join vc_vichy_result_product as rp on r.id = rp.result_id
                            inner join vc_vichy_product as p on rp.product_id = p.id
                            where qu.user_id = $user_id and map.answer_id = $answer_t_id and map2.answer_id = $answer_u_id ";
            $db->setQuery($query);
            $db->query();
            $result = null;
            $result = $db->loadObject();

            return $result;
        }        

        function getResultQuestionEnd($user_id, $cate_id, $question_id){
            $db =& JFactory::getDbo();
            $query = "SELECT q.*, q.text as question_text, a.id as answer_id, a.text as answer_text, r.name as result_text, rp.product_id, p.name as product_name, p.image as product_image, rp.product_id_2, p2.name as product_name_2, p2.image as product_image_2
                            from vc_vichy_qa as q inner join vc_vichy_qa_user as qu on q.id = qu.question_id inner join vc_vichy_qa as a on a.id = qu.answer_id 
                            inner join vc_vichy_diagnostic_map as map on a.id = map.answer_id
                            inner join vc_vichy_result as r on map.result_id = r.id
                            left join vc_vichy_result_product as rp on r.id = rp.result_id
                            left join vc_vichy_product as p on rp.product_id = p.id
                            left join vc_vichy_product as p2 on rp.product_id_2 = p2.id
                            where q.catid = $cate_id and qu.user_id = $user_id and q.id = $question_id";
            $db->setQuery($query);
            $db->query();
            $result = null;
            $result = $db->loadObject();

            return $result;
        }

        function updateMyskinFromResult($user_id, $result_id){
            $db =& JFactory::getDbo();
            $query = "UPDATE vc_users set myskin = $result_id where id = $user_id";
            $db->setQuery($query);
            if($db->query()){
                return true;
            }
            return false;
        }

        function getInfoUser($user_id){
            $db =& JFactory::getDbo();
            $query = "SELECT r.id as result_id, r.name as result_myskin, u.name, u.username, u.email, u.birthday
                        from vc_users as u inner join vc_vichy_result as r on u.myskin = r.id 
                        where u.id = $user_id";
            $db->setQuery($query);
            $db->query();

            $myskin = $db->loadObject();

            return $myskin;
        }

        function getAnswerT_U($user_id, $question_t_id, $question_u_id){
            $db =& JFactory::getDbo();
            $query = "SELECT qa.question_id as question_t_id, qa2.question_id as question_u_id, qa.answer_id as answer_t_id, qa2.answer_id as answer_u_id 
                        from vc_vichy_qa_user as qa inner join vc_vichy_qa_user as qa2 on qa.user_id = qa2.user_id and qa.question_id <> qa2.question_id
                        where qa.user_id = $user_id and qa.question_id = $question_t_id and qa2.question_id = $question_u_id";
            $db->setQuery($query);
            $db->query();

            $result = $db->loadObject();

            return $result;
        }

        function isUserDiagnostic($user_id){
            $db =& JFactory::getDbo();
            $query = "SELECT myskin from vc_users where id = $user_id ";
            $db->setQuery($query);
            $db->query();
            $result = null;
            $result = $db->loadObject();
            
            if(empty($result) || $result->myskin == null){
                return false;
            }
            return true;
        }

    }
    
?>