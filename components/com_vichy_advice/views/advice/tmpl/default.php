<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
    
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);

	$document->addStyleSheet('components/com_vichy_advice/css/advice.css');
    $document->addStyleSheet('components/com_vichy_product/css/style.css');
    // $document->addStyleSheet('components/com_vichy_store/css/spa.css');

    $arr = array();
foreach ($this->menu_left as $v) {
    $pa=$v->parent_id;   
    $arr[$pa][] = $v;
}

$lang = $this->lang;
if($lang == 'vi-VN'){
    $cate_id_default = 63;
    $lbl_skin_test = "KIỂM TRA DA";
    $lbl_title_answer = "BẠN CÓ BIẾT";
}else{
    $cate_id_default = 104;
    $lbl_skin_test = "SKIN DIAGNOSTIS";
    $lbl_title_answer = "DID YOU KNOW";
}

$data=$this->data;
$tmp =json_decode($data[0][0]->params);
$background = $tmp->image;
$current_menu = (!isset($_GET['catid'])||empty($_GET['catid']))? $cate_id_default : $_GET['catid'];
$color_large_font = '';
$color_small_font = '';
foreach($this->items as $v){ 
    switch ($v->main_color) {
       case 'normaderm':
        $color_large_font = '#197205';
        $color_small_font = '#197205';       
        $color_3 = '#70D759';
        $color_2 = '#50B33B';
        $color_1= '#35951F';
        $color_0 = '#197205';
        break;
    case 'capital':
        $color_large_font = '#ff7200';
        $color_small_font = '#ff7200';
        $color_3 = '#FFF257';
        $color_2 = '#FFC93A';
        $color_1 = '#FFA720';
        $color_0 = '#FF8B06';
        break;
    case 'thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color_3 = '#E2FFFF';
        $color_2 = '#BCFAFF';
        $color_1 = '#9CD0FF';
        $color_0 = '#82ADD8';
        break;
    case 'aqualia_thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color_3 = '#FFFFFF';
        $color_2 = '#D7FFFF';
        $color_1 = '#B3EEFF';
        $color_0 = '#95C6EA';
        break;
    case 'bi_white':
        $color_large_font = '#717e85';
        $color_small_font = '#717e85';
        $color_0 = '#C7C7C7';
        $color_1 = '#FFFFFF';
        $color_2 = '#EFEFEF';
        $color_3 = '#FFFFFF';
        break;
    case 'destock':
        $color_large_font = '#099575';
        $color_small_font = '#099575';
        $color_0 = '#099574';
        $color_1 = '#24B397';
        $color_2 = '#41D7B6';
        $color_3 = '#5BFFDB';
        break;
    case 'dercos':
        $color_large_font = '#8b0305';
        $color_small_font = '#8b0305';
        $color_0 = '#8B0306';
        $color_1 = '#A71D20';
        $color_2 = '#C9383A';
        $color_3 = '#F25457';
        break;
    case 'liftactiv':
        $color_large_font = '#1a74a3';
        $color_small_font = '#1a74a3';
        $color_0 = '#1A73A3';
        $color_1 = '#3496C4';
        $color_2 = '#4EB4EC';
        $color_3 = '#6ED8FF';
        break;
    case 'purete_thermal':
        $color_large_font = '#448ac9';
        $color_small_font = '#448ac9';
        $color_0 = '#00A1DB';
        $color_1 = '#1BC2FF';
        $color_2 = '#36E9FF';
        $color_3 = '#51FFFF';
        break;
    case 'aera-mineral':
        $color_large_font = '#e2c8af';
        $color_small_font = '#e2c8af';
        $color_0 = '#E2C7AF';
        $color_1 = '#FFEFD2';
        $color_2 = '#FFFFFC';
        $color_3 = '#FFFFFF';
        break;
        case 'idealia':
            $color_large_font = '#e55482';
            $color_small_font = '#e55482';
            $color_0 = '#e55482';
            $color_1 = '#ed88a7';
            $color_2 = '#ff9cbb';
            $color_3 = '#FFFFFF';
            break;

        default :
        $color_large_font = '#2f4c9d';
        $color_small_font = '#2f4c9d';  
        $color_0 = '#2B716F';
        $color_1 = '#459391';
        $color_2 = '#61B1AE';
        $color_3 = '#7FD5D1';
        break;
    }
}

$background_button="background:$color_0;";
                    // background: -moz-linear-gradient(bottom, $color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);
                    // background: -o-linear-gradient(bottom, $color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);
                    // background: -webkit-gradient(linear, left bottom, left top, color-stop(0, $color_0), color-stop(50, $color_1), color-stop(60, $color_2), color-stop(100, $color_3));
                    // background: -webkit-linear-gradient(bottom, $color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);
                    // background: linear-gradient(to top,$color_0 0%, $color_1 50%, $color_2 75%, $color_3  100%);  
                    // ";
?>
<div class="vichy-advice">
    <div class="content-left">
        <ul class="menu-left pc-style">
           <!--  <li> <a class="active" href="javascript:void(0);">Lời khuyên</a></li>
            <li> <a href="javascript:void(0);">Kiểm tra da</a></li> -->
            <?php foreach ($this->menu_left as $k => $v){ 
                if(isset($arr[$v->id])){ ?>
                    <li class="sub_1 expand"><a><span class="active"><?php echo $v->child_title; ?></span></a>
                        <img src="<?php echo JURI::root();?>components/com_vichy_advice/images/menu_left_indicator.png">
                    <div class="wrap_sub_menu_2">
                    <?php foreach($arr[$v->id] as $v1){ ?>
                            <div class="sub_menu_2 get-advice" id="<?php echo $v1->id;?>" title="<?php echo $v1->child_title; ?>"><a class="sub_menu_a  <?php if($v1->id==$current_menu) echo 'current_menu';?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice&catid='.$v1->id.'&Itemid=107') ;?>"><?php echo $v1->child_title; ?></a></div>
                        <?php }}}
                         ?>
                    </div>
                 </li>
                <?php foreach($arr[$this->yourneed_parent] as $v){ ?>
                    <?php if(!isset($arr[$v->id])){ ?>
                    <li class="sub_1 get-advice" id="<?php echo $v->id;?>" title="<?php echo $v->child_title; ?>">
                        <a class="sub_1_a" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice&catid='.$v->id.'&Itemid=107') ;?>"><?php echo $v->child_title; ?></a>
                    </li>
            <?php }
                } ?>
           
        </ul>
        <nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">
            <div class="container">
                <div class="navbar-header sub-menu-spa-header blog-menu">
                <?php 
                if($lang == 'vi-VN'){
                    $loikhuyen = 'Lời khuyên';
                }else{
                    $loikhuyen = 'Advice';
                }
                ?>
                    <a class="navbar-brand" href="#" style="color:white;"><?php echo $loikhuyen ?></a>
                    <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    </button>
                </div>
                <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1" style="margin-left:0 !important;margin-right:0 !important;">
                    <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83">
                        <?php 
                           foreach ($this->menu_left as $k => $v){ 
                            if(isset($arr[$v->id])){                 
                        ?>
                            <li id="<?php echo $v->id; ?>" class="bg-li-menu-mobile1" style="margin-bottom:1px;"><a class="sub-menu-a dropdown-toggle dropdown-a" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: #467ac2!important;" href="#"><?php echo $v->child_title; ?><img class="img-responsive" style="float:right;width:6%;margin-right:8px" src="<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png"></a>
                                <ul class="dropdown-menu">
                                <?php foreach($arr[$v->id] as $v1){ ?>
                                    <li id="<?php echo $v1->id;?>" title="<?php echo $v1->child_title; ?>" class="bg-li-menu-mobile1"><a class="sub-menu-a <?php if($v1->id==$current_menu) echo 'current_menu';?>" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice&catid='.$v1->id.'&Itemid=107') ;?>"><?php echo $v1->child_title; ?></a></li>
                                <?php } ?>     
                                 
                                </ul>
                            </li>
                        <?php }} ?>

                        <?php foreach($arr[$this->yourneed_parent] as $v){ ?>
                        <?php if(!isset($arr[$v->id])){ ?>
                        <li class="bg-li-menu-mobile1" id="<?php echo $v->id;?>" title="<?php echo $v->child_title; ?>">
                            <a class="sub-menu-a" style="color: #467ac2!important;" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice&catid='.$v->id.'&Itemid=107') ;?>"><?php echo $v->child_title; ?></a>
                        </li>
                        <?php }
                            } ?>
                    </ul>
                </div>
            </div>
        </nav>
        
        <ul class="mobile-style" style="display: none;">
            <li class="bg-li-menu-mobile active" id="<?php echo $data[0][0]->id ?>" > 
                <div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7">
                    <a class="sub-menu-a active" style="line-height:25px" href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice&catid='.$data[0][0]->id.'&Itemid=107'); ?>"><?php echo $data[0][0]->title; ?></a>
                </div>
                <div style="">
                    <img style="margin-left:-0.7px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />   
                </div>
            </li>
      
        </ul>
     <script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
    $(".dropdown-a").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
        }else
        {
            $('.dropdown-a').find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus.png');
        }
    });

</script>   
    </div> <!-- content left -->

    <div class="wrap-content-right">
    <?php if(empty($data[0][0]->type) && empty($data[0][0]->published))
            {
        ?>
        <div class="comming-soon">COMING SOON
        
        <?php 
            }
            else{
        ?>
        <div class="content-right">
    	<div class="banner" style="background:url(<?php echo JURI::root().'timbthumb.php?src='.$background.'&w=672&h=234&zc=0';?>) no-repeat;">
        	<a href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107'); ?>">
                <div class="content-banner-left">
                    <div class="content-banner-left-title" style="color:<?php echo $color_large_font;?>"><?php echo $data[0][0]->title;?></div>
                    <div class="content-banner-left-description" style="color:<?php echo $color_small_font;?>"><?php echo $data[0][0]->title_description;?></div>
                    <div class="content-banner-left-button" style="<?php echo $background_button;?>"><?php echo $lbl_skin_test; ?></div>
                </div>
            </a>
        </div>
        <div class="content">
            <ul class="question">
                <?php foreach($data[0] as $k => $v){ ?>
                <?php if(isset($data[$v->id])){ ?>
            	<li>
                	<a href="javascript:void(0);"><?php echo $v->text; ?></a>
                    <div class="answer">
                    	<h3><?php echo $lbl_title_answer; ?></h3>
                        <?php echo $data[$v->id][0]->text; ?>
                    </div>
                
                </li> <!-- item -->
                <?php } ?>
                <?php } ?>
            </ul>
        </div>
		
        <?php
             }
        ?>
        
    </div> <!-- content right -->
    </div> <!--wrap content right -->
    <div class="clearfix"></div>
</div>
<div class="wrap_list_product_ranges" style="<?php if(empty($data[0][0]->type) && empty($data[0][0]->published)) echo 'display:none;'; ?>">
    <div class="wrap_list_product_ranges_content">
    <?php if(empty($data[0][0]->type) && empty($data[0][0]->published))
        {
    ?>
    <div class="comming-soon">COMING SOON</div>
    <?php 
        }
        else{
            $color = '';
            foreach($this->items as $v){ 
                switch ($v->main_color) {
                    case 'normaderm':
                        $color = '#197205';
                        break;
                    case 'capital':
                        $color = '#ff8b06';
                        break;
                    case 'thermal':
                        $color = '#82add8';
                        break;
                    case 'aqualia_thermal':
                        $color = '#95c7ea';
                        break;
                    case 'bi_white':
                        $color = '#c7c7c7';
                        break;
                    case 'destock':
                        $color = '#32d2ae';
                        break;
                    case 'dercos':
                        $color = '#8e090c';
                        break;
                    case 'liftactiv':
                        $color = '#6cafc9';
                        break;
                    case 'purete_thermal':
                        $color = '#04a0db';
                        break;
                    case 'aera-mineral':
                        $color = '#e2c8af';
                        break;
                    case 'idealia':
                        $color = '#e55482';
                        break;
                    default :
                        $color = '#197205';
                        break;
                }
                $data = json_decode($v->params,true);
                $image = (!empty($data['image'])) ? $data['image']: 'components/com_vichy_product/uploads/product_range/bg.png';
    ?>
    <a href="<?php 
                    /*if($v->total_product==1){
                        if($v->parent_id==37)
                            echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v->product_id.'&rid='.$v->id.'&no_step=1');
                        else
                            echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v->product_id.'&rid='.$v->id);
                    }else {*/
                    if($v->parent_id==37) 
                        echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->range_id.'&no_step=1'); 
                    else
                        echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->range_id); 
                //}?>" style="display:block;">              
        <div class="wrap_product_range_item">
            
            <div class="product_range_name" style="background:<?php echo $color; ?>">
                <span><?php echo $v->title; ?></span>
                <span class="desc_range"><?php echo $v->short_desc; ?></span>
            </div>
            <div class="product_range_image">
                <img width="915" src="<?php echo JURI::root().'timbthumb.php?src='.$image.'&w=915&zc=0'; ?>" />
            </div>
        </div>
    </a>
    <?php } //end foreach
        }//end if
    ?>
    </div>
</div>
<div id="com-vichy-advice-base-url" data-url="<?php echo JURI::root();?>index.php?option=com_vichy_advice&view=advice&task=getAdvice"></div>
<script type="text/javascript" src="<?php echo JURI::root();?>components/com_vichy_advice/js/advice.js"></script>