<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_adviceViewDiagnostic extends JView{
    	function display($tpl=null){
            $this->user       = JFactory::getUser();
            $user_id = $this->user->id;
            $info_user = Vichy_adviceHelper::getInfoUser($user_id);
            if(!empty($info_user)){
                $this->setLayout('default_result');
                $answerTU = Vichy_adviceHelper::getAnswerT_U($user_id, 187, 192);
                $answer_t_id = $answerTU->answer_t_id;
                $answer_u_id = $answerTU->answer_u_id;
                $this->result_answer_1 = Vichy_adviceHelper::getResultFromT_U($user_id, $answer_t_id, $answer_u_id);
                $this->result_section_1 = Vichy_adviceHelper::getQuestionSectionOne($user_id, 167);
                $this->result_condition = Vichy_adviceHelper::getResultByCate($user_id, 165);
                $this->result_lifestyle = Vichy_adviceHelper::getResultByCate($user_id, 166);
                $this->result_condition_end = Vichy_adviceHelper::getResultSectionOne($user_id, 167);
                $this->result_condition_end_1 = Vichy_adviceHelper::getResultQuestionEnd($user_id, 168, 202);
                $this->result_condition_end_2 = Vichy_adviceHelper::getResultQuestionEnd($user_id, 168, 203);
            }else{
                $this->setLayout('default');
                $list = Vichy_adviceHelper::getQuestion();
                $this->list = $list;
            }
            
            $app    = JFactory::getApplication();
            $pathway = $app->getPathway();
            $pathway->addItem('KIỂM TRA DA','javascript:void(0);');
    		parent::display($tpl);
    	}
    }
?>