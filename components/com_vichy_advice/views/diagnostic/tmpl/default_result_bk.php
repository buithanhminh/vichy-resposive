<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_advice/css/advice.css');
    $document->addStyleSheet('components/com_vichy_advice/css/style.css');
    $document->addScript('components/com_vichy_advice/js/diagnostic.js');

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();

    if($_GET['task'] == 'dictionary'){
    	$title = ($language_tag == 'vi-VN') ? VICHY_TITLE_DICTIONARY : VICHY_TITLE_DICTIONARY_EN;
    }else{
    	$title = ($language_tag == 'vi-VN') ? VICHY_TITLE_TEST_SKIN : VICHY_TITLE_TEST_SKIN_EN;
    }
    
    $document->setTitle($title);
    $background = "background_result.gif";
?>

<div class="vichy-diagnostic">
    <div class="wrapper-diagnostic">
        <div class="content-diagnostic">
            <div class="note">
                <span>Trả lời các câu hỏi sau đây để hiểu hơn về loại da của bạn.</span>
            </div>
            <div class="cate-diagnostic"><h2>3. Kết quả</h2></div>
            <!--<div class="diagnostic-result" style="background:url(<?php //echo JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/images/diagnostics/'.$background.'&w=860&h=3232&zc=0';?>) no-repeat;">-->
            <div class="diagnostic-result" style="background:url(<?php echo JURI::root()?>components/com_vichy_advice/images/diagnostics/<?php echo $background; ?>) no-repeat;">
                <div class="header-result">
                    <div class="icon-result">
                        <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_result.png&w=58&h=64&zc=0">
                    </div>
                    <div class="title-result">
                        KẾT QUẢ PHÂN TÍCH DA
                    </div>
                    <div class="text-result">
                        <span>
                            Chúc mừng bạn đã hoàn thành các câu hỏi kiểm tra da sơ lược. Dưới đây là kết qủa<br/>
                            và lời khuyên của Vichy dành cho bạn. Hãy để Vichy đồng hành với bạn suốt hành<br/>
                            trình đạt được làn da lý tưởng nhé
                        </span>
                    </div>
                </div>
                <div class="content-result">
                    <div class="top-section">
                        PHẦN 1: LOẠI DA
                    </div>
                    <div class="section-1">
                        <div class="section-1-left">
                            <div class="question">
                                <div class="icon-small-question">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_skin_t.png&w=49&h=70&zc=0">
                                </div>
                                <div class="text-question">
                                    1. Tình trạng da ở vùng chữ T của bạn:
                                </div>
                                <div class="text-answer">
                                    Rất dầu
                                </div>
                            </div>

                            <div class="question">
                                <div class="icon-small-question">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_skin_u.png&w=49&h=70&zc=0">
                                </div>
                                <div class="text-question">
                                    1. Tình trạng da ở vùng chữ U của bạn:
                                </div>
                                <div class="text-answer">
                                    Dầu
                                </div>
                            </div>
                        </div>

                        <div class="section-1-cen">
                            <img src="<?php echo JURI::root().'components/com_vichy_advice/images/diagnostics/line_part_1.jpg';?>" />
                        </div>

                        <div class="section-1-right">
                            <div class="column-1">
                                <div class="column-1-title">
                                    Kết quả:
                                </div>
                                <div class="column-1-myskin"> 
                                    Da của bạn là Da Dầu
                                </div>
                                <div class="column-1-step">
                                    BƯỚC 1
                                </div>
                                <div class="column-1-advice">
                                    Hãy rửa mặt với:
                                </div>
                                <div class="column-1-link">
                                    NORMADERM GEL KIỂM SOÁT<BR/>
                                    BÓNG DẦU VÀ DƯỠNG ẨM
                                </div>
                                <div class="buy_now">
                                    Mua ngay
                                </div>
                            </div>
                            <div class="column-2">
                                <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/images_product_step1.jpg&w=120&h=200&zc=0">
                            </div>
                        </div>
                    </div>
                    <!-- SECTION 02 -->
                    <div class="clear"></div>

                    <div class="top-section">
                        PHẦN 2: MÔI TRƯỜNG SỐNG                        
                    </div>
                    <div class="divider"></div>

                    <div class="section-2">
                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_sunny.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    3. Bạn thường xuyên tiếp xúc với ánh nắng mặt trời vào thời điểm:
                                </div>
                                <div class="section-2-right-ans">
                                    Mùa hè: trước 9h sáng - Mùa đông: trước 10h sáng
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Ánh nắng trong khoảng thời gian này tương đối nhẹ, đặc biệt là trước 8g sáng rất tốt cho việc da hấp thụ vitamin D, giúp săn chắc xương. Tuy nhiên, bạn đừng nên chủ quan vì da vẫn có nguy cơ tối màu da, thoa 1ml  kem chống nắng trong thời gian này vẫn cần thiết
                                </div>
                            </div>
                        </div>

                        <div class=" divider_dot"></div>

                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_smoke.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    4. Môi trường bạn đang sống có ô nhiễm (khói, bụi, tiếng ồn…):
                                </div>
                                <div class="section-2-right-ans">
                                    Có
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Ô nhiễm, khói bụi, khói thuốc gây tắc nghẽn lỗ chân lông, da mất độ thông thoáng & sắc da tối màu.<br/>
                                            - Tẩy tế bào chết thường xuyên giúp loại bỏ độc tố tích trữ trong da. <br/>
                                            - Cung cấp đầy đủ nước (2.5-3.5 lít) mỗi ngày để thanh lọc độc tố trong cơ thể<br/>
                                            - Giảm việc tiếp xúc trực tiếp với khói thuốc <br/>
                                            - Kiểm tra sức khỏe định kỳ 6 tháng/lần với bác sĩ về sức khỏe của mình<br/>
                                            - Lên kế hoạch thư giãn ở nơi khí hậu trong lành hơn giúp sức khỏe bạn giảm sự tổn thương
                                </div>
                            </div>
                        </div>

                        <div class=" divider_dot"></div>

                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_air.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    5. Thói quen ăn uống của bạn:
                                </div>
                                <div class="section-2-right-ans">
                                    Bình thườnng (cân bằng, đa dạng)
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Duy trì chế độ ăn uống hợp lý cùng với nếp sống điều độ, lành mạnh là điều cần thiết để giữ gìn khả năng lao động và sức sống trẻ trung ở mỗi người.
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- SECTION 03 -->
                    <div class="clear"></div>

                    <div class="top-section">
                        PHẦN 3: THÓI QUEN SỐNG                       
                    </div>
                    <div class="divider"></div>

                    <div class="section-2">
                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_sunny.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    6. Bạn thường xuyên tiếp xúc với ánh nắng mặt trời vào thời điểm:
                                </div>
                                <div class="section-2-right-ans">
                                    Mùa hè: trước 9h sáng - Mùa đông: trước 10h sáng
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Ánh nắng trong khoảng thời gian này tương đối nhẹ, đặc biệt là trước 8g sáng rất tốt cho việc da hấp thụ vitamin D, giúp săn chắc xương. Tuy nhiên, bạn đừng nên chủ quan vì da vẫn có nguy cơ tối màu da, thoa 1ml  kem chống nắng trong thời gian này vẫn cần thiết
                                </div>
                            </div>
                        </div>

                        <div class=" divider_dot"></div>

                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_smoke.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    7. Môi trường bạn đang sống có ô nhiễm (khói, bụi, tiếng ồn…):
                                </div>
                                <div class="section-2-right-ans">
                                    Có
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Ô nhiễm, khói bụi, khói thuốc gây tắc nghẽn lỗ chân lông, da mất độ thông thoáng & sắc da tối màu.<br/>
                                            - Tẩy tế bào chết thường xuyên giúp loại bỏ độc tố tích trữ trong da. <br/>
                                            - Cung cấp đầy đủ nước (2.5-3.5 lít) mỗi ngày để thanh lọc độc tố trong cơ thể<br/>
                                            - Giảm việc tiếp xúc trực tiếp với khói thuốc <br/>
                                            - Kiểm tra sức khỏe định kỳ 6 tháng/lần với bác sĩ về sức khỏe của mình<br/>
                                            - Lên kế hoạch thư giãn ở nơi khí hậu trong lành hơn giúp sức khỏe bạn giảm sự tổn thương
                                </div>
                            </div>
                        </div>

                        <div class="divider_dot"></div>

                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_air.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    8. Thói quen ăn uống của bạn:
                                </div>
                                <div class="section-2-right-ans">
                                    Bình thườnng (cân bằng, đa dạng)
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Duy trì chế độ ăn uống hợp lý cùng với nếp sống điều độ, lành mạnh là điều cần thiết để giữ gìn khả năng lao động và sức sống trẻ trung ở mỗi người.
                                </div>
                            </div>
                        </div>

                        <div class="divider_dot"></div>

                        <div class="section-2-row">
                            <div class="section-2-left">
                                <div class="section-2-left-icon">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/icon_air.png&w=301&h=163&zc=0">
                                </div>
                            </div>
                            <div class="section-2-right">
                                <div class="section-2-right-ques">
                                    9. Thói quen ăn uống của bạn:
                                </div>
                                <div class="section-2-right-ans">
                                    Bình thườnng (cân bằng, đa dạng)
                                </div>
                                <div class="section-2-advice">
                                    <strong>Lời khuyên: </strong>Duy trì chế độ ăn uống hợp lý cùng với nếp sống điều độ, lành mạnh là điều cần thiết để giữ gìn khả năng lao động và sức sống trẻ trung ở mỗi người.
                                </div>
                            </div>
                        </div>

                        <div class="divider_dot"></div>

                        <div class="section-end-row-1">
                            <div class="section-end-left">
                                <div class="left-col-1">
                                    <div class="left-col-1-ques">
                                        10a. Mối quan tâm về da quan trọng nhất hiện nay:
                                    </div>
                                    <div class="left-col-1-ans">
                                        Giảm mụn (vài đóm mụn li ti)
                                    </div>
                                    <div class="left-col-1-step">
                                        BƯỚC 2
                                    </div>
                                    <div class="left-col-1-advice">
                                        Sử dụng sản phẩm giúp chuyển hóa làn da, đặc trị chuyên sâu:
                                    </div>
                                    <div class="left-col-1-link">
                                        KEM DƯỠNG GIẢM MỤN
                                    </div>
                                    <div class="buy_now">
                                        Mua ngay
                                    </div>
                                </div>
                                <div class="left-col-2">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/images_product_step1.jpg&w=120&h=200&zc=0">
                                </div>
                            </div>
                            <div class="section-end-right">
                                <div class="left-col-1">
                                    <div class="left-col-1-ques">
                                        10a. Mối quan tâm về da quan trọng nhất hiện nay:
                                    </div>
                                    <div class="left-col-1-ans">
                                        Giảm mụn (vài đóm mụn li ti)
                                    </div>
                                    <div class="left-col-1-step">
                                        BƯỚC 2
                                    </div>
                                    <div class="left-col-1-advice">
                                        Sử dụng sản phẩm giúp chuyển hóa làn da, đặc trị chuyên sâu:
                                    </div>
                                    <div class="left-col-1-link">
                                        KEM DƯỠNG GIẢM MỤN
                                    </div>
                                    <div class="buy_now">
                                        Mua ngay
                                    </div>
                                </div>
                                <div class="left-col-2">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/images_product_step1.jpg&w=120&h=200&zc=0">
                                </div>
                            </div>
                        </div>
                        <div class="section-end-row-2">
                            <div class="section-end-row-1">
                                <div class="left-col-1">
                                    <div class="left-col-1-ques">
                                        10a. Mối quan tâm về da quan trọng nhất hiện nay:
                                    </div>
                                    <div class="left-col-1-ans">
                                        Giảm mụn (vài đóm mụn li ti)
                                    </div>
                                    <div class="left-col-1-step">
                                        BƯỚC 2
                                    </div>
                                    <div class="left-col-1-advice">
                                        Sử dụng sản phẩm giúp chuyển hóa làn da, đặc trị chuyên sâu:
                                    </div>
                                    <div class="left-col-1-link">
                                        KEM DƯỠNG GIẢM MỤN
                                    </div>
                                    <div class="buy_now">
                                        Mua ngay
                                    </div>
                                </div>
                                <div class="left-col-2">
                                    <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/images/diagnostics/images_product_step1.jpg&w=120&h=200&zc=0">
                                </div>
                            </div>
                        </div>

                        <div class="divider_dot"></div>

                    </div>
                
                    <div class="footer-result">
                        <div class="btn_bottom">
                            <div class="print">In kết quả</div>

                            <div class="store_system">Hệ thống cửa hàng</div>
                        </div>
                    </div>

                </div>                

            </div>
        </div>
    </div>
</div>