<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_advice/css/advice.css');
?>
<div class="vichy-advice">
    <div class="content-left">
        <ul class="menu-left">
            <li> <a class="active" href="javascript:void(0);">Lời khuyên</a></li>
            <li> <a href="javascript:void(0);">Kiểm tra da</a></li>
           
        </ul>
        
        <div class="menu-locator">
        	<div class="locator" id="advice-locator">
                <img src="<?php echo JURI::root() ?>components/com_vichy_advice/images/map.jpg" />
            </div>
            <h3>CALL TO ACTION</h3>
            <p>LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT, SED DO EIUSMOD TEMPOR </p>
            <div class="btn"><a href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=store&Itemid=113'); ?>">STORE LOCATOR</a></div>
        </div>
        
    </div> <!-- content left -->
    
    <div class="content-right">
    	<div class="banner">
        	<a href="javascript:alert('COMING SOON...');"><img src="<?php echo JURI::root() ?>components/com_vichy_advice/images/vichy_advice_bg.jpg" alt="Banner"></a>
        </div>
        <div class="content">
        	<h3 class="title">Question & Answers by Experts</h3>
            <ul class="question">
                <?php foreach($this->data[0] as $k => $v){ ?>
                <?php if(isset($this->data[$v->id])){ ?>
            	<li>
                	<a href="javascript:void(0);"><?php echo $v->text; ?></a>
                    <div class="answer">
                    	<h3>DID YOU KNOW</h3>
                        <?php echo $this->data[$v->id][0]->text; ?>
                    </div>
                
                </li> <!-- item -->
                <?php } ?>
                <?php } ?>
            </ul>
        </div>
		
        
        
    </div> <!-- content right -->
    
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        // answer question
        jQuery(".question li a").click(function(){
            var parent=jQuery(this).parent("li");

            jQuery(".answer").not(parent).slideUp(function(){
                jQuery(".question li").not(parent).removeClass("active");
            });

            if(parent.hasClass("active"))
            {
                jQuery(this).next(".answer").slideUp(function(){
                    parent.removeClass("active");
                });
            }
            else
            {
                jQuery(this).next(".answer").slideDown(function(){
                    parent.addClass("active");
                });
            }
        });
    });
</script>