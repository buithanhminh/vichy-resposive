<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_advice/css/advice.css');
    $document->addStyleSheet('components/com_vichy_advice/css/style.css');
    $document->addScript('components/com_vichy_advice/js/diagnostic.js');

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();

    if($_GET['task'] == 'dictionary'){
    	$title = ($language_tag == 'vi-VN') ? VICHY_TITLE_DICTIONARY : VICHY_TITLE_DICTIONARY_EN;
    }else{
    	$title = ($language_tag == 'vi-VN') ? VICHY_TITLE_TEST_SKIN : VICHY_TITLE_TEST_SKIN_EN;
    }
    
    $document->setTitle($title);
    
    $list = $this->list;

    $user       = $this->user;
    
?>





<div class="vichy-diagnostic">
    <div class="wrapper-diagnostic">
        <div class="content-diagnostic" id="content-diagnostic">
            <?php            
                if(empty($user->username)){                    
            ?>
            <div class="warning-login">
                Vui lòng đăng nhập để kiểm tra da
            </div>
            <?php
                }else{
                    $info_user = Vichy_adviceHelper::getInfoUser($user->id);
                    if(!empty($info_user)){
            ?>
            <div class="info_user" style="float:left;width:860px;color:#6e8bb9;font-size:16px;">
                Bạn đã thực hiện kiểm tra da rồi
            </div>
            <?php
                    }else{
            ?>
            <div class="note">
                <span>Trả lời các câu hỏi sau đây để hiểu hơn về loại da của bạn.</span>
            </div>
            <?php
                if(!empty($list)){
                    $c = 'a';                    
                    $i = 1;                    
                    foreach ($list as $key => $value) {                        
                        if($i < 10){
                            $num = "0$i";
                        }else{
                            $num = $i;
                        }
                        $question = $value['question'];
                        $answers = $value['answers'];
                        
                        $background = $question->background;
                        $icon = $question->icon;
                        if($i == 1){
                            $style = "";
                        }else{
                            $style = "display: none;";
                        }
                        $current_item = current($list);
                        $next_item = next($list);

                        if($question->question_id == 187 || $question->question_id == 192 || $question->question_id == 197 ){
                            $w = "&w=150&h=230";
                        }else{
                            $w = "&w=300";
                        }

                        if($question->type_answer == 'selectbox'){
                            $question_01 = $next_item;
                            $question_02 = next($list);
            ?>
            <div class="diagnostic_item" style="<?php echo $style; ?>" id="item-<?php echo $question->question_id; ?>">
                <div class="cate-diagnostic"><h2><?php echo $c; ?>. <?php echo $value['title']; ?></h2></div>
                <div class="diagnostic" style="background:url(<?php echo JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'.$background.'&w=860&h=690&zc=0';?>) no-repeat;">
                    <div class="diagnostic-top">
                        <div class="diagnostic-icon">
                            <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/uploads/diagnostics/<?php echo $icon; ?><?php echo $w; ?>&zc=0">
                        </div>
                    </div>
                    <div class="diagnostic-cen">
                        <div class="diagnostic-question">
                            <div class="number">
                                <span><?php echo $num; ?></span>
                            </div>
                            <div class="question">
                                <?php echo $question->question_text; ?>
                            </div>

                            <div class="answer-end" style="clear:both;">
                                <input type="hidden" name="info[]" class="info" id="info_<?php echo $question_01['question']->question_id; ?>" value="<?php echo $question_01['question']->question_id.'|'.$question_01['answers'][0]->answer_id; ?>" />
                                <strong><?php echo $question_01['question']->question_text; ?></strong>
                                <select name="concern" class="concern" data-qid="<?php echo $question_01['question']->question_id; ?>">
                                <?php
                                    foreach ($question_01['answers'] as $k => $v) {
                                        
                            ?>                            
                                <option value="<?php echo $v->answer_id; ?>">
                                    <?php echo $v->answer_text; ?>
                                </option>

                            <?php
                                    }
                            ?>
                                </select>
                            
                            </div>

                            <div class="answer-end">
                                <input type="hidden" name="info[]" class="info" id="info_<?php echo $question_02['question']->question_id; ?>" value="<?php echo $question_02['question']->question_id.'|'.$question_02['answers'][0]->answer_id; ?>" />
                                <strong><?php echo $question_02['question']->question_text; ?></strong>
                                <select name="concern" class="concern"  data-qid="<?php echo $question_02['question']->question_id; ?>">
                                <?php
                                    foreach ($question_02['answers'] as $k => $v) {
                                        
                            ?>                            
                                <option value="<?php echo $v->answer_id; ?>">
                                    <?php echo $v->answer_text; ?>
                                </option>

                            <?php
                                    }
                            ?>
                                </select>
                            
                            </div>
                        </div>
                    </div>
                    <div class="diagnostic-bot">
                        <div class="diagnostic-next">
                            <a href="javascript:void(0);" name="finish" class="finish" current-item-one="<?php echo $question_01['question']->question_id; ?>" current-item-second="<?php echo $question_02['question']->question_id; ?>">
                                <img src="<?php echo JURI::root().'components/com_vichy_advice/images/diagnostics/next.png';?>" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>    
            <?php
                            break;
                        }else{
            ?>
            <div class="diagnostic_item" style="<?php echo $style; ?>" id="item-<?php echo $question->question_id; ?>">
                <div class="cate-diagnostic"><h2><?php echo $c; ?>. <?php echo $value['title']; ?></h2></div>
                <div class="diagnostic" style="background:url(<?php echo JURI::root().'timbthumb.php?src='.JURI::root().'components/com_vichy_advice/uploads/diagnostics/'.$background.'&w=860&h=690&zc=0';?>) no-repeat;">
                    <div class="diagnostic-top">
                        <div class="diagnostic-icon">
                            <img src="<?php echo JURI::root();?>timbthumb.php?src=<?php echo JURI::root(); ?>components/com_vichy_advice/uploads/diagnostics/<?php echo $icon; ?><?php echo $w; ?>&zc=0">
                        </div>
                    </div>
                    <div class="diagnostic-cen">
                        <div class="diagnostic-question">
                            <div class="number">
                                <span><?php echo $num; ?></span>
                            </div>
                            <div class="question">
                                <?php echo $question->question_text; ?>
                                <?php if($question->type_answer == 'checkbox'){ echo '<div class="note-multi-answer">(có thể chọn nhiều câu trả lời)</div>'; } ?>
                            </div>
                            <div class="answer">
                            <?php
                            if(!empty($answers)){
                                if($question->type_answer == 'radio'){
                                    foreach ($answers as $k => $v) {
                                        
                            ?>                            
                                <input type="hidden" name="info[]" class="info" id="info_<?php echo $question->question_id; ?>" />
                                <div class="add_answer">
                                    <div class="image_common image_off" data-qid="<?php echo $v->question_id; ?>" data-id="<?php echo $v->answer_id; ?>"><?php echo strip_tags($v->answer_text); ?></div>
                                </div>                                                            
                            <?php
                                    }
                                }else if($question->type_answer == 'checkbox'){
                                    foreach ($answers as $k => $v) {
                                        
                            ?>                            
                                <input type="hidden" name="info[]" class="info" id="info_<?php echo $question->question_id; ?>" />
                                <div class="add_answer">
                                    <div class="image_common image_off_check" data-qid="<?php echo $v->question_id; ?>" data-id="<?php echo $v->answer_id; ?>"><?php echo strip_tags($v->answer_text); ?></div>
                                </div>                                                            
                            <?php
                                    }
                                }
                            }
                            ?>
                            </div>
                        </div>
                    </div>
                    <div class="diagnostic-bot">
                        <div class="diagnostic-next">
                            <a href="javascript:void(0);" name="next" class="next" current-item="<?php echo $question->question_id; ?>" next-item="<?php echo $next_item['question']->question_id; ?>">
                                <img src="<?php echo JURI::root().'components/com_vichy_advice/images/diagnostics/next.png';?>" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <?php
                    }

                    if($current_item['cate_id'] != $next_item['cate_id']){
                        $c++;
                    }                    
                    $i++;
                }
            
            ?>

            <div id="com-vichy-diagnostic-base-url" data-url="<?php echo JURI::root();?>index.php?option=com_vichy_advice&view=diagnostic&task=storeResult"></div>
            <div id="user-info" data-uid="<?php echo $user->id; ?>"></div>
        <?php
            }
        ?>
        <?php
           }
        } 
        ?>
        </div>        
    </div>
</div>