jQuery(document).ready(function() {
        // answer question
        base_url = jQuery('#com-vichy-advice-base-url').attr('data-url');
        jQuery(".question li:first-child").addClass("active");
        jQuery(".question li a").click(function(){
            var parent=jQuery(this).parent("li");

            jQuery(".answer").not(parent).slideUp(function(){
                jQuery(".question li").not(parent).removeClass("active");
            });

            if(parent.hasClass("active"))
            {
                jQuery(this).next(".answer").slideUp(function(){
                    parent.removeClass("active");
                });
            }
            else
            {
                jQuery(this).next(".answer").slideDown(function(){
                    parent.addClass("active");
                });
            }
        });

        jQuery(".menu-left li:first-child>a").click(function(){
            var parent = jQuery(this).parent();
            var container = jQuery(".wrap_sub_menu_2");

            if(parent.hasClass("expand"))
            {
                container.animate({ height: "-=125px"},0,function(){
                    container.hide();
                    parent.removeClass("expand");
                });
                
            }
            else
            {   
                container.animate({ height: "+=125px"},0,function(){
                    container.show();
                    parent.addClass("expand");
                });
            }
        });// end of collapse menu

        // jQuery(".get-advice>a").click(function(){
        //     var catid = jQuery(this).parent().attr('id');
        //     var type='advice';
        //     jQuery(".get-advice>a").removeClass('current_menu');
        //     jQuery(this).addClass('current_menu');   

        //     jQuery.ajax ({
        //     type: "POST",
        //     url: base_url,
        //     dataType:'json',
        //     data: "catid="+catid+'&type='+type,
        //     success: function(data_page) { 
        //         // jQuery('#loading').fadeOut('fast');
        //         if(data_page.comming_soon==0)
        //             jQuery(".wrap-content-right").html(data_page.question);
        //         else
        //         {
        //             jQuery(".wrap-content-right").html(data_page.question);
        //         }
        //         if(data_page.range!='')
        //             jQuery(".wrap_list_product_ranges_content").html(data_page.range); 
        //         else
        //             jQuery(".wrap_list_product_ranges_content").attr('style','display:none')
        //         jQuery(".breadcrumb_level_3>span").html(jQuery(".current_menu").parent().attr("title"));
        //         jQuery(".question li:first-child").addClass("active");
        //         jQuery(".question li a").click(function(){
        //         var parent=jQuery(this).parent("li");

        //         jQuery(".answer").not(parent).slideUp(function(){
        //             jQuery(".question li").not(parent).removeClass("active");
        //         });

        //         if(parent.hasClass("active"))
        //         {
        //             jQuery(this).next(".answer").slideUp(function(){
        //                 parent.removeClass("active");
        //             });
        //         }
        //         else
        //         {
        //             jQuery(this).next(".answer").slideDown(function(){
        //                 parent.addClass("active");
        //             });
        //         }
        // });
        //     }
        //     });
        // });

});/**/