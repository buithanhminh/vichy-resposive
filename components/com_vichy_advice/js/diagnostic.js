jQuery(document).ready(function() {
    jQuery('.diagnostic-question').on('click','.image_off',function(){
        qid = jQuery(this).attr('data-qid');
        aid = jQuery(this).attr('data-id');
        jQuery('#info_'+qid).val(qid+'|'+aid);
        jQuery(this).parent().siblings().children('.image_common').removeClass('image_on');
        jQuery(this).parent().siblings().children('.image_common').addClass('image_off');
        jQuery(this).removeClass('image_off');
        jQuery(this).addClass('image_on');
    });

    jQuery('.diagnostic-question').on('click','.image_off_check',function(){
        qid = jQuery(this).attr('data-qid');
        aid = jQuery(this).attr('data-id');
        var current = jQuery('#info_'+qid).val();
        if(current == ''){
            jQuery('#info_'+qid).val(qid+'|'+aid);
        }else{
            jQuery('#info_'+qid).val(current+','+qid+'|'+aid);
        }
        jQuery(this).removeClass('image_off_check');
        jQuery(this).addClass('image_on_check');
    });

    jQuery('.diagnostic-question').on('click','.image_on_check',function(){
        qid = jQuery(this).attr('data-qid');
        aid = jQuery(this).attr('data-id');
        var current = jQuery('#info_'+qid).val();
        var del_val = qid+'|'+aid;
        current = current.replace(del_val, '');
        current = current.replace(',,', ',');
        if(current == ','){
            current = current.replace(',', '');
        }
        jQuery('#info_'+qid).val(current);
        jQuery(this).removeClass('image_on_check');
        jQuery(this).addClass('image_off_check');
    });

    jQuery('.diagnostic-question').on('click','.concern',function(){
        qid = jQuery(this).attr('data-qid');
        aid = jQuery(this).val();
        jQuery('#info_'+qid).val(qid+'|'+aid);        
    });

    base_url = jQuery('#com-vichy-diagnostic-base-url').attr('data-url');
    
    var result = '';
    jQuery('.next').on('click', function(){
        var current_qid = jQuery(this).attr('current-item');
        var next_qid = jQuery(this).attr('next-item');

        var answers = jQuery('#info_'+current_qid).val();
        if(answers == ''){
            alert('Vui lòng trả lời cho câu hỏi hiện tại');
            return false;
        }

        jQuery('#item-'+current_qid).hide();
        jQuery('#item-'+next_qid).show();

        result += ','+answers;

    });

    jQuery('.finish').on('click', function(){
        var current_qid_1 = jQuery(this).attr('current-item-one');
        var current_qid_2 = jQuery(this).attr('current-item-second');

        var answers1 = jQuery('#info_'+current_qid_1).val();
        var answers2 = jQuery('#info_'+current_qid_2).val();
        result += ','+answers1+','+answers2;
        var user_id = jQuery('#user-info').attr('data-uid');
        
        jQuery.ajax({
            type: "POST",
            url: jQuery('#com-vichy-diagnostic-base-url').attr('data-url'),
            dataType: "json",
            data: "result="+result+'&user_id='+user_id,
            success: function(data_page){
               jQuery("#content-diagnostic").html(data_page.content);

               jQuery('html, body').animate({scrollTop: 0}, 1000);              
            }
        });

    });

});