<?php
defined( '_JEXEC' ) or die;

function Vichy_adviceBuildRoute(&$query){
	$segments = array();
	$catid='';
	if(isset($query['view'])){			
		switch ($query['view']) {
			case 'advice':
				$curLanguage = JFactory::getLanguage();
				$language_tag = $curLanguage->getTag();

				if($language_tag == 'vi-VN'){
					$segments[] = 'loi-khuyen';
				}else{
					$segments[] = 'advice';
				}
				
				$catid=(!isset($query['catid']))?63:$query['catid'];
				$db = JFactory::getDbo();
				$db->setQuery("SELECT `alias` from #__categories where id = ".$catid);
				$name=$db->loadObject()->alias;
				$segments[] = $name;
		unset($query['catid']);
				break;
			case 'diagnostic':
				$segments[]='kiem-tra-da';
				break;	
			case 'advice_diagnosis':
				$segments[]='coming-soon';
				break;
			case 'dictionary':
				$segments[]='tu-dien-da';
				break;	
			default:
				$segments[]='coming-soon';
				break;	
		}
		unset($query['view']);
    }   
    
    return $segments;
}

function Vichy_adviceParseRoute($segments){
	$vars = array();

	$segments[0] = str_replace(':','-',$segments[0]);
	
	switch ($segments[0]) {
		case 'loi-khuyen':
		case 'advice':
			$curLanguage = JFactory::getLanguage();
			$lang = $curLanguage->getTag();
			if($lang == 'vi-VN'){
				$yourneedid = 10;
			}else{
				$yourneedid = 151;
			}

			$vars['view'] = 'advice';
			$index = count($segments) - 1;
			$alias=str_replace(':','-',$segments[$index]);
			$db = JFactory::getDbo();
			$query ="select c.id from #__categories c join #__categories p on c.parent_id = p.id where (c.parent_id = $yourneedid or p.parent_id =$yourneedid) and c.alias = '".$alias."' and c.extension ='com_vichy_product' and c.published=1";
			$db->setQuery($query);
			$id=$db->loadObject()->id;
			$vars['catid']=$id;
			break;
		case 'kiem-tra-da':
			$vars['view'] = 'diagnostic';
			break;			
		case 'diagnostic':
			$vars['view'] = 'diagnostic';
			break;
		case 'tu-dien-da':
			$vars['view'] = 'dictionary';
			break;	
		default:
			$vars['view'] = $segments[0];
			break;
	}
	return $vars;
}