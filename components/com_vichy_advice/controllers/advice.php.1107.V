<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_adviceControllerAdvice extends JController{
    	function __construct(){
    		parent::__construct();
    		JRequest::setVar('view','advice');
    	}

    	function getAdvice()
    	{
    		$type = $_POST['type'];
    		$catid=$_POST['catid'];

    		$data="";

    		$content=array();
    		$advices=Vichy_adviceHelper::getContentByType('advice',$catid);
    		foreach ($advices as $k => $v) {
    			if(empty($v->type) && empty($v->published))
    			{
    				$content[0][0]=$v;
                    $data.='<div class="coming-soon">COMING SOON</div>';
                    echo $data;exit();
    				break;
    			}
    			$pa = $v->parent_id;
    			$content[$pa][] = $v;
    		}

			if(count($content)>0)
			{
				$tmp =json_decode($content[0][0]->params);
				$background = $tmp->image;

				$data.='<div class="banner" style="background:url('.JURI::root().'timbthumb.php?src='.$background.'&w=672&h=234&zc=0.) no-repeat;"';
				$data.='<a href="javascript:alert(\'COMING SOON\');">';
                $data.='<div class="content-banner-left">';
                $data.='<div class="content-banner-left-title">'.$content[0][0]->title.'</div>';
                $data.='<div class="content-banner-left-description">'.$content[0][0]->short_desc.'</div>';
    			$data.='<div class="content-banner-left-button">KIỂM TRA DA</div>';
                $data.='</div>';
            	$data.='</a>';
				$data.='</div>';
				$data.='<div class="content">';
        		$data.='<h3 class="title">Question & Answers by Experts</h3>';
            	$data.='<ul class="question">';
                foreach($content[0] as $k => $v){
                	if(isset($content[$v->id])){
		            	$data.='<li>';
		                $data.='<a href="javascript:void(0);">'.$v->text.'</a>';
		                $data.='<div class="answer">';
		                $data.='<h3>BẠN CÓ BIẾT</h3>';
		                $data.=$content[$v->id][0]->text;
		                $data.='</div>';
		                $data.='</li>';
	                } 
                }
            	$data.='</ul>';
        		$data.='</div>';
			}
			echo $data;
			exit();
    	}
     }
?>