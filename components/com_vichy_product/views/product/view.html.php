<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_productViewProduct extends JView{
    	function display($tpl=null){
            $document = & JFactory::getDocument();
            $title = ucwords($document->getTitle());
            $title_item = ucwords(strtolower($this->getTitleByCategory($_GET['rid'])));
            
            $this->assignRef('title', $title);
            $this->assignRef('title_item', $title_item);

    		$product_id = $_GET['id'];
    		$items = Vichy_productHelper::getProduct($product_id);
    		$this->items = $items;
    		parent::display($tpl);
    	}

        function getTitleByCategory($id){
            $db = & JFactory::getDbo();
            $sql = "SELECT title FROM #__categories where id=$id";
            $db->setQuery($sql);
            $title = $db->loadResult();
            return $title;
        }
    }
?>