<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_productViewProduct_yourneed extends JView{
    	function display($tpl=null){
    		$yourneed_id = $_GET['yid'];

            $document = & JFactory::getDocument();
            $title = $document->getTitle();
            if($yourneed_id == 62){
                $title_item = VICHY_TITLE_NUTRITIVE;
            }else if($yourneed_id == 63){
                $title_item = VICHY_TITLE_REDUCE_OIL;
            }else if($yourneed_id == 64){
                $title_item = VICHY_TITLE_ANTI_AGING;
            }else{
                $title_item = $this->getTitleByCategory($yourneed_id);    
            }
            
            $this->assignRef('title', $title);
            $this->assignRef('title_item', $title_item);

    		$list_yourneed = Vichy_productHelper::getProductYourNeedById($yourneed_id);
            $this->list_yourneed=$list_yourneed;

            $yourneed = Vichy_productHelper::getYourNeedById($yourneed_id);
            $this->yourneed=$yourneed;
    		parent::display($tpl);
    	}

        function getTitleByCategory($id){
            $db = & JFactory::getDbo();
            $sql = "SELECT title FROM #__categories where id=$id";
            $db->setQuery($sql);
            $title = $db->loadResult();
            return $title;
        }
    }
?>