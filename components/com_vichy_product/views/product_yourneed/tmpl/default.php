<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
    
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);

	$document->addStyleSheet('components/com_vichy_product/css/style.css');	
	$list=$this->list_yourneed;
	$item=$this->yourneed;
	$item_img=array();
	$item_img=json_decode($item->image);
	$link_prod = "index.php?option=com_vichy_product&view=product";

	$color = '';
	switch ($item->main_color) {
		case 'normaderm':
            $color = '#197205';
            break;
        case 'capital':
            $color = '#ff8b06';
            break;
        case 'thermal':
            $color = '#82add8';
            break;
        case 'aqualia_thermal':
            $color = '#95c7ea';
            break;
        case 'bi_white':
            $color = '#c7c7c7';
            break;
        case 'destock':
            $color = '#32d2ae';
            break;
        case 'dercos':
            $color = '#8e090c';
            break;
        case 'liftactiv':
            $color = '#6cafc9';
            break;
        case 'purete_thermal':
            $color = '#95c7ea';
            break;
        case 'aera-mineral':
            $color = '#e2c8af';
            break;
        case 'idealia':
            $color = '#e55482';
            break;
        default :
            /*$color = '#197205';*/
            $color = '#919191';
            break;
    }
	
	$session =& JFactory::getSession();
	$lang = $session->get('lang');
	if($lang == 'vi-VN'){		
		$lbl_text_new = "Mới";
		$lbl_text_favorite = "Yêu thích";
		$lbl_number_review = "người đánh giá";
		$lbl_message = "Sản phẩm đang được cập nhật...Vui lòng chọn mục khác.";
	}else{
		$lbl_text_new = "NEW";
		$lbl_text_favorite = "Top Favorite";
		$lbl_number_review = "reviews";
		$lbl_message = "Our products are being updated ... Please choose another item.";
	}
		

?>


<script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
    $(".dropdown-a").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
        }else
        {
            $('.dropdown-a').find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-plus.png');
            $(this).find('img').attr("src",'<?php echo JURI::root(); ?>components/com_vichy_product/images/btn-minus.png');
        }
    });

</script>
<ul class="mobile-style" style="display:none;">
    <li class="bg-li-menu-mobile active breadrumsp" style="padding-left:0 !important" >
        <!-- <div style="overflow:hidden;max-width:43%; height:40px;float:left;background:#3c72c7">
            <a class="sub-menu-a active" style="padding-right:0"  href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing'); ?>">Nhu cầu của bạn</a>
        </div>
        <div>
            <img style="margin-left:-0.7px;float:left" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu3.png" />   
        </div> -->         
<!--         <div style="overflow:hidden;width:50%; height:40px;float:left;background:#50a5cd"> -->
        <div style="overflow:hidden;width:60%; height:40px;float:left;background:#3c72c7">
            <a class="sub-menu-a active" style="padding-right:0;line-height:25px;padding-left:7% !important"  href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing'); ?>"><?php echo $title_item ?></a>
        </div>
        <div>
            <img style="margin-left:-0.9px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />   
        </div>
    </li> 
</ul> 
<div class="wrap_your_need">
	<!-- <div class="wrap_full_left box_shadow">
		<div class="your_need_left" style="background: url(<?php echo $item_img->background_landing;?>) no-repeat;">
			<div class="title_your_need_left"><?php echo $item->title; ?></div>
		</div>
	</div>
	<div class="buy_product">
		<div class="buy_product_inner">
			<div class="top_buy_product_online">BẠN CÓ THỂ TÌM MUA SẢN PHẨM VICHY TẠI:</div>
			<img src="<?php echo JURI::root() ?>components/com_vichy_product/images/map_product.jpg" />
			<div class="bottom_buy_product_online">
				<div class="buy_product_inner_1">Mua hàng trực tuyến tại website</div>
				<a class="buy_product_inner_offline">Mua Online</a>
				<div class="buy_product_inner_2" style="font-weight:bold">Hay đến cửa hàng gần nhất để được tư vấn chuyên sâu</div>
				<a class="buy_product_inner_offline" href="<?php echo JRoute::_('index.php?option=com_vichy_store&view=store&Itemid=113'); ?>">Đến cửa hàng</a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	 -->
	<link href="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
	<!-- custom scrollbars plugin -->
	<script src="<?php echo JURI::root();?>templates/vichy/scroll_bar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#scroll_yourneed").mCustomScrollbar({
					autoHideScrollbar:false,
					theme:"light-thin"
				});
			});			
		})(jQuery);
		//set css for favorite with lang=en-GB
		jQuery(document).ready(function() {
			var sessionLang = "<?php echo $lang; ?>";
			if(sessionLang == 'en-GB'){
				jQuery('.favorite_featured').css({'font-size':'9px', 'margin-top':'18px'});
			}
		});
	</script>


	<div class="list_yourneed box_shadow">
		<div class="title_list_yourneed" style="background:<?php echo $color; ?>"><?php echo $item->title; ?></div>
		<div class="wrap_yourneed" id="scroll_yourneed">
			<?php
			if(!empty($list)){
				foreach($list as $k => $v){
					$link_prod.="&rid=$v->product_range_id&id=$v->product_id";
					if($v->product_range_id==37)
						$link_prod.="&nostep=1";

					$link_prod .= '&Itemid=103';

			?>
			<div class="item_yourneed">
				<?php if($v->is_new){ ?>
						<div class="product_new"><?php echo $lbl_text_new; ?></div>
						<?php }
						else
						{
							if($v->is_favorite)
							{ ?>
						<div class="product_favorite">
							<div class="favorite_featured"><?php echo $lbl_text_favorite; ?></div>
						</div>
					<?php }
					}
					?>
				<a href="<?php echo JRoute::_($link_prod) ;?>">
					<div class="your_img">
						<img src="<?php echo JURI::root()?>components/com_vichy_product/uploads/products/<?php echo $v->image;?>">
					</div>
					<div class="your_range"><?php echo $v->product_range ;?></div>
					<div class="your_name"><?php echo $v->product_name ; ?></div>
					<div class="your_skin_care"></div>
					
					<div class="your_box_detail">
						<div class="product_star_review">
							<?php for($j=1; $j<=5; $j++){
								 if(!empty($v->rating) && $v->rating >0 && $j<=$v->rating ){?>
								 	<img style="border:none;float:left;" src="<?php echo JURI::root()?>/media/small_star_blue.png">
								 <?php } else {?>
								 	<img style="border:none;float:left;" src="<?php echo JURI::root()?>/media/small_star_grey.png">
								 
							<?php }}?>
						</div>
						<div class="product_number_review"><?php echo (empty($v->reviews))?0 : $v->reviews;?> <?php echo $lbl_number_review; ?></div>
					</div>
					<div class="clear"></div>
					
				</a>
			</div>
			<?php }}
			else {?>
				<div><?php echo $lbl_message; ?></div>
			<?php }?>

			<div class="clear"></div>
		</div>
	</div>
</div>
