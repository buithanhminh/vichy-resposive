<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_product/css/style.css');
?>

<div class="wrap_list_product_ranges">
	<div class="wrap_list_product_ranges_content">

	<?php
	$color = '';
	foreach($this->items as $v){ 
		switch ($v->main_color) {
			case 'normaderm':
				$color = '#197205';
				break;
			case 'capital':
				$color = '#ff8b06';
				break;
			case 'thermal':
				$color = '#82add8';
				break;
			case 'aqualia_thermal':
				$color = '#95c7ea';
				break;
			case 'bi_white':
				$color = '#c7c7c7';
				break;
			// case 'blue-2':
			// 	$color = '#ff8b06';
			// 	break;	
			case 'destock':
				$color = '#32d2ae';
				break;
			case 'dercos':
				$color = '#8e090c';
				break;
			case 'liftactiv':
				$color = '#6cafc9';
				break;
			case 'purete_thermal':
				$color = '#04a0db';
				break;
			case 'aera-mineral':
				$color = '#e2c8af';
				break;
            case 'idealia':
                $color = '#e55482';
                break;
			default :
				$color = '#197205';
				break;
		}
		$data = json_decode($v->params,true);
		$image = (!empty($data['image'])) ? $data['image']: 'components/com_vichy_product/uploads/product_range/bg.png';
	?>
	<a href="<?php 
					/*if($v->total_product==1){
						if($v->parent_id==37)
							echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v->product_id.'&rid='.$v->id.'&no_step=1');
						else
						 	echo JROUTE::_('index.php?option=com_vichy_product&view=product&id='.$v->product_id.'&rid='.$v->id);
					}else {*/
					if($v->parent_id == $this->product_rang_without_step) 
						echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id.'&no_step=1'); 
					else
						echo JROUTE::_('index.php?option=com_vichy_product&view=product_range&rid='.$v->id); 
				//}?>" style="display:block;">				
		<div class="wrap_product_range_item">
			
			<div class="product_range_name" style="background:<?php echo $color; ?>">
				<span><?php echo $v->title; ?></span>
				<span class="desc_range"><?php echo $v->short_desc; ?></span>
			</div>
			<div class="product_range_image">
				<img width="915" src="<?php echo JURI::root().'timbthumb.php?src='.$image.'&w=915&zc=0'; ?>" />
			</div>
		</div>
	</a>
	<?php } ?>
	</div>
</div>
