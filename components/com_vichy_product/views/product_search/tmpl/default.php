<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_product/css/style.css');

	$session =& JFactory::getSession();
    $lang = $session->get('lang');    
    if($lang == 'vi-VN'){
        $lbl_result = "KẾT QUẢ TÌM KIẾM CỦA";
        $alert_message = "Từ khóa bạn đang tìm không có kết quả, hãy thử chọn từ khóa khác";
        $lbl_find_product = "Tìm kiếm sản phẩm";
    }else{
        $lbl_result = "RESULTS OF SEARCH";
        $alert_message = "Tag you're looking for no result, try other keyword selection";
        $lbl_find_product = "Search a product";
    } 
?>
<div class="vichy-search">
	<div class="box-search">
		<div class="box-title">
			<?php echo $lbl_result; ?>: <?php echo $this->filter; ?>
		</div>
		<div class="box-alert">
			<?php echo $alert_message; ?>
		</div>
		<div class="box-input">
			<div class="search-comp">                 
                 <input class="inputbox" type="text" name="query" placeholder="<?php echo $lbl_find_product; ?>" id="autocomplete-ajax-comp" />
                 <input class="btn-search"  type="button" id="btn-search-comp">
                 <div class="clear"></div>
            </div>
		</div>

	</div>
</div>