<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_productControllerProduct extends JController{
    	
    	function __construct(){
    		parent::__construct();    		
    	}

    	function display(){
			JRequest::setVar('view', 'product');

			$session =& JFactory::getSession();
			$lang = $session->get('lang');
			if($lang == 'vi-VN'){
                $title_item = "Tất cả sản phẩm";
            }else{
                $title_item = "Our products";
            }

            self::set_category_id();

            $product_id = $_GET['id'];
			$range_id =$_GET['rid'];

			$Breadcrumb_Item = $this->getBreadcrumbs($product_id,$range_id);
			$title_link="index.php?option=com_vichy_product&view=product_range&rid=$range_id";
			if($Breadcrumb_Item->parent_id == $this->_product_rang_without_step)
				$title_link.="&no_step=1";



			$app	= JFactory::getApplication();
			$pathway = $app->getPathway();
			$pathway->addItem($title_item,'javascript:void(0);');
			$pathway->addItem(shortDesc($Breadcrumb_Item->title,20), JROUTE::_($title_link));
			$pathway->addItem(shortDesc(strip_tags($Breadcrumb_Item->name),17), '');
			parent::display();
		}

		function getBreadcrumbs($product_id,$range_id)
		{
			$db = JFactory::getDbo();
			
			$query="SELECT p.name,c.title,c.parent_id from #__vichy_product p
							inner join #__vichy_product_category pc on pc.product_id=p.id
							inner join #__categories c on c.id = pc.category_id
							where p.id= $product_id and c.id=$range_id";
			
			$db->setQuery($query);
			$result = $db->loadObject();
			
			return $result;
		}

		public function registerEmail()
		{
			$session =& JFactory::getSession();
			$lang = $session->get('lang');
			if($lang == 'vi-VN'){
                $message_register = "Cảm ơn bạn đã đăng ký nhận bản tin.\nChúng tôi sẽ gởi đến bạn những thông tin mới nhất \nvề sản phẩm của Vichy \ntheo e-mail";
                $message_registered = "E-mail đã đăng ký từ trước .\nChúng tôi sẽ gởi đến bạn những thông tin mới nhất \nvề sản phẩm của Vichy \ntheo e-mail";
                $message_error = "Đăng ký không thành công .\n Xin vui lòng thử lại sau.";
            }else{
                $message_register = "Thank you for registering newsletter. \nWe will send you the latest information \nabout the Vichy's products \nthough e-mail";
                $message_registered = "E-mail has been registered in advance. \nWe will send you the latest information \nabout the Vichy's products \nthough e-mail";
                $message_error = "Registration is not successful .\nPlease try again later.";
            }

			$db		= JFactory::getDbo();
			$email  = $_POST['email'];

			$query = " INSERT INTO vc_vichy_subscribe_email (email) values ('$email')";
					  
			
			$db->setQuery($query);
			$res =  $db->query();

			if($res)
				echo "$message_register $email .";
			else
			{
				
				$query ="SELECT count(id) from vc_vichy_subscribe_email where email=$email";
				$db->setQuery($query);
				$res =  $db->query();
				if($res)
					echo "$message_registered $email .";
					else
						echo "$message_error";
			}
			
			exit();
		}
		
    	function autocomplete(){
    		$session =& JFactory::getSession();
			$lang = $session->get('lang');			

	    	if (isset($_GET['query'])) {
			  $text = $_GET['query'];
			}
			else {
			  $text = $_POST['query'];
			}
			if(!empty($text))
			{
				$rows = Vichy_productHelper::onProductSearch($text,'exact','alpha');
				
				
				$suggestions = array();
				//$suggestionids = array();
				if(!empty($rows)){
					foreach($rows as $k => $v) {
						$a = null;
						foreach($rows[$k] as $key => $value)
						{
							switch ($key) {
								case 'name':
									$a->value=$value;
									break;
								case 'id':
									$a->data=$value;
									break;
								case 'catid':
									$a->cid=$value;
									break;
								case 'pcatid':
								{
									$pos = strpos($this->_product_rang, $value);
									if ($pos === false) {
									    $a->isrange=0;
									} else {
									    $a->isrange=1;
									}
									
									break;
								}
								default:
									break;
							}
	//						if($key == 'name')
	//						{
	//							$a->value=$value;
	//						}
	//						if($key == 'id')
	//						{
	//							$a->data=$value;
	//							
	//						}
	//						if($key == 'catid')
	//						{
	//							$a->rid=$value;						
	//						}
								
						}
						$suggestions[] = $a;
					}
				}
				if(empty($suggestions)){
					$a = null;
					if($lang == 'vi-VN'){
						$a->value = 'Không tìm thấy kết quả';					
					}else{
						$a->value = 'No data from search of result';					
					}					
					$suggestions[] = $a;
				}

				$result = array(
				    "query" => "Unit",
				    "suggestions" => $suggestions
				);
				echo(json_encode($result));
				exit();
				
			}

		}

     }
?>