<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_productControllerProduct_yourneed extends JController{
    	function __construct(){
    		parent::__construct();
    		//JRequest::setVar('view','product_yourneed');
    	}
    	function display(){
			JRequest::setVar('view', 'product_yourneed');
			
			$your_id =$_GET['yid'];

			$Breadcrumb_Item = $this->getBreadcrumbs($your_id);
			
			
			$app	= JFactory::getApplication();
			$pathway = $app->getPathway();
			
			$pathway->addItem(shortDesc($Breadcrumb_Item->title,20), '');
			parent::display();
		}

		function getBreadcrumbs($your_id)
		{
			$db = JFactory::getDbo();
			
			$query="SELECT c.title from  vc_categories c
							where c.id=$your_id";

			$db->setQuery($query);
			$result = $db->loadObject();

			return $result;
		}
    }
?>