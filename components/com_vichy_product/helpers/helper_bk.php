<?php

	defined('_JEXEC') or die;

	class Vichy_productHelper {
		function getCategoriesByGroup($group){
			$db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
        	$sql = "SELECT id, title, alias, main_color,description, image, title_description, params from #__categories where published > 0 and parent_id = $group and language = '".str_replace(' ', '', $lg->getTag())."'";
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
		}
        function getProductByCate($cate_id){
        	$db = & JFactory::getDbo();
        	$sql = "SELECT p.id, p.name, p.image, p.rating, p.reviews, sl.title as category from vc_vichy_product as p left join (select pc.product_id, pc.category_id, c.title from vc_vichy_product_category as pc inner join vc_categories as c on pc.category_id = c.id where c.parent_id = 8) as sl on p.id = sl.product_id where p.published = 1 and sl.category_id = ".$cate_id;
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObjectList();
        }
        function getProduct($product_id){
            $db = & JFactory::getDbo();
            // $query = "SELECT p.* from vc_vichy_product as p left join vc_vichy_product_category as c on p.id = c.product_id where p.id = $product_id";
            // $query_count = "select if(count(c.id)>1,1,0) as hasSkincare from vc_vichy_product_category c where c.product_id=$product_id";
            
            // $db->setQuery($query_count);
            // $result = $db->loadObject();            
            // $hasSkincare= $result->hasSkincare;
            // $query="SELECT tb1.*,vc.title as product_range,vc.main_color 
            //         from (
            //                 SELECT p.*,c.main_color";         
         
            // if($hasSkincare)
            //     $query.=",Group_concat(c.title SEPARATOR ' & ') as skincare";                 
            // $query.=" from vc_vichy_product as p 
            //                     left join vc_vichy_product_category as pc on p.id = pc.product_id 
            //                     join vc_categories c on pc.category_id=c.id 
            //                     where p.id=$product_id";
            // if($hasSkincare)
            //     $query.=" and c.parent_id=14";
            // $query.=" GROUP BY c.parent_id
            //              ) as tb1 
            //             join vc_vichy_product_category vpc on tb1.id=vpc.product_id 
            //             join vc_categories vc on vpc.category_id = vc.id 
            //             where vc.parent_id=8
            // ";
            $query = "SELECT tb1.id as pid ,tb1.name,tb1.image,tb1.main_color,tb1.rating,tb1.reviews,tb1.description,
                            Group_Concat(tb1.skincare) as skincare,
                            Group_Concat(tb1.product_range) as product_range
                        from  (
                                select p.*,c.main_color,
                                        case when c.parent_id =14 then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id=8 then c.title end as product_range
                                        from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c
                                        where p.id = pc.product_id 
                                            and c.id = pc.category_id
                                            and p.id=$product_id
                                        Group by c.parent_id
                               ) as tb1 
                               group by tb1.id
                     ";
            $db->setQuery($query);
           
            $result = $db->loadObject();
            return $result;
        }
        
		function getProductRangById($productrange_id){
            $db = & JFactory::getDbo();
			$lg = &JFactory::getLanguage();
        	$sql = "SELECT id, title, title_description, alias, description, image, thumbnail, main_color from #__categories where published > 0 and id = $productrange_id";
        	$db->setQuery($sql);
        	$db->query();
        	return $db->loadObject();
        }

        function getNewProducts($start,$display)
        {
            $db = & JFactory::getDbo();

            // $query_new="SELECT tb1.name as product_name,tb1.pid,tb1.cid,tb1.is_new,tb1.title as product_range,tb1.rating,tb1.reviews,tb1.image,tb2.title as skincare 
            //             from (
            //                     (
            //                         select sl.*,vt.parent_id as type 
            //                             from (
            //                                     select p.id as pid,p.name,c.title,p.rating,p.reviews,p.image,p.is_new,c.id as cid
            //                                         from vc_vichy_product p 
            //                                         join vc_vichy_product_category pc on p.id=pc.product_id 
            //                                         join vc_categories c on c.id=pc.category_id 
            //                                         where p.is_new=1
            //                                     ) as sl 
            //                             join vc_categories vt on sl.cid = vt.id 
            //                             where vt.parent_id in (8)
            //                     )as tb1 
            //             left join (
            //                 select sl.*,vt.parent_id as type 
            //                     from (
            //                             select p.id as pid,c.title,c.id as cid
            //                                 from vc_vichy_product p 
            //                                 join vc_vichy_product_category pc on p.id=pc.product_id 
            //                                 join vc_categories c on c.id=pc.category_id 
            //                                 where p.is_new=1
            //                             ) as sl 
            //                     join vc_categories vt on sl.cid = vt.id 
            //                     where vt.parent_id in (14)
            //                         ) as tb2 on tb1.pid =tb2.pid
            //                 ) limit ".$start.",".$display;

            $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
                                Group_Concat(tb1.skincare) as skincare,
                                Group_Concat(tb1.product_range) as product_range
                            from  (
                                    select p.*,c.main_color,c.id as cid,
                                        case when c.parent_id =14 then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id=8 then c.title end as product_range
                                    from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

                                    where p.id = pc.product_id 
                                        and c.id = pc.category_id
                                        and p.is_new=1
                                    Group by p.id,c.parent_id
                                   ) as tb1 
                                 group by tb1.id 
                                 limit  ".$start.",".$display 
                            ;
        
            $db->setQuery($query_new);
            $list_data = $db->loadObjectList();
            // print_r($query_new);die();
            return $list_data;
        }

        function getFavoriteProducts($start,$display)
        {
            $db = & JFactory::getDbo();
            
            // $query_new="SELECT tb1.name as product_name,tb1.pid,tb1.cid,tb1.is_new,tb1.title as product_range,tb1.rating,tb1.reviews,tb1.image,tb2.title as skincare
            //             from (
            //                     (
            //                         select sl.*,vt.parent_id as type 
            //                             from (
            //                                     select p.id as pid,p.name,c.title,p.rating,p.reviews,p.image,p.is_new,c.id as cid
            //                                         from vc_vichy_product p 
            //                                         join vc_vichy_product_category pc on p.id=pc.product_id 
            //                                         join vc_categories c on c.id=pc.category_id 
            //                                         where p.is_favorite=1
            //                                     ) as sl 
            //                             join vc_categories vt on sl.cid = vt.id 
            //                             where vt.parent_id in (8)
            //                     )as tb1 
            //             left join (
            //                 select sl.*,vt.parent_id as type 
            //                     from (
            //                             select p.id as pid,c.title,c.id as cid
            //                                 from vc_vichy_product p 
            //                                 join vc_vichy_product_category pc on p.id=pc.product_id 
            //                                 join vc_categories c on c.id=pc.category_id 
            //                                 where p.is_new=1
            //                             ) as sl 
            //                     join vc_categories vt on sl.cid = vt.id 
            //                     where vt.parent_id in (14)
            //                         ) as tb2 on tb1.pid =tb2.pid
            //                 ) limit ".$start.",".$display;
            $query_new = "SELECT tb1.id as pid,tb1.cid,tb1.name as product_name,tb1.image,tb1.main_color,tb1.description,tb1.is_new,tb1.rating,tb1.reviews,
                                Group_Concat(tb1.skincare) as skincare,
                                Group_Concat(tb1.product_range) as product_range
                            from  (
                                    select p.*,c.main_color,c.id as cid,
                                        case when c.parent_id =14 then Group_Concat(c.title) end as skincare,
                                        case when c.parent_id=8 then c.title end as product_range
                                    from vc_vichy_product_category pc, vc_vichy_product p ,vc_categories c

                                    where p.id = pc.product_id 
                                        and c.id = pc.category_id
                                        and p.is_favorite=1
                                    Group by p.id,c.parent_id
                                   ) as tb1 
                                 group by tb1.id
                                 limit ".$start.",".$display 
                            ;
        
        
            $db->setQuery($query_new);
            $list_data = $db->loadObjectList(); 
            
            return $list_data;
        }

        function countNewProducts(){

             $db = & JFactory::getDbo();

             $query= "SELECT COUNT(p.id) from vc_vichy_product p where p.is_new =1";

            $db->setQuery($query);
            $result = $db->loadObject();
            
            return $result->count;
            

        }

        function countFavoriteProducts(){

            $db = & JFactory::getDbo();

            $query= "SELECT COUNT(p.id) from vc_vichy_product p where p.is_favorite =1";


            $db->setQuery($query);
            $result = $db->loadObject();
           
            return $result->count;
        }
    }
    
?>