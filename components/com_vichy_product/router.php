<?php
defined( '_JEXEC' ) or die;

function Vichy_productBuildRoute(&$query){
	$segments = array();
	if(isset($query['view'])){

		if($query['view'] == 'product_yourneed'){
			$db = JFactory::getDbo();
			$db->setQuery("SELECT `path` from #__categories where id = ".$query['yid']);
			$path =explode('/',$db->loadObject()->path);

			switch ($path[0]) {
						case 'your-needs':
							$segments[] = 'nhu-cau-cua-ban';
							break;
						case 'your-needs-en':
							$segments[] = 'your-needs';
							break;
						case 'skin-care':
							$segments[] = 'cham-soc-da';
							break;
						case 'skin-care-en':
							$segments[] = 'skin-care';
							break;
						default:
							$segments[]=$path[0];
							break;
			}
		}else if($query['view'] == 'product_range'){
			$curLanguage = JFactory::getLanguage();
			$language_tag = $curLanguage->getTag();

			if($language_tag == 'vi-VN'){
				$segments[] = 'dong-san-pham';
			}else{
				$segments[] = 'product-range';
			}
		}else if($query['view'] == 'product_landing'){
			$segments[] = '';

		}else{
			$segments[] = 'vichy';
		}

		unset($query['view']);
    };

    if(isset($query['no_step'])){

		$segments[] = 'nt';
		unset($query['no_step']);
    }

    $y_rid= (isset($query['rid']))?$query['rid']:((isset($query['yid']))?$query['yid']:'');
    if(!empty($y_rid))
    {
    	$db = JFactory::getDbo();
		$db->setQuery("SELECT `alias` from #__categories where id = ".$y_rid);
		$alias = $db->loadObject()->alias;
		$segments[]=$alias;
    }

		unset($query['yid']);
    unset($query['rid']);

    if(isset($query['id'])){
    	$db = JFactory::getDbo();
		$db->setQuery("SELECT `name` from #__vichy_product where id = ".$query['id']);
		$alias = $db->loadObject();
		$slug = mb_strtolower(url_title(removesign($alias->name)));
		$segments[] = $slug.':'.$query['id'];
		unset($query['id']);
    }

    return $segments;
}

function Vichy_productParseRoute($segments){
	$vars = array();
	foreach ($segments as $k => $v) {
		$segments[$k] = str_replace(':', '-', $segments[$k]);
		if($v == 'nt'){
			$vars['no_step'] = '1';
		}
	}

	// echo '<pre>';
	// print_r($segments);
	// echo '</pre>';
	// die();
	// echo '<pre>';
	// print_r($_GET);
	// echo '</pre>';
	// die();
	switch($segments[0])
	{
		case 'dong-san-pham':
		case 'product-range':
			$vars['view'] = 'product_range';
			break;
		case 'cham-soc-da':
		case 'skin-care':
			$vars['view'] = 'product_yourneed';
			break;
		case 'nhu-cau-cua-ban':
		case 'your-needs':
			$vars['view'] = 'product_yourneed';
			break;
		case '':
			$vars['view'] = 'product_landing';
			break;
		case 'vichy':
			$vars['view'] = 'product';
			$inx = count($segments) - 1;
			$id = explode( ':', $segments[$inx] );
			$vars['id'] = (int) array_pop(explode('-', $id[0]));
			break;
	}
	if($vars['view']!='product_landing')
	{
		$index = ($vars['view']=='product')?(count($segments) - 2):(count($segments) - 1);
		$alias = $segments[$index];
		$db = JFactory::getDbo();
		$db->setQuery("SELECT `id` from #__categories where alias = '{$alias}' and extension ='com_vichy_product'");
		$id = $db->loadObject()->id;
		if($vars['view']=='product_yourneed')
			$vars['yid']=$id;
		else
			$vars['rid']=$id;
	}

	return $vars;
}