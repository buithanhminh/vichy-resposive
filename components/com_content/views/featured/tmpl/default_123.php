<?php
defined('_JEXEC') or die;
$document = &JFactory::getDocument();
$document->addStyleSheet('components/com_content/views/featured/css/style_123.css');
$title = $document->getTitle();

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
if ($language_tag == 'vi-VN') {
    $title_item = VICHY_TITLE_WATER;
} else {
    $title_item = $this->title_item;
}

$document->setTitle($title . ' | ' . $title_item);

function getArticlesByCategory($id)
{
    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
    $db = &JFactory::getDbo();
    if ($language_tag == 'vi-VN') {
        $language = "'*'," . $db->quote($language_tag);
    } else {
        $language = $db->quote($language_tag);
    }

    $sql = "SELECT id, title, alias, introtext, images, created FROM #__content where catid = $id and state > 0 and language in($language) order by ordering";
    $db->setQuery($sql);
    $ret = $db->loadObjectList();
    return $ret;
}

$listArticle = getArticlesByCategory(25);
?>
<link type="text/css" rel="stylesheet"
      href="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/reset.css"/>
<link type="text/css" rel="stylesheet"
      href="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/rcarousel.css"/>
<div class="content swiper-container mobile-style" style="display: none">
    <div class="swiper-wrapper">
        <?php
        foreach ($listArticle as $k => $v) {
            $flag = $k % 2;
            $no_margin_right = '';?>
            <div class="swiper-slide">
        <?php
            if ($flag == '1') $no_margin_right = 'no_margin_right';
            $image = json_decode($v->images);
            if ($v->id == 21) {
                ?>

                <div class="content_right_full_img">
                    <div class="title_text"><?php echo $v->title; ?></div>
                    <div class="about_img_full_img">
                        <img src="<?php echo JURI::root() . (($language_tag == 'en-GB') ? 'images/contents/' : '');
                        echo $image->image_intro; ?>">
                    </div>
                </div>
            <?php } else {
                ?>
                <div class="content_right">
                    <div class="about_text">
                        <div class="title_text"><?php echo $v->title; ?></div>
                        <div class="content_text">
                            <?php echo $v->introtext; ?>
                        </div>
                    </div>
                    <div class="about_img <?php if ($v->id == 19) echo 'custom_article_css'; ?>">
                        <img src="<?php echo JURI::root() . (($language_tag == 'en-GB') ? 'images/contents/' : '');
                        echo $image->image_intro; ?>">
                    </div>
                    <div class="clear"></div>
                </div>

            <?php }?>
            </div>
                <?php
        } ?>
    </div>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next pc-style" style="display: none"></div>
    <div class="swiper-button-prev pc-style" style="display: none"></div>
    <!--	<div id="pages"></div>-->
</div>
<div class="content pc-style">
    <div id="carousel">
        <?php
        foreach($listArticle as $k => $v){
            $flag = $k%2;
            $no_margin_right = '';
            if($flag == '1') $no_margin_right = 'no_margin_right';
            $image = json_decode($v->images);
            if($v->id==21)
            {
                ?>
                <div class="content_right_full_img">
                    <div class="title_text"><?php echo $v->title; ?></div>
                    <div class="about_img_full_img">
                        <img src="<?php echo JURI::root().(($language_tag == 'en-GB') ? 'images/contents/' : '');echo $image->image_intro; ?>">
                    </div>
                </div>
            <?php }else{?>
                <div class="content_right">
                    <div class="about_text">
                        <div class="title_text"><?php echo $v->title; ?></div>
                        <div class="content_text">
                            <?php echo $v->introtext; ?>
                        </div>
                    </div>
                    <div class="about_img <?php if($v->id==19) echo 'custom_article_css';?>">
                        <img src="<?php echo JURI::root().(($language_tag == 'en-GB') ? 'images/contents/' : '');echo $image->image_intro; ?>">
                    </div>
                    <div class="clear"></div>
                </div>
            <?php }
        } ?>
    </div>
    <div id="pages"></div>

    <script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/jquery.ui.core.js"></script>
    <script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/jquery.ui.rcarousel.js"></script>
    <script type="text/javascript">
        jQuery(function($) {
            function generatePages() {
                var _total, i, _link;

                _total = $( "#carousel" ).rcarousel( "getTotalPages" );

                for ( i = 0; i < _total; i++ ) {
                    _link = $( "<a href='#'></a>" );

                    $(_link)
                        .bind("click", {page: i},
                        function( event ) {
                            $( "#carousel" ).rcarousel( "goToPage", event.data.page );
                            event.preventDefault();
                        }
                    )
                        .addClass( "bullet off" )
                        .appendTo( "#pages" );
                }

                // mark first page as active
                $( "a:eq(0)", "#pages" )
                    .removeClass( "off" )
                    .addClass( "on" )
                    .css( "background-image", "url(<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/ac.png)" );

            }

            function pageLoaded( event, data ) {
                $( "a.on", "#pages" )
                    .removeClass( "on" )
                    .css( "background-image", "url(<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/bt.png)" );

                $( "a", "#pages" )
                    .eq( data.page )
                    .addClass( "on" )
                    .css( "background-image", "url(<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/ac.png)" );
            }

            $("#carousel").rcarousel(
                {
                    visible: 1,
                    step: 1,
                    speed: 700,
                    auto: {
                        enabled: false
                    },
                    width:632,
                    height: 450,
                    start: generatePages,
                    pageLoaded: pageLoaded
                }
            );


        });
    </script>
</div>

<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/swiper.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
//        autoplay: 10000,
        autoplayDisableOnInteraction: false,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
</script>