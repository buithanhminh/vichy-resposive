<?php
	defined('_JEXEC') or die;
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

	$document = & JFactory::getDocument();
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);
    
    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
    if($language_tag == 'vi-VN'){
    	$lbl_skin_diagnosis = "KIỂM TRA DA";
    	$lbl_location_skin = "ĐỊA ĐIỂM SOI DA";
    }else{
    	$lbl_skin_diagnosis = "SKIN DIAGNOSIS";
    	$lbl_location_skin = "LOCATION SKIN";
    }

	function getImageCategory($id){
		$db = & JFactory::getDbo();
	    $sql = "SELECT params FROM #__categories where id = $id";
	    $db->setQuery($sql);
	    $ret = $db->loadObject();        
	    return $ret;
	}
	
	function getArticlesByCategory($id){
		$curLanguage = JFactory::getLanguage();
        $language_tag = $curLanguage->getTag();
	    $db = & JFactory::getDbo();
	    if($language_tag == 'vi-VN'){
	        $language = "'*',".$db->quote($language_tag);
	    }else{
	        $language = $db->quote($language_tag);
	    }

	    $sql = "SELECT id, title, alias, introtext, `fulltext`, images, created FROM #__content where catid = $id and state > 0 and language in($language) order by ordering limit 2";
	    $db->setQuery($sql);
	    $ret = $db->loadObjectList();        
	    return $ret;
	}
	$articles = getArticlesByCategory(27);
	$image = json_decode(getImageCategory(27)->params);
?>
<div class="content_right" style="background: url(<?php echo $image->image; ?>) no-repeat 10px center">
<div class="wrap_about_text">
	<?php foreach($articles as $k => $v){ ?>
	<div class="about_text" <?php echo ($k == 1)? 'style="margin-top:50px;"' : ''; ?>>
		<div class="title_text"><?php echo $v->title; ?></div>
		<div class="content_text">
			<?php
				echo $v->introtext.$v->fulltext;
			?>
		</div>
		<?php 
			$link = JROUTE::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107');
			$button = $lbl_skin_diagnosis;
			if($k == 1){
				$link = JROUTE::_('index.php?option=com_vichy_store&Itemid=113');
				$button = $lbl_location_skin;
			}
		?>
		<a class="detail_link" href="<?php echo $link; ?>"><?php echo $button; ?></a>
	</div>
	<?php } ?>
</div>
</div>