<?php
	defined('_JEXEC') or die;
	
	$document = & JFactory::getDocument();
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);
    
    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();

	function getArticlesByCategory($id, $language_tag){		
        $db = & JFactory::getDbo();
        if($language_tag == 'vi-VN'){
	        $language = "'*',".$db->quote($language_tag);
	    }else{
	        $language = $db->quote($language_tag);
	    }

        $sql = "SELECT id, title, alias, introtext, images, created FROM #__content where catid = $id and featured != '1' and state > 0 and language in($language) order by ordering";
        $db->setQuery($sql);
        $ret = $db->loadObjectList();        
        return $ret;
    }
    function getDetailArticle($id, $language_tag){
    	$db = & JFactory::getDbo();
    	
    	if($language_tag == 'vi-VN'){
	        $language = "'*',".$db->quote($language_tag);
	    }else{
	        $language = $db->quote($language_tag);
	    }
        
        $sql = "SELECT id, title, alias, `fulltext`,introtext, images, created FROM #__content where catid = $id and featured = '1' and language in($language) limit 1";
        $db->setQuery($sql);
        $ret = $db->loadObject();        
        return $ret;
    }
    $listArticle = getArticlesByCategory(26, $language_tag);
    $article = getDetailArticle(26, $language_tag);

    function getTranslationID($reference_id, $table, $language){
    	$db = & JFactory::getDbo();
    	$sql = "SELECT translation_id from #__jf_translationmap WHERE reference_id=$reference_id and language='".$language."' and reference_table='".$table."'";
    	$db->setQuery($sql);
    	$translation_id = $db->loadResult();
		
    	return $translation_id;
    }
?>
<script type="text/javascript">
$(document).ready(function(e) {
	 jQuery('.item').hover(function(){
	 	 var el=$(this).find('.item_text').animate({ height: "+=80px"},250)
      
    },function(){
    	var el=$(this).find('.item_text').animate({ height: "-=80px"},250)
      
    });
	jQuery('.pagi_2').hide();
	jQuery('.research_pagi a').click(function(){
		id = jQuery(this).attr('id');
		jQuery('.research_item').hide();
		jQuery('.'+id).show();
	})
	jQuery('.arrow_pagi a').click(function(){
		id = jQuery(this).attr('id');
		jQuery('.research_item').hide();
		jQuery('.'+id).show();
	})
	jQuery('.arrow_pagi2 a').click(function(){
		id = jQuery(this).attr('id');
		jQuery('.research_item').hide();
		jQuery('.'+id).show();
	})
});

</script>
<!-- <div class="content"> -->
<div class="research pc-style">
	<div class="research_item pagi_1">
		<?php
		foreach($listArticle as $k => $v){
			$flag = $k%2;
			$no_margin_right = '';
			if($flag == '1') $no_margin_right = 'no_margin_right';
			$image = json_decode($v->images);
			?>
			<div class="item <?php echo $no_margin_right; ?>">
				<div class="item_img">
					<img width="284" height="238" src="<?php echo JURI::root().(($language_tag == 'en-GB') ? 'images/contents/' : ''); echo $image->image_intro; ?>">
					<div class="item_text" style="height:55px;">
						<div class="title_text"><?php echo $v->title; ?></div>
						<div class="content_text"><?php echo $v->introtext; ?></div>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="clear"></div>
		<div class="research_pagi">
			<a href="javascript:void(0)" id="pagi_1" class="active"></a>
			<a href="javascript:void(0)" id="pagi_2"></a>
		</div>
		<div class="arrow_pagi">
			<a href="javascript:void(0)" id="pagi_1"></a>
			<a href="javascript:void(0)" id="pagi_2"></a>
		</div>
	</div>
	<div class="research_item pagi_2">
		<div class="about_text">
			<div class="title_text_2"><?php echo $article->title; ?></div>
			<div class="content_text_2">
				<?php echo $article->introtext; ?>
			</div>
		</div>
		<!-- <div class="clear"></div> -->
		<div class="research_pagi">
			<a href="javascript:void(0)" id="pagi_1"></a>
			<a href="javascript:void(0)" id="pagi_2" class="active"></a>
		</div>
		<div class="arrow_pagi2">
			<a href="javascript:void(0)" id="pagi_1"></a>
			<a href="javascript:void(0)" id="pagi_2"></a>
		</div>
	</div>

</div>



	<div class="swiper-container mobile-style" style="display: none;">
		<div class="swiper-wrapper">
		<div class="swiper-slide">
			<?php 
				foreach($listArticle as $k => $v){ 
					$flag = $k%2;
					$no_margin_right = '';
					if($flag == '1') $no_margin_right = 'no_margin_right';
					$image = json_decode($v->images);
			?>
			<div class="item <?php echo $no_margin_right; ?>">
				<div class="item_img">
					<img width="284" height="238" src="<?php echo JURI::root().(($language_tag == 'en-GB') ? 'images/contents/' : ''); echo $image->image_intro; ?>">
					<div class="item_text" style="height:55px;">
						<div class="title_text"><?php echo $v->title; ?></div>
						<div class="content_text"><?php echo $v->introtext; ?></div>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="clear"></div>
<!--			<div class="research_pagi">-->
<!--				<a href="javascript:void(0)" id="pagi_1" class="active"></a>-->
<!--				<a href="javascript:void(0)" id="pagi_2"></a>-->
<!--			</div>-->
<!--			<div class="arrow_pagi">-->
<!--				<a href="javascript:void(0)" id="pagi_1"></a>-->
<!--				<a href="javascript:void(0)" id="pagi_2"></a>-->
<!--			</div>-->
		</div>
		<div class="swiper-slide">
			<div class="about_text">
				<div class="title_text_2"><?php echo $article->title; ?></div>
				<div class="content_text_2">
				<?php echo $article->introtext; ?>
				</div>
			</div>
			<!-- <div class="clear"></div> -->
<!--			<div class="research_pagi">-->
<!--				<a href="javascript:void(0)" id="pagi_1"></a>-->
<!--				<a href="javascript:void(0)" id="pagi_2" class="active"></a>-->
<!--			</div>-->
<!--			<div class="arrow_pagi2">-->
<!--				<a href="javascript:void(0)" id="pagi_1"></a>-->
<!--				<a href="javascript:void(0)" id="pagi_2"></a>-->
<!--			</div>-->
		</div>
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-next pc-style" style="display: none"></div>
		<div class="swiper-button-prev pc-style" style="display: none"></div>
	</div>
<!-- </div> -->

<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/swiper.min.js"></script>
<script>
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
        autoplay: 10000,
		autoplayDisableOnInteraction: false,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
	});
</script>