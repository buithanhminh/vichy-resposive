<?php
	defined('_JEXEC') or die;
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
	$document = & JFactory::getDocument();
	$title = $document->getTitle();

	$curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
    if($language_tag == 'vi-VN'){
    	$title_item = VICHY_TITLE_IDEAL_SKIN;	
    }else{
    	$title_item = $this->title_item;
    }
    
    $document->setTitle($title.' | '.$title_item);

	function getArticleByCategory($id){
		$curLanguage = JFactory::getLanguage();
        $language_tag = $curLanguage->getTag();
	    $db = & JFactory::getDbo();
	    if($language_tag == 'vi-VN'){
	        $language = "'*',".$db->quote($language_tag);
	    }else{
	        $language = $db->quote($language_tag);
	    }

	    $sql = "SELECT id, title, alias, introtext, `fulltext`, images, created FROM #__content where catid = $id and state > 0 and language in($language) order by ordering limit 1";
	    $db->setQuery($sql);
	    $ret = $db->loadObject();        
	    return $ret;
	}
	$article = getArticleByCategory(24);
	$link_background = JURI::root().'components/com_content/views/featured/images/bg_lytuong.png';
?>
<div class="content_right pc-style" style="background:url('<?php echo JURI::root().'timbthumb.php?src='.$link_background.'&w=685&h=466&zc=0';?>') no-repeat center;">
	<div class="about_text">
		<div class="title_text"><?php echo $article->title; ?></div>
		<div class="content_text">
			<?php
				echo $article->introtext.$article->fulltext;
			?>
		</div>
	</div>
</div>
<div class="content_right mobile-style" >
	<div class="about_text">
		<div class="title_text"><?php echo $article->title; ?></div>
		<div class="content_text">
			<?php
				echo $article->introtext.$article->fulltext;
			?>
		</div>
	</div>
</div>