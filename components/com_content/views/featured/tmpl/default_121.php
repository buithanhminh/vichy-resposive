<?php
	defined('_JEXEC') or die;
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
	$document = & JFactory::getDocument();
    $title = $this->title;
    $title_item = $this->title_item;
    $document->setTitle($title.' | '.$title_item);
    
	function getArticlesByCategory($id){
        $curLanguage = JFactory::getLanguage();
        $language_tag = $curLanguage->getTag();
	    $db = & JFactory::getDbo();
        if($language_tag == 'vi-VN'){
            $language = "'*',".$db->quote($language_tag);
        }else{
            $language = $db->quote($language_tag);
        }

	    $sql = "SELECT id, title, alias, introtext, images, created FROM #__content where catid = $id and state > 0 and language in($language) order by ordering";
	    $db->setQuery($sql);
	    $ret = $db->loadObjectList();        
	    return $ret;
	}
$article = getArticlesByCategory(23);
?>
<div class="history-content">
    <div class="history-right-content">
        <?php $image = json_decode($article[0]->images); ?>
        <div class="img_history">
        <img src="<?php echo JURI::root()?>timbthumb.php?src=<?php echo ($language_tag == 'en-GB') ? 'images/contents/' : '';echo $image->image_intro; ?>&w=198&h=198&q=100&zc=0" alt="<?php echo $image->image_intro_alt; ?>">
        </div>
        <div class="title_history"><?php echo $article[0]->title; ?></div>
        <div class="info_history">
        <?php echo strip_tags($article[0]->introtext); ?>
        </div>
    </div>
    <div class="history-controls-content">
        <div class="time-line">
        <?php foreach($article as $k => $v){ ?>
        <?php $class = ($k==0)?'current':''; ?>
        <div class="time-line-item">
            <div id="<?php echo $v->id; ?>" class="ball-time <?php echo $class ?>" ></div>
            <div class="year"><?php echo $v->title; ?></div>
        </div>
        <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
    	jQuery('.menu-left li:first-child').addClass('current active');
        jQuery('.ball-time').click(function(){
            var container = jQuery(this);
            history_id = container.attr('id');
            jQuery.ajax({
                type : 'POST',
                url : '<?php echo JURI::root();?>index.php?option=com_content&task=article.getEventById',
                data : 'id=' + history_id,
                success : function(kq) {
                    data = jQuery.parseJSON(kq);
                    //animate title 
                    jQuery('.title_history').fadeOut('500',function(){
                        jQuery('.title_history').text(data.title);
                    });
                    jQuery('.title_history').fadeIn('500');

                    //animate info 
                    jQuery('.info_history').fadeOut('500',function(){
                        jQuery('.info_history').text(data.introtext);
                    });
                    jQuery('.info_history').fadeIn('500');

                    //animate image
                    jQuery('.img_history').children('img').fadeOut('500',function(){
                        jQuery('.img_history').children('img').attr('src',data.img_src);
                        jQuery('.img_history').children('img').attr('alt',data.img_alt);
                    });
                    jQuery('.img_history').children('img').bind('load',function(){
                        jQuery('.img_history').children('img').fadeIn('500');
                    });
                    
                    jQuery('.ball-time').removeClass('current');
                    container.addClass('current');
                    
                }
            });
            
        })
    })
</script>