<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_services/css/style.css');
	$document->addStyleSheet('components/com_vichy_services/css/nivo-slider.css');	
?>
<div class="vichy_home_main">
	<div class ="vichy_home_slide_top box_shadow">
		<div class="box">
			<a class="link_carousel" href="<?php echo JRoute::_($this->slide_items[0]->link); ?>">
				<div id="carousel">
				    <?php foreach ($this->slide_items as $v) { ?>
				            <img data-link="<?php echo JRoute::_($v->link); ?>" src="<?php echo JURI::root().'components/com_vichy_home/uploads/slide_show/'.$v->image;?>" alt="Banner" width="926" height="336">
				    <?php }	?>
				</div>
			</a>
			<div class="nivo-controlNav">
				<?php foreach($this->slide_items as $k=>$v): ?>
					<?php if($k == 0): ?>
			      		<a href="#" class="selected"><span></span></a>
			    	<?php else: ?>
			    		<a href="#" class=""><span></span></a>
			    	<?php endif; ?>
			    <?php endforeach; ?>
			</div>
		</div>
	</div>
	<div class = "vichy_home_product">
		<!-- product left -->
		<div class = "vichy_home_common vichy_home_large box_shadow">
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_LEFT]->link; ?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_LEFT]->image ;?>">
			</a>
		</div>
		<!-- product right -->
		<div class = "vichy_home_common vichy_home_large box_shadow" style="margin-right:0" >
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_RIGHT]->link; ?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_RIGHT]->image ;?>">
			</a>
		</div>
		<!-- advice diagnostics -->
		<div class="vichy_home_common vichy_home_large box_shadow">
			<a href="<?php echo JURI::root() ?>index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107&lang=vi">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/images/vichy_advice.png">
			</a>
		</div>	
	
		<!-- fanpage -->
		<div class="vichy_home_common vichy_home_small box_shadow">
			<img src="<?php echo JURI::root() ?>components/com_vichy_home/images/header_fanpage.png">
			<iframe src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/vichy.com.vn&amp;locale=vi_VN&amp;width=206&amp;height=312&amp;show_faces=true&amp;colorscheme=light&amp;show_border=false&amp;header=true&amp;size=small" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:206px; height:312px;" allowtransparency="true"></iframe>
		</div>

		<!--store location -->
		<div class="vichy_home_common vichy_home_small box_shadow" style="margin-right:0">
			<a href="<?php echo JURI::root() ?>index.php?option=com_vichy_store&view=store&Itemid=113&lang=vi">
				<img src="<?php echo JURI::root() ?>/components/com_vichy_home/images/Vichy_Home_03.png">
			</a>
		</div>

		<!-- blog -->
		<?php $image = json_decode($this->list_category->images) ;?>
		<div class ="vichy_home_common vichy_home_large box_shadow home_blog">
			<a href="<?php echo JRoute::_('index.php?option=com_content&view=article&Itemid=109&catid='.$this->list_category->catid.'&id='.$this->list_category->id); ?>">
			<div class="img_blog">
					<img src="<?php echo JURI::root() ?><?php echo $image->image_intro;?>">
			</div>
			<div class="text_blog">
				<div class="title_blog"><?php echo shortDesc($this->list_category->title,30)?></div>
				<div class="des_blog"><?php echo  shortDesc(strip_tags($this->list_category->introtext),120); ?></div>
			</div>
			</a>
		</div>

		<!-- book by phone -->
		<div class="vichy_home_common vichy_home_small box_shadow">
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1]->link; ?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1]->image ;?>">
			</a>
			<!--<a href="javascript:void(0);" >
				<!-- <img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php //echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1]->image ;?>"> -->
				<!--<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/box_left.png"/>
			</a>-->
			</div>

		<!--book online -->
		<div class="vichy_home_common vichy_home_small box_shadow" style="margin-right:0">
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2]->link; ?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2]->image ;?>">
			</a>
		</div>

	</div>
</div>

<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/home_js.js"></script>