<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_servicesViewServices extends JView{
    	function display($tpl=null){
            $category= Vichy_homeHelper::getCategory();
            $this->list_category=$category;

    		$slide_items = Vichy_homeHelper::getSlideShow();
    		$this->slide_items = $slide_items;

    		$other_items = Vichy_homeHelper::getOtherImages();		
            $item;
    		$others =array();
    		foreach ($other_items as $v) {                
    			$others[$v->position]->link = $v->link;
                $others[$v->position]->image = $v->image;
    		}

    		$this->listImages = $others;

    		parent::display($tpl);
    	}
    }
?>