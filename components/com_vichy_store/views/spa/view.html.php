<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.view');
    
    class Vichy_storeViewSpa extends JView{
    	function display($tpl=null){
    		$this->spa_hcm = Vichy_storeHelper::getSpa(67);
    		$this->spa_hn = Vichy_storeHelper::getSpa(66);
    		$introduction = Vichy_storeHelper::getIntroductionSpa();
    		$this->introduction=$introduction;
    		
    		parent::display($tpl);
    	}
    }
?>