<?php
    defined('_JEXEC') or die;
    $active = (isset($_GET['cid'])) ? $_GET['cid'] : '3';
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_store/css/spa.css');
    $document->addStyleSheet('components/com_vichy_store/css/spa_'.$active.'.css');

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
    if($language_tag == 'vi-VN'){       
        $lbl_introduction = "GIỚI THIỆU";
        $lbl_service = "DỊCH VỤ";
    }else{
        $lbl_introduction = "INTRODUCTION";
        $lbl_service = "SERVICE";
    }
?>

<div class="vichy-spa row no-width-height">
    <div class="content-left col-xs-12 col-sm-4 no-width-height pc-style">
        <ul class="menu-left">
            <li> <a <?php echo ($active=='3') ? 'class="active"' : '' ?> href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=3&Itemid=108'); ?>"><?php echo $lbl_introduction; ?></a></li>
            <li> <a <?php echo ($active=='2' || $active == '4') ? 'class="active"' : '' ?> href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=2&Itemid=108'); ?>"><?php echo $lbl_service; ?></a></li>
            <!--  <li> <a <?php echo ($active=='1') ? 'class="active"' : '' ?> href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=1&Itemid=108'); ?>">VỊ TRÍ</a></li>
            -->
        </ul>
    </div> <!-- content left -->

<!--    <nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">-->
<!--        <div class="container">-->
<!--            <!-- Brand and toggle get grouped for better mobile display -->
<!--            <div class="navbar-header sub-menu-spa-header">-->
<!---->
<!--            </div>-->
<!---->
<!--            <!-- Collect the nav links, forms, and other content for toggling -->
<!--            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
<!--                <ul class="nav navbar-nav" style="margin-top:0px;">-->
<!--<!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
<!--<!--                    <li><a href="#">Link</a></li>-->
<!--                    <li class="bg-li-menu-mobile1" style="margin-bottom:1px;"> <a class="sub-menu-a --><?php //echo ($active=='3') ? 'active' : '' ?><!--"  href="--><?php //echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=3&Itemid=108'); ?><!--">--><?php //echo $lbl_introduction; ?>
<!--                        </a></li>-->
<!--                    <li class="bg-li-menu-mobile1"> <a class="sub-menu-a --><?php //echo ($active=='2' || $active == '4') ? 'active' : '' ?><!--"  href="--><?php //echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=2&Itemid=108'); ?><!--">--><?php //echo $lbl_service; ?><!--</a></li>-->
<!--                </ul>-->
<!--            </div><!-- /.navbar-collapse -->
<!--        </div>-->
<!--    </nav>-->


    <nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">
        <div class="container">
            <div class="navbar-header sub-menu-spa-header">
                <a class="navbar-brand" href="#" style="color:white;">Vichy Spa</a>
                <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                </button>
            </div>
            <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">
                    <!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
                    <!--                    <li><a href="#">Link</a></li>-->
                    <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a <?php echo ($active=='3') ? 'active' : '' ?>"  href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=3&Itemid=108'); ?>"><?php echo $lbl_introduction; ?>
                        </a></li>
                    <li class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"> <a style="padding-left:15px!important;" class="sub-menu-a <?php echo ($active=='2' || $active == '4') ? 'active' : '' ?>"  href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=2&Itemid=108'); ?>"><?php echo $lbl_service; ?></a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <ul class="mobile-style" style="display: none;">
        <li class="bg-li-menu-mobile <?php echo ($active=='3') ? 'active' : '' ?>" style="margin-bottom:1px;"> 
            <div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7">
                <a class="sub-menu-a <?php echo ($active=='3') ? 'active' : '' ?>"  href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=3&Itemid=108'); ?>"><?php echo $lbl_introduction; ?></a>
            </div>
            <div>
            <img style="margin-left:-0.7px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />
            </div>
        </li>
        <li class="bg-li-menu-mobile <?php echo ($active=='2' || $active == '4') ? 'active' : '' ?>"> 
            <div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7">
                <a class="sub-menu-a <?php echo ($active=='2' || $active == '4') ? 'active' : '' ?>"  href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=2&Itemid=108'); ?>"><?php echo $lbl_service; ?></a>
            </div>
            <div>
                <img style="margin-left:-0.7px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />
            </div>
        </li>
    </ul>
    <div class="content-right col-xs-12 col-sm-8 no-width-height" style="padding-left:0;padding-right:0">

        <?php
            $defaultPath = JPATH_COMPONENT_SITE.'/'.'views/spa/tmpl/default_'.$active.'.php';
            jimport('joomla.filesystem.file');
            include($defaultPath);
        ?>
    </div> <!-- content right -->
</div>
<script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
</script>