<?php
	$document = & JFactory::getDocument();

    $title = ucwords($document->getTitle());

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();

    $title_item = ($language_tag == 'vi-VN') ? VICHY_TITLE_INTRODUCTION : VICHY_TITLE_INTRODUCTION_EN;
    
    $title = $title;
    $title_item = $title_item;
    $document->setTitle($title.' | '.$title_item);

    $introduction=$this->introduction;
?>
<div class="spa_right no-width-height">
	<div class="spa_text no-width-height">
		<div class="intro_text"></div>
		<div class="content_text content_text-intro"><?php echo $introduction->introtext; ?></div>
		<div class="title_text"></div>
		<div class="sub_text"></div>
	</div>
</div>