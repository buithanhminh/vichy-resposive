<?php
	defined('_JEXEC') or die;
	$document = & JFactory::getDocument();
	
	$id=$_GET['id'];

    $session =& JFactory::getSession();
	$lang = $session->get('lang');
	if($lang == 'vi-VN'){
		$title = VICHY_TITLE_SERVICE;
	}else{
		$title = VICHY_TITLE_SERVICE_EN;
	}

    $title_item = '';
    
    switch ($id) {
    	case '70':
    		$title_item = VICHY_SERVICE_MAINTAIN;
    		break;
    	case '71':
    		$title_item = VICHY_SERVICE_ENHANCE;
    		break;
    	case '72':
    		$title_item = VICHY_SERVICE_PROVICE;
    		break;
    	case '74':
    		$title_item = VICHY_SERVICE_RECOVERY;
    		break;
    	case '75':
    		$title_item = VICHY_SERVICE_TREATMENT;
    		break;
    	case '77':
    		$title_item = VICHY_SERVICE_LOTION;
    		break;
    	case '79':
    		$title_item = VICHY_SERVICE_REJUVENATION;
    		break;
    	case '81':
    		$title_item = VICHY_SERVICE_CLEAN;
    		break;
    	case '82':
    		$title_item = VICHY_SERVICE_ACNE_TREATMENT;
    		break;
    	case '84':
    		$title_item = VICHY_SERVICE_OLD_CONTROL;
    		break;
    	case '85':
    		$title_item = VICHY_SERVICE_SKIN_LIGHT;
    		break;
    	case '86':
    		$title_item = VICHY_SERVICE_ACNE_MEN;
    		break;
    	case '88':
    		$title_item = VICHY_SERVICE_REDUCE_WRINKLES;
    		break;
    	case '90':
    		$title_item = VICHY_SERVICE_SHOULDER_ARMS;
    		break;
    	case '91':
    		$title_item = VICHY_SERVICE_BLACK_WHITE;
    		break;
    	default:
    		$title_item = getTitleByCategory($id);
    		break;
    }
    
    $document->setTitle($title.' | '.$title_item);

	function getTitleByCategory($id){
        $db = & JFactory::getDbo();
        $sql = "SELECT title FROM #__categories where id=$id";
        $db->setQuery($sql);
        $title = $db->loadResult();
        return $title;
    }

	function getCategoryServicesContent($id){
        $db = & JFactory::getDbo();
        $sql = "SELECT c.rgt, c.id,c.description, c.title as child_title,parent.title as parent_title, c.params, parent.main_color from  vc_categories as c join vc_categories parent on c.parent_id=parent.id where c.parent_id=(SELECT vc_categories.parent_id from vc_categories where vc_categories.id = $id) and c.published=1 order by c.rgt";
        $db->setQuery($sql);
        $ret = $db->loadObjectList();        
        return $ret;
    }
   	$services= getCategoryServicesContent($id);
   	$arr2=json_decode($services[0]->params);
?>
<div class="spa_right_detail no-width-height pc-style" style="background:url(<?php echo JURI::root().'timbthumb.php?src='.$arr2->image.'&w=764&h=530&zc=1';?>) no-repeat">
	<div class="content_text content_text-detail no-width-height">
		
		<?php
		$current = '';
		foreach($services as $k => $v){
			if($v->id == $id){
				$current = $k;
			}
		?>
		<div class="wrapper_content no-width-height">
			<div class="padding-top-class2 title_text <?php echo $v->main_color; ?>"><?php echo $v->parent_title ?></div>
			<div class="width50 sub_text sub_<?php echo $v->main_color; ?>"><?php echo $v->child_title ?></div>
			<div class="padding-top-class des_text"><?php echo $v->description ?></div>
	</div>
		<?php } ?>
		
	</div>
	<div id="pages" class="paging"></div>
</div><?php if(count($services) > 1){ ?>
<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/jquery.ui.rcarousel.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		var _total ;
		function generatePages() {
			var i, _link;
			_total= $( ".content_text" ).rcarousel( "getTotalPages" );
			
			for ( i = 0; i < _total; i++ ) {
				_link = $( "<a href='#'></a>" );

				$(_link)
					.bind("click", {page: i},
						function( event ) {
							$( ".content_text" ).rcarousel( "goToPage", event.data.page );
							event.preventDefault();
						}
					)
					.addClass( "bullet off" )
					.appendTo( "#pages" );
			}
			
			// mark first page as active
			$( "a:eq(<?php echo $current; ?>)", "#pages" )
				.removeClass( "off" )
				.addClass( "on" )
				.css( "background-image", "url(<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/ac.png)" );

		}

		function pageLoaded( event, data ) {
			var tmp=<?php echo $current;?>+data.page;
			var index = tmp%_total;
			
			$( "a.on", "#pages" )
				.removeClass( "on" )
				.css( "background-image", "url(<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/bt.png)" );

			$( "a", "#pages" )
				.eq(index)
				.addClass( "on" )
				.css( "background-image", "url(<?php echo JURI::root(); ?>components/com_content/views/featured/button_jcaurosel/ac.png)" );
		}
		
		$(".content_text").rcarousel(
			{
				visible: 1,
				step: 1,
				speed: 1000,
				auto: {
					enabled: true,
					direction: "next",
					interval: 10000
				},
				startAtPage: <?php echo $current; ?>,
				width:355,
				height: 479,
				// orientation: "horizontal",
				start: generatePages,
				pageLoaded: pageLoaded
			}
		);
		
		
	});
</script>
<?php } ?>


<div class="spa_right_detail swiper-container no-width-height mobile-style" >
	<div class="content_text-detail swiper-wrapper no-width-height">
		<?php
		$current = '';
		foreach($services as $k => $v){
			if($v->id == $id){
				$current = $k;
			}
			?>
			<div class="swiper-slide no-width-height" style="position: relative;">
                <div class="dichvu-des">
                    <div class="padding-top-class2 title_text <?php echo $v->main_color; ?>"><?php echo $v->parent_title ?></div>
                    <div class="width50 spaheight sub_text sub_<?php echo $v->main_color; ?>"><?php echo $v->child_title ?></div>
                </div>
                <img width="100%" src="<?php echo JURI::root().'timbthumb.php?src='.$arr2->image.'&w=764&h=530&zc=1';?>" style="margin-left: -15px; margin-bottom: 10px"/>
                <div class="padding-top-class des_text"><?php echo $v->description ?></div>
			</div>
		<?php } ?>
	</div>
	<div class="swiper-pagination"></div>
	<div class="swiper-button-next pc-style" style="display: none"></div>
	<div class="swiper-button-prev pc-style" style="display: none"></div>
</div>

<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/swiper.min.js"></script>
<script>
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
//        autoplay: 10000,
		autoplayDisableOnInteraction: false,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
	});
</script>