<?php
	defined('_JEXEC') or die;
	$document = & JFactory::getDocument();

	function getCategoryServices(){
        $db = & JFactory::getDbo();
        $session =& JFactory::getSession();
		$lang = $session->get('lang');
        if($lang == 'vi-VN'){
            $language = "'*',".$db->quote($lang);            
        }else{
            $language = $db->quote($lang);            
        }

        $sql = "SELECT c.rgt, g.id,c.parent_id as grand_id,g.title as child_title,g.parent_id,c.title,c.main_color from #__categories as c Join #__categories as g On c.id=g.parent_id where c.published = 1 and g.published = 1 and LOCATE('com_vichy_services', c.extension) > 0 and c.language in($language)  ORDER BY g.rgt ";
        $db->setQuery($sql);
        $ret = $db->loadObjectList();        
        return $ret;
    }
    $list_service = getCategoryServices();
    $arr = array();
	foreach ($list_service as $v) {
		$pa=$v->parent_id;
		$arr[$pa][] = $v;
	}
	
	$session =& JFactory::getSession();
	$lang = $session->get('lang');
	if($lang == 'vi-VN'){		
		$lbl_detail = "Chi tiết";
		$title_item = VICHY_TITLE_SERVICE;
	}else{
		$lbl_detail = "Detail";
		$title_item = VICHY_TITLE_SERVICE_EN;
	}

	$title = ucwords($document->getTitle());
    // $title_item = VICHY_TITLE_SERVICE;
    $title = $title;
    $title_item = $title_item;
    $document->setTitle($title.' | '.$title_item);
?>
<div class="spa_right no-width-height">
	<?php
		foreach ($arr as $v1) {
	?>
		<div class="item no-width-height">
			<div class="title_item <?php echo $v1[0]->main_color ?> no-width-height"><?php echo $v1[0]->title ?> </div>
			<?php foreach($v1 as $item){ ?>
			<div class="sub_item no-width-height">
				<div class="sub_item_text no-width-height"><?php echo  $item->child_title ?></div>
				<a class="detail no-width-height" href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=4&id='.$item->id.'&Itemid=108'); ?>"><?php echo $lbl_detail; ?></a>
				<div class="clear"></div>
			</div>
			<?php } ?>
		</div>
	<?php }?>
	
</div>