<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_store/css/spa.css');
    $active = '3';
    if(isset($_GET['cat']) && $_GET['cat'] == 'article'){
        $db = & JFactory::getDbo();
        $lang = JFactory::getLanguage();
        $sql = "SELECT title, alias, introtext, `fulltext`, images from vc_content where catid = 43 and language = '".str_replace(' ', '', $lang->getTag())."' and state > 0 limit 1";
        $db->setQuery($sql);
        $result = $db->loadObject();
        $active = '1';
    }
?>
<div class="vichy-spa">
    <div class="content-left">
        <ul class="menu-left">
            <!--<li> <a <?php //echo ($active=='1') ? 'class="active"' : '' ?> href="<?php //echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cat=article&Itemid=108'); ?>">GIỚI THIỆU</a></li>
            <li> <a href="<?php //echo JROUTE::_('index.php?option=com_vichy_advice&view=advice_diagnosis'); ?>">DỊCH VỤ</a></li> -->
            <li> <a <?php echo ($active=='3') ? 'class="active"' : '' ?> href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&Itemid=108'); ?>">ĐỊA ĐIỂM</a></li>
        </ul>
    </div> <!-- content left -->
    
    <div class="content-right">
        <?php if(isset($_GET['cat']) && $_GET['cat'] == 'article'){ ?>
        <div class="description">
            <?php 
                if(count($result)){ 
                    echo $result->introtext.$result->fulltext;
                }else{
                    echo "Không có dữ liệu";
                }
            ?>
        </div>
        <?php }else{ ?>
        <div class="content">
            <h2>HỆ THỐNG VICHY SPA</h2>
            <div class="list-spa left">
                <h3 class="title"><?php echo $this->spa_hcm[0]->province; ?></h3>
                <ul>
                <?php foreach ($this->spa_hcm as $k => $v) { ?>
                    <li>
                        <div class="image"><a href="javascript:void(0);"><img src="<?php echo JURI::root() ?>components/com_vichy_store/uploads/spa/<?php echo $v->image; ?>" width="120" height="120" alt="Spa"></a></div>
                        <div class="info">
                            <h3><?php echo $v->name; ?></h3>
                            <div class="addr"><span>Địa chỉ:</span> <?php echo $v->address; ?></div>
                            <div class="addr"><span>Điện thoại:</span> <?php echo $v->phone; ?></div>
                        </div>
                    </li> <!-- item -->  
                <?php } ?>                 
                </ul>
            </div> <!-- list spa -->
            
             <div class="list-spa right">
                <h3 class="title"><?php echo $this->spa_hn[0]->province; ?></h3>
                <ul>
                <?php foreach ($this->spa_hn as $k => $v) { ?>
                    <li>
                        <div class="image"><a href="javascript:void(0);"><img src="<?php echo JURI::root() ?>components/com_vichy_store/uploads/spa/<?php echo $v->image; ?>" width="120" height="120" alt="Spa"></a></div>
                        <div class="info">
                            <h3><?php echo $v->name; ?></h3>
                            <div class="addr"><span>Địa chỉ:</span> <?php echo $v->address; ?></div>
                            <div class="addr"><span>Điện thoại:</span> <?php echo $v->phone; ?></div>
                        </div>
                    </li> <!-- item -->  
                <?php } ?>
                </ul>
            </div> <!-- list spa -->
            
            <div class="clearfix"></div>
            
            <div class="box-contact">
                <p>ĐỂ ĐẶT LỊCH HẸN SPA , HÃY GỌI ĐẾN <span>1800 545 463 </span><br>
                HOẶC E-MAIL TỚI <span><a href="mailto:INFO@VICHY.COM.VN">INFO@VICHY.COM.VN</a></span></p>
            </div>
            
            
        </div>
        <?php } ?>
    </div> <!-- content right -->
</div>