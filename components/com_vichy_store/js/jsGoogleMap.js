// JavaScript Document
var map;
var markersArray = [];
var lat, lng;
// initialize map
function initialize() 
{	
	if(!lng)  
	   lng=106.6472375190491;    
    if(!lat)
        lat=10.796316611436616;      
	  
	  var mapOptions = {
         zoom: 16,
		zoomControl: false,
		panControl: false,
		streetViewControl: false,
		
        center: new google.maps.LatLng(lat, lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map'),
        mapOptions);
		
	// set icon to map when icon_Point
	var location = new google.maps.LatLng(lat, lng);
        setMarkers(map, location);
		

	 
}

// get icon to map
function setMarkers(map, locations) 
{
    var base_url = jQuery('#com-vichy-store-url').attr('data-url');
    var image = base_url+'/images/icon_map.png';
 
    var beachMarker = new google.maps.Marker({
        position: locations,
        map: map,
        icon: image
    });
    markersArray.push(beachMarker);     // add maker to array
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
        markersArray.length = 0;
    }
}

function setLongLat(longtitude,latitude)
{
    if(longtitude)
        lng=longtitude;
    if(latitude)
        lat=latitude;
}
google.maps.event.addDomListener(window, 'load', initialize);

jQuery(document).ready(function() {
    jQuery(document).bind();

    // jQuery(".address").on('click',".map-location",function(){
    //     var longtitude = jQuery(this).attr('data-long');
    //     var latitude = jQuery(this).attr('data-lat');
    //     setLongLat(longtitude,latitude);
    //     initialize();
    // }); 
}


