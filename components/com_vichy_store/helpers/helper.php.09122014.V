<?php

	defined('_JEXEC') or die;

	class Vichy_storeHelper {

		function getStoresByType($type,$start=0,$display=5)
		{						
			$db = JFactory::getDBO();
			$sql="SELECT s.*, c.parent_id, c.params FROM `vc_vichy_store` as s join vc_categories as c on c.id = s.type where c.parent_id =  ".STORE." and s.published=1 and s.type = $type";		
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			return $result;
		}

		function getTypeStore(){
			$db = JFactory::getDBO();
			$sql="SELECT id, parent_id, title, params from vc_categories where parent_id = ".STORE." and published = 1";		
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			return $result;
		}

		function getStoreByAddress($lat,$long,$type){
			$db = JFactory::getDBO();
			$sql="Call geodist($lat,$long,1,0,$type)";
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			$link = $db->getConnection();
			while ($link->more_results()) {
				$link->next_result();
				if ($result1 = $link->store_result()) {
					$result1->free();
				}
			}
			return $result;
		}

		function getAllStoreByAddress($lat,$long){
			$db = JFactory::getDBO();
			$store = STORE;
			$sql="Call geodist($lat,$long,1,$store,0)";
			// $sql="Call geodist($store,$lat,$long,1)";
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			$link = $db->getConnection();
			while ($link->more_results()) {
				$link->next_result();
				if ($result1 = $link->store_result()) {
					$result1->free();
				}
			}
			return $result;
		}

		// private function clearStoredResults() {
		// 	$db = JFactory::getDBO();
		// 	$link = $db->getConnection();
		// 	while ($link->more_results()) {
		// 		$link->next_result();
		// 		if ($result = $link->store_result()) {
		// 			$result->free();
		// 		}
		// 	}
		// }

		function countStores(){
			$db = JFactory::getDBO();
			$sql="select count(s.id) as count from vc_vichy_store as s join vc_categories as c on c.id = s.type where c.parent_id = ".STORE." and published=1";
			$db->setQuery($sql);
			$result = $db->loadObject();
			return $result->count;        
		}

		function getSpa($province,$start=0,$display=4)
		{						
			$db = JFactory::getDBO();
			$sql="select s.name,s.address,s.image,s.phone, p.name as province from vc_vichy_store s join vc_provinces as p on s.province = p.id where s.type = ".SPA." and s.province = $province and s.published=1 limit ". $start.','.$display;		
			$db->setQuery($sql);
			$result = $db->loadObjectList();
			return $result;
		}
		function getIntroductionSpa()
		{						
			$db = JFactory::getDBO();
			$sql="select id,title,introtext,catid from vc_content where catid=43 and state>0 order by id limit 0,1";		
			$db->setQuery($sql);
			$result = $db->loadObject();
			return $result;
		}
        
    }
    
?>