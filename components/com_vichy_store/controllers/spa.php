<?php
    defined('_JEXEC') or die;
    jimport('joomla.application.component.controller');
    
    class Vichy_storeControllerSpa extends JController{
    	function __construct(){
    		parent::__construct();    		
    	}

        function display(){
            JRequest::setVar('view','spa');
            $brd = $_GET['brd'];
            $app    = JFactory::getApplication();
            $pathway = $app->getPathway();
            $breadcrum_name;
            switch ($brd) {
                           case 'st':
                               $breadcrum_name = VICHY_SPA_MENU_ST;
                               break;
                           
                           case 'nd':
                               $breadcrum_name = VICHY_SPA_MENU_ND;
                               break;

                           case 'rd':
                               $breadcrum_name = VICHY_SPA_MENU_RD;
                               break;
                           default :
                               $breadcrum_name = '';
                               break;
                       }           
            if(!empty($breadcrum_name))
                $pathway->addItem($breadcrum_name, '');
            parent::display();
        }

    	function getSpa()
    	{
            
        }  
    }
?>