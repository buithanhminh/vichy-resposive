<?php
/*------------------------------------------------------------------------
# Module Facebook Login
# ------------------------------------------------------------------------
# author    JLV Extension Team
# copyright Copyright (C) 2013 jlvextension.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://jlvextension.com
# Forum Support: http://jlvextension.com/forum.html
# Ticket Support: http://jlvextension.com/ticket/
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die;
class modJLVFacebookLoginHelper
{
	static function getReturnURL($params, $type)
	{
		$app	= JFactory::getApplication();
		$router = $app->getRouter();
		$url = null;
		if ($itemid =  $params->get($type))
		{
			$db		= JFactory::getDbo();
			$query	= $db->getQuery(true);

			$query->select($db->quoteName('link'));
			$query->from($db->quoteName('#__menu'));
			$query->where($db->quoteName('published') . '=1');
			$query->where($db->quoteName('id') . '=' . $db->quote($itemid));

			$db->setQuery($query);
			if ($link = $db->loadResult()) {
				if ($router->getMode() == JROUTER_MODE_SEF) {
					$url = 'index.php?Itemid='.$itemid;
				}
				else {
					$url = $link.'&Itemid='.$itemid;
				}
			}
		}
		if (!$url)
		{
			// stay on the same page
			$uri = clone JFactory::getURI();
			$vars = $router->parse($uri);
			unset($vars['lang']);
			if ($router->getMode() == JROUTER_MODE_SEF)
			{
				if (isset($vars['Itemid']))
				{
					$itemid = $vars['Itemid'];
					$menu = $app->getMenu();
					$item = $menu->getItem($itemid);
					unset($vars['Itemid']);
					if (isset($item) && $vars == $item->query) {
						$url = 'index.php?Itemid='.$itemid;
					}
					else {
						$url = 'index.php?'.JURI::buildQuery($vars).'&Itemid='.$itemid;
					}
				}
				else
				{
					$url = 'index.php?'.JURI::buildQuery($vars);
				}
			}
			else
			{
				$url = 'index.php?'.JURI::buildQuery($vars);
			}
		}
		//change the url
		$url = str_replace('fb=login','',$url);
		$url = str_replace('?&','?',$url);
		//end
		return base64_encode($url);
	}

	static function getType()
	{
		$user = JFactory::getUser();
		return (!$user->get('guest')) ? 'logout' : 'login';
	}
	
	function addJoomlaUser($name, $username, $password, $email,$facebook_id,$gender) {
		jimport('joomla.application.component.helper');
		$config	= JComponentHelper::getParams('com_users');
		// Default to Registered.
		$defaultUserGroup = $config->get('new_usertype', 2);
		
		$data = array(
            "name"=>$name, 
            "username"=>$username, 
            "password"=>$password,
            "password2"=>$password,
			"groups"=>array($defaultUserGroup),
            "email"=>$email,
            "usertype"=>$facebook_id,
            "gender"=>$gender
        );

        $user = clone(JFactory::getUser());
        //Write to database
        if(!$user->bind($data)) {
            throw new Exception("Could not bind data. Error: " . $user->getError());
        }
        if (!$user->save()) {
            throw new Exception("Could not save user. Error: " . $user->getError());
        }

        return $user;
	}
	function getJoomlaId($email) {
		$db		= JFactory::getDbo();
		$query = "SELECT id FROM #__users WHERE email='".$email."';";
		$db->setQuery($query);
		$ejuser = $db->loadResult();
		
		return $ejuser;
	}
	
	function loginFb($fbuser) {
		
		$db		= JFactory::getDbo();
		$query = "SELECT password FROM #__users WHERE id='".$fbuser->id."';";
		$db->setQuery($query);
		$oldpass = $db->loadResult();

		jimport( 'joomla.user.helper' );
		$password = JUserHelper::genRandomPassword(5);
		$query = "UPDATE #__users SET password='".md5($password)."' WHERE id='".$fbuser->id."';";
		$db->setQuery($query);
		$db->query();
		
		$app = JFactory::getApplication();

		$credentials = array();
		$credentials['username'] = $fbuser->username;
		$credentials['password'] = $password;

		$options = array();
		$options['remember']	= true;
		$options['silent']		= true;

		$app->login($credentials, $options);
		
		$query = "UPDATE #__users SET password='".$oldpass."' WHERE id='".$fbuser->id."';";
		$db->setQuery($query);
		$db->query();
		
		//$app->redirect('index.php?option=com_orfarm_contact');
	}

}
