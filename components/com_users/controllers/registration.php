<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';
require_once dirname(__FILE__).'/../helper.php';




/**
 * Registration controller class for Users.
 *
 * @package		Joomla.Site
 * @subpackage	com_users
 * @since		1.6
 */
class UsersControllerRegistration extends UsersController
{
	/**
	*Login with facebook
	*/
	public function login_facebook()
	{	
		require_once dirname(__FILE__).'/../Openid.php';
		require_once dirname(__FILE__).'/../src/facebook.php';
		$appId = '774064075967000';
		$secret = 'da88ef91384a2f34cdffc37790cde924';
		$fb = new Facebook(array('appId'=>FB_APP_ID, 'secret'=>FB_SECRET));		
		$user = $fb->getUser();
		if($user){
			$return = 'index.php';
			$user_profile = $fb->api('/me');
			$isjoomlauser = modJLVFacebookLoginHelper::getJoomlaId($user_profile['email']);
			
			if((int)$isjoomlauser==0) {
				jimport( 'joomla.user.helper' );
				$password = JUserHelper::genRandomPassword(5);										
				$joomlauser = modJLVFacebookLoginHelper::addJoomlaUser($user_profile['name'], $user_profile['email'], $password, $user_profile['email'],'face_'.$user_profile['id']);						
			}
			else {
				$joomlauser = JFactory::getUser($isjoomlauser);
			}
			modJLVFacebookLoginHelper::loginFb($joomlauser);
			// $app->setUserState('users.login.form.data', array());
			// $app->redirect(JRoute::_($app->getUserState('users.login.form.return'), false));
			
			// $this->setRedirect(JRoute::_('index.php',false));
			//$this->setRedirect(JURI::root(),true);
			$href = JROUTE::_('index.php?option=com_users&view=profile');	
			$this->setRedirect($href,true);
		}else{
			$loginUrl = $fb->getLoginUrl(array('scope' => 'email'));
			$this->setRedirect($loginUrl,false);
		}
	}
	/**
	 * Method to activate a user.
	 *
	 * @return	boolean		True on success, false on failure.
	 * @since	1.6
	 */
	public function activate()
	{	
		$user		= JFactory::getUser();
		$uParams	= JComponentHelper::getParams('com_users');

		// If the user is logged in, return them back to the homepage.
		if ($user->get('id')) {
			$this->setRedirect('index.php');
			return true;
		}

		// If user registration or account activation is disabled, throw a 403.
		if ($uParams->get('useractivation') == 0 || $uParams->get('allowUserRegistration') == 0) {
			JError::raiseError(403, JText::_('JLIB_APPLICATION_ERROR_ACCESS_FORBIDDEN'));
			return false;
		}

		$model = $this->getModel('Registration', 'UsersModel');
		$token = JRequest::getVar('token', null, 'request', 'alnum');

		// Check that the token is in a valid format.
		if ($token === null || strlen($token) !== 32) {
			JError::raiseError(403, JText::_('JINVALID_TOKEN'));
			return false;
		}

		// Attempt to activate the user.
		$return = $model->activate($token);

		// Check for errors.
		if ($return === false) {
			// Redirect back to the homepage.
			$this->setMessage(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $model->getError()), 'warning');
			$this->setRedirect('index.php');
			return false;
		}

		$useractivation = $uParams->get('useractivation');

		// Redirect to the login screen.
		if ($useractivation == 0)
		{
			$this->setMessage(JText::_('COM_USERS_REGISTRATION_SAVE_SUCCESS'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
		}
		elseif ($useractivation == 1)
		{
			// $this->setMessage(JText::_('COM_USERS_REGISTRATION_ACTIVATE_SUCCESS'));
			// $this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=success_activate', false));
		}
		elseif ($return->getParam('activate'))
		{
			$this->setMessage(JText::_('COM_USERS_REGISTRATION_VERIFY_SUCCESS'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=complete', false));
		}
		else
		{
			$this->setMessage(JText::_('COM_USERS_REGISTRATION_ADMINACTIVATE_SUCCESS'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=complete', false));
		}
		return true;
	}

	/**
	 * Method to register a user.
	 *
	 * @return	boolean		True on success, false on failure.
	 * @since	1.6
	 */
	public function register()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// If registration is disabled - Redirect to login page.
		if(JComponentHelper::getParams('com_users')->get('allowUserRegistration') == 0) {
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
			return false;
		}

		// Initialise variables.
		$app	= JFactory::getApplication();
		$model	= $this->getModel('Registration', 'UsersModel');

		// Get the user data.
		$requestData = JRequest::getVar('jform', array(), 'post', 'array');

		// Validate the posted data.
		$form	= $model->getForm();
		if (!$form) {
			JError::raiseError(500, $model->getError());
			return false;
		}
		$data	= $model->validate($form, $requestData);

		// Check for validation errors.
		if ($data === false) {
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState('com_users.registration.data', $requestData);

			// Redirect back to the registration screen.
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration', false));
			return false;
		}

		// Attempt to save the data.
		$return	= $model->register($data);

		// Check for errors.
		if ($return === false) {
			// Save the data in the session.
			$app->setUserState('com_users.registration.data', $data);

			// Redirect back to the edit screen.
			$this->setMessage(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration', false));
			return false;
		}

		// Flush the data from the session.
		$app->setUserState('com_users.registration.data', null);

		// Redirect to the profile screen.
		if ($return === 'adminactivate'){
			$this->setMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_VERIFY'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=complete', false));
		} elseif ($return === 'useractivate') {
			$this->setMessage(JText::_('COM_USERS_REGISTRATION_COMPLETE_ACTIVATE'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=registration&layout=complete', false));
		} else {
			$this->setMessage(JText::_('COM_USERS_REGISTRATION_SAVE_SUCCESS'));
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
		}


		return true;
	}

	public function register_user(){
		$fullname = $_POST["fullname"];
		$user_name_tmp = $_POST["user_name"];
		$user_name = $user_name_tmp.'.'.VICHY;
		$gender = $_POST["gender"];
		$birthday = $_POST["birthday"];
		$email = $_POST["email"];
		$phone = $_POST["phone"];
		$address = $_POST["address"];
		$question = $_POST['question'];
		$db = & JFactory::getDbo();
		//Check user name
		$query = "Select count(id) as count from #__users where username = '$user_name'";
        $db->setQuery($query);
        $ret_name = $db->loadObject();
        if($ret_name->count){
        	echo "error_user";
        	exit();
        }

		//Check email exist
		$query = "Select count(id) as count from #__users where email = '$email'";
        $db->setQuery($query);
        $ret = $db->loadObject();
        if($ret->count){
        	echo "error_email";
        	exit();
        }
        $config = JFactory::getConfig();
		$params = JComponentHelper::getParams('com_users');
		// $useractivation = $params->get('useractivation');
		// $sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		$activation = JApplication::getHash(JUserHelper::genRandomPassword());
		$block = 1;

		$password = JUserHelper::hashPassword($_POST["password"]);
		// $city = $_POST["city"];
		$registerDate = date("Y-m-d H:i:s"); 

		
        $query = "INSERT INTO 
        	`#__users`(`id`, `name`, `username`, `email`, `password`, `registerDate`, `gender`, `birthday`, `phone`, `address`,`activation`,`block`) 
        	VALUES (null,'$fullname','$user_name','$email','$password','$registerDate',$gender,'$birthday','$phone','$address','$activation','$block')";
        $db->setQuery($query);
        $db->query();

        $user_id = $db->insertid();
        $group_id = 2;
        $query = "INSERT INTO 
        	`#__user_usergroup_map`(`user_id`, `group_id`) 
        	VALUES ($user_id,$group_id)";
        $db->setQuery($query);
        $db->query();

        $arr_question = explode('#', $question);
        foreach ($arr_question as $k => $v) {
        	if($v != 'a'){
        		$tmp = explode('|', $v);
        		$query = "INSERT INTO vc_vichy_qa_user values (null, $tmp[0], $tmp[1],$user_id, null, null, 1)";
        		$db->setQuery($query);
        		$db->query();
        	}
        }
        // Initialise the table with JUser.
		// $user = new JUser;

        // Compile the notification mail values.
		// $data = $user->getProperties();
		$data['fromname']	= $config->get('fromname');
		$data['mailfrom']	= $config->get('mailfrom');
		$data['sitename']	= $config->get('sitename');
		

        // Set the link to activate the user account.
		$uri = JURI::getInstance();
		$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
		$data['activate'] = '<a href="'.$base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$activation, false).'">'.$base.JRoute::_('index.php?option=com_users&task=registration.activate&token='.$activation, false)."</a>";
		$data['siteurl']	='<a href="'.$base.JRoute::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107', false).'">'.$base.JRoute::_('index.php?option=com_vichy_advice&view=diagnostic&Itemid=107', false)."</a>";

		$emailSubject	= JText::sprintf(
			'COM_USERS_EMAIL_ACCOUNT_DETAILS',
			$fullname,
			$data['sitename']
		);

										$body_pre='
<table class="wrap_email" style="width:800px;
            table-layout:fixed;
			margin:0 auto;
			background:#3d80b0;
            border-collapse:collapse;" cellspacing="0" cellpadding="0">
                <tbody class="wrap_main_email_content">
                    <!--header-->
                <tr>
                    <td class="column_padding_left">
                    </td>
                    <td class="wrap_header_email" style="width:600px;background-repeat: no-repeat;height:62px;background:#eef4fa;">
                                                 <table background="" style="height:62px;table-layout:fixed;
                                                 width:100%;
								border-collapse:collapse;
							" cellpadding="0">
                            <tbody>
                            <tr>
                            <td style="width:20%;padding-left:2%;">
                        				<div class="header_email_home_icon" mc:edit="vc_vichy_confirm_icon_home_header_email_template">
					                 	 <a href="http://www.vichy.com.vn" target="_blank">
					                           <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/bac767e8-6c03-48c3-915a-23e516f700d5.png"></a>
					                           </div>
                        			</td>
                                    
                                                            			<td style="width:65%;">
                        				<div class="header_line_icon">
				                          <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/14557d45-7723-42f8-9d7b-12687f308908.png">
				                        </div>
                        			</td>
                                    
                                    <td style="width:7%;">
                        				<div class="header_email_facebook_icon" mc:edit="vc_vichy_confirm_icon_facebook_header_email_template">
			                          <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/21014a26-5458-4c36-b30c-806fffe33167.png"></a></div>
                        			</td>
                                    
                                    <td>
                        				                            <div class="header_email_youtube_icon" mc:edit="vc_vichy_confirm_icon_youtube_header_email_template">
                                           <a href="https://www.youtube.com/user/VichyVietnam" target="_blank">       <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/936eacc9-e5ab-4fae-a03a-a4cc1ab34969.png"></a>
                                                  </div>
                        			</td>
                            </tr>
                            </tbody>
                        	</table>
                            </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--main-->
                <!--top-->
                <tr>
                    <td class="column_padding_left">
                    </td>
                     <td class="content-top" style="width:600px;
												margin:0 auto;background:#ffffff;">
                     <div class="wrap_image_top" style="height:180px;"><a href="http://www.vichy.com.vn/vi/san-pham/dong-san-pham/aqualia-thermal" target="_blank">
                       <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/fcb3dbef-c4a7-4f53-82d7-307f49856d14.png" mc:edit="imagebannertoptemplatenhanbantin"></a>
                       </div>
                    </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--center-->
                <tr class="content-center">
                    <td class="column_padding_left">
                    </td>
                     <td>
                     <table style="width:600px;border-collapse:collapse;background:#ffffff;font-family:arial">
                     	<tbody>
                        <tr>
                        <td style="
			width:307px;
			padding-left:4%;
			margin-top:-12px;
			font-size:8pt;
            " mc:edit="content_center_left_email_template_nhan_ban_tin">
                         <p>';
		$body_suf='</p>
   
                        </td>
                       <td>
                        <img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/6b4fe5e9-48c5-4e12-a133-64b95345aa54.png" alt="" border="0" style="width: 100%;" mc:edit="image_center_info_email_subscribe_template">
                        </td>
                        </tr>
                        </tbody>
                       </table>
                     	
                     </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--logo-->
                <tr class="content-bottom" style="">
                    <td class="column_padding_left">
                    </td>
                    <td>
                     <table style="width:600px;
								margin:0 auto;
								table-layout:fixed;
								border-collapse:collapse;
                                height:95px;
                                background:#ffffff;
							" cellpadding="0">
                        	<tbody>
                        		<tr>
                        			<td style="width:21%;
														overflow:hidden;">
                        				<div class="bottom_left" style="font-size:10pt;">
                    <a href="http://www.vichy.com.vn/vi/my-skin/dang-ky" target="_blank" style="height:17px"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/22ec1c78-4e9a-481b-951b-a43c1fc8c60a.png" mc:edit="content_image_bottom_left_email_template_nhan_ban_tin"> </a>
                    </div><!--end of image-->
                        			</td>

                        			<td style="width:21%;
                        						padding-left:1%;
														overflow:hidden;">
                        				<div class="bottom_center">
                    <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/d948158f-e21a-4d4a-b17d-5a4c36313e83.png" mc:edit="content_image_bottom_center_email_template_nhan_ban_tin"> </a>
                    </div><!--end of image-->
                        			</td>

                        			<td style="width:21%;
                        						padding-left:2%;
														overflow:hidden;">
		                             <div class="bottom_right" style="
                    
                    display:inline-block;
			background:#ffffff;
			padding:5px 0px 1px 0px;">
                    <a href="http://www.vichy.com.vn/vi/he-thong-cua-hang" target="_blank"><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/f72ce317-4c75-456f-b600-9daff4fd5747.png" mc:edit="content_image_bottom_right_email_template_nhan_ban_tin"> </a>
                    </div>    <!--end of image-->
	
                        			</td>
                        		</tr>
                        	</tbody>
                        </table>
                    </td>
                    <td class="column_padding_right">

                    </td>
                </tr>

                <!--footer-->
                <tr>
                     <td class="column_padding_left">
                    </td>
                 <td style="background:#ffffff;width:600px;">
						
						<table style="width:600px;
								margin:0 auto;
								height:90px;
								table-layout:fixed;
								border-collapse:collapse;
							" cellpadding="0">
							<tbody>
<tr>
        <td><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/755409e6-0408-4348-84eb-5653dd1ed34c.png"></td>
</tr>
								<tr>
	
									<td>
				                         <table style="width:600px;
								margin:0 auto;
								height:90px;
								table-layout:fixed;
								border-collapse:collapse;
							" cellpadding="0">
							<tbody>
                              <tr>
									<td style="width:60%;height:90px;"></td>
                                    <td>
                                                        
                                    </td>
                                    <td>
                                        <a class="go_gome" href="http://www.vichy.com.vn" target="_blank">Về Vichy</a>
                                    </td>

                                </tr>
                            </tbody>
                            </table>               
                                    </td>

								</tr>
<tr>
<td><img src="https://gallery.mailchimp.com/2a05580db098b5f4f5fc7c1c2/images/755409e6-0408-4348-84eb-5653dd1ed34c.png"></td>
</tr>
	                </tbody>
	            </table>

					</td>
                     
                    <td class="column_padding_right">
                </td></tr>

            </tbody>
            </table>
';

		$emailBody_content = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
					// $fullname,
					// $data['sitename'],
					$data['activate']
					// $user_name_tmp
				);

		$emailBody=$body_pre.$emailBody_content.$body_suf;

		// Send the registration email.

		$mailer =	JFactory::getMailer();
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		$return = $mailer->sendMail($data['mailfrom'], $data['fromname'], $email, $emailSubject, $emailBody);
		if($return == 1){
			echo 'user_'.$user_id;
		}else{
			echo $return;
		}
		// echo "useractivate";

		exit();
	}

}
