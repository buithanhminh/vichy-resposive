<?php
defined('_JEXEC') or die;
$document = & JFactory::getDocument();
$document->addStyleSheet('components/com_users/views/registration/css/style.css');
$document->addStyleSheet('components/com_users/views/registration/css/complete.css');
$userid = $_GET['user'];
$db = & JFactory::getDbo();
$db->setQuery('Select email from #__users where id = '.$userid);
$email = $db->loadObject();

$session =& JFactory::getSession();
$lang = $session->get('lang');
if($lang == 'vi-VN'){
	$lbl_sign_box_top = "Tham gia câu lạc bộ my skin để được tư vấn và nhận nhiều ưu đãi";
	$lbl_sign_in = "ĐĂNG KÝ TÀI KHOẢN";
	$lbl_alert_message = "Thông tin của bạn đã được lưu lại <br />
						Bạn sẽ nhận được một email xác nhận đến địa chỉ mà bạn đã cung cấp ";
	$lbl_alert_note = "Vui lòng nhấn vào đường dẫn trong email để kích hoạt tài khoản của bạn";
	$lbl_btn_home = "Trở về trang chủ";
}else{
	$lbl_sign_box_top = "Join my club skin for advice and received many incentives";
	$lbl_sign_in = "Register an account";
	$lbl_alert_message = "Your information is stored. <br/>
						You wil be gived a confirm email to email address that you registerd ";
	$lbl_alert_note = "Please click on link in email to activate your account";
	$lbl_btn_home = "Return home page";
}

?>

<div class="registration">
	<div class="sign-box">
		<div class="sign_box_top">
			<?php echo $lbl_sign_box_top; ?>
		</div>
		<h1><?php echo $lbl_sign_in; ?></h1>
		<div class="intro-text">
			<?php echo $lbl_alert_message; ?><span style="color:#6e8bb7;font-weight:bold;"><?php echo $email->email; ?></span>
		</div>
		<div class="activate"><?php echo $lbl_alert_note; ?></div>
		<a class="return" href="<?php echo JURI::root(); ?>"><?php echo $lbl_btn_home; ?></a>
	</div>
</div>