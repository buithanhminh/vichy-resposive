<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.5
 */

defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<div class="login<?php echo $this->pageclass_sfx?> login-container">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<h1>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>

	<?php endif; ?>
    <!-- <h1 class="mobile-style title-dn" style="display: none;">Đăng nhập</h1> -->
	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	<div class="login-description">
	<?php endif ; ?>

		<?php if($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>

		<?php if (($this->params->get('login_image')!='')) :?>
			<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JTEXT::_('COM_USER_LOGIN_IMAGE_ALT')?>"/>
		<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
	</div>
	<?php endif ; ?>

	<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post">
		<fieldset>
			<div class="submit-block no-width-height" style="margin-bottom:7%">
			<button type="submit" class="button dn-btn"><?php echo JText::_('JLOGIN'); ?></button>
            </div>
			<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
			<?php foreach ($this->form->getFieldset('credentials') as $field): ?>
				<?php if (!$field->hidden): ?>
					<div class="login-fields pc-style"><?php echo $field->label; ?>
					<?php echo $field->input; ?></div>
                    <div class="login-fields mobile-style input-login-block" style="display: none;">
                        <?php echo $field->input; ?></div>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
			<div class="login-fields">
				
				<input id="remember" type="checkbox" name="remember" class="inputbox remember-box-check" value="yes"  alt="<?php echo JText::_('JGLOBAL_REMEMBER_ME') ?>" />
				<label id="remember-lbl" class="remember-label" for="remember"><?php echo JText::_('JGLOBAL_REMEMBER_ME') ?></label>
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>" style="padding-top:3px;font-size:15px;float:right;color: #337ab7;">
				<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
			</div>
			<?php endif; ?>
            
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
	</form>
</div>
<div>
	<ul>
	<!-- 	<li class="under-block-login">
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
		</li> -->
		<li class="under-block-login">
			<a class="dn-fb" href="<?php echo JRoute::_('index.php?option=com_users&task=registration.login_facebook') ;?>">
				HOẶC ĐĂNG NHẬP BẰNG FACEBOOK <img class="dn-fb-img" src="<?php echo JURI::root();?>templates/vichy/images/fb-icon2.png">
			</a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
		<li class="under-block-login" style="margin-top:15%; margin-bottom:10%;margin-left:-18%">
			<span style="font-style:italic;color:#acacac;margin-right:3%;">hoặc</span><a class="dk-btn" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>"><?php // echo JText::_('COM_USERS_LOGIN_REGISTER'); ?>Đăng ký</a>
		</li>
		<?php endif; ?>
	</ul>
</div>
