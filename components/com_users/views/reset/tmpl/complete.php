<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.5
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
$document = & JFactory::getDocument();
$document->addStyleSheet('components/com_users/views/reset/css/style.css');
?>
<div class="reset-complete<?php echo $this->pageclass_sfx?>">
	<div class="reset-complete-box">
		<div class="reset_box_top">
			<?php echo JText::_('COM_USERS_JOIN_MYSKIN'); ?>
		</div>
		<?php if ($this->params->get('show_page_heading')) : ?>
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
		<?php endif; ?>
		<h1><?php echo JText::_('COM_USERS_NEW_PASSOWRD'); ?></h1>
		<form action="<?php echo JRoute::_('index.php?option=com_users&task=reset.complete'); ?>" method="post" class="form-validate">

			<?php foreach ($this->form->getFieldsets() as $fieldset): ?>
			<p><?php echo JText::_($fieldset->label); ?></p>		<fieldset>
				<dl>
				<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field): ?>
					<dt><?php echo $field->label; ?></dt>
					<dd><?php echo $field->input; ?></dd>
				<?php endforeach; ?>
				</dl>
			</fieldset>
			<?php endforeach; ?>

			<div style="text-align:center;">
				<button type="submit" class="validate"><?php echo JText::_('JSUBMIT'); ?></button>
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</form>
	</div>
</div>
