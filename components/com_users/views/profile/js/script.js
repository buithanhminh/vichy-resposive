jQuery(document).ready(function() {

	var language_tag = jQuery("#language_tag").val();
    if(language_tag == 'vi-VN'){
        placeYear = "Năm";
        placeMonth = "Tháng";
        placeDay = "Ngày";
    }else{ 	
        placeYear = "Year";
        placeMonth = "Month";
        placeDay = "Day";
    }

	base_url = jQuery('.base_url').text();
	user_id = jQuery('.base_url').attr('data-user');
	var files;
	function prepareUpload(event)
	{
		jQuery('#status').text('LOADING...');
	  	files = event.target.files;
	  	var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});
		$.ajax({
	        url: base_url+'saveImage.php?files',
	        type: 'POST',
	        data: data,
	        cache: false,
	       	dataType: 'json',
	        processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	        success: function(data1, textStatus1, jqXHR1)
	        {
	        	if(typeof data1.error === 'undefined')
	        	{
	        		// Success so call function to process the form

	        		submitForm(data1);
	        	}
	        	else
	        	{
	        		// Handle errors here
	        		console.log('ERRORS: ' + data1.error);
	        	}
	        },
	        error: function(jqXHR, textStatus, errorThrown)
	        {
	        	// Handle errors here
	        	console.log('ERRORS: ' + textStatus);
	        	// STOP LOADING SPINNER
	        }
	    });
	}
	function submitForm(data){
		$.ajax({
				url: base_url+'saveImage.php',
		        type: 'POST',
		        data: 'filename='+data.files+'&user_id='+user_id,
		        cache: false,
		        dataType: 'json',
		        success: function(data2, textStatus2, jqXHR2)
		        {
		        	if(typeof data2.error === 'undefined')
		        	{
		        		// Success so call function to process the form
		        		if(data2.kq == true){
		        			location.reload();
		        		}
		        	}
		        	else
		        	{
		        		// Handle errors here
		        		console.log('ERRORS: ' + data2.error);
		        	}
		        }
		    })
	}
	jQuery("#picker1").birthdaypicker({
    	dateFormat: "littleEndian",
    	fieldName: "birthday",
    	placeYear: placeYear,
    	placeMonth: placeMonth,
    	placeDay: placeDay
    });
	jQuery(".cancel_profile").hide();
	jQuery(".save_profile").hide();
	jQuery(".edit_profile").click(function() {
		var lang_code = jQuery("#language_tag").val();
		if(lang_code == 'vi-VN'){
			var lbl_Male = "Nam";
			var lbl_Female = "Nữ";
		}else{
			var lbl_Male = "Male";
			var lbl_Female = "Female";
		}

		field = jQuery(this).attr('id');
		text = jQuery(this).parent().siblings('.'+field).text();
		if(field == 'gender'){
			value=jQuery(this).attr('data-value');
			if(value == '0'){
				jQuery(this).parent().siblings('.'+field).html('<input type="radio" value="1" name="'+field+'" />'+lbl_Male+'<br /><input type="radio" value="0" checked="checked" name="'+field+'" />'+lbl_Female);
			}else{
				jQuery(this).parent().siblings('.'+field).html('<input type="radio" checked="checked" value="1" name="'+field+'" />'+lbl_Male+'<br /><input type="radio" value="0" name="'+field+'" />'+lbl_Female);
			}
			
		}else if(field == 'address'){
			jQuery(this).parent().siblings('.'+field).html('');
			jQuery('.list_city').css('display','block');
			jQuery(this).parent().siblings('.'+field).append(jQuery('.list_city'));
		}else if(field == 'birthday'){
			jQuery(this).parent().siblings('.'+field).html('');
			jQuery('.select_birthday').css('display','block');
			jQuery(this).parent().siblings('.'+field).append(jQuery('.select_birthday'));
		}else{
			attr_id = '';
			// if(field == 'birthday'){
			// 	attr_id = 'id = "datepicker"';
			// }
			jQuery(this).parent().siblings('.'+field).html('<input type="text" value="'+text+'" name="'+field+'" class="input_profile" '+attr_id+' />');
		}
		jQuery(this).hide();
		jQuery(this).siblings('.save_profile').show();
		jQuery(this).parent().siblings('.col3').children('.cancel_profile').show();
	});
	jQuery(".cancel_profile").click(function() {
		var lang_code = jQuery("#language_tag").val();
		jQuery(this).hide();
		field = jQuery(this).attr('data-id');
		text = jQuery(this).attr('data-value');
		if(field == 'gender'){
			if(text == '1'){
				text_show = (lang_code == 'vi-VN') ? 'Nam' : 'Male';
			}else{
				text_show = (lang_code == 'vi-VN') ? 'Nữ' : 'Female';
			}
		}else{
			text_show = text;
		}
		if(field == 'address'){
			jQuery('.list_city').css('display','none');
			jQuery('.profile').append(jQuery('.list_city'));
		}
		if(field == 'birthday'){
			jQuery('.select_birthday').css('display','none');
			jQuery('.profile').append(jQuery('.select_birthday'));
		}
		jQuery(this).parent().siblings('.col4').children('.edit_profile').show();
		jQuery(this).parent().siblings('.col4').children('.save_profile').hide();
		jQuery(this).parent().siblings('.'+field).html(text_show);
		jQuery('.error-text').text('');
	});
	jQuery(".save_profile").click(function() {
		var lang_code = jQuery("#language_tag").val();
		if(lang_code == 'vi-VN'){
			var error_content_alert = 'Nội dung này không được bỏ trống';
			var error_email_alert = 'Email này đã được sử dụng';
		}else{
			var error_content_alert = 'This content is not empty';
			var error_email_alert = 'This email is already used';
		}

		field = jQuery(this).attr('data-id');
		if(field == 'gender'){
			text = jQuery('input[type=radio][name=gender]:checked').val();
		}else if(field == 'address'){
			text = jQuery('select[name=address]').val();
		}else if(field == 'birthday'){
			text = jQuery('input[name=birthday]').val();
		}else{
			text = jQuery(this).parent().siblings('.'+field).children('input[name='+field+']').val();
		}
		if(text == ''){
			jQuery('.error-text').text(error_content_alert);
			old_value = jQuery(this).attr('data-value');
			jQuery(this).parent().siblings('.'+field).children('input[name='+field+']').val(old_value);
		}else{
			jQuery.ajax({
				url: base_url+'index.php?option=com_users&task=profile.saveData',
				type: 'POST',
				data: "field="+field+"&text="+text+"&user_id="+user_id,
				success:function(kq){
					if(kq == 'error_email'){
						jQuery('.error-text').text(error_email_alert);
					}
					if(kq == '1'){
						location.reload();
					}
				}
			})
		}
	});

	jQuery('.add_item').on('click','.image_off',function(){
		qid = jQuery(this).attr('data-qid');
		aid = jQuery(this).attr('data-id');
		jQuery('#info_'+qid).val(qid+'|'+aid);
		jQuery(this).parent().siblings().children('.image_common').removeClass('image_on');
		jQuery(this).parent().siblings().children('.image_common').addClass('image_off');
		jQuery(this).removeClass('image_off');
		jQuery(this).addClass('image_on');
	})
	// jQuery('.add_item').on('click','.image_on',function(){
	// 	jQuery(this).removeClass('image_on');
	// 	jQuery(this).addClass('image_off');
	// })
	jQuery('.file-wrapper input[type=file]').bind('change', prepareUpload);
});