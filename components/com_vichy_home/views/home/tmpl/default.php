<?php
    defined('_JEXEC') or die;
    $document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_vichy_home/css/style.css');
	$document->addStyleSheet('components/com_vichy_home/css/nivo-slider.css');

	$curLanguage = JFactory::getLanguage();
	$language_tag = $curLanguage->getTag();
	if($language_tag == 'vi-VN'){
		$location = "vi_VN";
        $img_advice = "vichy_advice.png";
        $img_header_fanpage = "header_fanpage.png";
        $img_vichy_home_03 = "Vichy_Home_03.png";

        $message_login = "Hãy đăng ký tài khoản my skin để nhận mẫu thử";
        $message_answer = "trả lời thêm khảo sát trong tài khoản my skin để nhận mẩu thử";
    }else{
    	$location = "en_US";
        $img_advice = "vichy_home_advice_diagnostics.png";
        $img_header_fanpage = "header_fanpage.png";
        $img_vichy_home_03 = "vichy_home_store_location.png";

        $message_login = "Please register an account to receive my skin sample";
        $message_answer = "answer the survey added my account to receive pieces of skin test";
    }
?>
<style>
	.swiper-container {
		width: 100%;
		height: 100%;
		margin: 20px auto;
	}
	.swiper-slide {
		text-align: center;
		font-size: 18px;
		background: #fff;

		/* Center slide text vertically */
		display: -webkit-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		-webkit-box-pack: center;
		-ms-flex-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		align-items: center;
	}
</style>

<div class=" vichy_home_main">
	<div class =" vichy_home_slide_top box_shadow">
		<div class="box">
<!--			<a class="link_carousel" href="--><?php //echo JRoute::_($this->slide_items[0]->link); ?><!--">-->
<!--				<div id="carousel">-->
<!--				    --
<!--				</div>-->
				<div class="swiper-container" style="margin:0 auto!important;">
					<div class="swiper-wrapper">
						<?php foreach ($this->slide_items as $v) { ?>
							<div class="swiper-slide">
								<a href="<?php echo JRoute::_($this->slide_items[0]->link); ?>"><img class='img-responsive' data-link="<?php echo JRoute::_($v->link); ?>" src="<?php echo JURI::root().'timbthumb.php?src=components/com_vichy_home/uploads/slide_show/'.$v->image.'&w=910&zc=0';?>" alt="Banner"></a>
							</div>
						<?php }	?>
					</div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-next pc-style" style="none"></div>
					<div class="swiper-button-prev pc-style" style="none"></div>
				</div>
<!--			</a>-->

<!--			<div class="nivo-controlNav">-->
<!--				--><?php //foreach($this->slide_items as $k=>$v): ?>
<!--					--><?php //if($k == 0): ?>
<!--			      		<a href="#" class="selected"><span></span></a>-->
<!--			    	--><?php //else: ?>
<!--			    		<a href="#" class=""><span></span></a>-->
<!--			    	--><?php //endif; ?>
<!--			    --><?php //endforeach; ?>
<!--			</div>-->
		</div>
	</div>
	<div class = " vichy_home_product">
		<!-- product left -->
		<div class = "vichy_home_common vichy_home_large box_shadow pc-style">
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_LEFT]->link; ?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_LEFT]->image ;?>">
			</a>
		</div>
		<!-- product right -->
		<div class = "vichy_home_common vichy_home_large box_shadow pc-style" style="margin-right:0" >
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_RIGHT]->link; ?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_TOP_RIGHT]->image ;?>">
			</a>
		</div>
		<!-- advice diagnostics -->
		<div class="vichy_home_common vichy_home_large box_shadow col-xs-6 home-margin-right">
			<a class="pc-style" href="<?php echo JRoute::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107&lang=vi');?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/images/<?php echo $img_advice; ?>">
			</a>
			<a class="mobile-style" href="<?php echo JRoute::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107&lang=vi');?>">
				<img src="<?php echo JURI::root() ?>components/com_vichy_home/images/vichy_advice2.png">
			</a>
		</div>	
	
		<!-- fanpage -->
		<div class="vichy_home_common vichy_home_small box_shadow pc-style">
			<img src="<?php echo JURI::root() ?>timbthumb.php?src=components/com_vichy_home/images/<?php echo $img_header_fanpage; ?>&w=213&zc=0">
			<iframe src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/vichy.com.vn&amp;locale=<?php echo $location; ?>&amp;width=213&amp;height=312&amp;show_faces=true&amp;colorscheme=light&amp;show_border=false&amp;header=true&amp;size=small" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:213px; height:312px;" allowtransparency="true"></iframe>
		</div>

		<!--store location -->
		<div class="vichy_home_common vichy_home_small box_shadow pc-style" style="margin-right:0">
			<a href="<?php echo JRoute::_('index.php?option=com_vichy_store&Itemid=113&lang=vi');?>">
				<img src="<?php echo JURI::root() ?>timbthumb.php?src=/components/com_vichy_home/images/<?php echo $img_vichy_home_03; ?>&w=213&h=376&zx=0">
			</a>
		</div>

		<!-- blog -->
		<?php $image = json_decode($this->list_category->images) ;?>
		<div class ="vichy_home_common vichy_home_large box_shadow home_blog col-xs-6 home-momargin-right">
			<a href="<?php echo JRoute::_('index.php?option=com_content&view=article&Itemid=109&catid='.$this->list_category->catid.'&id='.$this->list_category->id); ?>">
			<div class="img_blog">
					<img src="<?php echo JURI::root() ?><?php echo ($language_tag == 'en-GB') ? 'images/contents/' : ''; echo $image->image_intro;?>">
			</div>
			<div class="text_blog">
				<div class="title_blog"><?php echo shortDesc($this->list_category->title,30)?></div>
				<div class="des_blog"><?php echo  shortDesc(strip_tags($this->list_category->introtext),120); ?></div>
			</div>
			</a>
		</div>

		<!-- My Skin -->
		<?php 
		$user = JFactory::getUser();
		$text = $message_login;
		$link = JRoute::_('index.php?option=com_users&view=registration');
		if(!empty($user->username)){
			$text = $message_answer;
			$link = JRoute::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107');
		}
		?>
		<div class="vichy_home_common vichy_home_small box_shadow col-xs-6 home-margin-right">
			<a href="<?php echo $link; ?>">
				<img src="<?php echo JURI::root() ?>timbthumb.php?src=components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1]->image ;?>&w=213&h=376&zc=0">
			</a>
			<!--<a href="javascript:void(0);" >
				<!-- <img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/<?php //echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_1]->image ;?>"> -->
				<!--<img src="<?php echo JURI::root() ?>components/com_vichy_home/uploads/others/box_left.png"/>
			</a>-->
			</div>

		<!--book online -->
		<div class="vichy_home_common vichy_home_small box_shadow col-xs-6 home-nomargin-right" style="margin-right:0">
			<a href="<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2]->link; ?>">
				<img src="<?php echo JURI::root() ?>timbthumb.php?src=components/com_vichy_home/uploads/others/<?php echo $this->listImages[HOME_POSITION_MAIN_ROW_BOTTOM_RIGHT_2]->image ;?>&w=213&h=376&zc=0">
			</a>
		</div>
		
	</div>
</div>

<!--<script type="text/javascript" src="--><?php //echo JURI::root(); ?><!--templates/vichy/js/jquery.carouFredSel-6.2.1-packed.js"></script>-->
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/home_js.js"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/swiper.min.js"></script>
<script>

	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		autoplay: 2500,
		autoplayDisableOnInteraction: false,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
	});
</script>