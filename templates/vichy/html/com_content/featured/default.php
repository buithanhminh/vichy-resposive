<?php
	defined('_JEXEC') or die;
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
	$document = & JFactory::getDocument();
    $document->addStyleSheet('components/com_content/views/featured/css/style.css');

    $document->addStyleSheet('components/com_vichy_store/css/spa.css');
    $document->addStyleSheet('components/com_vichy_store/css/spa_'.$active.'.css');

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();

	function getMenu($language){
        $db = & JFactory::getDbo();
        
        $sql = "SELECT id, title, alias, link FROM #__menu where menutype = 'menu-about' and published=1 and language = '".str_replace(' ', '', $language)."'";
        $db->setQuery($sql);
        $ret = $db->loadObjectList();
        return $ret;
    }

    $listmenu = getMenu($language_tag);
 
    $cate_id = isset($_GET['id']) ? $_GET['id'] : '121';

    $jfManager      = JoomFishManager::getInstance();
    $table = 'menu';
    $reference_id = $jfManager->getReferenceID($language_tag, $cate_id, $table);
    if(!empty($reference_id)){
        $origin_cat_id = $reference_id;
    }else{
        $origin_cat_id = $cate_id;
    }
    
    if($origin_cat_id != '121'){
        $document->addStyleSheet('components/com_content/views/featured/css/style_'.$origin_cat_id.'.css'); 
    }
    // foreach ($ret2 as $row) :
    //         $temp= $row->id;
    //         if($temp==$cate_id){
    //             $tempname = $row->title;
    //         }
    //         var_dump($temp);
    // endforeach;
?>
<div class="brand-history">
    <div class="content-left">
        <ul class="menu-left pc-style">
            <?php 
            foreach ($listmenu as $k => $v) {
                if($v->id == $cate_id){
                    $class = 'class="active"';
                }else{
                    $class = '';
                }
            ?>
            <li <?php echo $class; ?>> <a href="<?php echo JROUTE::_('index.php?option=com_content&view=featured&id='.$v->id.'&Itemid=101') ?>"><?php echo mb_strtoupper($v->title); ?></a></li>
            <?php } ?>
        </ul>
        <nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">
            <div class="container">
                <div class="navbar-header sub-menu-spa-header blog-menu">
                <?php 
                if($language_tag == 'vi-VN'){
                    $vevichy = 'Về Vichy';
                }else{
                    $vevichy = 'About Vichy';
                }
                ?>
                    <a class="navbar-brand" href="#" style="color:white;"><?php echo $vevichy ?></a>
                    <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    </button>
                </div>
                <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1" style="margin-left:0 !important;margin-right:0 !important;">
                    <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">
                     
                        <?php 
                            foreach ($listmenu as $k => $v) {
                            if($v->id == $cate_id){
                                $class = 'class="active"';
                            }else{
                                $class = '';
                            }
                        ?>
                            <li id="<?php echo $v->id; ?>" class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"><a  class="sub-menu-a" <?php echo ($cate_id == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=featured&id='.$v->id.'&Itemid=101'); ?>"><?php echo $v->title; ?></a></li>
                        <?php } ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <ul class="mobile-style" style="display: none;">
         <?php 
           foreach ($listmenu as $k2 => $v2) {
                $menuid = $v2->id;
                if($menuid == $cate_id){
                    $menuname = $v2->title;
                }
            }  
         ?> 
            <li class="bg-li-menu-mobile active" id="<?php echo $cate_id ?>"> 
            <div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7">
                <a class="sub-menu-a active"  href="<?php echo JROUTE::_('index.php?option=com_content&view=featured&id='.$cate_id.'&Itemid=101'); ?>"><?php echo $menuname; ?></a>
            </div>
            <div>
            <img style="margin-left:-0.7px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />
            </div>
            </li>

        </ul>
    </div>
    <div class="content-right">
    <?php
        $defaultPath = JPATH_COMPONENT_SITE.'/'.'views/featured/tmpl/default_'.$origin_cat_id.'.php';
        jimport('joomla.filesystem.file');
        include($defaultPath);
    ?>
    </div>
</div>
<script>
    $("button").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
</script>
