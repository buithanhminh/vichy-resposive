<?php
	defined('_JEXEC') or die;
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
	$document = & JFactory::getDocument();
    $document->addStyleSheet('components/com_content/views/featured/css/style.css'); 
	function getMenu(){
        $db = & JFactory::getDbo();
        $lang = JFactory::getLanguage();
        $sql = "SELECT id, title, alias, link FROM #__menu where menutype = 'menu-about' and published=1 and language = '".str_replace(' ', '', $lang->getTag())."'";
        $db->setQuery($sql);
        $ret = $db->loadObjectList();
        return $ret;
    }
    $listmenu = getMenu();
    $cate_id = isset($_GET['id']) ? $_GET['id'] : '121';
    if($cate_id != '121'){
        $document->addStyleSheet('components/com_content/views/featured/css/style_'.$cate_id.'.css'); 
    }
?>
<div class="brand-history">
    <div class="content-left">
        <ul class="menu-left">
            <?php 
            foreach ($listmenu as $k => $v) {
            ?>
            <li <?php echo ($v->id == $cate_id) ? 'class="active"' : ''; ?>> <a href="<?php echo JROUTE::_('index.php?option=com_content&view=featured&id='.$v->id.'&Itemid=101') ?>" ><?php echo mb_strtoupper($v->title); ?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="content-right">
    <?php
        $defaultPath = JPATH_COMPONENT_SITE.'/'.'views/featured/tmpl/default_'.$cate_id.'.php';
        jimport('joomla.filesystem.file');
        include($defaultPath);
    ?>
    </div>
</div>