<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
   	$document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
    $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');
    
?>
<div class="vichy-blog">
    <div class="box">
        <div id="slider" class="nivoSlider">
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog1.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog1.jpg" alt="Banner" width="926" height="336"></a>
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/up.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/up.jpg" alt="Banner" width="926" height="336" ></a>
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/toystory.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/toystory.jpg" alt="Banner" width="926" height="336" ></a>
            <a href="javascript:void(0);"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/walle.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/walle.jpg" alt="Banner" width="926" height="336" ></a>
        </div>
    </div> <!-- slider -->

     <div class="box">
        <ul class="list-category-blog">
            <li><a href="javascript:void(0);">ALL</a></li>
            <li><a href="javascript:void(0);">ANTI - AGEING</a></li>
            <li><a href="javascript:void(0);">FOUNDATION MAKE UP</a></li>
            <li><a href="javascript:void(0)">HYDRATION</a></li>
            <li><a href="javascript:void(0)">BODYCARE AND DEODORANTS</a></li>
            <li><a href="javascript:void(0)">SUN CARE</a></li>
             <li><a href="javascript:void(0)">HYDRATION</a></li>
            <li><a href="javascript:void(0)">BODYCARE AND DEODORANTS</a></li>
            <li><a href="javascript:void(0)">SUN CARE</a></li>
        </ul>
    </div> <!-- list blog -->

    <div class="wraper-list-blog">
    <ul class="list-blog">
    
        <li>
            <div class="image"><a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/thumb_blog.jpg"></a></div>
            <div class="social">
                <div class="fb-like" data-href="http://vtc.vn" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <div class="title"><span>ARTICLE TITLE: </span>LOREM IPSUM DOLOR SIT AMET,</div>
            <div class="date">21/05/2014</div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            </p>
            
            <div class="view-more"><a href="<?php echo JURI::root();?>index.php?option=com_content&view=article&id=1">VIEW MORE</a></div>
        </li> <!-- item -->
        
        <li>
            <div class="image"><a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/thumb_blog.jpg"></a></div>
            <div class="social">
                <div class="fb-like" data-href="http://vtc.vn" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <div class="title"><span>ARTICLE TITLE: </span>LOREM IPSUM DOLOR SIT AMET,</div>
            <div class="date">21/05/2014</div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            </p>
            
            <div class="view-more"><a href="javascript:void(0);">VIEW MORE</a></div>
        </li> <!-- item -->
        
        <li>
            <div class="image"><a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/thumb_blog.jpg"></a></div>
            <div class="social">
                <div class="fb-like" data-href="http://vtc.vn" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <div class="title"><span>ARTICLE TITLE: </span>LOREM IPSUM DOLOR SIT AMET,</div>
            <div class="date">21/05/2014</div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            </p>
            
            <div class="view-more"><a href="javascript:void(0);">VIEW MORE</a></div>
        </li> <!-- item -->
        
        <li>
            <div class="image"><a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/thumb_blog.jpg"></a></div>
            <div class="social">
                <div class="fb-like" data-href="http://vtc.vn" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <div class="title"><span>ARTICLE TITLE: </span>LOREM IPSUM DOLOR SIT AMET,</div>
            <div class="date">21/05/2014</div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            </p>
            
            <div class="view-more"><a href="javascript:void(0);">VIEW MORE</a></div>
        </li> <!-- item -->
        
        <li>
            <div class="image"><a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/thumb_blog.jpg"></a></div>
            <div class="social">
                <div class="fb-like" data-href="http://vtc.vn" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <div class="title"><span>ARTICLE TITLE: </span>LOREM IPSUM DOLOR SIT AMET,</div>
            <div class="date">21/05/2014</div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            </p>
            
            <div class="view-more"><a href="javascript:void(0);">VIEW MORE</a></div>
        </li> <!-- item -->
        
        <li>
            <div class="image"><a href="javascript:void(0)"><img src="<?php echo JURI::root();?>components/com_content/views/category/images/thumb_blog.jpg"></a></div>
            <div class="social">
                <div class="fb-like" data-href="http://vtc.vn" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <div class="title"><span>ARTICLE TITLE: </span>LOREM IPSUM DOLOR SIT AMET,</div>
            <div class="date">21/05/2014</div>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            </p>
            
            <div class="view-more"><a href="javascript:void(0);">VIEW MORE</a></div>
        </li> <!-- item -->
    
    </ul>
    
     <div class="paging">
        <a href="javascript:void(0);" class="active">1</a>
        <a href="javascript:void(0);">2</a>
        <a href="javascript:void(0);">3</a>
        <a href="javascript:void(0);">4</a>
    </div> <!-- paging  -->
    
</div>

</div>
<script type="text/javascript">
    $(window).load(function() {
         $('#slider').nivoSlider({
            prevText: '',
            nextText: ''
                    
            });
        });
</script>

