<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
   	$document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
    // $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');
    
    $db =& JFactory::getDBO();

    $query = "SELECT id, title FROM #__categories 
        WHERE parent_id = 29 order by rgt";
        
    $db->setQuery($query);
    $items = $db->loadObjectList();
    //tham so paginate
    $page = (!empty($_GET['page'])) ? $_GET['page'] : '1';
    $current_page = $page;
    $page -= 1;
    $display = 9;
    $start = $page * $display;


    $cat_id = mysql_escape_string(htmlentities($_GET['id']));

    $title = $document->getTitle();

    if($cat_id == 29){
        $title_item = null;
    }else if($cat_id == 30){
        $title_item = VICHY_BLOG_REDUCE_OIL;
    }else if($cat_id == 95){
        $title_item = VICHY_BLOG_ANTI_AGING;
    }else if($cat_id == 97){
        $title_item = VICHY_BLOG_LOTION;
    }else if($cat_id == 99){
        $title_item = VICHY_BLOG_ANTI_HAIR;
    }else{
        $title_item = getTitleByCategory($cat_id);    
    }
    if(!empty($title_item)){
        $document->setTitle($title.' | '.$title_item);
    }else{
        $document->setTitle($title);
    }    

    function getTitleByCategory($id){
        $db = & JFactory::getDbo();
        $sql = "SELECT title FROM #__categories where id=$id";
        $db->setQuery($sql);
        $title = $db->loadResult();
        return $title;
    }

    if($cat_id == '29'){
        $query = 'SELECT
        c.id,
        c.catid, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE cg.parent_id = '.$cat_id.' AND c.state > 0 
        ORDER BY c.ordering ASC, c.created DESC LIMIT '.$start.', '.$display;

        $query_page = "SELECT count(c.id) as count 
                    FROM #__content c
                    JOIN #__categories cg ON c.catid = cg.id
                    WHERE cg.parent_id = $cat_id AND c.state > 0";
    }else{
        $query = 'SELECT
        c.id,
        c.catid, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE c.catid = '.$cat_id.' AND c.state > 0 
        ORDER BY c.ordering ASC, c.created DESC LIMIT '.$start.', '.$display;

        $query_page = "SELECT count(id) as count 
                    FROM #__content c 
                    WHERE c.catid = $cat_id AND c.state > 0";
    }
    $db->setQuery($query);
    $list_blog = $db->loadObjectList();

    //Tong so trang
    $db->setQuery($query_page);
    $result2 = $db->loadObject();
    $count = $result2->count;
    $pages = ceil($count / $display);
?>
<div class="vichy-blog">
    <div class="box">
        <ul class="list-category-blog">
            <li class="get-blog" id="0"><a <?php echo ($cat_id == '29')? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id=29&Itemid=109'); ?>">Tất cả</a></li>
            <?php 
                foreach ($items as $k => $v) {
                    $style = "";
                    if($k%3 == 0){
                        $style = 'style="width:322px"';
                    }
                    if(($k-1)%3 == 0){
                        $style = 'style="width:210px"';
                    }
            ?>
                <li class="get-blog" <?php echo $style; ?> id="<?php echo $v->id; ?>"><a <?php echo ($cat_id == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
            <?php } ?>
        </ul>
    </div> <!-- list blog -->

    <div class="wraper-list-blog">
    <?php if(count($list_blog)){ ?>
    <ul class="list-blog">
        <?php 
            $html_code = '';
            foreach ($list_blog as $v) {
                $html_code .= '<li>';
                $src = './timbthumb.php?src='.json_decode($v->images)->image_intro.'&w=272&q=100';
                $html_code .= '<div class="image"><a href="index.php?option=com_content&view=article&catid='.$v->catid.'&id='.$v->id.'&Itemid=109"><img src="'.$src.'"></a></div>';
                $html_code .= '<div class="social">';
                $html_code .= '<div class="fb-like" data-href="'.JROUTE::_('index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id).'" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>';
                $html_code .= '</div>';
                // $html_code .= '<div class="title"><span>ARTICAL TITLE:</span>'.$v->title.'</div>';
                $html_code .= '<div class="title">'.$v->title.'</div>';
                $html_code .= '<div class="date">'.date("d/m/Y", strtotime($v->created)).'</div>';
                $html_code .= '<div class="content_blog">';
                $html_code .= shortDesc(strip_tags($v->introtext),120);
                $html_code .= '</div>';
                $html_code .= '<div class="view-more"><a href="index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id.'">Xem chi tiết >></a></div>';
                $html_code .= '</li>';
            }
            
            echo $html_code;
        ?>
    </ul>
    <?php
    if($pages > 1){
        $start_page = 1;
        $end_page = $pages;
         
        $data .= "<div class='paging'>";
         
        for ($i = $start_page; $i <= $end_page; $i++) {
            if ($current_page == $i)
                $data .= "<a href='".JROUTE::_('index.php?option=com_content&view=category&id='.$cat_id.'&Itemid=109&page='.$i)."' class='active'>{$i}</a>";
            else
                $data .= "<a href='".JROUTE::_('index.php?option=com_content&view=category&id='.$cat_id.'&Itemid=109&page='.$i)."'>{$i}</a>";
        }
         
        $data = $data . "</div>";
    }
    echo $data;
    ?>
    <?php }else{ ?>
        <div class="coming-soon">Coming soon</div>
    <?php } ?>

</div>

</div>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/jquery.carouFredSel-6.2.1-packed.js"></script>