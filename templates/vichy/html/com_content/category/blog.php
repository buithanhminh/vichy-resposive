<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
   	$document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
    $document->addStyleSheet('components/com_vichy_store/css/spa.css');
    $document->addStyleSheet('components/com_vichy_store/css/spa_'.$active.'.css');
    

    // $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');

    $curLanguage = JFactory::getLanguage();
    $language_tag = $curLanguage->getTag();
   


    $db =& JFactory::getDBO();
    if($language_tag == 'vi-VN'){
        $language = "'*',".$db->quote($language_tag);
        $view_detail = "Xem chi tiết";

        //$blog_id = 29;
    }else{
        $language = $db->quote($language_tag);
        $view_detail = "View detail";

        //$blog_id = $id_all = get_translateID_from_referenceID(29);
    }

    $query = "SELECT id, title from #__categories 
        where parent_id = 29 and published = 1 and language in($language) order by rgt";
        
    $db->setQuery($query);
    $items = $db->loadObjectList();
    
  
    //tham so paginate
    $page = (!empty($_GET['page'])) ? $_GET['page'] : '1';
    $current_page = $page;
    $page -= 1;
    $display = 9;
    $start = $page * $display;


    $cat_id = mysql_escape_string(htmlentities($_GET['id']));
    $cat_title= mysql_escape_string(htmlentities($_GET['title']));
    $title = $document->getTitle();

    if($cat_id == 29){
        $title_item = null;
    }else if($cat_id == 30){
        $title_item = VICHY_BLOG_REDUCE_OIL;
    }else if($cat_id == 95){
        $title_item = VICHY_BLOG_ANTI_AGING;
    }else if($cat_id == 97){
        $title_item = VICHY_BLOG_LOTION;
    }else if($cat_id == 99){
        $title_item = VICHY_BLOG_ANTI_HAIR;
    }else{
        $title_item = getTitleByCategory($cat_id);    
    }
    if(!empty($title_item)){
        $document->setTitle($title.' | '.$title_item);
    }else{
        $document->setTitle($title);
    }    

    function getTitleByCategory($id){
        $db = & JFactory::getDbo();
        $sql = "SELECT title FROM #__categories where id=$id";
        $db->setQuery($sql);
        $title = $db->loadResult();
        return $title;
    }

    if($cat_id == '29'){
        $query = 'SELECT
        c.id,
        c.catid, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE cg.parent_id = '.$cat_id.' AND c.state > 0 and c.language in('.$language.')
        ORDER BY c.ordering ASC, c.created DESC LIMIT '.$start.', '.$display;

        $query_page = "SELECT count(c.id) as count 
                    FROM #__content c
                    JOIN #__categories cg ON c.catid = cg.id
                    WHERE cg.parent_id = $cat_id AND c.state > 0 and c.language in(".$language.")";
    }else{
        if($language_tag == 'vi-VN'){
            $cat_id_reference = $cat_id;
        }else{
            $cat_id_reference = get_parentID_from_translateID($cat_id);
        }        

        $query = 'SELECT
        c.id,
        c.catid, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE c.catid = '.$cat_id_reference.' AND c.state > 0 and c.language in('.$language.')
        ORDER BY c.ordering ASC, c.created DESC LIMIT '.$start.', '.$display;

        $query_page = "SELECT count(id) as count 
                    FROM #__content c 
                    WHERE c.catid = $cat_id_reference AND c.state > 0 and c.language in(".$language.")";
    }
    $db->setQuery($query);
    $list_blog = $db->loadObjectList();

    //Tong so trang
    $db->setQuery($query_page);
    $result2 = $db->loadObject();
    $count = $result2->count;
    $pages = ceil($count / $display);

    function get_parentID_from_translateID($translation_id){
        $db = & JFactory::getDbo();
        $query = "select reference_id from vc_jf_translationmap where translation_id = $translation_id and language = 'en-GB' and reference_table='categories' ";
        $db->setQuery($query);
        $reference_id = $db->loadResult();
        return $reference_id;
    }

    function get_translateID_from_referenceID($reference_id){
        $db = & JFactory::getDbo();
        $query = "select translation_id from vc_jf_translationmap where reference_id = $reference_id and language = 'en-GB' and reference_table='categories' ";
        $db->setQuery($query);
        $translation_id = $db->loadResult();
        return $translation_id;
    }

    function getListReferTransID($arrayID){
        $db =& JFactory::getDbo();

        $listID = array();
        foreach ($arrayID as $key => $value) {
            $listID[] = $value->id;
        }
        $str_id =implode(',', $listID);
        
        $query = "SELECT translation_id, reference_id,
                CASE WHEN CHAR_LENGTH(cg.alias) 
                THEN CONCAT_WS(':', cg.id, cg.alias) ELSE cg.id END as catid  FROM vc_jf_translationmap as tm inner join vc_categories as cg on translation_id = cg.id where tm.language = 'en-GB' and reference_table = 'categories' and translation_id in ($str_id)";
        $db->setQuery($query);
        $list = $db->loadObjectList();
        $result = array();
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $reference_id = $value->reference_id;     
                $result[$reference_id] = $value->catid;
            }
        }
        
        return $result;
    }
   
?>

<nav class="navbar navbar-default mobile-style" style="display: none" role="navigation">
        <div class="container">
            <div class="navbar-header sub-menu-spa-header blog-menu">
                <a class="navbar-brand" href="#" style="color:white;">Blog</a>
                <button type="button" class="navbar-toggle collapsed sub-menu-spa-btn plus1" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                </button>
            </div>
            <div class="collapse navbar-collapse over-breadrum" id="bs-example-navbar-collapse-1" style="margin-left:0 !important;margin-right:0 !important;">
                <ul class="nav navbar-nav" style="margin-top:0px;background: #123f83;">
                    <!--                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>-->
                    <!--                    <li><a href="#">Link</a></li>-->
            <!--         <li class="bg-li-menu-mobile1" style="margin-bottom:1px;"> <a class="sub-menu-a"  href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&cid=3&Itemid=108'); ?>"><?php echo $title; ?></a></li> -->
                    <?php 
                        foreach ($items as $k => $v) {
                            $style = "";
                            if($k%3 == 0){
                                $style = 'width:322px';
                            }
                            if(($k-1)%3 == 0){
                                $style = 'width:210px';
                            }
                    ?>
                        <li id="<?php echo $v->id; ?>" class="bg-li-menu-mobile1" style="border-bottom:1px solid #2f5e9b;"><a  class="sub-menu-a" <?php echo ($cat_id == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
                    <?php } ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
 <ul class="mobile-style" style="display: none;">
 <?php 
foreach ($items as $row) :
    $temp= $row->id;
    if($temp==$cat_id){
        $tempname = $row->title;
    }
endforeach;
    if($cat_id=="29"){
        echo '<li class="bg-li-menu-mobile active" id="0" ><div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7"> <a '; echo ($cat_id == '29')? 'class = "cat_active"' : ''; echo 'class="sub-menu-a active"  href="'; echo JROUTE::_('index.php?option=com_content&view=category&id=29&Itemid=109'); echo '">'; echo ($language_tag=='en-GB') ? 'All' : 'Tất cả'; echo '</a></div><div><img style="margin-left:-0.7px;" height="40px" src="'?> <?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" /></div></li>
   <?php }else{
 ?> 
    <li class="bg-li-menu-mobile active" id="<?php echo $cat_id ?>"> 
    <div style="overflow:hidden;max-width:85%; height:40px;float:left;background:#3c72c7">
        <a class="sub-menu-a active"  href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$cat_id.'&Itemid=109'); ?>"><?php echo $tempname; ?></a>
    </div>
    <div>
        <img style="margin-left:-0.7px;" height="40px" src="<?php echo JURI::root(); ?>templates/vichy/images/arrow-sub-menu.png" />   
    </div>
    </li>
<?php } ?>
</ul>
<?php 
if($language_tag == 'vi-VN'){
    $text = 'Cùng Vichy chia sẻ những bí quyết làm đẹp và những câu chuyện thú vị về cuộc sống.';
}else{
    $text = 'Vichy share the same mystique and interesting stories about life.';
}
?>
<div id="skinlife" class="vichy-blog mobile-style">
    <div class="box">
        <div id="carousel" style="background:url(<?php echo JURI::root();?>images/skin_life2.png)">
            <img id="skin-life" src="<?php echo JURI::root();?>components/com_content/views/category/images/skin_life.png" alt="Banner">
            <div class="desc_banner">
                <div class="text"><?php echo $text; ?></div>
                <div class="social">
                    <div class="fb-like" data-href="<?php echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
                    <!--<div class="fb-like" data-href="<?php //echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
                    <div class="fb-share-button" data-href="<?php //echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>" data-layout="button" data-action="share" data-show-faces="true" data-share="true"></div>-->
                </div>
            </div>
        </div>
    </div> <!-- slider -->
</div>
<div class="vichy-blog" >
    <div class="box listblog">
        <ul class="list-category-blog">
            <li class="get-blog" id="0"><a <?php echo ($cat_id == '29')? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id=29&Itemid=109'); ?>"><?php echo ($language_tag=='en-GB') ? 'All' : 'Tất cả'; ?></a></li>
            <?php 
                foreach ($items as $k => $v) {
                    $style = "";
                    if($k%3 == 0){
                        $style = 'style="width:322px"';
                    }
                    if(($k-1)%3 == 0){
                        $style = 'style="width:210px"';
                    }
            ?>
                <li class="get-blog" <?php echo $style; ?> id="<?php echo $v->id; ?>"><a <?php echo ($cat_id == $v->id)? 'class = "cat_active"' : ''; ?> href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
            <?php } ?>
        </ul>
    </div> <!-- list blog -->

    <div class="wraper-list-blog">
    <?php if(count($list_blog)){ ?>
    <ul class="list-blog">
        <?php 
            $html_code = '';
            if($language_tag == 'vi-VN'){
                foreach ($list_blog as $v) {
                    $html_code .= '<li>';
                    $src = './timbthumb.php?src='.json_decode($v->images)->image_intro.'&w=272&q=100';
                    $html_code .= '<div class="image"><a href="index.php?option=com_content&view=article&catid='.$v->catid.'&id='.$v->id.'&Itemid=109"><img src="'.$src.'"></a></div>';
                    $html_code .= '<div class="social">';
                    //$html_code .= '<div class="fb-like" data-href="'.JROUTE::_('index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id).'" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>';
                    $html_code .= '<div class="fb-like" data-href="'.JROUTE::_('index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id).'" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>';
                    $html_code .= '</div>';
                    // $html_code .= '<div class="title"><span>ARTICAL TITLE:</span>'.$v->title.'</div>';
                    $html_code .= '<div class="title"><p class="title-blog">'.$v->title.'</p></div>';
                    $html_code .= '<div class="date">'.date("d/m/Y", strtotime($v->created)).'</div>';
                    $html_code .= '<div class="content_blog">';
                    $html_code .= shortDesc(strip_tags($v->introtext),120);
                    $html_code .= '</div>';
                    $html_code .= '<div class="view-more"><a href="index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id.'">'.$view_detail.' >></a></div>';
                    $html_code .= '</li>';
                }
            }else{
                $list_Refer_Trans = getListReferTransID($items);

                foreach ($list_blog as $v) {
                    $cate_id = explode(':', $v->catid);

                    $html_code .= '<li>';
                    $src = './timbthumb.php?src=images/contents/'.json_decode($v->images)->image_intro.'&w=272&q=100';
                    $html_code .= '<div class="image"><a href="index.php?option=com_content&view=article&catid='.$list_Refer_Trans[$cate_id[0]].'&id='.$v->id.'&Itemid=109"><img src="'.$src.'"></a></div>';
                    $html_code .= '<div class="social">';
                    //$html_code .= '<div class="fb-like" data-href="'.JROUTE::_('index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id).'" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>';
                    $html_code .= '<div class="fb-like" data-href="'.JROUTE::_('index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id).'" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>';
                    $html_code .= '</div>';
                    // $html_code .= '<div class="title"><span>ARTICAL TITLE:</span>'.$v->title.'</div>';
                    $html_code .= '<div class="title">'.$v->title.'</div>';
                    $html_code .= '<div class="date">'.date("d/m/Y", strtotime($v->created)).'</div>';
                    $html_code .= '<div class="content_blog">';
                    $html_code .= shortDesc(strip_tags($v->introtext),120);
                    $html_code .= '</div>';
                    $html_code .= '<div class="view-more"><a href="index.php?option=com_content&view=article&Itemid=109&catid='.$list_Refer_Trans[$cate_id[0]].'&id='.$v->id.'">'.$view_detail.' >></a></div>';
                    $html_code .= '</li>';
                }
            }            
            
            echo $html_code;
        ?>
    </ul>
    <?php
    if($pages > 1){
        $start_page = 1;
        $end_page = $pages;
         
        $data .= "<div class='paging'>";
         
        for ($i = $start_page; $i <= $end_page; $i++) {
            if ($current_page == $i)
                $data .= "<a href='".JROUTE::_('index.php?option=com_content&view=category&id='.$cat_id.'&Itemid=109&page='.$i)."' class='active'>{$i}</a>";
            else
                $data .= "<a href='".JROUTE::_('index.php?option=com_content&view=category&id='.$cat_id.'&Itemid=109&page='.$i)."'>{$i}</a>";
        }
         
        $data = $data . "</div>";
    }
    echo $data;
    ?>
    <?php }else{ ?>
        <div class="coming-soon">Coming soon</div>
    <?php } ?>

</div>

</div>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/jquery.carouFredSel-6.2.1-packed.js"></script>

<script>
    $(".sub-menu-spa-btn").click(function(){
        if($(this).attr('aria-expanded') == "true")
        {
            $(this).removeClass('minus1');
            $(this).addClass('plus1');
        }else
        {
            $(this).removeClass('plus1');
            $(this).addClass('minus1');
        }
    });
</script>