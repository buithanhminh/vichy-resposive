<?php
    defined('_JEXEC') or die;
    JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
   	$document = & JFactory::getDocument();
	$document->addStyleSheet('components/com_content/views/category/css/style.css');
    $document->addStyleSheet('components/com_content/views/category/css/nivo-slider.css');
    // $document->addScript('components/com_content/views/category/js/jquery.nivo.slider.js');
    
    $db =& JFactory::getDBO();

    $query = "SELECT id, title FROM #__categories 
        WHERE parent_id = 29";
        
    $db->setQuery($query);
    $items = $db->loadObjectList();

    $cat_id = mysql_escape_string(htmlentities($_GET['id']));
    if($cat_id == '29'){
        $query = 'SELECT
        c.id,
        c.catid, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE cg.parent_id = '.$cat_id.' AND c.state > 0 
        ORDER BY c.created DESC';
    }else{
        $query = 'SELECT
        c.id,
        c.catid, 
        c.title, 
        c.images, 
        c.introtext, 
        c.created, 
        c.language, 
        CASE WHEN CHAR_LENGTH(c.alias) 
        THEN CONCAT_WS(":", c.id, c.alias) ELSE c.id END as slug, 
        CASE WHEN CHAR_LENGTH(cg.alias) 
        THEN CONCAT_WS(":", cg.id, cg.alias) ELSE cg.id END as catid 
        FROM #__content c 
        JOIN #__categories cg ON c.catid = cg.id 
        WHERE c.catid = '.$cat_id.' AND c.state > 0 
        ORDER BY c.created DESC';
    }
    $db->setQuery($query);
    $list_blog = $db->loadObjectList();
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo FB_APP_ID; ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="vichy-blog">
    <div class="box">
        <div id="carousel">
            <img src="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog1.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/banner-blog1.jpg" alt="Banner" width="926" height="336">
            <img src="<?php echo JURI::root();?>components/com_content/views/category/images/up.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/up.jpg" alt="Banner" width="926" height="336" >
            <img src="<?php echo JURI::root();?>components/com_content/views/category/images/toystory.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/toystory.jpg" alt="Banner" width="926" height="336" >
            <img src="<?php echo JURI::root();?>components/com_content/views/category/images/walle.jpg" data-thumb="<?php echo JURI::root();?>components/com_content/views/category/images/walle.jpg" alt="Banner" width="926" height="336" >
        </div>
        <div class="nivo-controlNav">
            <a href="#" class="selected"></a>
            <a href="#" class=""></a>
            <a href="#" class=""></a>
            <a href="#" class=""></a>
        </div>
    </div> <!-- slider -->

    <div class="box">
        <ul class="list-category-blog">
            <li class="get-blog" id="0"><a href="<?php echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109'); ?>">ALL</a></li>
            <?php foreach ($items as $v) { ?>
                <li class="get-blog" id="<?php echo $v->id; ?>"><a href="<?php echo JROUTE::_('index.php?option=com_content&view=category&id='.$v->id.'&Itemid=109'); ?>"><?php echo $v->title; ?></a></li>
            <?php } ?>
        </ul>
    </div> <!-- list blog -->

    <div class="wraper-list-blog">
    <ul class="list-blog">
        <?php 
            $html_code = '';
            foreach ($list_blog as $v) {
    
                $html_code .= '<li>';
                $html_code .= '<div class="image"><a href="index.php?option=com_content&view=article&catid='.$v->catid.'&id='.$v->id.'&Itemid=109"><img src="'.JURI::root().json_decode($v->images)->image_intro.'"></a></div>';
                $html_code .= '<div class="social">';
                $html_code .= '<div class="fb-like" data-href="'.JROUTE::_('index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id).'" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>';
                $html_code .= '</div>';
                $html_code .= '<div class="title"><span>ARTICLE TITLE: </span>'.$v->title.'</div>';
                $html_code .= '<div class="date">'.date("d/m/Y", strtotime($v->created)).'</div>';
                $html_code .= '<p>';
                $html_code .= shortDesc(strip_tags($v->introtext),147);
                $html_code .= '</p>';
                $html_code .= '<div class="view-more"><a href="index.php?option=com_content&view=article&Itemid=109&catid='.$v->catid.'&id='.$v->id.'">XEM CHI TIẾT</a></div>';
                $html_code .= '</li>';
            }
            
            echo $html_code;
        ?>
    
    </ul>
<!--      <div class="paging">
        <a href="javascript:void(0);" class="active">1</a>
        <a href="javascript:void(0);">2</a>
        <a href="javascript:void(0);">3</a>
        <a href="javascript:void(0);">4</a>
    </div> <!-- paging  -->
</div>

</div>
<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $('#carousel').carouFredSel({
            items               : 1,
            direction           : "left",
            auto: true,
            scroll : {
                items           : 1,
                duration        : 1000,                         
                pauseOnHover    : true
            },
            pagination: { 
                container: '.nivo-controlNav', 
                anchorBuilder: false 
                }                  
        });
    });
</script>