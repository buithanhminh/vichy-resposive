<?php defined('_JEXEC') or die;
header( 'Content-type: text/x-component' );
$PIE_path=JURI::root().'PIE/PIE.htc';
include( $PIE_path );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?> ">
	<head>
		<link rel="shortcut icon" href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/favicon.ico" />
		<script src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/jquery.mockjax.js"></script>
    	<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/jquery.autocomplete.js"></script>
    	<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/countries.js"></script>
    	<script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/demo.js"></script>
        <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/html5.js"></script>
		<link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/styles_autocomplete.css" rel="stylesheet" type="text/css" />

		<jdoc:include type="head" />
	</head>
	<!--[if lt IE 7 ]>   <body class="ie6">          <![endif]-->
   	<!--[if IE 7 ]>      <body class="ie7">          <![endif]-->
   	<!--[if IE 8 ]>      <body class="ie8">          <![endif]-->
   	<!--[if IE 9 ]>      <body class="ie9">          <![endif]-->
   	<!--[if (gt IE 9) ]> <body class="ie10">       <![endif]-->
   	<!--[!(IE)]><!-->    <body class="notIE modern"> <!--<![endif]-->
  	
		<div class="container">
			<header class="main">
				<div class="main-center">
					<div class="logo-vichy">
						<a href="<?php echo JURI::root();?>" title="Home">
							<span>Vichy</span>
						</a>
					</div>
					<div class="nav-vichy">
						<div class="nav-vichy-top">
							<div class="nav-vichy-top-left">
								<div class="search">
			                        <!-- <input class="inputbox" type="text" name="keyword" id="keyword" placeholder="Tìm kiếm sản phẩm">
			                        <input class="btn-search" type="button" id="btn-search"> -->
			                         <input id="root-ajax" type="hidden" value="<?php echo JURI::root();?>"></input>
			                         <input class="inputbox" type="text" name="query" placeholder="Tìm kiếm sản phẩm" id="autocomplete-ajax" />
			                         <input class="btn-search"  type="button" id="btn-search">
			                         <input id="selction-ajax" type="hidden" value=""></input>
			                    </div>
							</div>
							<div class="nav-vichy-top-right">
							<div class="vichy-link">
									<?php
										$user = & JFactory::getUser();
										$user_id = $user->id;
										if($user_id) {
											$href = JROUTE::_('index.php?option=com_users&view=profile');
										}else{
											$href = '';
										}										
									?>
									<div class="my_skin">
										<a href="<?php echo $href; ?>">MY SKIN</a>
										<jdoc:include type="modules" name="login" />
									</div>
								<div class="basket hover-buy-online">
										<a href="#">Mua hàng online</a>
									<div class="buy-online common-buy-online">
											<a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/chonvn.png"></a>
											<a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/yes24com.png"></a>
											<a href="http://www.lazada.vn/vichy/" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/lazada.png"></a>
							</div>
									
									<script type="text/javascript" language="javascript">
										$(document).ready(function(e) {
									        $('.hover-buy-online').hover(function(e) {
												var left = $('.vichy-link').width() - $('.vichy-link').find('.my_skin').css('margin-right').replace("px","") - $('.vichy-link').find('.basket').css('margin-right').replace("px","");												
												
									            var el=$(this).find('.common-buy-online').css({'visibility':'visible','opacity':'1','transition-delay':'0s','left':left});
												
										
									        },function(e){
												var el=$(this).find('.common-buy-online').css({'visibility':'hidden','opacity':'0','transition':'visibility 0s linear 0.3s,opacity 0.3s linear'});
											});
									   });
 									</script>
								</div>
								<jdoc:include type="modules" name="store_locator" />
									<div class="clear"></div>
								</div><!--end vichy-link-->

								<!-- <jdoc:include type="modules" name="login" /> -->

								<div class="social-link">
									<jdoc:include type="modules" name="new_letter" />
									<a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/face.png"></a>
									<a href="https://www.youtube.com/user/VichyVietnam" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/youtube.png"></a>
								</div>
								<div class="clear"></div>
							</div>
							
						</div>
						<div class="clear"></div>
						<div class="nav-vichy-bottom">
							<jdoc:include type="modules" name="main_menu" />
						</div>
					</div>
				</div>
			</header>
			
			<jdoc:include type="modules" name="breadcrumb" />
			<?php 
			if(!$this->countModules('breadcrumb')){
				echo '<div class="border_home"></div>';
			}
			?>
			<div class="main-container" <?php echo ($_GET['option'] == 'com_vichy_home') ? 'style="padding-top:108px;"' : '' ?>>
				<a href="#" id="go-bottom" class="button-down">
			   		<img src="<?php echo JURI::root(); ?>templates/vichy/images/to-bottom-icon.png" />
				</a>
				<?php if($_GET['view']== 'featured') {?>
					<jdoc:include type="modules" name="vichy_about" />
				<?php } ?>
				<?php if($_GET['view'] == 'product_landing'){ ?>
                <jdoc:include type="modules" name="new_products" />
				<jdoc:include type="modules" name="favorite_products" />
				<?php } ?>
				<jdoc:include type="modules" name="banner_blog" />
				<jdoc:include type="component" />
				<?php if($_GET['view'] == 'advice_diagnosis'){ ?>
				<jdoc:include type="modules" name="advice_landing" />
				<?php } ?>
				<?php if($_GET['view'] == 'product'){ ?>
				<jdoc:include type="modules" name="vichy_comment_product" />
				<?php } ?>
				<?php if($_GET['view'] == 'product' || $_GET['view'] == 'product_range'){ ?>
				<jdoc:include type="modules" name="product_step_1" />
				<?php } ?>
				<?php if($_GET['view'] == 'product'){ ?>
				<jdoc:include type="modules" name="diagnosis_advice_product" />
				<?php } ?>
				<?php if($_GET['view'] == 'product_landing'){ ?>
				<jdoc:include type="modules" name="product_without_steps" />
				<?php } ?>
				<?php if($_GET['view'] == 'advice'){ ?>
				<jdoc:include type="modules" name="product_range_diagnosis_advice" />
				<?php } ?>
				<a href="#" id="go-top" class="button-up">					
			   		<img src="<?php echo JURI::root(); ?>templates/vichy/images/to-top-icon.png" />
				</a>
			</div>

			<footer class="main">				
				<div class="top clearfix">
					<div class="main-center">
						<jdoc:include type="modules" name="vichy_footer" />
					</div>
				</div>
				<div class="bottom">
					<div class="main-center clearfix">
						<div class="copyright">© 2014 VICHY&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Điều khoản sử dụng&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Từ điển làn da</div>
					</div>
				</div>
			</footer>
		</div>
		<!-- <jdoc:include type="modules" name="facebook_slider" /> -->
		<script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/common.js"></script>
		
		<noscript>
			<iframe src="//www.googletagmanager.com/ns.html?id=GTM-NPW6MD"
					height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>
		<script>
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-NPW6MD');
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-52701542-1', 'auto');
		  ga('send', 'pageview');
		  ga('create', 'UA-8955873-32', 'auto', {'name': 'newTracker'});
		  ga('newTracker.send', 'pageview');
		</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-8955873-32', 'auto');
		  ga('send', 'pageview');

		</script>

  	</body>
</html>