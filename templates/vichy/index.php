<?php defined('_JEXEC') or die;
header( 'Content-type: text/x-component' );
$PIE_path=JURI::root().'PIE/PIE.htc';
include( $PIE_path );

$curLanguage = JFactory::getLanguage();
$language_tag = $curLanguage->getTag();
$session =& JFactory::getSession();
$session->set('lang', $language_tag);
if($language_tag == 'vi-VN'){
  $lbl_register = "Đăng kí";
  $lbl_search = "Tìm kiếm sản phẩm";
  $lbl_buy_online = "Mua hàng online";
  $lbl_terms_use = "Điều khoản sử dụng";
  $lbl_dictionary_skin = "Từ điển làn da";
}else{
  $lbl_register = "Sign up";
  $lbl_search = "Search a product";
  $lbl_buy_online = "Buy Online";
  $lbl_terms_use = "Terms of use";
  $lbl_dictionary_skin = "Dictionary skin";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?> ">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/favicon.ico" />
    <script src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/jquery.mockjax.js"></script>
      <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/jquery.autocomplete.js"></script>
      <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/countries.js"></script>
      <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/autocomplete/demo.js"></script>
      <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/html5.js"></script>
    <link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/styles_autocomplete.css" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/bootstrap.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/css/swiper.min.css" rel="stylesheet" type="text/css" />

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
     <script type="text/javascript" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/js/dotdotdot/jquery.dotdotdot.js"></script>
    <script>
    $(document).ready(function() {
        
        //var aaa = $("#newletter_message_submit").val();
            function getUrlParameter(sParam)
            {
                var sPageURL = window.location.search.substring(1);
                var sURLVariables = sPageURL.split('&');
                for (var i = 0; i < sURLVariables.length; i++) 
                {
                    var sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] == sParam) 
                    {
                        return sParameterName[1];
                    }
                }
            }  
            var fail = getUrlParameter('common_error');
            var success = getUrlParameter('subscribe');
            if(fail=='true'){
              $("html, body").animate({ scrollTop: $("#footer-mobile").offset().top }, 800);
            }
            else if(success=='true'){
              $("html, body").animate({ scrollTop: $("#footer-mobile").offset().top }, 800);
            }
            //$("html, body").animate({ scrollTop: $("#footer-mobile").offset().top }, 800);
             // $("html, body").animate({ scrollTop: 0 }, "slow");
            // alert('aaa')

        // alert(aaa);
        // if($(aaa).click())
        // {
        //     $("html, body").animate({ scrollTop: $("#footer-mobile").offset().top }, 800);
        // }
        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            if (scroll>50)
            {
                $("#top-menu").addClass("navbar-fixed-top");
            }
            else{
                $("#top-menu").removeClass("navbar-fixed-top");
            }
        });
    });
</script>
    <!-- Latest compiled JavaScript -->

    <?php if($language_tag == 'en-GB'){ ?>
      <style>
        .logo-vichy a{
          background: url('<?php echo JURI::root();?>templates/vichy/images/Logo_Vichy3_en.png') left bottom no-repeat;          
        }
      </style>
    <?php } ?>

    <jdoc:include type="head" />
  </head>
  <!--[if lt IE 7 ]>   <body class="ie6">          <![endif]-->
     <!--[if IE 7 ]>      <body class="ie7">          <![endif]-->
     <!--[if IE 8 ]>      <body class="ie8">          <![endif]-->
     <!--[if IE 9 ]>      <body class="ie9">          <![endif]-->
     <!--[if (gt IE 9) ]> <body class="ie10">       <![endif]-->
     <!--[!(IE)]><!-->    
     <body class="notIE modern"> <!--<![endif]-->
    
    <div class="container">

      <header class="main">
        <div class="main-center mobile-style">
            <a href="<?php echo JURI::root();?>">
            <img class="img-responsive" style="margin:0 auto !important; width:32%" src="<?php echo JURI::root(); ?>templates/vichy/images/logo-mobile.png" />
            </a>
        </div>
        <nav id="top-menu" class="navbar navbar-default mobile-style" role="navigation">
          <div class="container">
            <div class="navbar-header" style="border-bottom:0 !important;background:url('<?php echo JURI::root(); ?>templates/vichy/images/bg-mobi.png')">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="search-box">
                <input id="root-ajax" type="hidden" value="<?php echo JURI::root();?>"></input>
                               
                <input class="inputbox" type="text" name="query" placeholder="<?php echo $lbl_search; ?>" id="autocomplete-ajax" />
                <input class="btn-search"  type="button" id="btn-search" style="z-index:9999 !important;">
                <input id="selction-ajax" type="hidden" value=""></input>
               </div>
                <a href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=store&Itemid=113&lang=vi') ?>">
                  <img width="35px" style="margin:9px auto" src="<?php echo JURI::root(); ?>templates/vichy/images/location-mobile-btn.png" />
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li style="border-top:0 !important"><a href="<?php echo JURI::root();?>" ><img class="img-responsive" style="width:35% !important;margin:0 auto !important;" src="<?php echo JURI::root(); ?>templates/vichy/images/logo-mobile-2.png" /></a>
                <img class="img-responsive" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="width:10% !important;position:absolute; right:7%; top:17%;" src="<?php echo JURI::root(); ?>templates/vichy/images/btn-close-mobile.png" />
                </li>
                <li><a href="<?php echo JROUTE::_('index.php?option=com_content&view=featured&Itemid=101&lang=vi') ?>">Về Vichy</a></li>
                <li><a href="<?php echo JROUTE::_('index.php?option=com_vichy_product&view=product_landing&Itemid=103&lang=vi') ?>">Sản phẩm</a></li>
                <li><a href="<?php echo JROUTE::_('index.php?option=com_vichy_advice&view=advice_diagnosis&Itemid=107&lang=vi') ?>">Lời Khuyên & Kiểm tra da</a></li>
                <li><a href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=spa&Itemid=108&lang=vi') ?>">Vichy spa</a></li>
                <li><a href="<?php echo JROUTE::_('index.php?option=com_content&view=category&layout=blog&id=29&Itemid=109&lang=vi') ?>">Blog</a></li>
               <!--  <li><a href="#">Về Vichy</a></li>
                <li><a href="#about">Sản phẩm</a></li>
                <li><a href="#contact">Lời Khuyên & Kiểm tra da</a></li>
                <li><a href="#">Vichy spa</a></li>
                <li><a href="#">Blog</a></li> -->
                <li class="last-li-menu">
<!--                <?php $userGrey = JFactory::getUser(); $params = JComponentHelper::getParams('com_users'); ?>
                    <?php if(!$userGrey->get('guest')) {
                        echo '<a href="' . JROUTE::_('index.php?option=com_users&amp;view=profile') . '"><img class="img-responsive" style="max-width:100% !important;" src="' . JURI::root() . 'templates/vichy/images/menu-item-1.png" /></a>';
                    }  else{ echo '<a href="' . JROUTE::_('index.php?option=com_users') . '"><img class="img-responsive" style="max-width:100% !important;" src="' . JURI::root() . 'templates/vichy/images/menu-item-1.png" /></a>'; };

                    ?> -->
                  <a id="login-mobi" href="#" >
                    <img class="img-responsive" style="max-width:100% !important;" src="<?php echo JURI::root(); ?>templates/vichy/images/menu-item-1.png" />
                  </a>

                  <a id="muahangonl" href="#">
                    <img class="img-responsive" style="max-width:100% !important;" src="<?php echo JURI::root(); ?>templates/vichy/images/menu-item-2.png" />
                  </a>
                  
                  <a href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=store&Itemid=113&lang=vi') ?>">
                    <img class="img-responsive" style="max-width:100% !important;" src="<?php echo JURI::root(); ?>templates/vichy/images/menu-item-3.png" />
                  </a>
                </li>
                
              </ul>
              <div class="buy-online common-buy-online-menu">
                      <a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/chonvn.png"></a>
                      <a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/yes24com.png"></a>
                      <a href="http://www.lazada.vn/vichy/" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/lazada.png"></a>
                      <a href="http://tiki.vn/thuong-hieu/vichy.html" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/tiki.png"></a>
                  </div>
                  <div class="login-popup-mobi">
                       <?php 
                       if(!$userGrey->get('guest')) {
                            echo '
                            <a href="' . JROUTE::_('index.php?option=com_users&view=profile') . '">'.$userGrey->name.'</a> / 
                            <form action="' . JRoute::_('index.php?option=com_users&task=user.logout') .'" method="post" id="login-form">
                                <input type="submit" name="Submit" class="button" id="button-logout" style="text-transform: none; font-weight: normal;" value="' . JText::_('JLOGOUTGREY') .'" />
                                <input type="hidden" name="option" value="com_users" />
                                <input type="hidden" name="task" value="user.logout" />
                                <input type="hidden" name="return" value="' . $return . '" />
                                ' . JHtml::_('form.token') . '
                            </form>
                            ';
                        } else{ echo '<a href="' . JROUTE::_('index.php?option=com_users&view=registration') . '">'.$lbl_register.'</a> / <a href="' . JROUTE::_('index.php?option=com_users') . '">'.JText::_('JLOGIN').'</a>'; };
                      // <a href="<?php echo JROUTE::_('index.php?option=com_users&view=registration') 
                      ?>
                  </div>
                  <script type="text/javascript" language="javascript">
                                $(document).ready(function(e) {
                                    $(".common-buy-online-menu").hide();
                                    $("#muahangonl").hover(function(){
                                        $(".common-buy-online-menu").toggle("slow");
                                    });
                                    // $("#muahangonl").click(function(){
                                    //     $(".common-buy-online-menu").toggle("slow");
                                    // });
                                    $(".login-popup-mobi").hide();
                                    $("#login-mobi").hover(function(){
                                        $(".login-popup-mobi").toggle("slow");
                                    });
                                    // $("#login-mobi").click(function(){
                                    //     $(".login-popup-mobi").toggle("slow");
                                    // });

                               });
                  </script>
            </div><!--/.nav-collapse -->
                  
          </div>
        </nav>
          <div class="menu-catology-product mobile-style">
              <jdoc:include type="modules" name="main_menu_mobile" />
          </div>
        <div class="main-center pc-style">
          <div class="logo-vichy">
            <a href="<?php echo JURI::root();?>" title="Home">
              <span>Vichy</span>
            </a>
          </div>

          <div class="nav-vichy">
            <div class="nav-vichy-top">
              <div class="nav-vichy-top-left">
                <div class="search">
                               <input id="root-ajax3" type="hidden" value="<?php echo JURI::root();?>"></input>
                               <input class="inputbox" type="text" name="query" placeholder="<?php echo $lbl_search; ?>" id="autocomplete-ajax3" />
                               <input class="btn-search"  type="button" id="btn-search3">
                               <input id="selction-ajax3" type="hidden" value=""></input>
                </div>
              </div>
              <div class="nav-vichy-top-right">
              <div class="vichy-link">
                  <div class="my_skin">
                    <a href="">MY SKIN</a>
                    <jdoc:include type="modules" name="login" />
                  </div>
                <div class="basket hover-buy-online">
                    <a href="#"><?php echo $lbl_buy_online; ?></a>
                  <div class="buy-online common-buy-online">
                      <a href="http://www.chon.vn/cua-hang/vichy.aspx?ref=home-merchant" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/chonvn.png"></a>
                      <a href="http://www.yes24.vn/khuyen-mai/471450/vichy-khuyen-mai.html" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/yes24com.png"></a>
                      <a href="http://www.lazada.vn/vichy/" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/lazada.png"></a>
                      <a href="http://tiki.vn/thuong-hieu/vichy.html" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/tmp/tiki.png"></a>
                  </div>

                  <script type="text/javascript" language="javascript">
                    $(document).ready(function(e) {
                          $('.hover-buy-online').hover(function(e) {
                        var left = $('.vichy-link').width() - $('.vichy-link').find('.my_skin').css('margin-right').replace("px","") - $('.vichy-link').find('.basket').css('margin-right').replace("px","");

                              var el=$(this).find('.common-buy-online').css({'visibility':'visible','opacity':'1','transition-delay':'0s','left':left});


                          },function(e){
                        var el=$(this).find('.common-buy-online').css({'visibility':'hidden','opacity':'0','transition':'visibility 0s linear 0.3s,opacity 0.3s linear'});
                      });


                     });
                  </script>
                </div>
                <jdoc:include type="modules" name="store_locator" />
                  <div class="clear"></div>
                </div><!--end vichy-link-->

                <!-- <jdoc:include type="modules" name="login" /> -->

               <div class="social-link">
                  <jdoc:include type="modules" name="multi_language" />
                  <input type="hidden" name="language_tag" id="language_tag" value="<?php echo $language_tag; ?>" />
                  <jdoc:include type="modules" name="new_letter" />
                  <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/face.png"></a>
                  <a href="https://www.youtube.com/user/VichyVietnam" target="_blank"><img src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/youtube.png"></a>
                </div>
                <div class="clear"></div>
              </div>

            </div>
            <div class="clear"></div>
            <div class="nav-vichy-bottom">
                <jdoc:include type="modules" name="main_menu" />
            </div>
          </div>
        </div>

      </header> 
      <jdoc:include type="modules" name="breadcrumb" />
      <?php 
      if(!$this->countModules('breadcrumb')){
        echo '<div class="border_home pc-style"></div>';
      }
      ?>
      
      <div class="main-container" <?php echo ($_GET['option'] == 'com_vichy_home') ? 'style="padding-top:102px;"' : '' ?>>
        <a href="#" id="go-bottom" class="button-down">
             <img src="<?php echo JURI::root(); ?>templates/vichy/images/to-bottom-icon.png" />
        </a>
        <?php if($_GET['view']== 'featured') {?>
          <jdoc:include type="modules" name="vichy_about" />
        <?php } ?>
        <?php if($_GET['view'] == 'product_landing'){ ?>
                <jdoc:include type="modules" name="new_products" />
        <jdoc:include type="modules" name="favorite_products" />
        <?php } ?>
        <jdoc:include type="modules" name="banner_blog" />
        <jdoc:include type="component" />
        <?php if($_GET['view'] == 'advice_diagnosis'){ ?>
        <jdoc:include type="modules" name="advice_landing" />
        <?php } ?>
        <?php if($_GET['view'] == 'product'){ ?>
        <jdoc:include type="modules" name="vichy_comment_product" />
        <?php } ?>
        <?php if($_GET['view'] == 'product' || $_GET['view'] == 'product_range'){ ?>
        <jdoc:include type="modules" name="product_step_1" />
        <?php } ?>
        <?php if($_GET['view'] == 'product'){ ?>
        <jdoc:include type="modules" name="diagnosis_advice_product" />
        <?php } ?>
        <?php if($_GET['view'] == 'product_landing'){ ?>
        <jdoc:include type="modules" name="product_without_steps" />
        <?php } ?>
        <?php if($_GET['view'] == 'advice'){ ?>
        <jdoc:include type="modules" name="product_range_diagnosis_advice" />
        <?php } ?>
        <a href="#" id="go-top" class="button-up">          
             <img src="<?php echo JURI::root(); ?>templates/vichy/images/to-top-icon.png" />
        </a>
      </div>

      <footer class="main pc-style">        
        <div class="top clearfix">
          <div class="main-center">
            <jdoc:include type="modules" name="vichy_footer" />
          </div>
        </div>
        <div class="bottom pc-style">
          <div class="main-center clearfix">
            <div class="copyright">© 2014 VICHY&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;<?php echo $lbl_terms_use; ?>&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;<?php echo $lbl_dictionary_skin; ?></div>
          </div>
        </div>
      </footer>

      <footer id="footer-mobile" class="main mobile-style">
        <!--store location -->

        <div  class="container location mobile-style" style="margin-right:0">
          <a href="<?php echo JRoute::_('index.php?option=com_vichy_store&Itemid=113&lang=vi');?>">
            <img id="locationmobi" class="img-responsive" src="<?php echo JURI::root(); ?>templates/vichy/images/map-mobile.png">
          </a>
        </div>
        <div class="container social social2">
            <?php
            /**
             * @package		Joomla.Site
             * @subpackage	mod_search
             * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
             * @license		GNU General Public License version 2 or later; see LICENSE.txt
             */

            // no direct access
            defined('_JEXEC') or die;
            $document = & JFactory::getDocument();
            $document->addStyleSheet('modules/mod_new_letter/css/style.css');
            $cat_error = $_GET['cat_error'];
            $common_error = $_GET['common_error'];
            $success = $_GET['subscribe'];

            $curLanguage = JFactory::getLanguage();
            $lang = $curLanguage->getTag();

            $db = & JFactory::getDbo();

            if($lang == 'vi-VN'){
                $query = "Select sg.catid, sg.subcribe_group, c.title from vc_vichy_subcribe_group as sg join vc_categories as c on sg.catid = c.id where sg.status = 1";
            }else{
                $query = "Select sg.catid, sg.subcribe_group, c.title from vc_vichy_subcribe_group as sg join
			(select title, reference_id as id from vc_categories as cate inner join vc_jf_translationmap as tm on cate.id = tm.translation_id and reference_table='categories') as c on sg.catid = c.id where sg.status=1";
            }

            $db->setQuery($query);
            $group_name = $db->loadObjectList();

            if($lang == 'vi-VN'){
                $title_newsletter = "ĐĂNG KÝ NHẬN BẢN TIN";
                $mess_success = "Chúc mừng bạn đã đăng kí thành công! Vichy sẽ gửi đến bạn email về những vấn đề bạn quan tâm.";
                $error_register = "*E-mail nhập vào không đúng đinh dạng";
                $mess_remind = "*Bạn chưa chọn nhu cầu nào";
                $alert_error = "*Có lỗi xảy ra";
                $mess_input_email = "Nhập địa chỉ e-mail";
                $mess_check_yourneed = "Để nhận được email theo đúng nhu cầu da của bạn, vui lòng đánh dấu những nhu cầu mà bạn quan tâm:";

                $lbl_submit = "Nhận E-mail";
            }else{
                $title_newsletter = "Sign up to receive e-mail Newsletter";
                $mess_success = "Congratulations, you have successfully registered! Vichy email will be sent to you on issues you care about.";
                $error_register = "*E-mail entered incorrect formatting";
                $mess_remind = "*You have not selected any demand";
                $alert_error = "*An error has occurred";
                $mess_input_email = "Enter your e-mail address";
                $mess_check_yourneed = "To receive e-mail in accordance with need of your skin, please check the need that interest you:";

                $lbl_submit = "Receive E-mail";
            }
            ?>
            <p class="title-footer-mobile"><?php echo $title_newsletter; ?></p>
            <?php if($success){ ?>
                <div class="label_email"><?php echo $mess_success; ?></div>
            <?php }else{ ?>
            <form method="POST" action="<?php echo JROUTE::_('index.php?option=com_vichy_new_letter&task=registerEmail'); $_GET['common_error'] ?>">
                <p style="color:red;display:none;" class="validate-error" ><?php echo $error_register; ?></p>
                <?php
                if($cat_error){
                    echo '<p style="color:red;">'.$mess_remind.'</p>';
                }
                if($common_error){
                    echo '<p style="color:red;">'.$alert_error.'</p>';
                }
                ?>
                <input size="20" name="email" id="email-newletter" class="input-nhan-ban-tin" type="text" placeholder="Email" >
                <div style="display: none;">
                    <?php
                    foreach($group_name as $v){
                        echo '<input checked="checked" type="checkbox" name="catid[]" value="'.$v->subcribe_group.'" />'.$v->title.'<br />';
                    }
                    ?>
                </div>

                <input type="hidden" name="grouping_id" value="<?php echo $groups[0]['name']; ?>" />
                <input type="hidden" name="return_fail" value="<?php echo $_SERVER['QUERY_STRING']; ?>" />
                <input type="submit" value="" name="commit" id="newletter_message_submit" class="btn-nbt"/>
<!--                <img class="loading_newsletter" src="--><?php //echo JURI::root() ?><!--modules/mod_new_letter/images/9.gif" />-->
<!--                <input class="btn-nbt" type="button">-->
            </form>
            <?php } ?>
          <div class="ketnoi" style="max-width:300px; margin:0 auto">

            <p class="text-footer-mobile">KẾT NỐI VỚI VICHY </p>
            <a href="https://www.facebook.com/vichy.com.vn" target="_blank"><img style="width:27px" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/face.png"></a>
            <a href="https://www.youtube.com/user/VichyVietnam" target="_blank"><img style="width:23px" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/youtube.png"></a>
            <a href="#" target="_blank"><img style="width:80px;padding-top:4px" src="<?php echo JURI::root();?>/templates/<?php echo $this->template; ?>/images/myskin-mobile.png"></a>         
          
          </div>
        </div>
        <div class="container last-footer-content">

            <?php if(!$userGrey->get('guest')) {
                echo '
                <form action="' . JRoute::_('index.php?option=com_users&task=user.logout') .'" method="post" id="login-form">
                    <input type="submit" name="Submit" class="button" id="button-logout" style="text-transform: none; font-weight: normal;" value="' . JText::_('JLOGOUTGREY') .'" />
                    <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.logout" />
                    <input type="hidden" name="return" value="' . $return . '" />
                    ' . JHtml::_('form.token') . '
                </form>
                ';
            }  else{ echo '<a href="' . JROUTE::_('index.php?option=com_users') . '">'.JText::_('JLOGIN').'</a>'; };

            ?> / <a href="<?php echo JROUTE::_('index.php?option=com_vichy_store&view=store&Itemid=113&lang=vi') ?>">Hệ thống cửa hàng</a> / Hotline: 1800242424 <br>
            Các điều khoản và điều kiện / Chính sách bảo mật
        </div>
      </footer>
    </div>
    <!-- <jdoc:include type="modules" name="facebook_slider" /> -->
    <script type="text/javascript" src="<?php echo JURI::root(); ?>templates/vichy/js/common.js"></script>

    <noscript>
      <iframe src="//www.googletagmanager.com/ns.html?id=GTM-NPW6MD"
          height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-NPW6MD');
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-52701542-1', 'auto');
      ga('send', 'pageview');
      ga('create', 'UA-8955873-32', 'auto', {'name': 'newTracker'});
      ga('newTracker.send', 'pageview');
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-8955873-32', 'auto');
      ga('send', 'pageview');

    </script>

    </body>
</html>

